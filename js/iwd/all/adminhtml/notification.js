;
IWD ={};
IWD.notification={
		markReadUrl: null,
		countNoRead: 0,
		removeUrl: null
};
jQuery(document).ready(function(jQuery){
	
	jQuery('#mark-all-read').click(function(){
		jQuery.post(IWD.notification.markReadUrl, {"form_key": FORM_KEY}, function(){},'json');
	});
	
	jQuery('#mark-all-remove').click(function(){
		jQuery.post(IWD.notification.removeUrl, {"form_key": FORM_KEY}, function(){},'json');
	});
});