<?php
    header('Content-type: text/css; charset: UTF-8');
    header('Cache-Control: must-revalidate');
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 3600) . ' GMT');
    $url = $_REQUEST['url'];
?>

.mobilemenu>li.first {
	-webkit-border-radius: 4px 4px 0 0;
	-moz-border-radius: 4px 4px 0 0;
	border-radius: 4px 4px 0 0;
	behavior: url(<?php echo $url; ?>css/css3.htc);
	position: relative;
}
.mobilemenu>li.last {
	-webkit-border-radius:0 0 4px 4px;
	-moz-border-radius: 0 0 4px 4px;
	border-radius: 0 0 4px 4px;
	behavior: url(<?php echo $url; ?>css/css3.htc);
	position: relative;
}
#Example_F {
	-moz-box-shadow: 0 0 5px 5px #888;
	-webkit-box-shadow: 0 0 5px 5px#888;
	box-shadow: 0 0 5px 5px #888;
}
.product_detail_item:hover {
    -webkit-box-shadow: 0px 2px 15px rgba(50, 50, 50, 0.75);
    -moz-box-shadow:    0px 2px 15px rgba(50, 50, 50, 0.75);
    box-shadow:         0px 2px 15px rgba(50, 50, 50, 0.75);
    behavior: url(<?php echo $url; ?>css/css3.htc);
}
#tabsproduct-content .products-grid .item .product_tab_item:hover {
    -webkit-box-shadow: 0px 2px 15px rgba(50, 50, 50, 0.75);
    -moz-box-shadow:    0px 2px 15px rgba(50, 50, 50, 0.75);
    box-shadow:         0px 2px 15px rgba(50, 50, 50, 0.75);
    behavior: url(<?php echo $url; ?>css/css3.htc);
}
.products-list li.item:hover {
    -webkit-box-shadow: 0px 2px 15px rgba(50, 50, 50, 0.75);
    -moz-box-shadow:    0px 2px 15px rgba(50, 50, 50, 0.75);
    box-shadow:         0px 2px 15px rgba(50, 50, 50, 0.75);
    behavior: url(<?php echo $url; ?>css/css3.htc);
}