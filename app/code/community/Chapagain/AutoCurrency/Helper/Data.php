<?php
/**
 * Loads GeoIP binary data files 
 *
 * @category   Chapagain
 * @package    Chapagain_AutoCurrency
 * @version    0.2.0
 * @author     Mukesh Chapagain <mukesh.chapagain@gmail.com> 
 * @link 	   http://blog.chapagain.com.np/category/magento/
 */
 
class Chapagain_AutoCurrency_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * Check if the module is enabled
	 * 
	 * @return boolean 0 or 1
	 */ 
	public function isEnabled()
    { 		
        return Mage::getStoreConfig('catalog/autocurrency/enable_disable');
    }
    
    /**
	 * Check IP database source
	 * 
	 * @return string
	 */ 
	public function getDatabaseSource()
    { 		
        $db = Mage::getStoreConfig('catalog/autocurrency/database_source');
        if ($db == null) { 
			$db = 'ip2country';
		}
		return $db;
    }
    
	/**
     * Get IP Address
     *
     * @return string
     */
	public function getIpAddress() 
	{			
		// return "46.16.33.139";
		// return "210.2.142.147";
		// echo "here: " . $_SERVER['REMOTE_ADDR']; exit;
		return $_SERVER['REMOTE_ADDR'];
	}
	
	/**
     * Include GeoIP Inc file
     */
	public function loadGeoIpInc() 
	{	
		// Load geoip.inc
		include_once(Mage::getBaseDir().'/var/geoip/geoip.inc');	
	}
	
	/**
     * Load GeoIPv4 binary data file
     */
	public function loadGeoIpv4() 
	{			
		$geoIPv4 = geoip_open(Mage::getBaseDir().'/var/geoip/GeoIP.dat',GEOIP_STANDARD);				
		return $geoIPv4;
	}
	
	/**
     * Load GeoIPv6 binary data file
     */
	public function loadGeoIpv6() 
	{			
		$geoIPv6 = geoip_open(Mage::getBaseDir().'/var/geoip/GeoIPv6.dat',GEOIP_STANDARD);		
		return $geoIPv6;
	}
	
	/**
     * Check whether the given IP Address is valid
     * 
     * @param string IP Address
     * @return boolean True/False
     */
	public function checkValidIp($ip) 
	{			
		if(!filter_var($ip, FILTER_VALIDATE_IP)) {
			return false;
		}		
		return true;
	}
	
	/**
     * Check whether the given IP Address is IPv6
     * 
     * @param string IP Address
     * @return boolean True/False
     */
	public function checkIpv6($ip) 
	{			
		if(!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
			return false;
		}		
		return true;
	}		
	
	/**
     * Include IP2Country.php file
     * and load ip2country.dat file 
     * 
     */
	public function loadIp2Country()
	{
		include_once(Mage::getBaseDir().'/var/geoip/ip2country/Ip2Country.php');
		$ipc = new Ip2Country(Mage::getBaseDir().'/var/geoip/ip2country/ip2country.dat');
		$ipc->preload();
		return $ipc;
	}
	
	/**
	 * Get Currency code by Country code
	 * 
	 * @return string
	 */ 
	public function getCurrencyByCountry($countryCode)
	{
		$map = array( '' => '',
		"EU" => "USD", "AD" => "USD", "AE" => "USD", "AF" => "USD", "AG" => "USD", "AI" => "USD", 
		"AL" => "USD", "AM" => "USD", "CW" => "USD", "AO" => "USD", "AQ" => "USD", "AR" => "USD", "AS" => "USD", 
		"AT" => "USD", "AU" => "USD", "AW" => "USD", "AZ" => "USD", "BA" => "USD", "BB" => "USD", 
		"BD" => "USD", "BE" => "USD", "BF" => "USD", "BG" => "USD", "BH" => "USD", "BI" => "USD", 
		"BJ" => "USD", "BM" => "USD", "BN" => "USD", "BO" => "USD", "BR" => "USD", "BS" => "USD", 
		"BT" => "USD", "BV" => "USD", "BW" => "USD", "BY" => "USD", "BZ" => "USD", "CA" => "USD", 
		"CC" => "USD", "CD" => "USD", "CF" => "USD", "CG" => "USD", "CH" => "USD", "CI" => "USD", 
		"CK" => "USD", "CL" => "USD", "CM" => "USD", "CN" => "USD", "CO" => "USD", "CR" => "USD", 
		"CU" => "USD", "CV" => "USD", "CX" => "USD", "CY" => "USD", "CZ" => "USD", "DE" => "USD", 
		"DJ" => "USD", "DK" => "USD", "DM" => "USD", "DO" => "USD", "DZ" => "USD", "EC" => "USD", 
		"EE" => "USD", "EG" => "USD", "EH" => "USD", "ER" => "USD", "ES" => "USD", "ET" => "USD", 
		"FI" => "USD", "FJ" => "USD", "FK" => "USD", "FM" => "USD", "FO" => "USD", "FR" => "USD", "SX" => "USD",
		"GA" => "USD", "GB" => "USD", "GD" => "USD", "GE" => "USD", "GF" => "USD", "GH" => "USD", 
		"GI" => "USD", "GL" => "USD", "GM" => "USD", "GN" => "USD", "GP" => "USD", "GQ" => "USD", 
		"GR" => "USD", "GS" => "USD", "GT" => "USD", "GU" => "USD", "GW" => "USD", "GY" => "USD", 
		"HK" => "USD", "HM" => "USD", "HN" => "USD", "HR" => "USD", "HT" => "USD", "HU" => "USD", 
		"ID" => "USD", "IE" => "USD", "IL" => "USD", "IN" => "USD", "IO" => "USD", "IQ" => "USD", 
		"IR" => "USD", "IS" => "USD", "IT" => "USD", "JM" => "USD", "JO" => "USD", "JP" => "USD", 
		"KE" => "USD", "KG" => "USD", "KH" => "USD", "KI" => "USD", "KM" => "USD", "KN" => "USD", 
		"KP" => "USD", "KR" => "USD", "KW" => "USD", "KY" => "USD", "KZ" => "USD", "LA" => "USD", 
		"LB" => "USD", "LC" => "USD", "LI" => "USD", "LK" => "USD", "LR" => "USD", "LS" => "USD", 
		"LT" => "USD", "LU" => "USD", "LV" => "USD", "LY" => "USD", "MA" => "USD", "MC" => "USD", 
		"MD" => "USD", "MG" => "USD", "MH" => "USD", "MK" => "USD", "ML" => "USD", "MM" => "USD", 
		"MN" => "USD", "MO" => "USD", "MP" => "USD", "MQ" => "USD", "MR" => "USD", "MS" => "USD", 
		"MT" => "USD", "MU" => "USD", "MV" => "USD", "MW" => "USD", "MX" => "USD", "MY" => "USD", 
		"MZ" => "USD", "NA" => "USD", "NC" => "USD", "NE" => "USD", "NF" => "USD", "NG" => "USD", 
		"NI" => "USD", "NL" => "USD", "NO" => "USD", "NP" => "USD", "NR" => "USD", "NU" => "USD", 
		"NZ" => "USD", "OM" => "USD", "PA" => "USD", "PE" => "USD", "PF" => "USD", "PG" => "USD", 
		"PH" => "USD", "PK" => "PKR", "PL" => "USD", "PM" => "USD", "PN" => "USD", "PR" => "USD", "PS" => "USD", "PT" => "USD", 
		"PW" => "USD", "PY" => "USD", "QA" => "USD", "RE" => "USD", "RO" => "USD", "RU" => "USD", 
		"RW" => "USD", "SA" => "USD", "SB" => "USD", "SC" => "USD", "SD" => "USD", "SE" => "USD", 
		"SG" => "USD", "SH" => "USD", "SI" => "USD", "SJ" => "USD", "SK" => "USD", "SL" => "USD", 
		"SM" => "USD", "SN" => "USD", "SO" => "USD", "SR" => "USD", "ST" => "USD", "SV" => "USD", 
		"SY" => "USD", "SZ" => "USD", "TC" => "USD", "TD" => "USD", "TF" => "USD", "TG" => "USD", 
		"TH" => "USD", "TJ" => "USD", "TK" => "USD", "TM" => "USD", "TN" => "USD", "TO" => "USD", "TL" => "USD",
		"TR" => "USD", "TT" => "USD", "TV" => "USD", "TW" => "USD", "TZ" => "USD", "UA" => "USD", 
		"UG" => "USD", "UM" => "USD", "US" => "USD", "UY" => "USD", "UZ" => "USD", "VA" => "USD", 
		"VC" => "USD", "VE" => "USD", "VG" => "USD", "VI" => "USD", "VN" => "USD", "VU" => "USD",
		"WF" => "USD", "WS" => "USD", "YE" => "USD", "YT" => "USD", "RS" => "USD", 
		"ZA" => "USD", "ZM" => "USD", "ME" => "USD", "ZW" => "USD",
		"AX" => "USD", "GG" => "USD", "IM" => "USD", 
		"JE" => "USD", "BL" => "USD", "MF" => "USD", "BQ" => "USD", "SS" => "USD"
		);
		
		return $map[$countryCode];
	}
}
