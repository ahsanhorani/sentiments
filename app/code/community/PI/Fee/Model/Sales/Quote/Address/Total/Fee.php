<?php
class PI_Fee_Model_Sales_Quote_Address_Total_Fee extends Mage_Sales_Model_Quote_Address_Total_Abstract{
	protected $_code = 'fee';
	// protected	$_date_selected	=	"hello";
	// protected	$time	=	0;
	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		parent::collect($address);

		$this->_setAmount(0);
		$this->_setBaseAmount(0);
		
		
		$items = $this->_getAddressItems($address);
		if (!count($items)) {
			return $this; //this makes only address type shipping to come through
		}


			$quote = $address->getQuote();
			
		/* Getting Data from Customer Session 	-	By Ahsan*/
			$date				=	Mage::getSingleton( 'customer/session' )->getData('orderdate');
			$time				=	Mage::getSingleton( 'customer/session' )->getData('ordertime');
			$date_selected	=	Mage::getSingleton( 'customer/session' )->getData('date_select');
			$time_selected	=	Mage::getSingleton( 'customer/session' )->getData('time_select');
		/* Getting Data from Customer Session */
		
			$timestamp = strtotime($date);	//	Format : mm/dd/yyyy
			$day = date('l', $timestamp);		//Get name of the Day by submitted date
			$currency	=	Mage::app()->getStore()->getCurrentCurrencyCode(); 
			$qty = Mage::helper('checkout/cart')->getSummaryCount();
			// echo $qty;
		if(PI_Fee_Model_Fee::canApply($address))
		{
				
				
					$exist_amount = $quote->getFeeAmount();
					$fee = PI_Fee_Model_Fee::getFee();
					$balance=0;
					
					if($date_selected==1 && $day=='Sunday' && $time_selected==1)
					{
						$balance	= ($fee ) * 2;
						$applyFee=true;
						// if($currency=='USD'){ $balance	=	$balance/100;}
						
					}
					
					else if($date_selected==1 && $day=='Sunday')
						{
							$balance = $fee ;
							$applyFee=true;
						}
					
					//If Time stipulated Delivery, add more fee Rs. 300	- By Muhammad Ahsan Horani
					else if($date_selected==1 && $time_selected==1)
					{
						$balance	=	$fee - $exist_amount;
						$applyFee=true;
					}
					
					// 	$balance = $fee;
					//$this->_setAmount($balance);
					//$this->_setBaseAmount($balance);
					
					if($currency=='USD')
					{ 
						$address->setFeeAmount($balance/100);
					}
					else
					{
					
						$address->setFeeAmount($balance);
					}
					
					$address->setBaseFeeAmount($balance);
					
					if($currency=='USD')
					{
						$quote->setFeeAmount($balance/100);
					}
					else
					{
						$quote->setFeeAmount($balance);
					}
					
					if($currency=="USD" && $applyFee)
					{
					
						$address->setGrandTotal($address->getGrandTotal() + $address->getFeeAmount());
						$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseFeeAmount() + 300 + ($qty*300));
					}
					else if($currency=="USD")
					{
						$address->setGrandTotal($address->getGrandTotal() + $address->getFeeAmount());
						$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseFeeAmount() + 300 + ($qty*300));
					}
					else{
					
						$address->setGrandTotal($address->getGrandTotal() + $address->getFeeAmount());
						$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseFeeAmount());
					}
				}
		}
	

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
			
		
		
		$setFee = Mage::getStoreConfig('checkout/fee/active');
		
		if($setFee==1)
		{
				
				$amt = $address->getFeeAmount();
				$address->addTotal(array(
						'code'=>$this->getCode(),
						'title'=>Mage::helper('fee')->__('Special Service Charges'),
						'value'=> $amt
				));
				return $this;
			
		}
	}
}
