<?php
class PI_Fee_Model_Fee extends Varien_Object{

	public static function getFee(){
		$feeAmt = Mage::getStoreConfig('checkout/fee/fee_amt');
		return $feeAmt;
	}
	public static function canApply($address){
			$setFee = Mage::getStoreConfig('checkout/fee/active');
		
		/* Getting Data from Customer Session 	-	by Ahsan	*/
			$date				=	Mage::getSingleton( 'customer/session' )->getData('orderdate');
			$time				=	Mage::getSingleton( 'customer/session' )->getData('ordertime');
			$date_selected	=	Mage::getSingleton( 'customer/session' )->getData('date_select');
			$time_selected	=	Mage::getSingleton( 'customer/session' )->getData('time_select');
		/* Getting Data from Customer Session */
		
			$currency	=	Mage::app()->getStore()->getCurrentCurrencyCode(); 
			
		if($setFee==1)
		{	
			// Charge only if Order Delivery Date has been selected	- By Ahsan
			if($date_selected==1 || $currency=="USD")
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
