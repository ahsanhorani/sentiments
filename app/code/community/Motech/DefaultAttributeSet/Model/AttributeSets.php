<?php

class Motech_DefaultAttributeSet_Model_AttributeSets
{
    /**
     * Retrieve all  options
    */
    public function toOptionArray()
    {
		$entityTypeId = Mage::getModel('eav/entity')
						->setType('catalog_product')
						->getTypeId();

		$values = Mage::getResourceModel('eav/entity_attribute_set_collection')
			->setEntityTypeFilter($entityTypeId)
			->load()
			->toOptionArray();
			
		return $values;

    }
}


?>