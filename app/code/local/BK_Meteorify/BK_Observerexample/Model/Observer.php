<?php
class Meteorify_Observerexample_Model_Observer extends Varien_Event_observer {

	/* Observer to capture  events on checkout  before save order*/
    public function example(Varien_Event_Observer $observer) {
		
		// echo "<pre>";
		// print_r(Mage::getModel('adminhtml/session_quote')->getOrder()->getOrderId()); exit;
		// print_r($_REQUEST); exit;

		$event = $observer->getEvent();
	    $order = $event->getOrder();
	    $items = $order->getQuote()->getAllItems();
		
		//GET ATTRIBUTE ID BY CODE
		$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product','allowed_city');
		
		//LOAD ATTRIBUTE
		$_product = Mage::getModel('catalog/product');
		$attributeLabel = $_product->getResource()->getAttribute("allowed_city");
		// Mage::throwException("Please Stop here");
		// $order	=	$observer->getOrder()->getOrderId();
		// echo $order;
		// echo "testing"; exit;
        //$observer contains data passed from when the event was triggered.
        //You can use this data to manipulate the order data before it's saved.
        //Uncomment the line below to log what is contained here:
		// $get	=	$observer->getData();
		// Mage::log($observer->getEvent()->getItem(),null,'mylog22.log');
		// $product=Mage::getModel("checkout/session")->getQuoteItems();
		
		/*  $event = $observer->getEvent();
		$test = (get_class_methods($event));
		Mage::log($test,null,'methods.log');
		echo "<pre>"; print_r($test); exit; */
		
		
		// echo "\nAttribute ID: ". $attributeId;
		
		// $collection =Mage::getResourceModel('eav/entity_attribute_option_collection')
                // ->setPositionOrder('asc')
                // ->setAttributeFilter($attributeId)
                // ->setStoreFilter(0)
                // ->load();
                // echo "<pre>";
                // print_r($collection->toOptionArray());
                // echo "</pre>"; exit;

		/*  $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
									->setCodeFilter('allowed_city');
									
		$attributeId = $attributeInfo->getData('attribute_id'); */
		
		// $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'allowed_city');

		// print_r($attributeModel->getData('attribute_id')); exit;
		// $attributeId = $attributeInfo->getAttributeId();
		
		// $optionId = 242;
		
		
		
		// if ($attributeLabel->usesSource()) {
			// echo "Label: ". $color_label = $attr->getSource()->getOptionText("242");
		// }
		// $attributeOptionSingle = Mage::getModel('eav/entity_attribute_option') 
											// ->setPositionOrder('asc')
											// ->setAttributeFilter($attributeId)
											// ->setIdFilter($optionId)
											// ->setStoreFilter()
											// ->load();
		// print_r($attributeOptionSingle->getOptionText()); exit;
		
		// print_r("\n".$attributeModel->getData('attribute_id')); exit; 
		
		//GET CITY NAME FROM CURRENT CHECKOUT SESSION
		if(Mage::getModel('checkout/session')->getQuote()->getShippingAddress()->getCity())
		{
			$shipcity	=	Mage::getModel('checkout/session')->getQuote()->getShippingAddress()->getCity();
		}
		else
		{	//GET CITY NAME FROM CURRENT ADMIN
			$shipcity	=	 Mage::getModel('adminhtml/sales_order_create')->getShippingAddress()->getCity(); //exit;
		}
		//GET CITY NAME FROM SHIPPING ADDRESS
		
		// $shipcity	=	$_REQUEST["order"]["shipping_address"]["city"]; // echo $shipcity; exit;
		// $shipcity = Mage::App()->getRequest()->getPost();// echo $shipcity; exit;
		
		// $shipcity	=	$_POST["order"]["shipping_address"]["city"];			// exit;
		
		
		

		$shipcity	=	strtolower($shipcity);
		// echo("\nShipping City is " . $shipcity); 
		
		//VALIDATE CITY FOR EACH ITEM FROM THE CART
		foreach($items as $item){
		
            // $product = $item;
            // $id[] = $product->getProductId();
            $name[] = $item->getName();
			
			$product = Mage::getModel('catalog/product')->load($item->getProductId());
			
			//CHECK IF PRODUCT HAS THIS ATTRIBUTE
			if($product->getData('allowed_city'))
			{
				$cities	=	$product->getData('allowed_city');
				
				//BREAK ARRAY AND STORE  OPTION ID INTO ANOTHER ARRAY, OPTIONS
				$cityId	=	(explode(",",$cities));
				
				//GET OPTION ID AND LABEL (CITY NAME), CITY LABEL BY ID
				foreach($cityId as $city) {
					
					//OPTION ID
					// echo "\nOption Id: ". $city;
					 // $attr = Mage::getModel('catalog/resource_eav_attribute')->load($city);
					 
					 //DISPLAY OPTION LABEL OF CITY, GET CITY NAME BY OPTION ID
					if ($attributeLabel->usesSource()) {
						
						//STORING ALLOWED CITY NAMES FROM PRODUCT ATTRIBUTE INTO ARRAY
						 $allowed_cities[] = strtolower( $attributeLabel->getSource()->getOptionText("$city"));
					
					}
						
					 
				}// INNER END FOREACH [$city]
				
				//DEBUG CITIES
				// print_r($allowed_cities); exit;
				
						//CHECK THE PRODUCT AVAILABILITY IN THE CITY
						 if(!in_array($shipcity,$allowed_cities)) {
						 
							//IF PRODUCT IS NOT ALLOWED IN THE SHIPPING CITY, THROW EXCEPTION
							 Mage::throwException("We are SORRY !\t". $item->getName() ." is not available in $shipcity");
						}
						else
						{	
							// echo "Product can be shipped !\n";
							// print_r( "\nAllowed in: " . $allowed_cities);
						}
			
			}//END OUTER IF
			
		}//END OUTER FOREACH [$items as $item]
		
		
		// exit;
		// Mage::log($name,null,'myQuoteItems.log');
		// echo "\nProducts in Cart :\n";
		 // print_r($name); //exit;
		 		// echo "\n";

		 // print_r($cities); //exit;
		// echo "\n";
		// echo "\n";
		 // print_r($id);
		 
		 
		// echo "<pre>"; print_r($product); exit;
		// echo "yes";
		// Mage::log("City name: ".$observer->getCustomerAddress()->getCity(),null,'customer.log');
		// $addrs = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress();
		// $billing = $observer->getPost();
		// Mage::log($addrs,null,'customerBill.log');
		// Mage::throwException("test");
		// print_r($observer->getCustomerAddress());
		// print_r($observer->getPost());
		// $city	= (string)$observer->getShipping() ;
		// $city	= Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress() ; print_r($city);
		
		// $quote->getShippingAddress()->getCity();
		
		// $shipcity	=	Mage::getModel('checkout/session')->getQuote()->getShippingAddress()->getCity();
		
		// print_r("City is ".$shipcity); exit;
		// print_r( "\n". $quote) ;exit;
		// if($city!='karachi')
		// {
				// echo "Selected city is not karachi";
		// }
		// else
		// {
				// print_r($city );
				// echo(Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress());
		// }
		 // exit;
		// $observer->getEvent();
		// $total=$observer->getEvent()->getBaseTotal();
		
        // Mage::log($observer->getCustomerAddress(),null,'address.log');
	   // Mage::log($observer->getEvent()->getPayment(),null,'payment.log');

	   // Mage::log($observer,null,'myobserver.log');

				// print_r($observer->getCustomerAddress()->getData());
				// if($total>1000)
					// exit;
				
				// echo "This is observing the event sales_order_place_before"; exit;

			// return $this;
			
			
    }
	
	/* After product added to cart */
	public function afterAdd(Varien_Event_Observer $observer) {
		
		if (Mage::getSingleton('customer/session')->isLoggedIn())
		{
			//CAPTURE EVENT
			$event = $observer->getEvent();
			//GET ITEM FROM CART
			$item = $event->getProduct();
			//GET SKU
			$itemSku	=	$item->getData('sku');
			//GET ITEM ID
			$itemId	=	$item->getData('entity_id');
			//GET ITEM NAME
			$itemName	=	$item->getData('name');
			
			
			
			// echo "<pre>"; print_r($item->getData('entity_id')); exit;
			
			//GET ATTRIBUTE ID BY CODE
			$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product','allowed_city');
			
			//LOAD ATTRIBUTE
			$_product = Mage::getModel('catalog/product');
			$attributeLabel = $_product->getResource()->getAttribute("allowed_city");
			
			
			if($item->getData('allowed_city'))
			{
				$cities	=	$item->getData('allowed_city');
					
				//BREAK ARRAY AND STORE  OPTION ID INTO ANOTHER ARRAY, OPTIONS
				$cityId	=	(explode(",",$cities));
				
				//GET OPTION ID AND LABEL (CITY NAME), CITY LABEL BY ID
				foreach($cityId as $city) {
					 
					 //DISPLAY OPTION LABEL OF CITY, GET CITY NAME BY OPTION ID
					if ($attributeLabel->usesSource()) {
						
						//STORING ALLOWED CITY NAMES FROM PRODUCT ATTRIBUTE INTO ARRAY
						 $allowed_cities[] = strtolower( $attributeLabel->getSource()->getOptionText("$city"));
					}
							
						 
				}// INNER END FOREACH [$cityId as $city]
				
				//GET CUSTOMER INFO
				$customer = Mage::getSingleton('customer/session')->getCustomer();
				//GET DEFAULT  SHIPPING ADDRESS ID
				$address_id = $customer->getDefaultShipping();

				//IF GET ADDRESS ID
				if ((int)$address_id){
						
						//LOAD FROM  CUSTOMER/ADDRESS
					   $address = Mage::getModel('customer/address')->load($address_id);
					   //GET CITY NAME FROM ADDRESS
						$city		  =	strtolower($address->getCity());
					  
					   
					}
					
				
				if(!in_array($city,$allowed_cities))
				{ 
					$warning	=	Mage::getModel("checkout/session")->addNotice("Warning ! This product is not available in your default address city. Please change shipping address according to the product availability. See product's additional information");
					echo $warning;
					// Mage::throwException("This product is not available in your default address city. Please change shipping address according to the product availability");
				 }
			}
			
		}
	}

}

?>