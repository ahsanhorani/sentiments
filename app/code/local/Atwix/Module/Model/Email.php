<?php

class Atwix_Module_Model_Email extends Mage_Core_Model_Email_Template
{
 
    /**
     * Send email to recipient
     *
     */
    public function sendEmail($templateId, $sender, $email, $name, $subject, $params = array())
    {
        $this->setDesignConfig(array('area' => 'frontend', 'store' => $this->getDesignConfig()->getStore()))
				->setTemplateSubject($subject)
				->sendTransactional(
					$templateId,
					$sender,
					$email,
					$name,
					$params
			);
    }
}

?>