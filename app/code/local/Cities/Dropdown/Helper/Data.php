<?php
/**
 * @category   Cities as Drop Down
 * @package    Cities_Dropdown
 * @author     ahsan.horani@tcs-e.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Cities_Dropdown_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getPKCities()
    {
        $helper = Mage::helper('directory');
        $cities = array(
		"Abbottabad"
		,"Ahmed Pur East"
		,"Attock"
		,"Badin"
		,"Bahawalpur"
		,"Burewala"
		,"Chichawatni"
		,"Chakwal"
		,"Dadu"
		,"Dera Ghazi Khan"
		,"Dera Ismail Khan"
		,"Daska"
		,"Dharki"
		,"Faisalabad"
		,"Gujrat"
		,"Gurjanwala"
		,"Hyderabad"
		,"Hub"
		,"Chiniot"
		,"Islamabad"
		,"Jacobabad"
		,"Jhang"
		,"Jhelum"
		,"Joharabad"
		,"Karachi"
		,"Khairpur"
		,"Kamoki"
		,"Kot Addu"
		,"Kharian"
		,"Kasur"
		,"Khanewal"
		,"Larkana"
		,"Lahore"
		,"Lalamusa"
		,"Muzafarabad A. K."
		,"Mandi bahuddin"
		,"Mardan"
		,"Mirpur Khas"
		,"Moro"
		,"Multan"
		,"Mianwali"
		,"Muzafargarh"
		,"Bahawalnagar"
		,"Nowshera"
		,"Khanpur"
		,"Narowal"
		,"Kohat"
		,"Okara"
		,"Peshawar"
		,"Pak Pattan Sharif"
		,"Mirpur A. K."
		,"Murree"
		,"Rawalpindi"
		,"Rahim Yar Khan"
		,"Sakrand"
		,"Sanghar"
		,"Sadiqabad"
		,"Sarghoda"
		,"Shikarpur"
		,"Sukkur"
		,"Sialkot"
		,"Shahdadpur"
		,"Sheikhupura"
		,"Sahiwal"
		,"Tando Allahyar"
		,"Tando Jam"
		,"Tando Adam"
		,"Thatta"
		,"Toba Tek Singh"
		,"Quetta"
		,"Vehari"
		,"Wah Cantt"
		,"Nawabshah"
		,"Wazirabad"
								);
        return $cities;
    }
	
	/* Set UAE Cities */
	public function getUAECities()
	{
		$helper = Mage::helper('directory');
		
        $cities = array(
			"Abu Dhabi"
			,"Al Ain"
			,"Dubai"
			,"Ras Al Khaimah"
			,"Sharjah"
			," Umm Al Quwain");
			
		return $cities;
	}

	/* Get  UAE Cities  as drop down*/
	public function getUAECitiesAsDropdown($selectedCity = '')
    {
		$cities = $this->getUAECities();
        $options = '';
        foreach($cities as $city)
		{
            $isSelected = $selectedCity == $city ? ' selected="selected"' : null;
            $options .= '<option value="' . $city . '"' . $isSelected . '>' . $city . '</option>';
        }
		
        return $options;
	}

    public function getPKCitiesAsDropdown($selectedCity = '')
    {
        $cities = $this->getPKCities();
        $options = '';
        foreach($cities as $city){
            $isSelected = $selectedCity == $city ? ' selected="selected"' : null;
            $options .= '<option value="' . $city . '"' . $isSelected . '>' . $city . '</option>';
        }
        return $options;
    }
}

?>