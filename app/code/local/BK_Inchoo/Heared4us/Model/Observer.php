<?php
class Inchoo_Heared4us_Model_Observer
{
	
	const ORDER_ATTRIBUTE_FHC_ID = 'heared4us';
	const ORDER_ATTRIBUTE_FHC_ID2 = 'instruct';
	
    /**
     * Event Hook: checkout_type_onepage_save_order
     * 
     * @author Muhammad Ahsan Horani
     * @param $observer Varien_Event_Observer
     */
	 
	 /* Onepage checkout save shipping */
	 public function hookToSaveShipping($observer)
	 {		
	 		// print_r($_REQUEST['getvoice']); 
			
			/* Get giftcard value assign to customer session for later use */
			if($_POST['getvoice']){
				$giftcard	=	mysql_real_escape_string($_POST['getvoice']);
				Mage::getSingleton( 'customer/session' )->setData( 'giftcard',  $giftcard );
			}
			
			if($_POST['spec_instruct']) {
				$instruction	=	mysql_real_escape_string($_POST['spec_instruct']);
				Mage::getSingleton( 'customer/session' )->setData( 'instruction',  $instruction );
			}
			// print_r($_REQUEST['getvoice']); exit;
			// print_r($observer->getFrontController()->getRequest()->getParams());
			// print_r($observer->getEvent()->getData());
			// print_r($_REQUEST);
			// print_r(Mage::App()->getRequest()->getPost()); exit;
	 }
	 
	 /* After place order from frontend, on success page, add giftcard to the order, only for frontend */
	public function hookToOrderSaveEvent($observer)
	{ 
		

		
		// echo $instruction; exit;
		// print_r($observer->getEvent()->getData()); exit;
		// $_inchoo_Heared4us = $observer->getEvent()->getControllerAction()->getRequest()->getPost('getvoice');
		// echo "App:" . Mage::app()->getRequest()->getPost(); exit;
		// echo "\n <b>Inchoo:" . $_inchoo_Heared4us; exit;
		// print_r($observer->getEvent()->getData()); exit;
		// echo $_REQUEST['getvoice']; exit;
		// print_r($_REQUEST); exit;
				
		/**
		* NOTE:
		* Order has already been saved, now we simply add some stuff to it,
		* that will be saved to database. We add the stuff to Order object property
		* called "heared4us"
		*/
		
		
		$order = new Mage_Sales_Model_Order();
		$incrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
		$order->loadByIncrementId($incrementId);
		
		$instruction	=	Mage::getSingleton( 'customer/session' )->getData('instruction');
		//Fetch the data from select box and throw it here
		$_heared4us_data = null;
		$_heared4us_data = Mage::getSingleton( 'customer/session' )->getData('giftcard');
		
		//Save special instructions to order
		$order->setData(self::ORDER_ATTRIBUTE_FHC_ID2,$instruction);
		
		//Save FHC id to order object
		$order->setData(self::ORDER_ATTRIBUTE_FHC_ID, $_heared4us_data);
		$order->save();
	}
	
	//after
	public function hookToOrderBeforeSaveEvent( $observer)
	{	
		// print_r($observer->getEvent()->getData('getvoice')); exit;
		// print_r($_REQUEST['getvoice']); exit;
		// echo "hello"; exit;
		// print_r($_POST); exit;
		// print_r($_REQUEST['getvoice']); exit;
		// print_r($observer->getEvent()->getData()); exit;
	/* 	$_inchoo_Heared4us = $observer->getEvent()->getRequest()->getPost('getvoice');
		echo "App:" . Mage::app()->getRequest()->getPost('getvoice');
		echo "\n <b>Inchoo:" . $_inchoo_Heared4us; exit; */
	}
	
	/* public function beforeOrderPlaced($observer)
	{
			
			$giftcard	=	$_POST['getvoice'];
			Mage::getSingleton( 'customer/session' )->setData( 'giftcard',  $giftcard );
			echo $giftcard; exit;
	} */
	
	/* Add gift card after admin order created, from backend only */
	public function adminOrderCreated($observer)
	{
			// $observer->getEvent();
			// echo "<pre>";
			// print_r($observer->getEvent()->getOrder()->getData('increment_id'));
			// echo $increment_id;
			// print_r($observer->getEvent()->getQuote());
			// exit;
			// print_r($observer->getEvent()->getData()); exit;
			// $observer->getEvent();
			// print_r($observer->getOrder()); exit;
			// $giftcard	=	null;
			
			
			$increment_id	=	$observer->getEvent()->getOrder()->getData('increment_id');			//Get order increment id
			
			$order = Mage::getModel('sales/order')->loadByIncrementId($increment_id);					//load order
			
			if($_POST['getvoice'])
			{
				$giftcard	=	mysql_real_escape_string($_POST['getvoice']);
				
				$order->setData(self::ORDER_ATTRIBUTE_FHC_ID, $giftcard);						 	//add to order
			}
			
			if($_POST['spec_instruct'])
			{
				$instruction	=	mysql_real_escape_string($_POST['spec_instruct']);
				$order->setData(self::ORDER_ATTRIBUTE_FHC_ID2,$instruction);
			}
			
			$order->save();
			
			/* Clear admin quote */
			Mage::getSingleton('adminhtml/session_quote')->getQuote()->removeAllItems();
	}
	



}