<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
/* $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();
//START Add order attribute by Ahsan Horani
$sql = "SELECT entity_type_id FROM ".$this->getTable('eav_entity_type')." WHERE entity_type_code='order' ";

// $row = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchRow($sql);

$row = Mage::getSingleton('core/resource')->getConnection('core_read')->addColumn(Mage::getSingleton('core/resource')->getTableName('sales/order'), 'giftaid', 'TEXT NULL');

$attribute = array(
'type' => 'text',
'label' => 'heared4us',
'visible' => false,
'required' => false,
'user_defined' => false,
'searchable' => false,
'filterable' => false,
'comparable' => false,
);

$attribute2 = array(
'type' => 'text',
'label' => 'instruct',
'visible' => false,
'required' => false,
'user_defined' => false,
'searchable' => false,
'filterable' => false,
'comparable' => false,
);

$installer->addAttribute($row['entity_type_id'], 'heared4us', $attribute);
$installer->addAttribute($row['entity_type_id'], 'instruct', $attribute2);
//END Add customer attribute by Ahsan Horani

$installer->endSetup();