<?php

class Clarion_Paymentgateway_Model_Standard extends Mage_Payment_Model_Method_Abstract
{

     /*new added payment method code define here*/
	 protected $_code  = 'paymentgateway';
     
	 protected $_isInitializeNeeded      = true;
     
	 protected $_canUseInternal          = false;
     
	 protected $_canUseForMultishipping  = false;
     
	 /*set redirect url after order place */
	 
	 public function getOrderPlaceRedirectUrl()
	 {
	   return Mage::getUrl("payget/paygateway/redirect");
	 }
		
	public function omsApi()
	 {		
	 
		$sTime	=	array(
						"12:00" =>"11:00 AM - 12:00 PM",
						"13:00" =>"12:00 PM - 1:00 PM",
						"14:00" =>"1:00 PM - 2:00 PM",
						"15:00" =>"2:00 PM - 3:00 PM",
						"16:00" =>"3:00 PM - 4:00 PM",
						"17:00" =>"4:00 PM - 5:00 PM",
						"18:00" =>"5:00 PM - 6:00 PM",
						"19:00" =>"6:00 PM - 7:00 PM",
						"20:00" =>"7:00 PM - 8:00 PM",
						);
						

						
			
									
		//City Codes
		$cityCode	=	array(
							'ABT' =>	'ABBOTTABAD',
							'AIP' =>	'ALIPUR',
							'AMO' =>	'SAMARO',
							'APC' =>	'ALI PUR CHATTA',
							'ATT' =>	'ATTOCK',
							'AWH' =>	'RABWAH',
							'AWR' =>	'AWARAN',
							'BDN' =>	'BADIN',
							'BJR' =>	'BAJWAR',
							'BKK' =>	'BHAKKAR',
							'BLA' =>	'BELA',
							'BNP' =>	'BANNU',
							'BPR' =>	'BHAI PHAIRU',
							'BRA' =>	'BHERA',
							'BRC' =>	'BHIRIA CITY',
							'BRD' =>	'BHIRIA ROAD',
							'BSH' =>	'BHIT SHAH',
							'BTK' =>	'BATKHELA',
							'BWA' =>	'BHAWANA',
							'BWL' =>	'BHALWAL',
							'CAZ' =>	'CHOWK AZAM',
							'CKL' =>	'CHAKWAL',
							'CSD' =>	'CHARSADA',
							'CSS'	=>   'SARWAR SHAHEED',
							'CWD' =>	'CHAWINDA',
							'DDU' =>	'DADU',
							'DIG' =>	'DIGRI',
							'DIK'	=>   'DERA ISMAIL KHAN',
							'DIN' =>	'DINA',
							'DKA' =>	'DASKA',
							'DKI' =>	'DAHARKI',
							'DKT' =>	'DAKOTA',
							'DMJ'	 =>  'DERA MURAD JAMALI',
							'DNG' =>	'DINGA',
							'DOR' =>	'DOUR',
							'DPA'	 =>  'DEPAL PUR',
							'DRG' =>	'DARGAI',
							'DWA'	=> 'DAHRAN WALA',
							'ELA' =>	'ELLAH ABAD',
							'ERA' =>	'NAUSHERA',
							'FAB' =>	'FORT ABBAS',
							'FAD' =>	'FAROOQABAD',
							'FPR' =>	'FATEHPUR',
							'FSD' =>	'FAISALABAD',
							'FTG' =>	'FATEH JANG',
							'FWA' =>	'FEROZ WATOWAN',
							'FZP' =>	'FAZIL PUR',
							'GAD' =>	'GHAZI ABAD',
							'GBT' =>	'GAMBAT',
							'GDU' =>	'GUDDU',
							'GHK' =>	'GHAKAR',
							'GJT' =>	'GUJRAT',
							'GKN' =>	'GUJJAR KHAN',
							'GMD' =>	'GAGGO MANDI',
							'GOL' =>	'GOLARCHI',
							'GRO' =>	'GHARO',
							'GTI' =>	'GHOTKI',
							'HDD' =>	'HYDERABAD',
							'HLA' =>	'HALA',
							'HOO' =>	'HAZRO',
							'HRI' =>	'HARI PUR',
							'HSL' =>	'HASIL PUR',
							'HSM' =>	'HUJRA SHAMUQEEM',
							'HSN' =>	'HASSAN ABDAL',
							'HVL' =>	'HAVELIAN',
							'HZD' =>	'HAFIZ ABAD',
							'IAA' =>	'MURID WALA',
							'ING' =>	'IQBAL NAGAR',
							'IOT' =>	'CHINIOT',
							'JCB' =>	'JACCOAB ABAD',
							'JDW' =>	'JAMALDIN WALI',
							'JHG' =>	'JHANG',
							'JLJ' =>	'JALALPUR JATTAN',
							'JLM' =>	'JHELUM',
							'JPT' =>	'JHAT PAT',
							'JRD' =>	'JOHARABAD',
							'JRI' =>	'JHANGIRA',
							'JTI' =>	'JATOI',
							'JWA' =>	'JARAN WALA',
							'KAK' =>	'KAMBAR ALI KHAN',
							'KAR' =>	'KAROR LALESAN',
							'KDC' =>	'KOT RADHA KISHA',
							'KDW' =>	'KHIDDER WALA',
							'KGM' =>	'KOT GHULAM MUHD',
							'KHI' =>	'KARACHI',
							'KHM' =>	'KASHMORE',
							'KHP' =>	'KHAIR PUR',
							'KKK' =>	'KANDHKOT',
							'KLA' =>	'KOT KHATRAN',
							'KLK' =>	'KALAR KAHAR',
							'KLY' =>	'KOTLI AZAD KASHMIR',
							'KMK' =>	'KAMOKE',
							'KMN' =>	'KOT MOMIN',
							'KND' =>	'KANDYARO',
							'KNI' =>	'CHAMAKNI',
							'KNU' =>	'KANA NAU',
							'KOT' =>	'KOT ADDU',
							'KPR' =>	'KHIPRO',
							'KRC' =>	'KHARIAN CANTT',
							'KRN' =>	'KHARIAN',
							'KSR' =>	'KASUR',
							'KSW' =>	'KASOWAL',
							'KTM' =>	'KOT MITTHAN',
							'KUL' =>	'KULDANA',
							'KUL' =>	'KULDANA',
							'KZK' =>	'KHAZAKHELA',
							'LAR' =>	'LARKANA',
							'LHE' =>	'LAHORE',
							'LLI' =>	'LALIAN',
							'LNK' =>	'LANDIKOTAL',
							'LQR' =>	'LIAQAT PUR',
							'MBD' =>	'MANDI BAHAUDDIN',
							'MCN' =>	'MIAN CHANNU',
							'MDN' =>	'MARDAN',
							'MGL' =>	'MANGLA',
							'MGR' =>	'MIRWAH GHORCHANI',
							'MHD' =>	'MINCHANABAD',
							'MHR' =>	'MEHAR',
							'MIT' =>	'MITHI',
							'MLK' =>	'MALAK WAL',
							'MLT' =>	'MITRO',
							'MPK' =>	'MIRPURKHAS',
							'MPM' =>	'MIR PUR MATELO',
							'MRO' =>	'MORO',
							'MRP' =>	'MEHRAB PUR',
							'MRY' =>	'MUREEDKAY',
							'MTI' =>	'KARIO GHANWAR',
							'MUX' =>	'MULTAN',
							'MWI' =>	'MIANWALI',
							'NFZ' =>	'NOSHERO FEROZ',
							'NJT' =>	'NEW JATOI',
							'NKS' =>	'NANKANA SAHIB',
							'NOP' =>	'NOORPUR',
							'NOW' =>	'NOWSHERA',
							'NRL' =>	'NAROWAL',
							'PAC' =>	'PACCA CHANG',
							'PAN' =>	'PAINSRA',
							'PBI' =>	'PABBI',
							'PDK' =>	'PIND DADAN KHAN',
							'PGB' =>	'PINDI GHEB',
							'PHP' =>	'PAHAR PUR',
							'PNQ' =>	'PANU AQIL',
							'PRR' =>	'PASROOR',
							'PTI' =>	'PATTOKI',
							'QAZ' =>	'QAZI AHMAD',
							'QML' =>	'MIR PUR AZAD KASMIR',
							'REE' =>	'MURREE',
							'RJP' =>	'RAJAN PUR',
							'RND' =>	'RAIWAND',
							'RNI' =>	'RANI PUR',
							'RRA' =>	'GOJRA',
							'RYK' =>	'RAHIM YAR KHAN',
							'SAB' =>	'KHUSHAB',
							'SAG' =>	'SARAI ALAMGEER',
							'SAK' =>	'SAKRAND',
							'SAM' =>	'SAMUNDRI',
							'SAN' =>	'SANGHAR',
							'SDA' =>	'SADIQABAD',
							'SES' =>	'SEHWAN',
							'SGD' =>	'SARGODHA',
							'SGR' =>	'SHAKAR GARH',
							'SHH' =>	'SHAHDAD KOT',
							'SIP' =>	'SHIKAR PUR',
							'SKT' =>	'SHAH KOT',
							'SKZ' =>	'SUKKUR',
							'SLT' =>	'SIALKOT',
							'SNL' =>	'SILANWALI',
							'SNW' =>	'SANAWAN',
							'SPR' =>	'SHAHDAD PUR',
							'SQD' =>	'SHABQADAR',
							'SQR' =>	'SHARAQPUR',
							'SQT' =>	'SHOR KOT',
							'SRA' =>	'SHEIKHUPURA',
							'SUA' =>	'SUNDAR ADDA',
							'SWT' =>	'SWAT',
							'TBI' =>	'TAKHAT BAI',
							'TDA' =>	'TANDO ALLAH YAR',
							'TDJ' =>	'TANDO JAM',
							'TDM' =>	'TANDO ADAM',
							'TLG' =>	'TALA GANG',
							'TMG' =>	'TEMAR GARAH',
							'TMK' =>	'TANDO MUHAMMAD KHAN',
							'TSA' =>	'THARO SHAH',
							'TSP' =>	'TIBBA SULTAN',
							'TTS' =>	'TOBA TEK SINGH',
							'TXL' =>	'TAXILA',
							'UKT' =>	'UMAR KOT',
							'UPD' =>	'UPPER DIR',
							'USM' =>	'USTA MOHAMMAD',
							'VDR' =>	'VARI DIR',
							'VRI' =>	'VEHARI',
							'WAH' =>	'WAH CANTT',
							'WNS' =>	'NAWAB SHAH',
							'WZD' =>	'WAZIR ABAD',
							'YZM' =>	'YAZMAN MANDI',
							);
			
			

			$order_collection = Mage::getModel('sales/order')->getCollection()
										->addFieldToFilter('oms_sync', '0')
										->addFieldToFilter('status','processing')
										->addAttributeToSelect('*');
										
			// echo count($order_collection);echo "here";
			$count=0;
			$client = new SoapClient("http://58.65.196.203:8232/?wsdl");  echo "Connected";
			foreach($order_collection as $order)
			{	
			
				if(!$order->getShippingAddress()){continue;}
				if(!$order->getId()){continue;}
				
				/* Get occasion code by Gift card*/
					$occ_cards	=	array(
										"AL" => 'Apology',
										"EI" => 'Eid Mubarak',
										"BD" => 'Happy Birthday',
										"MD" => 'Mother\'s Day',
										"RM" => 'Ramadan Kareem',
										"VD" => 'Valentines Day',
										"IFD" => 'Eid-ul-Adha',
										"NY" => 'Happy New Year',
										"CD" => 'Christmas',
										"FD" => "Father\'s Day",
										"CO" => 'Congratulations',
										"PD" => "Parent\'s Day",
										"FRD" => 'Friendship Day',
										"GW" => 'Get Well Soon',
										"GL" => 'Good Luck',
										"HM" => 'Happy Marriage',
										"NB" => 'New Born',
										"PO" => 'Promotion',
										"SP" => 'Sympathy',
										"SY" => 'Sorry',
										"TY" => 'Thank You',
										"WD" => 'Wedding Anniversary'
										);
					$occ_code	=	array_search($order->getData('heared4us'),$occ_cards);		//Get Code by Gift Card
					
					
				/* Get Gift message Start*/
				$message = Mage::getModel('giftmessage/message');
				/* Add Gift Message*/ 
				$gift_message_id = $order->getGiftMessageId();
				// echo $gift_message_id;
				if(!is_null($gift_message_id)) {
				$message->load((int)$gift_message_id);
				// $gift_sender = $message->getData('sender');
				// $gift_recipient = $message->getData('recipient');
			   $gift_message = $message->getData('message');
				}
			   /* Gift Msg End */
				// $count++;
				// unset($shipAddrs);
				// unset($billAddrs);
				// unset($items);
				$incrementId	= $order->getData('increment_id');
				$shipAddrs	=	$order->getShippingAddress()->getData();
				$billAddrs		=	$order->getBillingAddress()->getData();
				
				//Get city code by City Name
				$ship					=	strtoupper($shipAddrs['city']);
				$shipCityCode		=	array_search($ship,$cityCode);
				$bill						=	strtoupper($billAddrs['city']);
				$billCityCode			=	array_search($bill,$cityCode);
							
				// $order = $observer->getEvent()->getOrder();
				// $quote	=	$order->getQuote();										//Get Quote
				$items	=	$order->getAllItems();									//Get All order items
				$storeId = $order->getStoreId();										//Get Store Id
				
				
				//GET SHIPPING AND BILLING DATA FROM CUSTOMER SESSION
				// $shipAddrs	=	Mage::getModel('checkout/session')->getQuote()->getShippingAddress()->getData();
				// $billAddrs		=	Mage::getModel('checkout/session')->getQuote()->getShippingAddress()->getData();
				
				//GET ORDER DATE
				$oms_quote_date 	= $order->getData('created_at');
				$oms_quote_date	=	explode(' ',$oms_quote_date);
				// $oms_booking_date	=	$oms_quote_date[0];
				// echo $quote->getData('fee_amount'); exit;
				// print_r($shipAddrs['firstname']);
				// print_r($billAddrs); 
				// Exit;
			
			//
			
			
			$countryBill = Mage::getModel('directory/country')->loadByCode($billAddrs['country_id']);
			
			$countryShip = Mage::getModel('directory/country')->loadByCode($shipAddrs['country_id']);
			
			//Get customer account
			if($countryBill=='Pakistan')
			{
				$custAccountNo	=	'1156-1';
			}
			else
			{
				$custAccountNo	=	'1156';
			}
			
			$shipCountry = $countryShip->getName();
			$billCountry 	= $countryBill->getName();
			
			$oms_orderId			=	$incrementId;
			$oms_booking_date	=	$oms_quote_date[0];
			$oms_bill_name			= $billAddrs['firstname'] ." ". $billAddrs['lastname'];
			$oms_bill_addrs1		= $billAddrs['street'];
			$oms_bill_phone		= $billAddrs['telephone'];
			$oms_bill_email			=	$billAddrs['email'];
			$oms_ship_name		= $shipAddrs['firstname'] . " " . $shipAddrs['lastname'];
			$oms_ship_addrs		=	$shipAddrs['street'] ;
			$oms_ship_phone		= $shipAddrs['telephone'];
			// $oms_dlvry_date		=	"2015-04-21";
			// $oms_dlvry_time		=	"";
			$oms_spcl_srvc			= $order->getData('base_fee_amount') ;
			$oms_total_amount	=	$order->getData('base_grand_total') ;
			
			// Mage::getSingleton( 'customer/session' )->setData('orderdate','04/03/2015');
			// Mage::getSingleton( 'customer/session' )->setData('ordertime','11am-12pm');
			
			$date				=	$order->getData('delivery_date'); 	//Mage::getSingleton( 'customer/session' )->getData('orderdate');
			$time				=	$order->getData('delivery_time');	//Mage::getSingleton( 'customer/session' )->getData('ordertime');
			$time				=	array_search($time,$sTime);

			if($date)
			{
				$timestamp = strtotime($date);	//	Format : mm/dd/yyyy
				$Odate = date('c', $timestamp);
				$Odate=explode('T',$date);
				$oms_dlvry_date	=	$Odate[0];	//Set Special  service: Delivery date
			}
			else
			{
				$oms_dlvry_date	=	"";
			}
			
			if($time)
			{
				$oms_dlvry_time	=	$time;
			}
			else
			{
				$oms_dlvry_time	=	"";
			}
			
			/* Convert special services into corresponding codes */
			$timestamp = strtotime($date);	//	Format : mm/dd/yyyy
			$day = date('l', $timestamp);		//Get name of the Day by submitted date
			
			if($day=='Sunday' && $time)
			{
				$specialServ	=	'H/D,T/S';
			}
			else if($day=='Sunday')
			{
				$specialServ	=	'H/D';
			}
			else if($time)
			{
				$specialServ	=	'T/S';
			}
			else
			{
				$specialServ	=	'';
			}
			/* 	End		 */
		
			echo "Dlvry date: " . $oms_dlvry_date;
	/* Loop through each item */
	foreach($items as $item)
	{
		// print_r($item->getData()); exit;
		// echo $item->getQtyOrdered();
		$params1 = array (
					"consignmentNo"=> 	$oms_orderId,
					"item_Code"		=>	$item->getSku(),
					"QTY" 				=> 	$item->getQtyOrdered(),
					"rate"				=> 	$item->getPrice(),
					
			);
			
		// echo "<pre>";
		echo "here1";
		$response1 = $client->InsertGOFItem($params1);
		// echo "<b>GOF ITEM :</b><br> "; print_r($response1);
		$result1		=	$response1->InsertGOFItemResult; 		

				if($result1==1)
					echo "GOF Item Success<br>\n";
				
	}
	

	$params2 = array (
            "consignmentNo"=> 	$oms_orderId,
			"occ_Code"		=>	$occ_code,
            "DLVR_Date" 		=> 	$oms_dlvry_date,
			"DLVR_Time"		=> 	$oms_dlvry_time,
			"remarks"			=>	$gift_message,						//Gift message
			"remarks2"		=>	$order->getData('instruct'),		//Special instructions
			
    );
	
	$params3 = array (
            "consignmentNo" =>$oms_orderId,
			"specialServ"		 =>$specialServ,
            
			
    );
	
	$params4 = array (
																				/* DATA TYPE : else are string types*/
            "consignmentNo" => $oms_orderId,
			"bookingDate"		=>$oms_booking_date,								//datetime
			"customerAccountNo"	=>$custAccountNo,
			"CUS_NAM"=>$oms_bill_name,
			"CUS_ADDR1"=>$oms_bill_addrs1,
			"CUS_ADDR2"=>$billAddrs['city'] ,
			"CUS_ADDR3"=>$billCountry,
			"CUS_PHN"=>$oms_bill_phone,
			"CUS_Email"=>$oms_bill_email,
			"CNSGEE_NAM"=>$oms_ship_name,
			"CNSGEE_ADDR1"=>$oms_ship_addrs,
			"CNSGEE_ADDR2"=>$shipAddrs['city'],
			"CNSGEE_ADDR3"=>$shipCountry,
			"CNSGEE_PHN"=>$oms_ship_phone,
			"serviceNo"=>"O",
			"prod_No"=>"S",
			"origin"=>$billCityCode,	//$billAddrs['city'],
			"des"=>$shipCityCode,		//$shipAddrs['city'],
			"bookingstaff_No"=>'7238',
			"route_No"=>"",												//double
			"amount_Cal"=>0,											//double
			"ot_srvs_amt"=>0,											//double
			"handlingCharges"=>0,										//double
			"othercharges"=>0,											//double
			"amount"=>$oms_total_amount,													//double
			"partner_Amt"=>0,											//double
			"part_Comm"=>0,												//double
			"handlingInstruction"=>"",
			"excise"=>0,														//double
			"cnsg_val"=>0,													//double
			"insurance_Amt"=>0,										//double
    );
	


			$response2 = $client->InsertGOFREM($params2);
			// echo "<b>GOFREM :</b><br> "; print_r($response2);
			$response3 = $client->InsertSE_Dlvry($params3);
			// echo "<b>SE Delivery :</b><br> "; print_r($response3);
			$response4 = $client->InsertTrackData($params4);
			// echo "<b>TRACK DATA :</b><br>"; print_r($response4);
			$order->setData('oms_sync','1');
			$order->save();
			Mage::log($oms_orderId . ' was synced', null, $oms_orderId . '_oms_sync.log');			
			echo "SUCCESS All\n<br>";
			// unset($order);
			
			} 
	 }

}