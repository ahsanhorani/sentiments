<?php
 class Clarion_Paymentgateway_PaygatewayController extends Mage_Core_Controller_Front_Action
{
   
   
    // redirect action method 
	  public function redirectAction() {
	    $this->loadLayout();
        $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','paymentgatewaytemplate',array('template' => 'paymentgatewaytemplate/redirect.phtml'));
		$this->getLayout()->getBlock('content')->append($block);
        $this->renderLayout();
	}
	
	public function ublAction() {
			
			// echo "hello";
			
		//Load Order by order Id
		$lastOrderIncrement_id	=	Mage::getSingleton('checkout/session')->getLastRealOrderId();
		$order = Mage::getModel('sales/order')->loadByIncrementId($lastOrderIncrement_id);
		
		$ExternalLibPath=Mage::getBaseDir('lib') . DS . 'dbconmgr' . DS .'dbconmgr.php';
		require_once ($ExternalLibPath);
		
		$db = new dbconmgr();
	
		// if(Mage::getSingleton( 'customer/session' )->getData('orderId'))
		{
				
			 // $lastOrderIncrement_id  =  Mage::getSingleton( 'customer/session' )->getData('orderId');;
			 $data = $db->getRedirectURL($lastOrderIncrement_id);		
			// print_r($data); //exit;
			
			 
			 $cmd = "/finalize.sh " . $data[0]['transactionid'] . " " . $data[0]['orderid'] ;		
			$output = exec($cmd);
			
			//echo "Output is $output<BR><BR>";		
			
			$output = explode("|", $output);
			$approvalcode = $output[0];
			$respcode     = $output[1];
			$respdesc	  = $output[2];
			
			if( strlen($approvalcode) > 10 ){
				$approvalcode = "-";
			}
			
				//print_r($output); //exit;
				$db->updateCCLogStatus($data[0]['orderid'], $respcode, $respdesc, $data[0]['transactionid'], $data[0]['orderamount'], $approvalcode);
				
				$order_no	=	$data[0]['orderid'];
				
		}
			
			if( $respcode == 0 ) 
			{
				
				$this->_redirect('checkout/onepage/success', array('_secure'=>true));
				
			}
			else
			{
				$this->_redirect("checkout/onepage/failure");
			} 
			
			
			//Custom Action for Ubl Payment Method
	}
  	
	
	  public function  successAction()
    {
        $this->_redirect('checkout/onepage/success', array('_secure'=>true));
    }
	
	
		
	// cancel action will hit when some one cancel the order and state changes to canceled	
	 public function cancelAction() {
        if (Mage::getSingleton('checkout/session')->getLastRealOrderId()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
            if($order->getId()) {
				// Flag the order as 'cancelled' and save it
				$order->cancel()->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, 'Gateway has declined the payment.')->save();
			}
        }
	}
	
}
?>