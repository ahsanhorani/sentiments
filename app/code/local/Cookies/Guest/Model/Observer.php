<?php 
	
class Cookies_Guest_Model_Observer extends Varien_Event_observer {
	
	/* Before Order Placed */
	public function setCookies($observer)
	{		
		
			$checkout				=	Mage::getModel('checkout/session');
			$type					=	$checkout->getQuote()->getCheckoutMethod();							// GET CUSTOMER TYPE
			$address_billing		=	$checkout->getQuote()->getBillingAddress()->getData();				//GET CUSTOMER BILLING ADDRESS
			$address_shipping	=	$checkout->getQuote()->getShippingAddress()->getData();			//GET CUSTOMER SHIPPING ADDRESS
			$period					=	60	*	60	*	24	*	100;													//SET COOKIE TIMEOUT FOR 100 DAYS
		/* Customer's Billing Information' */
			$first_name			= $address_billing['firstname'];
			$last_name			= $address_billing['lastname'];
			$company				= $address_billing['company'];
			$email					= $address_billing['email'];
			$postcode				= $address_billing['postcode'];
			$street					= $address_billing['street'];
			$region					= $address_billing['region'];
			$city						= $address_billing['city'];
			$country				= $address_billing['country_id'];
			$telephone			= $address_billing['telephone'];
			$fax						= $address_billing['fax'];
			
			/* Customer's Shipping Information' */
			$firstname_ship				= $address_shipping['firstname'];
			$last_name_ship			= $address_shipping['lastname'];
			$company_ship				= $address_shipping['company'];
			$email_ship					= $address_shipping['email'];
			$postcode_ship				= $address_shipping['postcode'];
			$street_ship					= $address_shipping['street'];
			$region_ship					= $address_shipping['region'];
			$city_ship						= $address_shipping['city'];
			$country_ship				= $address_shipping['country_id'];
			$telephone_ship				= $address_shipping['telephone'];
			$fax_ship						= $address_shipping['fax'];
		
		/* If  checkout method is Guest */
			if($type=='guest')
			{
				
				$cookie					=		Mage::getModel('core/cookie');
				
				/* Set Cookies for guest  with the billing information */
				$cookie					->	set('firstname',$first_name,$period);
				$cookie					->	set('lastname',$last_name,$period);
				$cookie					->	set('company',$company,$period);
				$cookie					->	set('email',$email,$period);
				$cookie					->	set('postcode',$postcode,$period);
				$cookie					->	set('street',$street,$period);
				$cookie					->	set('region',$region,$period);
				$cookie					->	set('city',$city,$period);
				$cookie					->	set('country',$country,$period);
				$cookie					->	set('telephone',$telephone,$period);
				$cookie					->	set('fax',$fax,$period);
				
				/* Set Cookies for guest  with the shipping information */
				$cookie					->	set('firstname_ship',$firstname_ship,$period);
				$cookie					->	set('lastname_ship',$last_name_ship,$period);
				$cookie					->	set('company_ship',$company_ship,$period);
				$cookie					->	set('email_ship',$email_ship,$period);
				$cookie					->	set('postcode_ship',$postcode_ship,$period);
				$cookie					->	set('street_ship',$street_ship,$period);
				$cookie					->	set('region_ship',$region_ship,$period);
				$cookie					->	set('city_ship',$city_ship,$period);
				$cookie					->	set('country_ship',$country_ship,$period);
				$cookie					->	set('telephone_ship',$telephone_ship,$period);
				$cookie					->	set('fax_ship',$fax_ship,$period);
			
			}
			
			
			
			// echo "\n"; 
			// print_r($type);
			// echo "\nBilling:\n";
			// print_r($address_billing); 
			// print_r($firstname_ship); 
			
			
			// echo $_COOKIE['firstname_ship'];
			// echo $address_shpping; $_COOKIE["firstname_ship"];
			// exit; 

	}
	
	
	
}
	
	
	
?>