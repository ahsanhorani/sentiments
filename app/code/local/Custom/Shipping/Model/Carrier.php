<?php

class  Custom_Shipping_Model_Carrier 
										extends Mage_Shipping_Model_Carrier_Abstract
										implements Mage_Shipping_Model_Carrier_Interface 
{
	protected $_code	=	"custom_shipping";
	
	
	/*  This is the method that receives 
		a shipping request, 
		appends applicable shipping methods 
		and returns a shipping result 
	*/
		
	
	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
	{
		
		$result	= 		Mage::getModel("shipping/rate_result");
		
		$result	->	append($this->_getStandardShippingRate());
		
		return			$result;
	}
	
	/*  This method returns array of allowed methods for shipping */
	
	public function getAllowedMethods()
	{
		return array(
							"standard"	=>	'Free Shipping',
							"express"		=>	'Express',
							);
	}
	
	protected function _getStandardShippingRate()
	{
		/* Call to a model class that  has shipping methods defined */
		$rate		=		Mage::getModel("shipping/rate_result_method");
		
		if(Mage::getSingleton('checkout/session')->getQuote()->getAllItems())
		{
			/* Get all items from the cart/quote */
			$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
		}
		
		if(Mage::getSingleton('adminhtml/session_quote')->getQuote()->getAllItems())
		{
			
			/* Get all items from the cart/quote */
			$items = Mage::getSingleton('adminhtml/session_quote')->getQuote()->getAllItems();
			
		}
		
		
		
		/*  Set custom carrier info */
		$rate->setCarrier($this->_code);
		$rate->setCarrirerTitle($this->getConfigData('title'));
		$rate->setMethod("standard");
		$rate->setMethodTitle("Free Shipping");
		
		/*  Calculate total price w.r.t weight and quantity
			format:
					(price	=	[item weight]	*	[item quantity] 	*	shipping rate)
		*/
		
		//Get Price from system->config
		$inputPrice	=	(float)$this->getConfigData('price_input');
		
		foreach($items as $item) {
		
			//Calculate price per item
			$price		+=	($item->getWeight() * $item->getQty()	* $inputPrice);
		}
		
		$currency	=	Mage::app()->getStore()->getCurrentCurrencyCode(); 
		
		if($currency=="USD")
		{
			$rate->setPrice(-300);		//SET SHIPPING PRICE
		
			$rate->setCost(-300);
		}
		else
		{
			$rate->setPrice(0);		//SET SHIPPING PRICE
		
			$rate->setCost(0);
		}
		
		return $rate;
	}

}
	
?>