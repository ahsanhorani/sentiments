<?php
	
	/* This is grid container block*/
class Mycompany_Mymodule_Block_Adminhtml_Mymodule extends Mage_Adminhtml_Block_Widget_Grid_Container {
 
    public function __construct() {
        $this->_controller = 'adminhtml_mymodule';
        $this->_blockGroup = 'mymodule';
        $this->_headerText = Mage::helper('mymodule')->__('Custom Item Sales Report');
        parent::__construct();
        $this->_removeButton('add');
    }
 
}

?>