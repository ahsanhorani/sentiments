<?php
	/* Creating grid and specifying all columns for report*/
class Mycompany_Mymodule_Block_Adminhtml_Mymodule_Grid extends Mage_Adminhtml_Block_Report_Grid {
 
    public function __construct() {
        parent::__construct();
        $this->setId('mymoduleGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setSubReportSize(false);
    }
 
    protected function _prepareCollection() {
        parent::_prepareCollection();
		// this line indicates the model to use for get the data
        $this->getCollection()->initReport('mymodule/mymodule');
        return $this;
    }
	
	/* Prepare columns for the report */
    protected function _prepareColumns() {
	
        $this->addColumn('ordered_qty', array(
            'header'    =>Mage::helper('reports')->__('Total Completed Orders'),
            'index'    =>'ordered_qty',
			'align' 		=> 'left',
            'total'     =>'sum',					 // it's used to indicate that this field must be totalized at the end
            'type'      =>'number'
        ));
		
		$this->addColumn('total_qty_ordered', array(
            'header'    =>Mage::helper('reports')->__('Quantity Ordered'),
            'index'    =>'total_qty_ordered',
			'align' 		=> 'left',
            'total'     =>'sum',					 // it's used to indicate that this field must be totalized at the end
            'type'      =>'number'
        ));
		
		$this->addColumn('item_id', array(
            'header' => Mage::helper('mymodule')->__('Item ID'),
            'align' => 'left',
            'index' => 'item_id',
            'type'  => 'number',
           // 'total' => 'sum',
        ));
		
        $this->addColumn('name', array(
            'header' =>'Item Name',
            'index' => 'name',
			'align'  =>	'right',
			'width' => '200px',
            'type'  => 'text',
           // 'total' => 'sum',
        ));
		
		$this->addColumn('price_incl_tax', array(
            'header' => Mage::helper('mymodule')->__('Price+tax'),
            'align' => 'left',
            'index' => 'price_incl_tax',
			'width' => '200px',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
			'total' => 'sum',
        ));
		
		 
        $this->addExportType('*/*/exportCsv', Mage::helper('mymodule')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('mymodule')->__('XML'));
        return parent::_prepareColumns();
    }
 
	 // this function is executed when you click on the rows
    public function getRowUrl($row) {
        return false;
    }
 
	/* Get report within specified range */
    public function getReport($from, $to) {
	
        if ($from == '') {
            $from = $this->getFilter('report_from');
        }
        if ($to == '') {
            $to = $this->getFilter('report_to');
        }
        $totalObj = Mage::getModel('reports/totals');
		
        $totals = $totalObj->countTotals($this, $from, $to);
		
		//Set total
        $this->setTotals($totals);
		
		//add totals into grand total
        $this->addGrandTotals($totals);
		
		//get report from the collection
        return $this->getCollection()->getReport($from, $to);
    }
}

?>