<?php
	
	/* This is Block container */
class Mycompany_Mymodule_Block_Mymodule extends Mage_Core_Block_Template {
 
    public function _prepareLayout() {
        return parent::_prepareLayout();
    }
 
    public function getMymodule() {
        if (!$this->hasData('mymodule')) {
            $this->setData('mymodule', Mage::registry('mymodule'));
        }
        return $this->getData('mymodule');
    }
 
}
?>