<?php 
	/* 
		
		@By: Muhammad Ahsan Horani
		@Package: Update Cart Totals
		
		*/
		
class Order_Delivery_Model_Observer extends Varien_Event_observer {
	
	/* Hook to save shipping method */
	public function getInfo($observer)
	{
		// print_r($_POST); 
		if($_POST['date_select'])
		{
			Mage::getSingleton( 'customer/session' )->setData('date_select',true);
			
			if($_POST['orderdate'] != "" && !empty($_POST["orderdate"]))
			{
				$orderDate	=	$_POST["orderdate"];
				Mage::getSingleton( 'customer/session' )->setData('orderdate',$orderDate);
				
			}
			
			if($_POST["time_select"])
			{
				Mage::getSingleton( 'customer/session' )->setData('time_select',true);
				
				if($_POST["ordertime"])
				{
					$orderTime	=	$_POST['ordertime'];
					Mage::getSingleton( 'customer/session' )->setData('ordertime',$orderTime);
				}
			}

		}
	}
	
	/* Update all order Totals on Success page after order has been placed */
	public function updateTotal($observer)
	{
			$order = new Mage_Sales_Model_Order();
			$incrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
			$order->loadByIncrementId($incrementId);
			
			/* Getting Data from Customer Session */
			$date				=	Mage::getSingleton( 'customer/session' )->getData('orderdate');
			$time				=	Mage::getSingleton( 'customer/session' )->getData('ordertime');
			$date_selected	=	Mage::getSingleton( 'customer/session' )->getData('date_select');
			$time_selected	=	Mage::getSingleton( 'customer/session' )->getData('time_select');
			/* End */
			
			/* Get All  Order totals */
			$grandTotal						=	$order->getGrandTotal();
			$baseGrandTotal				=	$order->getBaseGrandTotal();
			// $subTotal							=	$order->getSubtotal();
			// $baseSubTotal					=	$order->getBaseSubtotal();
			
			
			//Check if Order Delivery Date Selected
			if($date_selected)
			{
				if($date)
				{
					$order->setDeliveryDate($date);	//Set Order Delivery Date into Order
					
					$timestamp = strtotime($date);	//	Format : mm/dd/yyyy
					$day = date('l', $timestamp);		//Get name of the Day by submitted date
					
					if($day=='Sunday')
					{	
						/* Update totals if selected day is Sunday. Add Rs.300 */
						// $order->setBaseSubtotal($baseSubTotal + 300);
						// $order->setSubtotal($subTotal + 300);
						$order->setBaseGrandTotal($baseGrandTotal + 300);
						$order->setGrandTotal($grandTotal + 300);
						$order->save(); 
					}
					
					/* Reload All totals */
					$grandTotal						=	$order->getGrandTotal();
					$baseGrandTotal				=	$order->getBaseGrandTotal();
					// $subTotal							=	$order->getSubtotal();
					// $baseSubTotal					=	$order->getBaseSubtotal();
					
					
					//Check if Time Stipulated Delivery
					if($time_selected)
					{	
						$order->setDeliveryTime($time);									//Set order delivery time into Order
						
						/* Update Totals if Time stipulated Delivery. Add Rs.300 */
						// $order->setBaseSubtotal($baseSubTotal + 300);
						// $order->setSubtotal($subTotal + 300);
						$order->setBaseGrandTotal($baseGrandTotal + 300);
						$order->setGrandTotal($grandTotal + 300);
						$order->save(); 
					}
					
					
					
					// echo $date; 
					// echo "\n";
					// echo $day;
					// echo "\n";
					/* echo $order->getGrandTotal();
					// echo "<pre>"; print_r($order->getData());
					Exit; */
					
				}//End if date
			
			}//End if date selected
	}
	
	/* Change QuoteTotals Before Order Placed */
	public function setTotals($observer)
	{		
		/* Getting Data from Customer Session */
			$date				=	Mage::getSingleton( 'customer/session' )->getData('orderdate');
			$time				=	Mage::getSingleton( 'customer/session' )->getData('ordertime');
			$date_selected	=	Mage::getSingleton( 'customer/session' )->getData('date_select');
			$time_selected	=	Mage::getSingleton( 'customer/session' )->getData('time_select');
			/* Getting Data from Customer Session */
			
			$checkout							=	Mage::getModel('checkout/cart');
			$quote								=	$checkout->getQuote();
			
			/* Get Quote Totals */
			$grandTotal						=	$quote->getGrandTotal();
			$baseGrandTotal				=	$quote->getBaseGrandTotal();
			$subTotal							=	$quote->getSubtotal();
			$baseSubTotal					=	$quote->getBaseSubtotal();
			$subTotalWithDiscount		=	$quote->getSubtotalWithDiscount();
			$baseSubTotalWithDiscount	=	$quote->getBaseSubtotalWithDiscount();
			/* End	-	Get  Quote Totals */
			
			
			
			//Check if Order Delivery Date is selected by User
			if($date_selected)
			{
				if($date)
				{
					$timestamp = strtotime($date);	//	Format : mm/dd/yyyy
					$day = date('l', $timestamp);		//Get name of the Day by submitted date
					
					if($day=='Sunday')
					{	
						/* Update totals if selected day is Sunday. Add Rs.300*/
						$quote->setSubtotal($subTotal + 300);
						$quote->setBaseSubtotal($baseSubTotal + 300);
						$quote->setSubtotalWithDiscount($subTotalWithDiscount + 300);
						$quote->setBaseSubtotalWithDiscount($baseSubTotalWithDiscount + 300);
						$quote->setBaseGrandTotal($baseGrandTotal + 300);
						$quote->setGrandTotal($grandTotal + 300);
						$quote->save(); 
					}
					
					/* Reload All totals */
					$grandTotal						=	$quote->getGrandTotal();
					$baseGrandTotal				=	$quote->getBaseGrandTotal();
					$subTotal							=	$quote->getSubtotal();
					$baseSubTotal					=	$quote->getBaseSubtotal();
					$subTotalWithDiscount		=	$quote->getSubtotalWithDiscount();
					$baseSubTotalWithDiscount	=	$quote->getBaseSubtotalWithDiscount();
					
					//Check if Time Stipulated Delivery
					if($time_selected)
					{	
						/* Update Totals if Time stipulate Delivery. Add Rs.300 */
						$quote->setSubtotal($subTotal + 300);
						$quote->setBaseSubtotal($baseSubTotal + 300);
						$quote->setSubtotalWithDiscount($subTotalWithDiscount + 300);
						$quote->setBaseSubtotalWithDiscount($baseSubTotalWithDiscount + 300);
						$quote->setBaseGrandTotal($baseGrandTotal + 300);
						$quote->setGrandTotal($grandTotal + 300);
						$quote->save(); 
					}
					
					
					
					// echo $date; 
					// echo "\n";
					// echo $day;
					// echo "\n";
					// echo $quote->getGrandTotal();
					// Exit;
					
				}//End if date
			
			}//End if date selected
			
	}//End function setTotals
	

	
	
}//End Class
	
	
	
?>