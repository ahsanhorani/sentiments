<?php
	
/* This is an Observer class for the event  core_block_abstract_prepare_layout_before */

class CustomGrid_Adminhtml_Model_Observer extends Varien_Event_Observer
{
	
    /*  This function appends column to any block */
    public function appendCustomColumn(Varien_Event_Observer $observer)
    {
		/* LOAD BLOCKS */
        $block = $observer->getBlock();
	
        if (!isset($block)) {
		
            return $this;
        }
		

		
		/* IDENTIFY SPECIFIC BLOCK */
        if ($block->getType() == 'adminhtml/sales_order_grid') {
		
            /* $block  = Mage_Adminhtml_Block_Customer_Grid */
			
			/* ADD COLUMN AFTER '  '   COLUMN*/
            $block->addColumnAfter('total_paid', array(
																			'header'    => 'Total Paid',
																			'type'      => 'currency',
																			'index'     => 'total_paid', 					//DB table column
																			'currency' => 'order_currency_code',
																		), 'grand_total');										//New column will be added after this column
																		
			
			/*  Index is the value that requests data from Database i.e it is the column name in table*/
			
        }
    }
}

?>