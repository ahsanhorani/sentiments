<?php
	
	class Mementia_CatalogSearch_Model_Query extends Mage_Core_Model_Abstract
	{
		/**
			* Event prefix
			*
			* @var string
		*/
		protected $_eventPrefix = 'catalogsearch_query';
		
		/**
			* Event object key name
			*
			* @var string
		*/
		protected $_eventObject = 'catalogsearch_query';
		
		const CACHE_TAG                     = 'SEARCH_QUERY';
		const XML_PATH_MIN_QUERY_LENGTH     = 'catalog/search/min_query_length';
		const XML_PATH_MAX_QUERY_LENGTH     = 'catalog/search/max_query_length';
		const XML_PATH_MAX_QUERY_WORDS      = 'catalog/search/max_query_words';
		
		/**
			* Init resource model
			*
		*/
		protected function _construct()
		{
			$this->_init('catalogsearch/query');
		}
		
		/**
			* Retrieve search collection
			*
			* @return Mage_CatalogSearch_Model_Resource_Search_Collection
		*/
		public function getSearchCollection()
		{
			return Mage::getResourceModel('catalogsearch/search_collection');
		}
		
		/**
			* Retrieve collection of search results
			*
			* @return Mage_Eav_Model_Entity_Collection_Abstract
		*/
		public function getResultCollection()
		{
			$collection = $this->getData('result_collection');
			if (is_null($collection)) {
				$collection = $this->getSearchCollection();
				
				$text = $this->getSynonymFor();
				if (!$text) {
					$text = $this->getQueryText();
				}
				
				$collection->addSearchFilter($text)
                ->addStoreFilter()
                ->addMinimalPrice()
                ->addTaxPercents();
				$this->setData('result_collection', $collection);
			}
			return $collection;
		}
		
		/**
		* Retrieve collection of suggest queries
		*
		* @return Mage_CatalogSearch_Model_Resource_Query_Collection
		*/
		public function getSuggestCollection()
		{
				$collection = $this->getData('suggest_collection');
				if (is_null($collection)) {
				$collection = Mage::getModel('catalog/product');
				Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection); //Filter disabled products
				$collection->getCollection()            
								->addAttributeToSelect('name')			//select name of product
								->addAttributeToFilter('name', array($this->getQueryText()))		//filter product by attribute
								->addExpressionAttributeToSelect('query_text', '{{name}}', 'name')		//assign alias   'query_text' 
								->addAttributeToSort('name', 'ASC')
								->setPageSize(5)
								->addStoreFilter($this->getStoreId())
				;
				/*
					* Filter out of disable, out of stock and invisible products
					*
					*
				*/
				Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
				Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
				Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($collection);
				$this->setData('suggest_collection', $collection);
			}
			return $collection;
		}
		
		/**
			* Load Query object by query string
			*
			* @param string $text
			* @return Mage_CatalogSearch_Model_Query
		*/
		public function loadByQuery($text)
		{
			$this->_getResource()->loadByQuery($this, $text);
			$this->_afterLoad();
			$this->setOrigData();
			return $this;
		}
		
		/**
			* Load Query object only by query text (skip 'synonym For')
			*
			* @param string $text
			* @return Mage_CatalogSearch_Model_Query
		*/
		public function loadByQueryText($text)
		{
			$this->_getResource()->loadByQueryText($this, $text);
			$this->_afterLoad();
			$this->setOrigData();
			return $this;
		}
		
		/**
			* Set Store Id
			*
			* @param int $storeId
			* @return Mage_CatalogSearch_Model_Query
		*/
		public function setStoreId($storeId)
		{
			$this->setData('store_id', $storeId);
		}
		
		/**
			* Retrieve store Id
			*
			* @return int
		*/
		public function getStoreId()
		{
			if (!$storeId = $this->getData('store_id')) {
				$storeId = Mage::app()->getStore()->getId();
			}
			return $storeId;
		}
		
		/**
			* Prepare save query for result
			*
			* @return Mage_CatalogSearch_Model_Query
		*/
		public function prepare()
		{
			if (!$this->getId()) {
				$this->setIsActive(0);
				$this->setIsProcessed(0);
				$this->save();
				$this->setIsActive(1);
			}
			
			return $this;
		}
		
		/**
			* Retrieve minimum query length
			*
			* @deprecated after 1.3.2.3 use getMinQueryLength() instead
			* @return int
		*/
		public function getMinQueryLenght()
		{
			return Mage::getStoreConfig(self::XML_PATH_MIN_QUERY_LENGTH, $this->getStoreId());
		}
		
		/**
			* Retrieve minimum query length
			*
			* @return int
		*/
		public function getMinQueryLength(){
			return $this->getMinQueryLenght();
		}
		
		/**
			* Retrieve maximum query length
			*
			* @deprecated after 1.3.2.3 use getMaxQueryLength() instead
			* @return int
		*/
		public function getMaxQueryLenght()
		{
			return 0;
		}
		
		/**
			* Retrieve maximum query length
			*
			* @return int
		*/
		public function getMaxQueryLength()
		{
			return Mage::getStoreConfig(self::XML_PATH_MAX_QUERY_LENGTH, $this->getStoreId());
		}
		
		/**
			* Retrieve maximum query words for like search
			*
			* @return int
		*/
		public function getMaxQueryWords()
		{
			return Mage::getStoreConfig(self::XML_PATH_MAX_QUERY_WORDS, $this->getStoreId());
		}
	}
