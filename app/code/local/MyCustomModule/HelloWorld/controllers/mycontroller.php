<?php
class MyCustomModule_HelloWorld_mycontroller {
	
public function indexAction() {
$customer = Mage::getModel('customer/customer');
//$customer  = new Mage_Customer_Model_Customer();
 
 
$password = '123456';
$email = 'ajzele@mymail.com';
 
$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
$customer->loadByEmail($email);
//Zend_Debug::dump($customer->debug()); exit;
 
if(!$customer->getId()) {
 
	$customer->setEmail($email);
	$customer->setFirstname('Johnny');
	$customer->setLastname('Doels');
	$customer->setPassword($password);
}
 
try {
	
	$customer->save();
	$customer->setConfirmation(null);
	$customer->save();
 
	//Make a "login" of new customer
	Mage::getSingleton('customer/session')->loginById($customer->getId());
}

catch (Exception $ex) {
	//Zend_Debug::dump($ex->getMessage());
}	
//Build billing and shipping address for customer, for checkout
$_custom_address = array (
	'firstname' => 'Branko',
	'lastname' => 'Ajzele',
	'street' => array (
		'0' => 'Sample address part1',
		'1' => 'Sample address part2',
	),
 
	'city' => 'Osijek',
	'region_id' => '',
	'region' => '',
	'postcode' => '31000',
	'country_id' => 'HR', /* Croatia */
	'telephone' => '0038531555444',
);
 
$customAddress = Mage::getModel('customer/address')
//$customAddress = new Mage_Customer_Model_Address();
$customAddress->setData($_custom_address)
			->setCustomerId($customer->getId())
			->setIsDefaultBilling('1')
			->setIsDefaultShipping('1')
			->setSaveInAddressBook('1');
 
try {
	$customAddress->save();
}
catch (Exception $ex) {
	//Zend_Debug::dump($ex->getMessage());
}
 
Mage::getSingleton('checkout/session')->getQuote()->setBillingAddress(Mage::getSingleton('sales/quote_address')->importCustomerAddress($customAddress));
/* If we wish to load some product by some attribute value diferent then id */
$product = Mage::getModel('catalog/product')->getCollection()
	/* Remember, you can load/find product via any attribute, better if its attribute with unique value */
	->addAttributeToFilter('sku', 'some-sku-value')
	->addAttributeToSelect('*')
	->getFirstItem();
 
/* Do a full product load, otherwise you might get some errors related to stock item */
$product->load($product->getId());
 
$cart = Mage::getSingleton('checkout/cart');
 
/* We want to add only the product/products for this user and do so programmatically, so lets clear cart before we start adding the products into it */
$cart->truncate();
$cart->save();
$cart->getItems()->clear()->save();			
 
try {
	/* Add product with custom oprion? =>  some-custom-option-id-here: value to be read from database or assigned manually, hardcoded? Just example*/
	//$cart->addProduct($product, array('options'=> array('some-custom-option-id-here' => 'Some value goes here');
	$cart->save();				
}
catch (Exception $ex) {
	echo $ex->getMessage();
}
 
unset($product);
$storeId = Mage::app()->getStore()->getId();
 
$checkout = Mage::getSingleton('checkout/type_onepage');
 
$checkout->initCheckout();
 
$checkout->saveCheckoutMethod('register');
 
$checkout->saveShippingMethod('flatrate_flatrate');
 
$checkout->savePayment(array('method'=>'checkmo'));
 
try {
	$checkout->saveOrder();
}
catch (Exception $ex) {
	//echo $ex->getMessage();
}			
 
/* Clear the cart */
$cart->truncate();
$cart->save();
$cart->getItems()->clear()->save();		
 
/* Logout the customer you created */
Mage::getSingleton('customer/session')->logout();
}
}
?>