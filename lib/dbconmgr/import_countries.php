<?php
	include "./dbconmgr.php";
	
	$db = new dbconmgr();
	$db->connect();
	
	//Read the CSV file
	$fh = fopen("countries_list.csv", "r");
	$count = 0;
	
	while( ($line = fgetcsv($fh,1000,",", '"')) !== FALSE ) {	
		$countryname = mysql_escape_string(trim($line[0]));
		$countryabbr = mysql_escape_string(trim($line[1]));
		$zone     	 = mysql_escape_string(trim($line[2]));
		
		echo "$countryname - $countryabbr - $zone\n";
		
		$db->addcountry($countryname, $countryabbr, $zone);
	}
	
	echo "\n\nDONE\n\n";
?>