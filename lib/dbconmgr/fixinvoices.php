<?php
	include "db_creds.php";
	include "dbconmgr.php";

	include "../../app/Mage.php";
	
	Mage::app('admin');
	
	$db = new dbconmgr();
	$db->connect();	
	
	function update_order_status($entity_id) {
		$sql = "update sales_flat_order set status='complete' WHERE entity_id=$entity_id LIMIT 1";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		$connection->query($sql);
		
		$sql = "update sales_flat_order_grid set status='complete' WHERE entity_id=$entity_id LIMIT 1";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		$connection->query($sql);
	}
	
	$entity_ids = array (584,648,756,762,857,898,922,933,941,942,948,968,978,980,991,994,1004,1010,1012,1013,1022,1025,1033,1034,1056,1059,1060,1070,1075,1076,1121,1123,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1141,1142,1143,1146,1147,1148,1150,1151,1153,1154,1162,1163,1164,1165,1166,1167,1168,1169,1208,1211,1212,1213,1214,1215,1216,1217,1218,1222,1224,1225,1226,1269,1270,1271,1274,1275,1276,1280,1281,1282,1283,1284,1285,1286,1287,1288,1293,1298,1299,1307,1322,1323,1324,1325,1326,1327);
	
	for($i=0; $i < count($entity_ids); $i++) {
		$entity_id  = $entity_ids[$i];
			
		echo "$entity_id \n";
				
		try {
			$orderObj=Mage::getModel('sales/order')->load($entity_id);
			
			$convertOrderObj=Mage::getSingleton('sales/convert_order');
			$invoiceObj=$convertOrderObj->toInvoice($orderObj);
			
			foreach ($orderObj->getAllItems() as $item) {
				$invoiceItem = $convertOrderObj->itemToInvoiceItem($item);
				$row = array();
				
				// $productname 	 = $item->getName();
				// $productvendorid = $item->getData('udropship_vendor');
				// $productvendor   = $db->getVendor($productvendorid);
				
				// $row["productname"] = $item->getName();
				// $row["vendor"]      = $productvendor;
				// $row["qty"]		    = $item->getData('qty_ordered');
				// $row["weight"]		= $item->getWeight();
				// $row["ordernum"]	= $orderObj->getIncrementId();
				
				// $products[] = $row;

				if ($item->getParentItem()) {
					$invoiceItem->setParentItem($invoiceObj->getItemById($item->getParentItem()->getId()));
				}
				$invoiceItem->setQty($item->getQtyToInvoice());
				$invoiceObj->addItem($invoiceItem);
			}

			$invoiceObj->collectTotals();
			$invoiceObj->register();
			
			$orderPaymentObj=$orderObj->getPayment();
			
			$orderPaymentObj->pay($invoiceObj);
			$invoiceObj->getOrder()->setIsInProcess(true);
			$transactionObj = Mage::getModel('core/resource_transaction');
			$transactionObj->addObject($invoiceObj);
			$transactionObj->addObject($invoiceObj->getOrder());
			$transactionObj->save();

			$invoiceObj->save();
			$invoiceId=$invoiceObj->getId();
			
			//fix the invoice gen date:
			$db->setInvoiceDate($entity_id, '2013-07-12');
			update_order_status($entity_id);
		}
		catch(Exception $e) { echo "$entity_id : " . $e->getMessage(); }
	}
	

?>