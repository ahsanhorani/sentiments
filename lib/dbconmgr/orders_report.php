<?php
	include "db_creds.php";
	include "dbconmgr.php";
	include "/var/www/tcsconnect/app/Mage.php";
	
	Mage::app('admin');
	
	$db = new dbconmgr();
	$db->connect();
	
	$fromdate = "2012-03-23";
	$todate   = "2012-10-09";
	
	$orders   = $db->getCompleteOrdersForPLR($fromdate, $todate);		
	$filename = "orderreport_" . date("d_m_Y") . ".csv";
	
	$fh = fopen($filename, "w");
	
	
	$header .= "S.No, Order #, Customer name, Area Location, Method, Received at, Total RP\n";
	
	for($i=0; $i < count($orders); $i++) {
		$sno = $i+1;
		$entityid = $orders[$i]['entity_id'];
		$incrementid = $orders[$i]['increment_id'];
		$orderObj = Mage::getModel('sales/order')->load($entityid);
		$api 	  = $orders[$i]['additionalpaymentinfo'];
		$method   = $db->getPaymentmethod($entityid);
		$method   = $method['method'];		
		$paymethod 	= $db->getRealPaymentMethodName($method);
		$totalrp  = $db->getTotals($entityid);
		$arealocation = "";
		
		$orderObj=Mage::getModel('sales/order')->load($entityid);
		
		$customername = $orderObj->getCustomerName();
		
		$arealocation = $db->getUBDBDB($method, $api);
		
		if( $arealocation == "UBD" ) {
			$api = trim($api);
			$shipping = $orderObj->getShippingAddress();			
			$shippingcity = $shipping->getCity();
			
			$billing = $orderObj->getBillingAddress();
			$billingcity = $billing->getCity();			
			
			if( $api == "Door-step" || $method == "cod") {
				$arealocation = $db->getCityAreaId($shippingcity);
				$arealocation = $db->getPMAreaCode($arealocation);
			}
			
			if( $api == "CC Offline - Doorstep" ) {
				$arealocation = $db->getCityAreaId($billingcity);
				$arealocation = $db->getPMAreaCode($arealocation);
			}
			
			if( $api == "Door-step" && $method == "checkatbank") {
				$arealocation = $db->getCityAreaId($billingcity);
				$arealocation = $db->getPMAreaCode($arealocation);
			}
			
			if( $api == "Express Center"  && $method == "cash" ) {
				$arealocation = $db->getCityAreaId($billingcity);
				$arealocation = $db->getPMAreaCode($arealocation);
			}
			
			if( $method == "creditcardoffline" && $api == "CC Offline - Express Center" ) {
				$arealocation = $db->getCityAreaId($billingcity);
				$arealocation = $db->getPMAreaCode($arealocation);
			}
			
		}
		
		// $transaction = $db->getTransactionByOrderID($incrementid);
		// if( $transaction['eccode'] <> 0 ) {
			// $ecinfo = $db->getECInformationByEcID($transaction['eccode']);
			// $arealocation = $ecinfo[0]['ecstation'];
		// }	
		
		// $api = trim($api);
		// if( $api == "Door-step" || $api = "CC Offline - Doorstep" ) {
			// $arealocation = $db->getCityAreaId($shippingcity);
		// }
		
		// if( $method == "cod" ) {
		
		// }
			
		// if( strlen($transaction['pm_area']) > 0 ) { $arealocation = $transaction['pm_area']; }
		
		$line = "$sno, $incrementid, $customername, $arealocation, $paymethod, $api, $totalrp\n";
		
		fwrite($fh, $line);
	}
	
	fclose($fh);
	
	echo "done";
?>