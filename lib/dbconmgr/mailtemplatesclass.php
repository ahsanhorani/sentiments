<?php

	
	include "db_creds.php";
	
	class mailtemplatesclass {
		var $DB_CON=null;
		
		
		//Returns database resource/null otherwise
		public function connect() {
			global $DBSERVER, $DBUSER, $DBPWD, $DBNAME;
			
			$link = mysql_connect($DBSERVER, $DBUSER, $DBPWD);
			if (!$link) {
				echo 'Could not connect: ' . mysql_error();
				return;
			}
			//Select the database
			mysql_select_db( $DBNAME, $link) or die("Database doesn't exist: " . mysql_error() );
			$this->DB_CON=$link;
		}
		
		//public function which executes the query
		public function runquery($Sql) {
			if( $this->DB_CON != null ) {
				$result = mysql_query($Sql, $this->DB_CON);
				
				if( !$result) { 
					//echo 'Query is : ' . mysql_error();
					return null; 
				}
				return $result;
			}
		}
		
		public function create_customerpaymentemail($ordernumber, $customername, $payment_method) {
			
			$body = "<table width='100%' cellpadding='0' cellspacing='0' style='font:12px Arial, Helvetica, sans-serif;'><tr><td>Dear $customername<br /><br />".
					"We'd like to confirm that the payment against order number $ordernumber has been received through $payment_method.<br /><br />You will receive an update when your order has been picked from the vendor.<br /><br />We thank you for choosing TCS Connect.<br /><br />Have a good day.<br /><br />Regards,
TCS Connect<br /><br />Call: 111-192-986<br />Email:	cs@tcsconnect.com<br /><br />" .
				"<font size='-1'>Have a question or query? Have a look at our FAQs to get the answers.<br />Have a feedback or complaint? Let us know here <a href='http://tcsconnect.com/contact-us'>Contact Us</a></td></tr></table>";
			return $body;	
		}
		public function getPaymentMethod($order_id) {
			$sql = "SELECT method FROM sales_flat_order_payment where parent_id='$order_id' LIMIT 1";
			$result = $this->runquery($sql);
			$ret = array();
			if( mysql_num_rows($result) > 0 ) { 
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;	
		}
	
		public function getcustomerdemo($increment_id) {
			$sql = "SELECT customer_firstname, customer_lastname, customer_email FROM sales_flat_order WHERE increment_id='$increment_id'";
			$result = $this->runquery($sql);
			$ret = array();
			if( mysql_num_rows($result) > 0 ) { 
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;	
		}
		
		public function getOrderDetails($increment_id) {
			$sql = "select entity_id, increment_id, total_due, billing_address_id, base_discount_amount, shipping_address_id, customer_firstname, customer_lastname, additionalpaymentinfo,order_currency_code FROM sales_flat_order o where increment_id='$increment_id';";
			$result = $this->runquery($sql);
			$ret = array();
			if( mysql_num_rows($result) > 0 ) { 
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			
			return $ret;	
		}
		
		public function getOrderAddress($address_id) {
			$sql = "SELECT * FROM sales_flat_order_address WHERE entity_id=$address_id";
			$result = $this->runquery($sql);
			$ret = array();
			if( mysql_num_rows($result) > 0 ) { 
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;
		}
		
		//sales_flat_order.entity_id = $ordernumber
		public function getOrderItems($ordernumber) {
			$sql = "select name,sku,qty_ordered,price,base_price,v.vendor_name FROM sales_flat_order_item i
INNER JOIN udropship_vendor v on v.vendor_id=i.udropship_vendor where order_id=$ordernumber";
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 ) { 
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getOrderShipping($entity_id) {
			$sql = "SELECT * FROM sales_flat_order WHERE entity_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 ) { 
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}
		
		public function checkCustomerCreditWasUsed($order_id) {
			$sql = "SELECT * FROM customercredit_credit_log where order_id=$order_id";
			$row = array();
			$result = $this->runquery($sql);
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}
		
		/**
		 * Muzammil Code , to sent exptected delivery date in email.
		 * to custsomer on order placement
		 */
		 public function get_deliverydate($increment_id)
		 {
		 	
				$order					=	Mage::getModel('sales/order')->loadByIncrementId($increment_id);
				$order_delivery_date    =	$order['delivery_date'];
                $delivery_date     =	date('D d, M  Y',strtotime($order_delivery_date));
			 	return $delivery_date;
		 }
		
		
		public function create_customerneworderemail($increment_id) {

			$order			=	Mage::getModel('sales/order')->loadByIncrementId($increment_id);
			$billing_address 	= $order->getBillingAddress();
			$shipping_address 	= $order->getShippingAddress();
			$paymentmethod = $order->getPayment()->getMethod();

			// zend_Debug::dump($paymentmethod, $label = "paymentmethod", $echo = true);

			$api 		   = $order->getAdditionalpaymentinfo();
			$currencycode  = "Rs.";
			$exptected_delivery_date	=	$this->get_deliverydate($increment_id);

			if( $order->getOrderCurrencyCode() == 'USD' ) {
				$currencycode = "$";
			}
			else if( $order->getOrderCurrencyCode() == 'AED' ) {
				$currencycode = "AED";
			}

			// file_put_contents("/tmp/create_customerneworderemail.log", date("d-m-Y H:i:s") . "\n" . print_r($order, true) . "\n" . print_r($shipping, true) .  "\n-----------------\n", FILE_APPEND);
			
			$countryModel = Mage::getModel('directory/country')->loadByCode( $billing_address->getCountryId() );
			$billingcountry = $countryModel->getName();

			$countryModel = Mage::getModel('directory/country')->loadByCode( $shipping_address->getCountryId() );
			$shippingcountry = $countryModel->getName();

			$billing_street 	= $billing_address->getStreet();
			$bill_str 			= $billing_street[0];

			$shipping_street 	= $shipping_address->getStreet();
			$ship_str 			= $shipping_street[0];
			
			$customer_billing_address		= 		$order->getBillingAddress()->getFormated(true);
	 		$customer_shipping_address		= 		$order->getShippingAddress()->getFormated(true);
			

			$body = "<table width='100%' cellpadding='0' cellspacing='0' style='font:12px Arial, Helvetica, sans-serif;'><tr><td>";
			$body  .= "Hello " . $order->getCustomerFirstname() . " " . $order->getCustomerLastname() . "<br /><br />";
			$body .= "Thank you for choosing TCS Connect. Here are the details of your order:<br /><br />";
			
			// $body .= "Thank you for choosing TCS Connect. Here are the details of your order:<br /><br />";
			$body .= "<B>Billing Address:</B><br />" . $customer_billing_address;
//			$body .= "<br />" . $bill_str . ", " . $billing_address->getCity() . ", $billingcountry<br /><br />";
			$body .= "<br /><br /><B>Shipping Address:</B><br />" . $customer_shipping_address;
//			$body .= "<br />"  . $ship_str . ", " . $shipping_address->getCity() .", $shippingcountry<br /><br />";
			$body .= "<br /><br /><B>Expected Delivery Date :</B> <br />$exptected_delivery_date<br /><br />";
			$body .= "<B>Order Details</B>: ";
			
			$orddetails = "<table width='100%' cellpadding='0' cellspacing='0' border='1'>" .
					   "<tr>".
							"<td width='70px'><B>S.NO.</B></td>" .
							"<td width='350px'><B>Product Name</B></td> ".
							"<td width='150px'><B>SKU</B></td> " .
							"<td width='70px'><B>Qty</B></td> " .
							"<td><B>Price</B></td></tr>";
			$rows = "";



			$sno = 0;
			$orderinsurance = 0;
			foreach($order->getAllItems() as $item) {
				
				$item_id = $item->getId();
				//IS CONFIGURABLE OR SIMPLE
				$ispartofconf = $this->ispartofconfigurableitem($item_id);
				
				if( $ispartofconf > 0 ) {
					continue;
				}			
				
			    
				// zend_Debug::dump($item->getData(), $label = "items", $echo = true);

			    $sno++;
			    $item->getProductId();
			    $rows .= "<tr><td>" . $sno . "</td>" .
					   "<td>" . $item->getName() . "</td>" .
					   "<td>" . $item->getSku()  . "</td>" .
					   "<td>" . intval( $item->getQtyOrdered() ) . "</td>" .
					   "<td>$currencycode " . number_format( $item->getPrice() * $item->getQtyOrdered() ,2) . "</td></tr>";

				$orderinsurance += $item->getBaseInsurance();
			}
			
			$rows .= "</table>";
			
			$shippingamount = $this->getOrderShipping($order['entity_id']);
			$vascharges = $order->getVasCharges();
			
			$discount_amount = $order->getBaseDiscountAmount();

			if( $order->getOrderCurrentCode() <> "PKR" ) {
				$rate  = Mage::helper('directory')->currencyConvert(1, "PKR", "USD");
				$vascharges = $vascharges * $rate;
				$discount_amount = $discount_amount * $rate;				
			}
			
			$discount_amount = number_format($discount_amount, 2);			
			$vascharges 	 = number_format($vascharges,2);			
			// $orderinsurance  = $this->getOrderInsurance($order['entity_id']); 	
			
			$rows .= "<br /><br /><table width='100%' cellpadding='0' cellspacing='0' border='0'>" .
					  "<tr><td align='right'><B>Shipping &amp; Handling</B>: $currencycode ". number_format($order->getShippingAmount(),2) . 
					  "/-</td></tr>";

			// zend_Debug::dump($order->getShippingAmount(), $label = "billing information", $echo = true);
					  
			$rows .="<tr><td align='right'><B>VAS Charge</B>: $currencycode " . $vascharges . "/-</td></tr>";

			//SANA SAFINAZ SHIPPING AMOUNT
			 $ExternalLibPath=Mage::getBaseDir('lib') . DS . 'dbconmgr' . DS .'dbconmgr.php';
			 require_once ($ExternalLibPath);
			 
			 $db = new dbconmgr();
			 $db->connect();
			 
		    $quoteId 			  = $order->getQuoteId();
			$incrementId 		  =	$order->getIncrementId();
			
			$rows .="<tr><td align='right'><B>Discount amount</B>: $currencycode " . $discount_amount . "/-</td></tr>";
			
			if( $orderinsurance > 0 ) {
				$rows .="<tr><td align='right'><B>Insurance amount</B>: $currencycode " . number_format($orderinsurance,2) . "/-</td></tr>";
			}
			
			$rows .="<tr><td align='right'><B>Grand Total</B>: $currencycode " . number_format($order->getGrandTotal(),2) . "/-</td></tr>";
			
			
			
			$rows .= "</table>";
			
			$instructions = "";
			
			if( $api == "Bank Deposit- (Allied Bank Limited)" || $api == "Bank Deposit - (Allied Bank Limited)") {
				$instructions = $this->getABLBankInst();
			}
			else if( $api == "CITI Net-Banking" ) {
				$instructions = $this->getCitiNetbanking();
			}
			else if( $api == "Door-step" || $api == " Door-step" || $api == "CC Offline - Doorstep") {
				$instructions = $this->getDoorstep();
			}
			else if( $api == "Express Center" || $api == "CC Offline - Express Center" ) {
				$instructions = $this->getExpresscenter();
			}
			else if( $api == "Bank Deposit - (Habib Bank Limited)" || $api == "Bank Deposit- (Habib Bank Limited)") {
				$instructions = $this->getHblbanking();
			}
			else if( $api == "HBL Net-Banking" ) {
				$instructions = $this->getHblNB();
			}
			else if( $api == "ABL Net-Banking") {
				$instructions = $this->getABLNetbanking();
			}
			else if( $api == "Bank Deposit- (United Bank Limited)" || $api == "Bank Deposit - (United Bank Limited)" ) {
				$instructions = $this->getUBLBanking();
			}
			else if( $api == "UBL Net-Banking" ) {
				$instructions = $this->getUBLNB();
			}
			
			if( $paymentmethod == "easypaisa") {
				$instructions = $this->getEasypaisa();
			}
			
			if( $paymentmethod == "cod" ) {
				$instructions = "<br /><span>Payment will be collected from you on delivery.</span><br />";
			}
			
			//get UBL Disclaimer Link
			$get_ubl_discalaimer_link_for_email	= $this->get_ubl_discalaimer_link_for_email($increment_id);
			
			$footer = "<br />We look forward to hearing from you and serving you better.<br /><br /> Have a good day.<br /><br />Best Regards, <br />CS @ TCS Connect<br /><font size='-1'>Serving you better, everyday!</I></FONT><br /><br />Call: 111-192-986<br />Email:	cs@tcsconnect.com<br /><br />" .
				"<font size='-1'>Have a question or query? Have a look at our <a href='http://tcsconnect.com/faq/'>FAQs</a> to get the answers.<br />Have a feedback or complaint? Let us know <a href='http://tcsconnect.com/contact-us'>here</a><br />Review our terms and conditions <a href='http://tcsconnect.com/terms-of-use/'>here</a> $get_ubl_discalaimer_link_for_email</td></tr></table>";

			
				
			return $body . $orddetails . $rows . $instructions . $footer;
			
		}
		
		public function getUBLNB() {
			$ret = "<br /><table width='100%' cellpadding='0' cellspacing='0'><tr><td>Next,  Login to the <a href='https://www.ubldirect.com/Corporate/ebank.aspx'>UBL  NetBanking Website</a></td></tr><tr><td>Click  on &lsquo;My Payments&rsquo;, scroll down to Online Shopping</td></tr><tr><td>Under  TCS Connect Click on &lsquo;Pay&rsquo; and enter your TCS Order Number in &lsquo;Reference  Number&rsquo;</td></tr><tr><td>Click  &lsquo;Pay&rsquo;</td></tr><tr><td>Enter  your PIN and then click &lsquo;Confirm&rsquo;</td></tr></table>";
			
			return $ret;
		}
		
		public function getUBLBanking() {
			$ret = "<br /><span>For payment instructions <a href='https://tcsconnect.com/media/paymentinstructions/ubl_bankinstructions.html'>Click Here</a></span>";
			
			return $ret;
		}
		
		public function getHblNB() {
			$ret = "<br /><span>For payment instructions <a href='https://tcsconnect.com/media/paymentinstructions/hbl_internetBanking.html'>Click Here</a></span>";
			
			return $ret;
		}
		
		public function getHblbanking() {
			$ret = "<br /><span>For payment instructions <a href='https://tcsconnect.com/media/paymentinstructions/hbl_bankinstructions.html'>Click Here</a></span>";
			return $ret;
		}
		
		public function getExpresscenter() {
			$ret = "<br /><span>For payment instructions <a href='https://tcsconnect.com/media/paymentinstructions/express_center_Instructions.html'>Click Here</a></span>";
			
			return $ret;
		}
		
		public function getEasypaisa() {
			$ret = "<br /><span>For payment instructions <a href='https://tcsconnect.com/media/paymentinstructions/easypaisa_Instructions.html'>Click Here</a></span>";
			
			return $ret;
		}
		
		public function getDoorstep() {
			$ret = "<br /><span>For payment instructions <a href='https://tcsconnect.com/media/paymentinstructions/doorstep_Instructions.html'>Click Here</a></span>";
			
			return $ret;
		}	
		
		
		public function getCitiNetbanking() {
			$ret = "<br /><span>For payment instructions <a href='https://tcsconnect.com/media/paymentinstructions/citi_internetBanking.html'>Click Here</a></span>";	
			return $ret;
		}
		
		
		
		public function getABLNetbanking() {
				$ret = "<br /><span>For payment instructions <a href='https://tcsconnect.com/media/paymentinstructions/abl_internetBanking.html'>Click Here</a></span>";			
			return $ret;
		}
		
		
		public function getABLBankInst() {
			$ret = "<br /><span>For payment instructions <a href='https://tcsconnect.com/media/paymentinstructions/abl_bankinstructions.html'>Click Here</a></span>";	
			return $ret;
		}
		
		public function getOrderVAS($entity_id) {
			$sql = "SELECT vas_charges FROM sales_flat_order where entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			$ret = "0.00";
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['vas_charges'];
			}
			return $ret;
		}
		
		
		
		public function create_orderintimationemail($increment_id) {
			$order = $this->getOrderDetails($increment_id);
			$orderitems = $this->getOrderItems($order['entity_id']);
			$billing = $this->getOrderAddress($order['billing_address_id']);
			$shipping = $this->getOrderAddress($order['shipping_address_id']);
			$paymentmethod = $this->getPaymentMethod($order['entity_id']);
			$paymentmethod = $paymentmethod['method'];
			
			// Check Customer is loggedin or not
			//if(Mage::getSingleton('customer/session')->isLoggedIn()){
				  // Get group Id
				  $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
				  
				  //Get customer Group Id
				  $group = Mage::getModel('customer/group')->load($groupId);
				  
				  //Get customer Group name
				  $customer_group_name		=		$group->getCustomerGroupCode();
			//}
			
			$countryModel = Mage::getModel('directory/country')->loadByCode($billing['country_id']);
			$billingcountry = $countryModel->getName();
			
			$countryModel = Mage::getModel('directory/country')->loadByCode($shipping['country_id']);
			$shippingcountry = $countryModel->getName();
			
			$body = "Team TCS Connect,<br /><br />New Order has been placed on TCS Connect.<br /><br />Order Details<br /><br />" .
					"<B>Customer Group : </B>" . " " . $customer_group_name . "<br />". 
					$order['customer_firstname'] . " " . $order['customer_lastname'] . "<br />" .
					"<B>Billing Address:</B><br />" . $billing['firstname'] . " " . $billing['lastname'] .
					"<br />" . $billing['street'] . ", " . $billing['city'] . ", $billingcountry<br />" .
					"<B>Telephone:</B> " . $billing['telephone'] . "<br /><br />" .
					"<B>Shipping Address:</B><br />" . $shipping['firstname'] . " " . $shipping['lastname'] .
					"<br />"  . $shipping['street'] . ", " . $shipping['city'] .", $shippingcountry<br />" .
					"<B>Telephone:</B> " . $shipping['telephone'] . "<br /><br />" .
					"<B>Order Details</B>: ";
			
			$orddetails = "<table width='100%' cellpadding='0' cellspacing='0' border='1'>" .
					   "<tr>".
							"<td width='70px'><B>S.NO.</B></td>" .
							"<td width='350px'><B>Product Name</B></td> ".
							"<td width='150px'><B>SKU</B></td> " .
							"<td width='70px'><B>Qty</B></td> " .
							"<td><B>Price</B></td></tr>";
			$rows = "";
			$entity_id = $order['entity_id'];
			$ccode = $this->getOrderCurrencyCode($entity_id);
			
			for($i=0; $i < count($orderitems); $i++) {
				
				if( $ccode <> "PKR" )
					$price = $orderitems[$i]['base_price'];
				else
					$price = $orderitems[$i]['price'];
				
				$sno = $i+1;
				$rows .= "<tr><td>" . $sno . "</td>" .
					   "<td>" . $orderitems[$i]['name'] . "</td>" .
					   "<td>" . $orderitems[$i]['sku'] . "</td>" .
					   "<td>" . intval($orderitems[$i]['qty_ordered']) . "</td>" .
					   "<td>" . number_format($price,2) . "</td></tr>";
			}
			
			$shippingamount = $this->getOrderShipping($order['entity_id']);
			
			if( $ccode <> "PKR" ) {
				$shipping = $shippingamount['base_shipping_amount'];
				$grandtotal = $shippingamount['base_grand_total'];
			}
			else {
				$shipping = $shippingamount['shipping_amount'];
				$grandtotal = $shippingamount['grand_total'];
			}
			
			$row = array();
			$row = $this->checkCustomerCreditWasUsed($order['entity_id']);
			
			$orderinsurance = $this->get_TotalInsurance($order['entity_id']);
			
			$vascharges = $this->getOrderVAS($order['entity_id']);
			//$vascharges = number_format($vascharges,2);
			
			if( count( $row ) > 0 ) {
				$icline = number_format($row['value_change'],2);
			} else { $icline = "0.00"; }	
			
			$rows .="<tr><td colspan='5' align='right'><B>Shipping &amp; Handling</B>: " . number_format($shipping,2) . "</td></tr>";
			$rows .="<tr><td colspan='5' align='right'><B>Customer Credit</B>: " . $icline . "</td></tr>";
			
			if( $orderinsurance > 0 ) {
				$rows .="<tr><td colspan='5' align='right'><B>Insurance</B>: " . number_format($orderinsurance,2) . "</td></tr>";
			}
			
			$rows .="<tr><td colspan='5' align='right'><B>VAS Charge</B>: " . $vascharges . "</td></tr>";
			
			$rows .="<tr><td colspan='5' align='right'><B>Grand Total</B>: " . number_format($grandtotal,2) . "</td></tr>";
			
			$rows .= "</table>";
			
			$api = "";
			$api = $order['additionalpaymentinfo'];
			if( strlen($api) > 0 ) { $api = " - " . $api;}
			
			$paymentoption = "<br />Payment Option Selected:  $paymentmethod $api";
			$footer = "<br /><br /><I>THIS IS A SYSTEM GENERATED E-MAIL, PLEASE DO NOT RESPOND TO THE E-MAIL ADDRESS SPECIFIED ABOVE.</I>";
			
			$body .= $orddetails . $rows . $paymentoption . $footer;
			
			return $body ;
		}
		

		public function create_adminorderintimationemail($increment_id) {
			
			try {
			$order			=	Mage::getModel('sales/order')->loadByIncrementId($increment_id);
			$billing_address 	= $order->getBillingAddress();
			$shipping_address 	= $order->getShippingAddress();
			$paymentmethod = $order->getPayment()->getMethod();
			// $paymentmethod = $order->getPayment()->getData();

			// zend_Debug::dump($paymentmethod, $label = "yahoo - paymentmethod", $echo = true);exit;
			file_put_contents("/tmp/orderintimation_template.log", print_r($billing_address->getData(),true)."\n\n", FILE_APPEND );
			
			$countryModel = Mage::getModel('directory/country')->loadByCode( $billing_address->getCountryId() );
			$billingcountry = $countryModel->getName();
			
			$countryModel = Mage::getModel('directory/country')->loadByCode( $shipping_address->getCountryId() );
			$shippingcountry = $countryModel->getName();

			$billing_street 	= $billing_address->getStreet();
			$bill_str 			= $billing_street[0];

			$shipping_street 	= $shipping_address->getStreet();
			$ship_str 			= $shipping_street[0];
			
			$body = "Team TCS Connect,<br /><br />New Order has been placed on TCS Connect.<br /><br />Order Details<br /><br />" .
					$order->getCustomerFirstname() . " " . $order->getCustomerLastname() . "<br />" .
					"<B>Billing Address:</B><br />" . $billing_address->getFirstname() . " " . $billing_address->getLastname() .
					"<br />" . $bill_str . ", " . $billing_address->getCity() . ", $billingcountry<br />" .
					"<B>Telephone:</B> " . $billing_address->getTelephone() . "<br /><br />" .
					"<B>Shipping Address:</B><br />" . $shipping_address->getFirstname() . " " . $shipping_address->getLastname() .
					"<br />"  . $ship_str . ", " . $shipping_address->getCity() .", $shippingcountry<br />" .
					"<B>Telephone:</B> " . $shipping_address->getTelephone() . "<br /><br />" .
					"<B>Order Details</B>: ";
			
			$orddetails = "<table width='100%' cellpadding='0' cellspacing='0' border='1'>" .
					   "<tr>".
							"<td width='70px'><B>S.NO.</B></td>" .
							"<td width='350px'><B>Product Name</B></td> ".
							"<td width='150px'><B>SKU</B></td> " .
							"<td width='70px'><B>Qty</B></td> " .
							"<td><B>Price</B></td></tr>";
			$rows = "";
			$entity_id = $order->getId();
			$ccode = $order->getOrderCurrencyCode();

			$sno = 0;
			$orderinsurance = 0;
			foreach($order->getAllItems() as $item) {
			    
				// zend_Debug::dump($item->getData(), $label = "items", $echo = true);

			    $sno++;
			    $item->getProductId();
			    $rows .= "<tr><td>" . $sno . "</td>" .
					   "<td>" . $item->getName() . "</td>" .
					   "<td>" . $item->getSku()  . "</td>" .
					   "<td>" . intval( $item->getQtyOrdered() ) . "</td>" .
					   "<td>" . number_format( $item->getBasePrice(), 2 ) . "</td></tr>";

				$orderinsurance += $item->getBaseInsurance();
			}
			
			$shipping = $order->getBaseShippingAmount();
			$grandtotal 	= $order->getBaseGrandTotal();
			
			// $row = array();
			// $row = $this->checkCustomerCreditWasUsed($order['entity_id']);
			
			// $orderinsurance = $this->get_TotalInsurance($order['entity_id']);
			
			$vascharges = $order->getVasCharges();
			//$vascharges = number_format($vascharges,2);
			
			// if( count( $row ) > 0 ) {
				// $icline = number_format($row['value_change'],2);
			// } else { $icline = "0.00"; }
			$icline = "0.00";
			
			$rows .="<tr><td colspan='5' align='right'><B>Shipping &amp; Handling</B>: " . number_format($shipping,2) . "</td></tr>";
			$rows .="<tr><td colspan='5' align='right'><B>Customer Credit</B>: " . $icline . "</td></tr>";
			
			if( $orderinsurance > 0 ) {
				$rows .="<tr><td colspan='5' align='right'><B>Insurance</B>: " . number_format($orderinsurance,2) . "</td></tr>";
			}
			
			$rows .="<tr><td colspan='5' align='right'><B>VAS Charge</B>: " . $vascharges . "</td></tr>";
			
			$rows .="<tr><td colspan='5' align='right'><B>Grand Total</B>: " . number_format($grandtotal,2) . "</td></tr>";
			
			$rows .= "</table>";
			
			$api = "";
			$api 		   = $order->getAdditionalpaymentinfo();
			if( strlen($api) > 0 ) { $api = " - " . $api;}
			
			$paymentoption = "<br />Payment Option Selected:  $paymentmethod $api";
			$footer = "<br /><br /><I>THIS IS A SYSTEM GENERATED E-MAIL, PLEASE DO NOT RESPOND TO THE E-MAIL ADDRESS SPECIFIED ABOVE.</I>";
			
			$body .= $orddetails . $rows . $paymentoption . $footer;
			
			return $body ;
			} catch (Exception $e) {
				file_put_contents("/tmp/orderintimation_template.log", $e->getTraceAsString() );
			}
		}
		
		public function getOrderCurrencyCode($entity_id) {
			$sql = "SELECT order_currency_code FROM sales_flat_order WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['order_currency_code'];
			}
			return $ret;
		}
		
		public function getOrderInsurance($entity_id) {
			$sql = " select SUM(base_insurance) as totalbaseinsurance FROM sales_flat_order_item WHERE order_id=$entity_id";
			
			$result = $this->runquery($sql);
			$ret = 0;
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['totalbaseinsurance'];
			}
			
			return $ret;
		}
		
		
		public function create_pickupcoordinatoremail( $ordernumber, $pickupregion, $products ) {
			
			$body = "<table width='100%' cellpadding='0' cellspacing='0' style='font:14px Arial, Helvetica, sans-serif;'><tr><td>Dear Pickup Coordinator<br /><br />" .
					"Please arrange to pick the following orders:<br /><br />";
					
			$body .= "<table width='100%' cellpadding='0' cellspacing='0' border='1'>" .
					 "<tr>".
						"<td><B>S.No.</B></td>".
						"<td colspan='6'><B>TCS e-Business PICKUP Summary - $pickupregion Area Dated (" . date('d-m-Y') . ")</B></td>".
					"</tr>";
					
			$body .= "<tr>" .
						"<td><td>".
						"<td colspan='3'><B>Vendor Details</B></td>" .
						"<td colspan='3'><B>Order Details</B></td>" .
					"</tr>";
					
			$body .= "<tr>" .
						"<td width='100px'></td>". 
						"<td width='150px'><B>Name</B></td>".
						"<td width='200px'><B>Address</B></td>".
						"<td width='150px'><B>Contact #</B></td>".
						"<td width='150px'><B>Order #</B></td>".
						"<td width='150px'><B>Qty</B></td>".
						"<td><B>Description</B></td>".
					"</tr>";
				
			
				
			for($i=0; $i < count($products); $i++) {
				$vendor_address = $products[$i]['vendor']['street'] . " " . $products[$i]['vendor']['city'] . " " . $products[$i]['vendor']['zip'];
			
				$body .= "<tr>" .
							"<td>" . $i . "</td>" .
							"<td>" . $products[$i]['vendor']['vendor_name'] . "</td>" .
							"<td>" . $vendor_address. "</td>" .
							"<td>" . $products[$i]['vendor']['telephone']. "</td>" .
							"<td>" . $products[$i]['ordernum']. "</td>" .
							"<td>" . $products[$i]['qty']. "</td>" .
							"<td>" . $products[$i]['productname']. "</td>" . 
						"</tr>";
			}
			
			$body .= "</table><br /><br /><I>THIS IS A SYSTEM GENERATED E-MAIL, PLEASE DO NOT RESPOND TO THE E-MAIL ADDRESS SPECIFIED ABOVE.<I></td></tr></table>";
			return $body;
		}
		
		private function get_Orderdetails($entity_id) {
			$sql = "SELECT o.increment_id, i.name,o.created_at, i.base_original_price, i.base_insurance, i.qty_ordered FROM sales_flat_order o INNER JOIN sales_flat_order_item i ON i.order_id=o.entity_id WHERE o.entity_id=$entity_id AND i.base_insurance > 0";
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			return $read->fetchAll($sql);
		}
		
		private function get_TotalInsurance($entity_id) {
			$sql  = "SELECT SUM(base_insurance) AS 'totalinsurance' FROM sales_flat_order_item WHERE order_id=$entity_id";
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$ret  = $read->fetchAll($sql);
			return $ret[0]['totalinsurance'];
		}
		
		private function get_OrderCustomerName($entity_id) {
			$sql = "SELECT customer_firstname, customer_lastname, customer_middlename FROM sales_flat_order WHERE entity_id='$entity_id' LIMIT 1";
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$customer = $read->fetchAll($sql);
			if( count($customer) > 0 ) {
				return $customer[0]['customer_firstname'] . " " . $customer[0]['customer_lastname'];
			}
			return "";
		}
		
		private function get_TotalInsuranceLeft($entity_id) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT balance_left FROM tcs_insurance_balance ORDER BY balanceid DESC LIMIT 1";
			$ret = $read->fetchAll($sql);
			return $ret[0]['balance_left'];
		}
		
		public function get_InsuranceTemplate($entity_id, $increment_id) {
			$subject         = "Insurance against TCS Connect Order # $increment_id";
			$customername    = $this->get_OrderCustomerName($entity_id);
			$total_insurance = $this->get_TotalInsurance($entity_id);
			$totalleft       = $this->get_TotalInsuranceLeft($entity_id);
			
			$total_insurance = number_format($total_insurance,2);
			
			$body = "Hi Team AIG,<br/>" .
					"A new order has been placed with insurance add on.<br/><br/>" .
					"Order Number: Rs. $increment_id<br/>" .
					"Total insurance paid by the customer: Rs. $total_insurance<br/>" .
					"Total premium left: Rs. $totalleft (left over premium of TCS prepaid amount to AIG)<br/><br/>" .
					
					"For any assistance on the above details, please feel free to email us at : Payments@tcsconnect.com<br/><br/>" .
 
					"Regards,<br/>" . 
					"TCSConnect Team";
			
			return $body;
		}
		
		
		//Get UBL Discalaimer Link
		function get_ubl_discalaimer_link_for_email($order_id, $orderPayment = null){
			
		 $get_base_url	=	Mage::getBaseUrl();
	
			//add disclamer on UBL Omni and internet banking
			//@dated 9th June 2014
			$_order					=	Mage::getModel('sales/order')->loadByIncrementId($order_id);
			$payment_method_code 	= 	$_order->getPayment()->getMethodInstance()->getCode(); 
			
			//get additional payment information from session if new ordre is place b/c email sent before saving additional info in DB
			if($orderPayment==null){
				$additionalpaymentinfo	=	@$_SESSION['additionalpaymentinfo'];
			}else{
				$_orderData				=	$_order->getData();
				$additionalpaymentinfo	=	$_orderData['additionalpaymentinfo'];#get additional payment info for internet banking
			}
	
			if($payment_method_code=='internetbanking'){
				$payment_method_code	=	$additionalpaymentinfo;
			}
			
			$disclamerPaymentMethod	=	array('ublomni','UBL Net-Banking');
			
			$html	=	"";
			
			if(in_array($payment_method_code,$disclamerPaymentMethod)){
				$html	=	"<br />UBL Disclaimer <a href='".$get_base_url."/ubl-disclaimer/'>here</a>";
			}
			
			return $html;
		}
		
		//IS CONFIGURABLE OR SIMPLE
		function ispartofconfigurableitem( $item_id) {
			$read  = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql   = "SELECT count(item_id) as cnt FROM sales_flat_order_item WHERE item_id='$item_id' AND product_type='simple' AND parent_item_id IS NOT NULL";
			$ret = $read->fetchAll($sql);
			if( $ret[0]['cnt'] > 0 ) return 1;
			return 0;
		}

	}
	
?>