<?php
	include "dbconmgr.php";
	include "/var/www/tcsconnect_newui/lib/dbconmgr/excellib/Classes/PHPExcel.php";
	include "/var/www/tcsconnect_newui/lib/dbconmgr/phpmailer/class.phpmailer.php";
	include "/var/www/tcsconnect_newui/app/Mage.php";
	include "cron_class.php";
	
	Mage::app('admin');
	
	$db = new dbconmgr();
	$db->connect();
	
	$croncls = new cron_class();
	
	if( $croncls->if_cron_is_active("6") <> 1 ) {
		$croncls->update_cron_log("6", 0, "Cron is not active" );
		exit;
	}
	
	$cclist = $db->getConfigConstant("discrepancy_emailcc");
	
	//$cclist		=	'';
	
	$cclist = $cclist['config_value'];
	
	$managers = $db->getPaymentManagers();
	$transactions   = $db->getOrdersForDiscrepancyReport();
	
	$numberoforders = count($transactions);
	
	if( $numberoforders < 1 ) {
		$croncls->update_cron_log("6", 0, "No orders were found" );
	}
	
	$ordersdata = array();
	$ind		= array();
	
	for($i=0; $i < count($transactions); $i++) {
		$transactionid = $transactions[$i]['transactionid'];
		$details = array();
		$details = $db->getTransactionDetail($transactionid);
		
		if( $transactionid == 793 ) {		
			$areacode   = "KHI";
			$areacodeid = "1";
		} else {		
			$areacode      = $details['areacode'];
			$areacodeid    = $details['pmarea'];
		}
		
		$areacode = $details['pm_area'];
		
		$transactionid = $details['transactionid'];
		$ordernum      = $details['ordernum'];
		$orderdate     = $details['paymentts'];
		$currencycode  = $details['order_currency_code'];
		
		$order_id = $db->getRealOrderId($ordernum);
		$invoice_date = date("d-m-Y H:i:s", strtotime($db->getInvoiceCreateDate($order_id)));
		$details['invoice_date'] = $invoice_date;
		
		
		if($details['orderamount'] < 1 ) {
			$orderamount = $details['base_grand_total'];
			$details['orderamount'] = $orderamount;
		}
		else { $orderamount   = $details['orderamount']; }
		
		if( $currencycode <> "PKR" ) {
			//$rate  = Mage::helper('directory')->currencyConvert(1, "PKR", "USD");
			//$newamount = $orderamount;
			//$orderamount = $newamount;

			$orderamount = $details['base_grand_total'];
			$details['orderamount'] = $orderamount;
		}
		
		$ordercity     = $details['city'];
		$paymethod	   = $details['method'];
		$additionalpay = trim($details['additionalpaymentinfo']);
		
		if( $paymethod == "cod" ) {
			$paymethod = "Cash On Delivery";
			$details['method'] = "Cash On Delivery";
		}
		
		if( $paymethod == "creditcardoffline" && $additionalpay <> "CC Offline - Doorstep" ) {
			continue;
		}
		
		if( $paymethod == 'paypal_standard' || $paymethod == 'twocheckout_shared' || $paymethod == 'customercredit' || $paymethod == 'internetbanking' || $paymethod == 'easypaisa' || $paymethod == 'marketingexpense' || $paymethod == 'financesettlement' ) continue;
		
		if( stripos($additionalpay, "Bank Deposit") !== FALSE ) continue;
		
		if( array_key_exists( $areacode, $ordersdata ) == TRUE ) {
			$tmp = array();
			$tmp = $ordersdata[$areacode];
			$tmp[] = $details;
		}
		else {
			$tmp = array();
			$tmp[] = $details;
		}
		$ordersdata[$areacode] = $tmp;
	}
	
	$keys = array_keys($ordersdata);
	
	for($i=0; $i < count($keys); $i++) {
		echo $keys[$i] . " - " . count($ordersdata[$keys[$i]]) . "\n";
	}
	
	for($i=0; $i < count($managers); $i++) {
		$areacode 	  = $managers[$i]['areacode'];
		$managerlogin = $managers[$i]['managerlogin'];
		$area 		  = $managers[$i]['paymentarea'];
		
		if( $managerlogin == "irfan.soomro@tcs.com.pk" )
			//$managerlogin = "rafiq.ahmed@tcs.com.pk";
		
		if( array_key_exists($areacode,$ordersdata ) !== TRUE ) continue;
		
		$orders = $ordersdata[$areacode];
		createAndEmailReport($orders, $areacode,$managerlogin,$area, $cclist );
	}
	if( $numberoforders > 0 )
		$croncls->update_cron_log("6", $numberoforders, "Success!" );
	
	function createAndEmailReport($orders, $areacode,$managerlogin,$area, $cclist ) {
		$filename = "/tmp/discrepancyreport_" . date("d_m_Y") . "_$areacode.xlsx";
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("TCS Connect");
		$objPHPExcel->getProperties()->setLastModifiedBy("TCS Connect");
		$objPHPExcel->getProperties()->setTitle("Area Manager Discrepancy Report");
		$objPHPExcel->getProperties()->setSubject("");
		$objPHPExcel->getProperties()->setDescription("");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->SetCellValue('B2', "Area Discrepancy Report");
		
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Report Date");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', date("d-m-Y"));
		
		$objPHPExcel->getActiveSheet()->SetCellValue('B4', "Area");
		$objPHPExcel->getActiveSheet()->SetCellValue('C4', $area);
		
		$objPHPExcel->getActiveSheet()->SetCellValue('B5', "Area Code");
		$objPHPExcel->getActiveSheet()->SetCellValue('C5', $areacode);
		
		//Header starts
		$objPHPExcel->getActiveSheet()->SetCellValue('B7', "S.No");
		$objPHPExcel->getActiveSheet()->SetCellValue('C7', "Order Number");
		$objPHPExcel->getActiveSheet()->SetCellValue('D7', "Amount Paid");
		$objPHPExcel->getActiveSheet()->SetCellValue('E7', "Date Paid");
		$objPHPExcel->getActiveSheet()->SetCellValue('F7', "Billing City");
		//Header ends
		
		$linenumber = 9;
		
		for($i=0; $i < count($orders); $i++) {
			$sno = $i+1;
			$ordernumber = $orders[$i]['ordernum'];
			$amountpaid  = $orders[$i]['orderamount'];
			$datepaid    = date("d-m-Y", strtotime($orders[$i]['orderdate']));
			$billingcity = $orders[$i]['city'];
			
			$fieldname = "B$linenumber";
			$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($sno);
			
			$fieldname = "C$linenumber";
			$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($ordernumber);
			
			$fieldname = "D$linenumber";
			$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($amountpaid);
			
			$fieldname = "E$linenumber";
			$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($datepaid);
			
			$fieldname = "F$linenumber";
			$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($billingcity);
			
			$linenumber++;
		}
		
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($filename);
		
		$body = createEmailBody($areacode,$orders);
		echo "\n\nSending Email for Areacode: $areacode\nOrders: " . count($orders) . " .......................\n\n";
		sendEmail($managerlogin, $areacode, $body, $cclist);
	}
	
	function createEmailBody($areacode, $orders) {
		$body = "Dear $areacode Area Accountant,<BR><BR>Please look into the following order payments not deposited that pertains to your area.<BR><BR><table width='100%' cellpadding='0' cellspacing='0'>" .
			"<tr><td width='70px'><B>Sr#</B></td><td width='70px'><B>Area</B></td><td width='120px'><B>Order #</B></td>" .
			"<td width='120px'><B>CN Number</B>" . 
			"<td width='200px'><B>Amount</B></td><td width='200px'><B>Payment Mode</B></td><td width='150px'><B>Date Paid</B></td><td width='150px'><B>Due Days</B></td><td><B>Payment Received</B></td></tr>";
		
		$db = new dbconmgr();
		$db->connect();
			
		$table = "";
		$sno = 0;
		for($i=0; $i < count($orders); $i++) {
			$ordernumber = $orders[$i]['ordernum'];
			$amount 	 = number_format($orders[$i]['orderamount'],2);
			$datepaid    = date("d-m-Y H:i:s", strtotime($orders[$i]['paymentts']));
			$cndata		 = $db->getOmsTransaction($ordernumber);
			$transaction = $db->getTransactionByOrderID($ordernumber);
			$cnnums		 = "";
			
			for($a=0; $a < count($cndata); $a++) {
				$cnnums .= $cndata[$a]['cnnumber'] . ", ";
			}
			
			$method = $orders[$i]['method'];
			if( $method == "checkatbank") $method = "Cheque";
			else if( $method == "cash"  ) $method = "Cash";
			else if( $method == "internetbanking" ) $method = "Internet Banking";
			else if( $method == "creditcardoffline" ) $method = "Credit Card";
			
			if( $method == "Cash On Delivery" ) {
				if( $cndata < 1 ) continue;
			}
			
			echo "$areacode - $ordernumber - $amount\n";
			
			$api = $orders[$i]['additionalpaymentinfo'];			
			$table .="<tr><td>$sno</td><td>$areacode</td><td>$ordernumber</td><td>$cnnums</td><td>$amount</td><td>$method $api</td><td>$datepaid</td><td>".$transaction['lag']."</td><td>No</td></tr>";
			$sno++;
		}
		
		$table .= "</table><BR><BR>";
		$footer = "<I>THIS IS A SYSTEM GENERATED E-MAIL, PLEASE DO NOT RESPOND TO THE E-MAIL ADDRESS SPECIFIED ABOVE.</I>";
		
		return $body . $table . $footer;
	}
	
	function sendEmail($emailaddress, $areacode, $body,$cclist) {
		$db = new dbconmgr();
		$db->connect();
	
		$mail = new PHPMailer();
		$mail->SetFrom('cs@tcsconnect.com', 'TCSConnect');
		$mail->Subject = "TCS Connect � ($areacode) Area accountant discrepancy report " . date("d-m-Y");
		$mail->MsgHTML($body);
		$mail->AddAttachment($filename);   
		
		$mail->AddAddress($emailaddress);
		
		if( $areacode == "KHI" ) {
			$mail->AddAddress('mzakir@tcs.com.pk');
			$mail->AddAddress('rana.amir@tcs.com.pk');
		}

		if( $areacode == "LHE" || $areacode == "FSD" || $areacode == "MUX") {
			$mail->AddAddress('ghazanfar@tcs.com.pk');
		}
		
		$areacc = $db->getDiscrepancyCCList($areacode);
		
		for($i=0; $i < count($areacc); $i++) {
			$mail->AddCC($areacc[$i]['emailaddress']);
		}
		
		$mail->AddCC('atif.sami@tcs.com.pk');
		$mail->AddCC('mohsin.ali@tcs.com.pk');
		
		$cc	= $db->getConfigConstant("discrepancy_email_cc");
		$cc = $cc['config_value'];
		
		$tmp_cc = explode(";", $cc);
		
		for($i=0; $i < count($cc); $i++ ) {
			$mail->AddCC($tmp_cc[$i]);
		}
		
		$tmp = explode(";", $cclist);
		for($i=0; $i < count($tmp); $i++) {
			$mail->AddCC($tmp[$i]);
		}
		
		if(!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo . "\n\n";
		} else {
			echo "Message sent!\n\n";
		}
	}
	echo "DONE";
	
?>
