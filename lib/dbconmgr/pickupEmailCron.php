<?php
	include "db_creds.php";
	include "dbconmgr.php";
	include "mailtemplatesclass.php"; 
	include "receiptclass.php"; 
	include "/var/www/tcsconnect/lib/dbconmgr/excellib/Classes/PHPExcel.php";
	include "/var/www/tcsconnect/lib/dbconmgr/phpmailer/class.phpmailer.php";
	include "/var/www/tcsconnect/app/Mage.php";
	include "cron_class.php";
	
	Mage::app('admin');
	
	$croncls = new cron_class();
	
	if( $croncls->if_cron_is_active("8") <> 1 ) {
		$croncls->update_cron_log("8", 0, "Cron is not active" );
		exit;
	}
	
	$db = new dbconmgr();
	$db->connect();
	
	$orders = $db->getOrdersForPickUpEmail();
	
	$directorders = $db->getDirectOrders();
	
	$tmp = array();
	$tmp = array_merge($orders, $directorders);
	$orders = $tmp;
	
	$numberoforders = count($orders);
	if( $numberoforders < 1 ) { $croncls->update_cron_log("8", 0, "Cron is not active" ); }	
	echo "Orders to be processed: " . count($orders) . "\n\n";	
	processOrders($orders);
	
	function processOrders($orders) {
		$db = new dbconmgr();
		$db->connect();
		
		$ordersbycity = array();
		$ordersbycity['khi'] = array();
		$ordersbycity['lhe'] = array();
		$ordersbycity['dxb'] = array();
		$ordersbycity['mux'] = array();
		$ordersbycity['slt'] = array();
		$ordersbycity['isb'] = array();
		
		
		try {
			for($i=0; $i < count($orders); $i++) {
				$entity_id    = $orders[$i]['entity_id'];
				$increment_id = $orders[$i]['increment_id'];
				$products     = array();
				$orderObj	  = null;
				$item		  = null;
				
				echo "Processing ...... $increment_id - $entity_id\n\n";
				
				$orderstatus = $db->getOrderStatus($increment_id);
				$paymethod   = $db->getPaymentmethod($entity_id);
				$paymethod	 = $paymethod['method'];
				$orderObj    = Mage::getModel('sales/order')->loadByIncrementId($increment_id);
				$billingadd  = $db->getOrderAddressDetails($entity_id);
				$billingname = $billingadd['firstname'] . " " . $billingadd['lastname'];
				$cndata		 = $db->getOmsTransaction($increment_id);
				$cnnums		 = "";
				
				$vascharges  = $db->getOrderVasInfoByEntityId($entity_id);
				$vascharges  = $vascharges['vas_charges'];
				
				$shippingamount = $db->getShippingAmount($increment_id);
				
				$prodnum = 1;
				
				for($a=0; $a < count($cndata); $a++) {
					$cnnums .= $cndata[$a]['cnnumber'] . ", ";
				}
				
				foreach ($orderObj->getAllItems() as $item) {					
					$row = array();
					$sku = $item->getData('sku');
					
					//check if cn number is entered against this SKU
					$cninfo = $db->getOmsInfoByOrderIdAndSku($increment_id, $sku);
					
					if( count($cninfo) > 0 ) {
						if( $cninfo[0]['quantity'] == $item->getData('qty_ordered') || count($cninfo) == $item->getData('qty_ordered') ) {
							continue;
						}
					}
					
					$productname 	 = $item->getName();
					$productvendorid = $item->getData('udropship_vendor');
					$productvendor   = $db->getVendor($productvendorid);
					
					$vendorcity = $productvendor['city'];
					$row['cnnum']		= $cnnums;
					$row['billname']	= $billingname;
					$row["productname"] = $item->getName();
					$row["vendor"]      = $productvendor;
					$row["vendorname"]  = $productvendor['vendor_name'];
					$row["address"]  	= $productvendor['street'] . ", " . $productvendor['city'] . ", " . $productvendor['zip'];
					$row['telephone']   = $productvendor['telephone'];
					$row["qty"]		    = $item->getData('qty_ordered');
					$row["weight"]		= $item->getWeight();
					$row["ordernum"]	= $orderObj->getIncrementId();
					$row['status']		= $orderstatus;				
					
					if( $paymethod == "cod" ) {						
						if($prodnum == 1 )					
							$row['totaldue'] = $item->getPrice() * $row['qty'] + $shippingamount + $vascharges;
						else 
							$row['totaldue'] = $item->getPrice() * $row['qty'];
						
						$row['method']   = "COD";
					} 
					else { 
						$row['method']   = "Paid";
						$row['totaldue'] = "Paid"; 
					}
					
					if( $vendorcity == "Karachi" ) {
						$tmp = array();
						$tmp = $ordersbycity['khi'];
						$tmp[] = $row;
						$ordersbycity['khi'] = $tmp;
					}
					else if( $vendorcity == "Lahore" ) {
						$tmp = array();
						$tmp = $ordersbycity['lhe'];
						$tmp[] = $row;
						$ordersbycity['lhe'] = $tmp;
					}
					else if( $vendorcity == "Multan" ) {
						$tmp = array();
						$tmp = $ordersbycity['mux'];
						$tmp[] = $row;
						$ordersbycity['mux'] = $tmp;
					}
					else if( $vendorcity == "Dubai" ) {
						$tmp = array();
						$tmp = $ordersbycity['dxb'];
						$tmp[] = $row;
						$ordersbycity['dxb'] = $tmp;
					}
					else if( $vendorcity == "Bahawalpur" ) {
						$tmp = array();
						$tmp = $ordersbycity['bhv'];
						$tmp[] = $row;
						$ordersbycity['bhv'] = $tmp;
					}
					else if( $vendorcity == "Sialkot" ) {
						$tmp = array();
						$tmp = $ordersbycity['slt'];
						$tmp[] = $row;
						$ordersbycity['slt'] = $tmp;
					}
					else if( $vendorcity == "Islamabad" ) {
						$tmp = array();
						$tmp = $ordersbycity['isb'];
						$tmp[] = $row;
						$ordersbycity['isb'] = $tmp;
					}
					
					
					$prodnum++;
				}
			}
			
			$keys = array_keys($ordersbycity);			
			for($i=0; $i < count($keys); $i++) {
				createemail($keys[$i], $ordersbycity[$keys[$i]], $db);
			}
			
			if( $numberoforders > 0 ) {
				$croncls->update_cron_log("8", $numberoforders, "Success!" );
			}
			
		} catch (Exception $e) {  echo "Error for $increment_id : " . $e->getMessage(); }
	}
	
	
	function getCustomerReceipt($ordernum, $db) {		
		$rc = new receiptclass();
		
		$filename = "/tmp/" . $ordernum . ".html";
		
		$order = Mage::getModel('sales/order');
		$order->loadByIncrementId($ordernum);
		$entity_id = $order->getId();
		
		echo "$ordernum - $filename \n\n";
		
		if( $order->getId() ) {
		
			echo "Order id is : " . $order->getId() . "\n\n";
		
			$orderdate		    	 = date("m-d-Y H:i:s", strtotime($order->getCreatedAt()));
			$details['orderid'] 	 = $order->getId();
			$details['ordernumber']  = $order->getIncrementId();
			$details['customername'] = $order->getCustomerName();				
			$details['totaldue'] 	 = $order->getGrandTotal();	
			$details['orderdate']	 = $orderdate;
			
			$cccsflag = $db->getCashCCFlag($order->getIncrementId());
			echo "this is atest \n";
			
			if( $cccsflag == "" ) { $flag = "N/A"; }
			else {
				if( $cccsflag['ccvscash'] == "cc" )  { 
					$flag="Credit Card"; 
					$ccextra = $details['totaldue'] + 2.5 * $details['totaldue']/100;
				}
				else { 
					$flag = "Cash"; 
					$ccextra = "";
				}
			}
			
			$shipping 			= $order->getShippingAddress();
			$shippingname 		= $shipping->getData('firstname') . " " . $shipping->getData('lastname');
			$shippingaddress 	= $shipping->getData('street');
			$shippingaddress   .= ", " . $shipping->getData('city');
			$shippingaddress   .= ", " . $shipping->getData('region');
			$shippingaddress   .= ", " . $shipping->getData('postcode');
			$shippingaddress   .= ", " . $shipping->getCountryModel()->getName();
			$details['shipping'] = $shippingaddress;
			
			$billing = $order->getBillingAddress();
			$billingaddress = $billing->getData('street');
			$billingaddress .= ", " . $billing->getData('city');
			$billingaddress .= ", " . $billing->getData('region');
			$billingaddress .= ", " . $billing->getData('postcode');
			$billingaddress .= ", " . $billing->getCountryModel()->getName();
			$details['billing'] 		= $billingaddress;
			$details['telephone'] 	   = $billing->getData('telephone');
			$details['status'] 		   = $order->getStatusLabel();
			$details['flag'] 		   = $flag;
			$details['shippingamount'] = $order->getShippingAmount();
			$details['subtotal']	   = $order->getSubtotal();
			
			// $details['rsoname'] = Mage::getSingleton('admin/session')->getData('rsoname');			
			// $details['rsousername'] = Mage::getSingleton('admin/session')->getData('rsousername');	

			$product_block = "";
			$body_block = "Team CS at TCS Connect<BR><BR>Please coordinate with customer to verify the status of the order and confirm to logistics manager to arrange payments collection.<BR><BR>";
			$body_product_block = "<table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td><B>Product Name</B></td><td><B>Vendor</B></td><td><B>Quantity</B></td><td><B>Unit Price</B></td><td><B>Sub total</B></td></tr>";
			
			foreach ($order->getAllItems() as $item) {
			
				$item_id = $item->getId();
				$ispartofconf = ispartofconfigurableitem($item_id);
				
				if( $ispartofconf > 0 ) {
					continue;
				}
				
				$row = array();
				$row['name'] = $item->getName();
				$row['price'] = $item->getPrice();
				$row["weight"]		= $item->getWeight();
				$row['qty']	  = intval($item->getData('qty_ordered'));
				$qty = intval($item->getData('qty_ordered'));
				
				$priceperrow = $item->getPrice() * $item->getData('qty_ordered');
				
				$vendorid = $item->getData('udropship_vendor');					
				//$row['vendor'] = $db->getVendor($vendorid);
				$vendor = $db->getVendor($vendorid);
				$items[] = $row;		

				$product_block .= '<tr>'.
									'<td align="left" valign="top">' . $row['name'] .'</td>' .
									'<td align="left" valign="top"><strong>' . $vendor['vendor_name'] . '</strong><br />' .	
									$vendor['street'] . " " . $vendor['city'] . '<br />' .
									$vendor['telephone'] . '</td>' .
									'<td align="center">' . $row['qty'] .'</td>' .
									'<td align="center"><p>' . number_format($row['price'],2) .'</p></td>' .
									'<td align="center"><p>' . number_format($priceperrow,2) . '</p></td>' .
								   '</tr>';
			}
			
			$body_block .= $body_product_block . $product_block . "</table><BR><BR>Payment Option Selected $method @ Doorstep<BR><BR>Payment Pickup at:<BR> $billingaddress<BR><BR><I>THIS IS A SYSTEM GENERATED E-MAIL, PLEASE DO NOT RESPOND TO THE E-MAIL ADDRESS SPECIFIED ABOVE.</I>";
			
			$details['items'] = $items;
			$details['shippingrate'] = $order->getShippingAmount();
			
			//check if internal credit was used;
			$row = array();
			$row = $db->checkCustomerCreditWasUsed($entity_id);
			
			$vascharges = $db->getOrderVAS($entity_id);
			$rawvascharges = $vascharges;
			
			if( $vascharges > 0 ) {				
				$vascharges = number_format($vascharges,2);
			} else { $vascharges = "0.00"; $rawvascharges=0;}
			
			if( count( $row ) > 0 ) {
				$icline = number_format($row['value_change'],2);
			} else { $icline = "0.00"; }
			
			$ordertotal = $details['totaldue'] + $rawvascharges;
			
			
			$servicecharges = "0.00"; 
			$ccextra 	    = "0.00";
			
			if( $orgmethod == "creditcardoffline" ) {
				$servicecharges = $ordertotal * 2.5 / 100;
				$ccextra = $ordertotal + $servicecharges;
				$servicecharges = number_format($servicecharges,2);
			}
		
		$email_template = "";
		$email_template = $rc->getDoorstepReceipt($details['customername'], $shippingname, $details['ordernumber'], number_format($ordertotal,2),$billingaddress,$shippingaddress, $details['telephone'],$method, $ccextra, number_format($details['shippingamount'],2), number_format($details['subtotal'],2),$icline,$vascharges, $servicecharges, $product_block);
		}
		
		return $email_template;
	}
	
	function ispartofconfigurableitem( $item_id) {
		$read  = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql   = "SELECT count(item_id) as cnt FROM sales_flat_order_item WHERE item_id='$item_id' AND product_type='simple' AND parent_item_id IS NOT NULL";
		$ret = $read->fetchAll($sql);
		if( $ret[0]['cnt'] > 0 ) return 1;
		return 0;
	}
	
	function createemail($pickupregion, $regionorders, $db ) {
		if( count($regionorders) < 1 ) return;
		$croncls = new cron_class();
	
		echo "$pickupregion - " . count($regionorders) . "\n\n";
	
		$body = "Dear " . strtoupper($pickupregion) . " area pickup coordinator,<BR><BR>Please arrange pickup of following order item(s) from the respective vendor and confirm the pick-up status through email to Operation Manager e Business:<BR><BR>";
		
		$header = "<table width='100%' cellpadding='0' cellspacing='0' style='font-size:12px' border='1'>" .
				  "<tr><td width='50px'><B>S.No.</B></td><td width='120px'><B>Order No.</B></td>".
				  "<td width='150px'><B>Bill to name</B>" . 
				  "<td width='150px'><B>CN-Number</B>" . 
				  "<td width='250px'><B>Order Item Description</td><td width='200px'><B>Vendor Name</B></td><td width='250px'><B>Address</B></td><td width='200px'><B>Phone</B></td><td width='70px'><B>Qty</B></td><td width='120px'><B>Payment Status</B></td><td><B>COD Amount</B></td></tr>";
				  
		$rows = "";
		$mail = new PHPMailer();

		for($i=0; $i < count($regionorders); $i++) {
			$sno = $i+1;
			$cnum		   = $regionorders[$i]['cnnum']; 	
			$ordernum      = $regionorders[$i]['ordernum'];
			$productname   = $regionorders[$i]['productname'];
			$vendorname    = $regionorders[$i]['vendorname'];
			$vendoraddress = wordwrap($regionorders[$i]['address'], 20, '<br />');
			$vendorphone   = wordwrap($regionorders[$i]['telephone'], 15, '<br />');
			$quantity	   = intval($regionorders[$i]['qty']);
			$paymethod	   = $regionorders[$i]['method'];
			$totaldue	   = $regionorders[$i]['totaldue'];
			$billname	   = $regionorders[$i]['billname'];
			
			$rows .= "<tr><td>$sno</td><td>$ordernum</td><td>$billname</td><td>$cnum</td><td>$productname</td><td>$vendorname</td><td>$vendoraddress</td><td>$vendorphone</td><td>$quantity</td><td>$paymethod</td><td>$totaldue</td></tr>";
			
			if( $paymethod == "COD" ) {
				$receiptfile = "/tmp/pickupCron/$ordernum.html";
				@file_put_contents($receiptfile, getCustomerReceipt($ordernum, $db));
				$mail->AddAttachment($receiptfile);
			}
		}
		
		$rows .= "</table><BR><BR>For COD orders receipt is attached. Send two prints along with the shipment in which one print must be handed over to the customer and second copy needs to be submitted in accounts with the cash and PAPCR / DSSP<BR><BR><I>THIS IS A SYSTEM GENERATED E-MAIL, PLEASE DO NOT RESPOND TO THE E-MAIL ADDRESS SPECIFIED ABOVE.</I>";
			
		$emailbody = $body . $header . $rows;	
		
		if( $pickupregion == "khi" ) {
			if( $totalweight > 1 ) {		
				$pickupcoordemail = $db->getConfigConstant("pickup_khiover1");
				$pickupcoordemail = $pickupcoordemail['config_value'];				
			}
			else {
				$pickupcoordemail = $db->getConfigConstant("pickup_khibelow1");
				$pickupcoordemail = $pickupcoordemail['config_value'];
			}
			
			$pickupcclist = $db->getConfigConstant("pickup_khicclist");
			$pickupcclist = $pickupcclist['config_value'];
		}
		else if( $pickupregion == "dxb" ) {
			$pickupcoordemail = $db->getConfigConstant("pickup_dxb");
			$pickupcoordemail = $pickupcoordemail['config_value'];
			
			$pickupcclist = $db->getConfigConstant("pickup_dxbcclist");
			$pickupcclist = $pickupcclist['config_value'];
		}
		else if( $pickupregion == "lhe" ){
			$pickupcoordemail = $db->getConfigConstant("pickup_lhe");
			$pickupcoordemail = $pickupcoordemail['config_value'];
			
			$pickupcclist = $db->getConfigConstant("pickup_lhecclist");
			$pickupcclist = $pickupcclist['config_value'];
		}
		else if ( $pickupregion == "mux" ) {
			$pickupcoordemail = $db->getConfigConstant("pickup_mux");
			$pickupcoordemail = $pickupcoordemail['config_value'];
			
			$pickupcclist = $db->getConfigConstant("pickup_muxcclist");
			$pickupcclist = $pickupcclist['config_value'];
		}
		else if( $pickupregion == "bhv" ) {
			$pickupcoordemail = $db->getConfigConstant("pickup_bhv");
			$pickupcoordemail = $pickupcoordemail['config_value'];
			
			$pickupcclist = $db->getConfigConstant("pickup_bhvcclist");
			$pickupcclist = $pickupcclist['config_value'];
		}
		else if( $pickupregion == "slt" ) {
			$pickupcoordemail = $db->getConfigConstant("pickup_slt");
			$pickupcoordemail = $pickupcoordemail['config_value'];
			
			$pickupcclist = $db->getConfigConstant("pickup_sltcclist");
			$pickupcclist = $pickupcclist['config_value'];
		}
		else if( $pickupregion == "isb" ) {
			$pickupcoordemail = $db->getConfigConstant("pickup_isb");
			$pickupcoordemail = $pickupcoordemail['config_value'];
			
			$pickupcclist = $db->getConfigConstant("pickup_isbcclist");
			$pickupcclist = $pickupcclist['config_value'];
		}
		
		
		$cclist = $db->getConfigConstant("cc_list");
		$cclist = $cclist['config_value'];
		
		$bcclist = $db->getConfigConstant("bcc_list");
		$bcclist = $bcclist['config_value'];
		
		$mail->SetFrom('cs@tcsconnect.com', 'TCS Connect');
		$mail->Subject  = "TCS Connect � Order Pickup Email " . strtoupper($pickupregion);
		$mail->MsgHTML($emailbody);
		$mail->AddAddress($pickupcoordemail, "Area Pickup Coordinator");
		// $mail->AddAddress("farhan.munir@nexdegree.com", "Area Pickup Coordinator");
		$mail->AddBCC("farhan.munir@nexdegree.com", "farhan.munir@nexdegree.com" );
		
		//Add Pickup CC List
		$pickupcclist = explode(";", $pickupcclist);
		for($i=0; $i < count($pickupcclist); $i++) {
			if( strlen($pickupcclist[$i]) > 0 )
				$mail->AddCC($pickupcclist[$i], $pickupcclist[$i]);
		}
		
		$cclist = explode(";", $cclist);
		for($i=0; $i < count($cclist);$i++) {
			if( strlen($cclist[$i]) > 0 )
				$mail->AddCC($cclist[$i],$cclist[$i]);
		}
		
		$bcclist = explode(";", $bcclist);
		for($i=0; $i < count($bcclist); $i++) {
			if( strlen($bcclist[$i]) > 0 ) 
				$mail->AddBCC($bcclist[$i], $bcclist[$i]);
		}
		
		if(!$mail->Send()) { $croncls->update_cron_log("8", $numberoforders, "Mail error" ); } 
		else { $croncls->update_cron_log("8", $numberoforders, "Success!" ); }
		
		$receiptfile = "/tmp/pickupCron/$ordernum.html";
		@unlink($receiptfile);
		
	}
	
?>