<?php
	include "db_creds.php";
	
	class dbconmgr {
	
		var $DB_CON=null;
		
		//Returns database resource/null otherwise
		public function connect() {
			global $DBSERVER, $DBUSER, $DBPWD, $DBNAME;
			
			$link = mysql_connect($DBSERVER, $DBUSER, $DBPWD);
			if (!$link) {
				echo 'Could not connect: ' . mysql_error();
				return;
			}
			//Select the database
			mysql_select_db( $DBNAME, $link) or die("Database doesn't exist: " . mysql_error() );
			$this->DB_CON=$link;
		}
		//RMS OTC Products functions <!-- Zaid Code -->
		
		public function get_otc_stock($product_sku,$station_code,$_productId)
		{
			$read								=	Mage::getSingleton('core/resource')->getConnection('core_read');
			
			$get_listing_stock					=	"SELECT stock FROM rms_otc_listing WHERE item_name='$_productId' AND station_code='$station_code'";
			// get item_name
			$listing_stock						=	$read->fetchOne($get_listing_stock);
			
			if($listing_stock == NULL)
			{
				$get_item_name					=	"SELECT item_name FROM rms_product_mapping WHERE product_id='$_productId'";
				// get item_name
				$item_name						=	$read->fetchOne($get_item_name);
				
				$get_stock						=	"SELECT stock FROM rms_otc_listing WHERE station_code='$station_code' AND item_name='$item_name' ";
				//get_stock product stock
				$stock							=	$read->fetchOne($get_stock);
				
				return array("stock"=>$stock,"item_name"=>$item_name);
			}
			else
			{
				return array("stock"=>$listing_stock,"item_name"=>$item_name);
			}
			
		}
		public function otc_insert_update($qty,$product_sku,$station_code,$order_id,$product_name,$order_date_time,$productId)
		{
			$get_stock_arr				=	$this->get_otc_stock($product_sku,$station_code,$productId);
			$get_stock					=	$get_stock_arr['stock'];
			$get_item					=	$get_stock_arr['item_name'];
			
			if(isset($get_item))
			{
			    $productId	=	$get_item;	
			}
			
			
			$product_final_qty		=	($get_stock)-($qty);
			
			// update data in listing table
			$sql = "UPDATE rms_otc_listing set stock='$product_final_qty' WHERE item_name='$productId' AND station_code='$station_code' LIMIT 1";
			$result = $this->runquery($sql);
			
			// insert data in reporting table
			$sql_update = "INSERT INTO rms_otc_report (station_code,order_id,product_name,sku,quntity,order_date_time) VALUES('$station_code', '$order_id', '$product_name', '$product_sku', '$qty', '$order_date_time')";
			
			$result = $this->runquery($sql_update);
			return mysql_insert_id();
		
		}
		
		public function get_express_center_name($station_code)
		{
			$read							=	Mage::getSingleton('core/resource')->getConnection('core_read');
			$get_express_center_name		=	"SELECT ecname FROM expresscenter WHERE station_code=$station_code LIMIT 1";
			$express_center_name			=	$read->fetchOne($get_express_center_name);
			return 	$express_center_name;
		}
	 	
		public function express_center_exist($station_code) {
			$sql = "SELECT station_code FROM expresscenter WHERE station_code='$station_code' ";
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 )
			 { 
				return true; 
			}
			else
			{
					//return "express center ".$station_code." not exist";
					return "0";
			}
			
		}
		
		//RMS OTC Products functions <!-- Zaid Code -->
		
		public function doesExpresscenteralreadyexists( $address, $telephone, $fax, $regioncode) {
			$sql = "SELECT eccode FROM expresscenter WHERE address='$address' AND telephone='$telephone' AND fax='$fax' AND regioncode='$regioncode'";
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 ) { return true; }
			return false;
		}
		
		public function addExpresscenter($ecadmin, $address, $regioncode, $pwd, $ecname,$ecarea, $ecstation, $exptype, $tcs_eccode, $station_code) { 
			$sql = "INSERT INTO expresscenter (ecadmin,address,regioncode,pwd,ecname,ecarea,ecstation,exptype,tcs_eccode,station_code) VALUES('$ecadmin', '$address', '$regioncode', '$pwd', '$ecname', '$ecarea', '$ecstation', '$exptype', '$tcs_eccode', '$station_code')";
			
			$result = $this->runquery($sql);
			return mysql_insert_id();
		}
		
		public function assignEcusertoEc($username, $eccode) {
			$sql = "UPDATE expresscenter set ecadmin='$username' WHERE eccode=$eccode LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		//public function which executes the query
		public function runquery($Sql) {
			if( $this->DB_CON != null ) {
				$result = mysql_query($Sql, $this->DB_CON);
				
				if( !$result) { 
					return null; 
				}
				return $result;
			}
		}
		
		public function getEccode($userid) {
			$ret = "";
			$sql = "SELECT eccode FROM expresscenter where ecadmin='$userid' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result != null ) { 
				if( ($row=mysql_fetch_array($result, MYSQL_BOTH)) !== FALSE ) {
					return $row['eccode'];
				}
			}
			return "";
		}
		
		//public function which retrieves information about the user based on provided user id
		public function getECInformation($eccode) {
			$ret = array();
			$Sql = "SELECT * FROM expresscenter WHERE ecadmin='$eccode' LIMIT 1";
			$result = $this->runquery($Sql);
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function updateECInformation($eccode, $ecadmin, $address, $telephone,$fax,$regioncode,$pwd) { 
			$sql = "UPDATE expresscenter SET ecadmin='$ecadmin', address='$address', telephone='$telephone', fax='$fax', regioncode='$regioncode', pwd='$pwd' WHERE eccode=$eccode LIMIT 1";
			$this->runquery($sql);
		}
		
		public function getECInformationByEcID($eccode) {
			$ret = array();
			$Sql = "SELECT * FROM expresscenter WHERE eccode='$eccode' LIMIT 1";
			$result = $this->runquery($Sql);
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		
		//public function which marks transaction as printed
		public function markPrinted($transactionid) {
			$sql = "UPDATE transactions SET dsrprinted=1, dsrprintdate=NOW() WHERE transactionid=$transactionid AND dsrprinted=0 LIMIT 1";
			$this->runquery($sql);
		}
		
		public function markAdsrPrinted($transactionid) {
			$sql = "UPDATE transactions SET adsrprinted=1, adsrprintdate=NOW() WHERE transactionid=$transactionid AND adsrprinted=0 LIMIT 1";
			$this->runquery($sql);
		}
		
		public function markNdsrPrinted($transactionid) {
			$sql = "UPDATE transactions SET ndsrprinted=1, ndsrprintdate=NOW() WHERE transactionid=$transactionid AND ndsrprinted=0 LIMIT 1";
			$this->runquery($sql);
		}
		
		public function getReportPrintDates($reporttype,$fromdate,$todate) {
			$ret = array();
			
			if( $reporttype == "dsr") {
				$sql = "SELECT DISTINCT(DATE(dsrprintdate)) as rptdate FROM transactions WHERE DATE(dsrprintdate)  >= '$fromdate' AND DATE(dsrprintdate) <= '$todate' ORDER BY dsrprintdate";
			}
			else if( $reporttype == "adsr" ) {
				$sql = "SELECT DISTINCT(DATE(adsrprintdate)) as rptdate FROM transactions WHERE DATE(adsrprintdate) >= '$fromdate' AND DATE(adsrprintdate) <= '$todate' ORDER BY adsrprintdate";
			}
			else if( $reporttype == "ndsr") { 
				$sql = "SELECT DISTINCT(DATE(ndsrprintdate)) as rptdate FROM transactions WHERE DATE(ndsrprintdate) >= '$fromdate' AND DATE(ndsrprintdate) <= '$todate' ORDER BY ndsrprintdate";
			}
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { 
				$ret[] = $row;
			}
			return $ret;
		}
		
		//public function checkIfReport  is printed
		public function isReportPrinted($eccode, $reporttype, $reportdate) {
			$sql = "SELECT * FROM reportsPrinted WHERE eccode='$eccode' AND reporttype='$reporttype' AND DATE(NOW())='$reportdate' LIMIT 1";
			$result = $this->runquery($sql);
			
			if( mysql_num_rows($result) > 0 ) {
				if( $row = mysql_fetch_array($result, MYSQL_BOTH)) { return $row; }
			}
			return array();
		}
		
		public function markReportPrinted($eccode, $reporttype, $printedby) {
			$sql = "INSERT INTO reportsPrinted (eccode,reporttype,printedon,printedby) VALUES($eccode,'$reporttype',NOW(),'$printedby')";
			$reult = $this->runquery($sql);
		}
		
		public function getDataForADSR($eccode,$reportdate) {
			if( $reportdate != null) {
				$sql = "SELECT * FROM transactions WHERE eccode=$eccode AND DATE(orderdate)='$reportdate' ORDER BY orderdate ASC";
			}
			else {
				$sql = "SELECT * FROM transactions WHERE eccode=$eccode AND DATE(orderdate) =DATE(NOW()) ORDER BY orderdate ASC";
			}
			
			$ret = array();
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}
			return $ret;
		}
		
		public function getNdsrReport($reportdate, $eccode) {
			$sql = "SELECT transactionid,ordernum,orderamount,paymentts,approved_by, org_txnid FROM transactions WHERE DATE(paymentts) ='$reportdate' AND eccode=$eccode";
			$ret = array();
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 ) { 
				while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { $ret[] = $row; }
			}
			else {
				$sql = "SELECT transactionid,ordernum,orderamount,paymentts,approved_by, org_txnid FROM transactions WHERE DATE(ndsrprintdate) ='$reportdate' AND eccode=$eccode AND ndsrprinted=1";
				
				$result = $this->runquery($sql);
				while($row = mysql_fetch_array($result, MYSQL_BOTH) ) { $ret[] = $row; }
			}
			return $ret;
		}
				
		public function getAdsrReport($reportdate, $eccode) {
			//$sql = "SELECT transactionid,ordernum,orderamount,paymentts, approved_by, org_txnid FROM transactions WHERE DATE(paymentts) <='$reportdate' AND eccode=$eccode AND adsrprinted=0";
			$sql = "SELECT transactionid,ordernum,orderamount,paymentts, approved_by, org_txnid FROM transactions WHERE DATE(paymentts) ='$reportdate' AND eccode=$eccode";
			
			$ret = array();
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 ) { 
				while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { $ret[] = $row; }
			}
			else {
				$sql = "SELECT transactionid,ordernum,orderamount,paymentts,approved_by,org_txnid FROM transactions WHERE DATE(adsrprintdate) ='$reportdate' AND eccode=$eccode AND adsrprinted=1";
				
				$result = $this->runquery($sql);
				while($row = mysql_fetch_array($result, MYSQL_BOTH) ) { $ret[] = $row; }
			}
			return $ret;
		}
		
		public function getNdsrPrintDates($fromdate,$todate) {
			//$sql = "SELECT DISTINCT(DATE(ndsrprintdate)) as rptdate FROM transactions WHERE DATE(ndsrprintdate) >= '$fromdate' AND DATE(ndsrprintdate) <= '$todate' AND ndsrprinted=1 ORDER BY ndsrprintdate DESC";
			$sql = "SELECT DISTINCT(DATE(paymentts)) as rptdate FROM transactions WHERE DATE(paymentts) >= '$fromdate' AND DATE(paymentts) <= '$todate' AND eccode > 0 ORDER BY paymentts DESC";
			
			$ret = array();
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result,MYSQL_BOTH)) {
				$ret[] = $row;
			}
			return $ret;
		}
		
		public function getAdsrDates($fromdate,$todate,$regioncode) {
			$sql = "SELECT DISTINCT(DATE(paymentts)) as rptdate FROM transactions t INNER JOIN expresscenter e ON e.eccode=t.eccode WHERE DATE(paymentts) >= '$fromdate' AND DATE(paymentts) <= '$todate' AND e.regioncode=$regioncode ORDER BY paymentts DESC";
			
			$ret = array();
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result,MYSQL_BOTH)) {
				$ret[] = $row;
			}
			return $ret;
		}
		
		public function getAdsrPrintDates($fromdate, $todate) {
			//$sql = "SELECT DISTINCT(DATE(adsrprintdate)) as rptdate FROM transactions WHERE DATE(adsrprintdate) >= '$fromdate' AND DATE(adsrprintdate) <= '$todate' AND adsrprinted=1 ORDER BY adsrprintdate DESC";
			
			$sql = "SELECT DISTINCT(DATE(paymentts)) as rptdate FROM transactions WHERE DATE(paymentts) >= '$fromdate' AND DATE(paymentts) <= '$todate' ORDER BY paymentts DESC";
			
			$ret = array();
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result,MYSQL_BOTH)) {
				$ret[] = $row;
			}
			return $ret;
		}
		
		public function getECDetailReportForDate($reportdate, $eccode) {
			if( $reportdate != null ) {
				$sql = "SELECT transactionid,ordernum,orderamount,paymentts,org_txnid, approved_by FROM transactions WHERE DATE(paymentts)='$reportdate' AND eccode=$eccode"; 
			}
			else { 
				$sql = "SELECT transactionid,ordernum,orderamount,paymentts, org_txnid, approved_by FROM transactions WHERE DATE(paymentts)=DATE(NOW()) AND eccode=$eccode";
			}
			$ret = array();
			$result = $this->runquery($sql);
			
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) { $ret[] = $row; }
			return $ret;
		}
		
		public function getECDetailReportForDateRange($fromdate, $todate, $eccode) {
			if( $fromdate != null && $todate != null ) {
				$sql = "SELECT transactionid,ordernum,orderamount,paymentts FROM transactions WHERE orderdate >= '$fromdate' AND orderdate <= '$todate' AND eccode=$eccode";
			}
			else {
				$sql = "SELECT transactionid,ordernum,orderamount,paymentts FROM transactions WHERE DATE(orderdate) = DATE(NOW()) AND eccode=$eccode";
			}
				
			$ret = array();
			$result = $this->runquery($sql);
			
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) { $ret[] = $row; }
			return $ret;
		}
		
		public function getDSRReportForDate($reportdate, $eccode) {
			if( $reportdate != null ) {
				$sql = "SELECT COUNT(transactionid) as 'numorders', SUM(orderamount) as 'totalamt' FROM transactions WHERE DATE(orderdate)='$reportdate' AND eccode=$eccode";
			}
			else {
				$sql = "SELECT COUNT(transactionid) as 'numorders', SUM(orderamount) as 'totalamt' FROM transactions WHERE DATE(orderdate)=DATE(NOW()) AND eccode=$eccode";
			}
			$ret = array();
			$result = $this->runquery($sql);
			
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) { $ret[] = $row; }
			return $ret;
		}
		
		public function getDSRReportForDateRange($fromdate, $todate, $eccode) {
			if( $fromdate != null && $todate != null ) {		
				$sql = "SELECT COUNT(transactionid) as 'numorders', SUM(orderamount) as 'totalamt' FROM transactions WHERE orderdate >= '$fromdate' AND orderdate <= '$todate' AND eccode=$eccode";
			}
			else {
				$sql = "SELECT COUNT(transactionid) as 'numorders', SUM(orderamount) as 'totalamt' FROM transactions WHERE DATE(orderdate) = DATE(NOW()) AND eccode=$eccode";
			}
			
			$ret = array();
			$result = $this->runquery($sql);
			
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) { $ret[] = $row; }
			return $ret;
		}
		
		//public function which retrieves data from daily sales report
		public function getDataForDSR($eccode) {
			$ret = array();
			$sql = "SELECT * FROM transactions t INNER JOIN expresscenter e ON e.eccode=t.eccode WHERE e.ecadmin='$eccode' AND dsrprinted=0 ORDER BY orderdate ASC";
			
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getECRegions() { 
			$ret = array();
			$sql = "SELECT * FROM regions";
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) { $ret[] = $row; }
			return $ret;
		}
		
		public function getRegionId($region) {
			$sql = "SELECT regioncode FROm regions WHERE regioncity='$region' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result != null ) {
				return mysql_fetch_array($result,MYSQL_BOTH);
			}
			return array();
		}
		
		public function getECByRegionCode($Regionid) {
			$ret = array();
			$sql = "SELECT * FROM expresscenter WHERE regioncode=$Regionid";
			
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { $ret[] = $row; }
			return $ret;
		}
		
		public function orderExists($ordernum) {
			$sql = "SELECT transactionid FROM transactions WHERE ordernum=$ordernum LIMIT 1";
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 ) { return true; }
			return false;
		}
		
		public function getRegionByRegioncode($regioncode) {
			$sql = "SELECT regioncity FROM regions WHERE regioncode='$regioncode' LIMIT 1";
			$result = $this->runquery($sql);
			if( ($row=mysql_fetch_array($result, MYSQL_BOTH)) != null ) {
				return $row['regioncity'];
			}
		}	
		
		public function getAreaSummaryReport($regioncode, $reportdate) {
			$ret = array();
			
			if( $reportdate != null ) { 
				$sql = "SELECT  e.eccode, e.address, e.telephone, COUNT(transactionid) AS numorders, SUM(orderamount) as totalamt FROM transactions t INNER JOIN expresscenter e ON e.eccode=t.eccode WHERE e.regioncode=$regioncode AND DATE(adsrprintdate) = '$reportdate' GROUP BY e.eccode";
			}
			else {
				$sql = "SELECT e.eccode, e.address, e.telephone, COUNT(transactionid) AS numorders, SUM(orderamount) as totalamt FROM transactions t INNER JOIN expresscenter e ON e.eccode=t.eccode WHERE e.regioncode=$regioncode AND DATE(adsrprintdate)=DATE(NOW())  GROUP BY e.eccode";
			}
			
			$result = $this->runquery($sql);
			
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}
			
			return $ret;
		}
		
	
		//public function which saves the order information in the database
		public function saveTransaction($ordernum, $orderdate, $customername,$billingaddress, $shippingaddress, $paymentmethod, $bankname, $branchname, $eccode,$paymentlocation, $dsrprinted, $orderamount, $orgtxnid, $approved_by) {
			$sql = "SELECT * FROM transactions where ordernum='$ordernum'";		
			$result = $this->runquery($sql);			
			$row = mysql_fetch_array($result, MYSQL_BOTH);
			
			if( count($row) > 1 ) {
				return;
			}
		
			$sql = "INSERT INTO transactions( ordernum,orderdate,customername,billingaddress,shippingaddress,paymentmethod,bankname,branchname,eccode,paymentlocation,dsrprinted,paymentts,orderamount, org_txnid,approved_by) VALUES( '$ordernum', '$orderdate','$customername', '$billingaddress', '$shippingaddress','$paymentmethod', '$bankname', '$branchname', '$eccode', '$paymentlocation', '$dsrprinted', NOW(),$orderamount, '$orgtxnid', '$approved_by')";
			
			$this->runquery($sql);
			
			if( $eccode <> 0 && $eccode <> "")
				$this->update_order_ec_pm_area($eccode, $ordernum);
		}
		
		public function order_confirmed_by( $ordernum, $managerlogin ) {
			$sql = "UPDATE transactions SET flagged_by='$managerlogin' WHERE ordernum='$ordernum' LIMIT 1";
			$this->runquery($sql);
		}
		
		public function update_order_ec_pm_area($eccode, $ordernum) {
			$sql = "SELECT ecarea FROM expresscenter WHERE eccode='$eccode' LIMIT 1";
			$result = $this->runquery($sql);			
			$row = mysql_fetch_array($result, MYSQL_BOTH);
			
			if( count($row) < 2 ) {
				return;
			}
			
			$pm_area = $row['ecarea'];
			
			$sql = "UPDATE transactions set pm_area='$pm_area' WHERE ordernum='$ordernum' LIMIT 1";
			$result = $this->runquery($sql);			
		}
		
		//public function for dsr report
		public function getDSRReport($reportdate, $eccode) {
			$sql = "SELECT * FROM transactions WHERE DATE(dsrprintdate)='$reportdate' AND eccode='$eccode' ORDER BY orderdate DESC";
			
			$ret = array();
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}
			return $ret;
		}

		public function getNationalSummaryReport($reportdate,$eccode) {
			$sql = "SELECT count(transactionid) as numorders, SUM(orderamount) as totalamt FROM transactions WHERE DATE(ndsrprintdate)='$reportdate' AND eccode='$eccode' ORDER BY orderdate DESC";
			
			$ret = array();
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}
			return $ret;
		}
		
		public function getSummaryDSRReport($reportdate,$eccode) {
			$sql = "SELECT count(transactionid) as numorders, SUM(orderamount) as totalamt FROM transactions WHERE DATE(dsrprintdate)='$reportdate' AND eccode='$eccode' ORDER BY orderdate DESC";
			
			$ret = array();
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}
			return $ret;
		}
		
		public function getUnusedConsignmentNumber() {
			$sql = "SELECT id, consignmentnumber FROM consignment_numbers WHERE number_used=0 LIMIT 1";
			$ret = array();
			$result = $this->runquery($sql);
			if ( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				return $row;
			} 
			return array();
		}
		
		public function markConsignmentNumberAsUsed($consignment_id) {
			$sql = "UPDATE consignment_numbers SET number_used=1 WHERE id=$consignment_id LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function checkOrderConsignmentNumberExists($order_id) {
			$sql = "SELECT order_id FROM order_consignment_nos WHERE order_id='$order_id'";
			$result = $this->runquery($sql);
			
			if( mysql_num_rows($result) > 0 ) { return true; }
			return false;
		}
		
		public function getIncrementOrder($entity_id) {
			$sql = "SELECT increment_id FROM sales_flat_order WHERE entity_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);
			
			if( mysql_num_rows($result) > 0 ) { $row = mysql_fetch_array($result, MYSQL_BOTH); return $row['increment_id']; }
			return null;
		}
		
		public function assignOrderConsignmentNumber($order_id, $consignmentnumber) {
			$sql = "INSERT INTO order_consignment_nos (order_id, consignmentnumber, date_used) VALUES( '$order_id', '$consignmentnumber', NOW())";
			$result = $this->runquery($sql);
		}
		
		public function getOrderConsignmentMap() {
			$sql = "SELECT * FROM order_consignment_nos ORDER BY date_used DESC";
			$result = $this->runquery($sql);
			$ret = array();
			
			while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { $ret[] = $row; }
			return $ret;
		}
		
		public function getNumUsersByRegioncode($regioncode) {
			$sql = "SELECT COUNT(eccode) AS numusers FROM expresscenter WHERE regioncode=$regioncode";
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $row = mysql_fetch_array($result, MYSQL_BOTH)) { return $row; }
			return array();
		}
		
		
		public function get_cn_number($increment_id)
		{
			$read					=	Mage::getSingleton('core/resource')->getConnection('core_read');
			$get_cn_number_query	=	"SELECT cnnumber FROM oms_transactions WHERE orderid = $increment_id LIMIT 1";
			$cn_number				=	$read->fetchOne($get_cn_number_query);
			return 	$cn_number;
		}
		
		public function getOrderConsignmentNumber($incrementid) {
			$sql = "SELECT consignmentnumber FROM order_consignment_nos WHERE order_id='$incrementid' LIMIT 1";
			$result = $this->runquery($sql);
			
			if( $row = mysql_fetch_array($result, MYSQL_BOTH)) { return $row; }
			return array();
		}
		
		public function getTransactionByOrderID($incrementid) {
			$sql = "SELECT *, DATEDIFF(NOW(),paymentts) AS 'lag' FROM transactions where ordernum='$incrementid' LIMIT 1";
			$result = $this->runquery($sql);
			if( $row = mysql_fetch_array($result, MYSQL_BOTH)) { return $row; }
			return array();
		}
		// Move function to ordersforward helper function
		public function getVendor($vendorid) {
			$sql = "SELECT vendor_name,email,street,city,zip,telephone FROM udropship_vendor WHERE vendor_id=$vendorid LIMIT 1";
			$result = $this->runquery($sql);
			if( $row = mysql_fetch_array($result, MYSQL_BOTH)) { return $row; }
			return array();
		}
		
		public function addTransaction($orderid, $cnnumber,$loggeduid,$pcs,$couriercode,$bookingtimestamp,$servicetype,$routecode,$tcsaccount,$insurancecode,$modeofpayment,$weight) {
			$sql = "INSERT INTO oms_transactions(orderid,cnnumber,loggeduid,pcs,couriercode,bookingtimestamp,servicetype,routecode,tcsaccount,insurancecode,senttooms,modeofpayment,weight) VALUES('$orderid', '$cnnumber','$loggeduid', '$pcs','$couriercode', '$bookingtimestamp','$servicetype', '$routecode', '$tcsaccount', '$insurancecode', 0,'$modeofpayment', '$weight')";
			$result = $this->runquery($sql);
			
			//Get the last insert id
			return mysql_insert_id();
		}
		
		public function addTransactionProducts($ordercnid, $sku, $quantity) {
			$sql = "INSERT INTO oms_transaction_products (ordercnid,sku,quantity) VALUES('$ordercnid', '$sku', '$quantity')";
			$result = $this->runquery($sql);
		}
		
		public function editTransaction($ordercnid, $cnnumber,$loggeduid,$pcs,$couriercode,$bookingtimestamp,$servicetype,$routecode,$tcsaccount,$insurancecode,$modeofpayment,$weight) {
			$sql = "UPDATE oms_transactions set cnnumber='$cnnumber',loggeduid='$loggeduid',pcs='$pcs',couriercode='$couriercode',bookingtimestamp='$bookingtimestamp',servicetype='$servicetype',routecode='$routecode',tcsaccount='$tcsaccount', insurancecode='$insurancecode',modeofpayment='$modeofpayment',weight='$weight' WHERE ordercnid='$ordercnid' LIMIT 1";
			
			$result = $this->runquery($sql);
		}
		
		public function getTransactionProducts($ordercnid) {
			$sql = "SELECT * FROM oms_transaction_products WHERE ordercnid='$ordercnid'";
			$result = $this->runquery($sql);
						
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}	
			return $ret;
		}
		
		public function deleteTransactionProducts($ordercnid) {
			$sql = "DELETE FROM oms_transaction_products WHERE ordercnid='$ordercnid'";
			$result = $this->runquery($sql);
		}
		
		public function deleteOmsTransaction($ordercnid) {
			$sql = "DELETE FROM oms_transactions where ordercnid='$ordercnid' AND senttooms=0 LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getOmsTransactionById($ordercnid) {
			$sql = "SELECT * FROM oms_transactions where ordercnid='$ordercnid' LIMIT 1";
			$result = $this->runquery($sql);
			$row = mysql_fetch_array($result,MYSQL_BOTH);
			
			return $row;
		}
		
		public function getOmsTransaction($orderid) {
			$sql = "SELECT * FROM oms_transactions WHERE orderid='$orderid' ORDER BY bookingtimestamp DESC";
			$result = $this->runquery($sql);
			
			while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { $ret[] = $row; }
			return $ret;
		}
		
		public function isCNUnique($cnnumber) {
			$sql = "SELECT ordercnid FROM oms_transactions WHERE cnnumber='$cnnumber'";
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 ) return 0;
			return 1;
		}
		
		public function getOmsTransactionProductsByOrderID($orderid) {
			$sql = "SELECT otp.* FROM oms_transaction_products otp INNER JOIN oms_transactions ot ON ot.ordercnid=otp.ordercnid WHERE ot.orderid='$orderid'";
			
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}	
			return $ret;	
		}
		
		public function isOrderPaid($orderid) {
			$sql = "SELECT status FROM sales_flat_order WHERE increment_id='$orderid' AND (status='paid' or status='complete' or status='received' or status='cod' or status='holded')";
			$result = $this->runquery($sql);
			
			if( mysql_num_rows($result) > 0  ) return 1;
				return 0;
		}
		
		public function checkOrderSkuExists($ordercnid, $sku) {
			$sql = "SELECT * from oms_transaction_products WHERE ordercnid='$ordercnid' AND sku='$sku'";
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 ) return 1;
			return 0;
		}
		
		public function updateOrderSkuQuantity( $ordercnid, $sku) {
			$sql = "UPDATE oms_transaction_products SET quantity=quantity+1 WHERE ordercnid='$ordercnid' AND sku='$sku' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function updateCashCCFlag($increment_id, $flag) {
			$sql = "UPDATE sales_flat_order SET ccvscash='$flag' WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getCashCCFlag($increment_id) {
			$sql = "SELECT ccvscash FROM sales_flat_order WHERE increment_id='$increment_id' LIMIT 1";
			
			@file_put_contents("/tmp/pickupemail.log", $sql);
			
			$result = $this->runquery($sql);
			if( $result == null ) return "";
			else
				return  mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public function getAdditionalPaymentInfo($increment_id) {
			$sql = "SELECT additionalpaymentinfo FROM sales_flat_order WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return "";
			else
				return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		// Move function to ordersforward helper function
		public function getConfigConstant($config_name) {
			$sql = "SELECT * FROM config_constants WHERE config_name='$config_name' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			else
				return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public function getPaymentmethod($order_id) {
			$sql ="SELECT method FROM sales_flat_order_payment WHERE parent_id=$order_id LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			else
				return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public function getRealOrderId($increment_id) {
			$sql = "SELECT entity_id FROM sales_flat_order WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			else
				return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public function updatePaymentmethod($order_id, $newmethod) {
			$sql = "UPDATE sales_flat_order_payment SET method='$newmethod' WHERE parent_id='$order_id' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		
		
		public function addrso($rsoname, $username, $password, $role) {
			$sql = "INSERT INTO tcs_ec_user (rso_name,rso_mobile,rso_pin,active, role) VALUES('$rsoname', '$username', md5('$password'),1, '$role')";
			$result = $this->runquery($sql);
		}
		
		public function getOrderAddressDetails($entity_id) {
			$sql = "SELECT * FROM sales_flat_order_address WHERE parent_id='$entity_id' AND address_type='billing' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			else
				return mysql_fetch_array($result, MYSQL_BOTH);
		}

		public function getUnpaidDoorstepOrders() {
			$sql = "SELECT entity_id, increment_id,additionalpaymentinfo FROM sales_flat_order WHERE additionalpaymentinfo like '%door%' AND (status='pending' OR status='processing') AND lm_notified=0";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}	
			return $ret;
			}
			
		/*public function getPaymentMethod($parent_id) {
			$sql = "SELECT method FROM sales_flat_order_payment WHERE parent_id=$parent_id LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			return mysql_fetch_array($result, MYSQL_BOTH);
		}*/	
		
		public function updateLMNotified($ordernumber) {
			$sql = "UPDATE sales_flat_order SET lm_notified=1 WHERE increment_id='$ordernumber' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function addservicearea($cityname,$citycode,$area,$dp_hub,$regionid) {
			$sql = "INSERT INTO tcs_service_area_cities (cityname,citycode,area,dp_hub,regionid) VALUES('$cityname','$citycode','$area','$dp_hub', $regionid)";
			$this->runquery($sql);
		}
		
		public function importcities($cityname, $zone) {
			$sql = "INSERT INTO tcs_service_area_cities (cityname,zone, citycode,area, dp_hub, regionid) VALUES('$cityname', '$zone','','','',0)";
			$result = $this->runquery($sql);
			return mysql_insert_id();
		}
		
		public function isCityExists($cityname) {
			$sql = "SELECT count(servicearea_id) as valid FROM tcs_service_area_cities WHERE cityname='$cityname'";
			$result = $this->runquery($sql);
			if( $result == null ) { return 0; }
			$row = mysql_fetch_array($result,MYSQL_BOTH);
			return $row['valid'];
		}
		
		public function getShippingCity($cityname) {
			$sql = "SELECT * FROM tcs_service_area_cities WHERE cityname LIKE '$cityname%'";
			$result = $this->runquery($sql);
			$ret = array();
			
			while( $row = mysql_fetch_array($result,MYSQL_BOTH) ) { $ret[] = $row; }
			return $ret;
		}
		
		public function getCities() {
			$ret = array();
			$ret[] = "Karachi"; $ret[] = "Lahore"; $ret[] = "Islamabad"; $ret[] = "Hyderabad"; $ret[] = "Sukkur";
			$ret[] = "Quetta"; $ret[] = "Multan"; $ret[] = "Faisalabad"; $ret[] = "Gujranwala"; $ret[] = "Peshawar";
			
			$sql = "SELECT * FROM tcs_service_area_cities WHERE cityname NOT IN ('Karachi','Lahore','Islamabad','Hyderabad','Sukkur','Quetta','Multan','Faisalabad','Gujranwala','Peshawar') ORDER BY cityname ASC";
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result,MYSQL_BOTH) ) {
				$ret[] = ucwords(strtolower($row['cityname']));
			}	
			return $ret;
		}
		
		public function setadminorder($entity_id, $method, $additionalpaymentinfo) {
			$sql = "UPDATE sales_flat_order SET additionalpaymentinfo='$additionalpaymentinfo' WHERE entity_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);
			
			$sql = "UPDATE sales_flat_order SET method='$method' WHERE parent_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getTodayCNOrders($bookingdate) {
			$sql = "SELECT * FROM oms_transactions WHERE DATE(bookingtimestamp)=DATE('$bookingdate')";
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result,MYSQL_BOTH) ) { $ret[] = $row; }
			return $ret;
		}
		
		public function getCityZone($cityname) {
			$sql = "SELECT zone FROM tcs_service_area_cities WHERE LOWER(cityname)=LOWER('$cityname') LIMIT 1";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				return $row['zone'];
			}
			return "";
		}
		
		public function getOvernightrates($weight) {
			$sql = "SELECT * FROM tcs_overnight_rates where weight=$weight LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			
			return $row;
		}
		
		public function getSamedayrates($weight) {
			$sql = "SELECT * FROM tcs_sameday_rates WHERE weight=$weight LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}
		
		public function flagOrders($ordernumber, $flag) {
			$sql = "UPDATE transactions SET flagged=$flag, dateconfirmed=NOW() where ordernum='$ordernumber' LIMIT 1";
			$this->runquery($sql);
		}
		
		public function getHomePaymentsTransactions() {
			$sql = "select * FROM transactions WHERE date(paymentts) = date(date_sub(NOW(), INTERVAL 2 DAY)) AND reported=0 AND paymentlocation LIKE 'door%'";
			$result = $this->runquery($sql);
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;	
		}
		
		public function checkOrderIsConfirmed($ordernumber) {
			$sql = "SELECT flagged FROM transactions WHERE ordernum='$ordernumber' LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row['flagged'];
		}
		
		public function getShippingmethod($ordernumber) {
			$sql = "SELECT shipping_method FROM sales_flat_order WHERE increment_id='$ordernumber' LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			
			if( $result != null )
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			return $row['shipping_method'];
		}
		
		public function updateShipping($increment_id, $method, $cost) {
			$sql = "UPDATE sales_flat_order SET shipping_method='$method', shipping_amount=$cost WHERE increment_id='$increment_id' LIMIT 1";
			$this->runquery($sql);
		}
		
		public function updateShippingDescription($entity_id, $shipping_description) {
			$sql = "UPDATE sales_flat_order SET shipping_description='$shipping_description' WHERE entity_id=$entity_id LIMIT 1";
			$this->runquery($sql);
		}
		
		public function getShippingAmount($ordernumber) {
			$sql = "SELECT shipping_amount FROM sales_flat_order WHERE increment_id='$ordernumber' LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			
			if( $result != null )
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			return $row['shipping_amount'];
		}
		
		public function getPaymentAreaId($areacode) {
			$sql = "SELECT * FROM tcs_paymentareas WHERE areacode='$areacode' LIMIT 1";
			$result = $this->runquery($sql);
			
			$ret = "0";
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['paymentareaid'];
			}	
			return $ret;
		}
		
		public function addCityAreaMap($cityid, $areaid) {
			$sql = "INSERT INTO tcs_pmarea_city (cityid, pmarea) VALUES($cityid, $areaid)";
			$result = $this->runquery($sql);
		}
		
		public function getAreaManagerAreaCode($managerlogin) {
			$ret = 0;
			$sql = "SELECT areacode FROM tcs_paymentareas WHERE managerlogin='$managerlogin' LIMIT 1";
			
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row =mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['areacode'];
			}
			return $ret;
		}
		
		public function getAreaManagerAreaId( $managerlogin ) {
			$ret = 0;
			$sql = "SELECT paymentareaid FROM tcs_paymentareas WHERE managerlogin='$managerlogin' LIMIT 1";
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row =mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['paymentareaid'];
			}
			return $ret;
		}
		
		public function getBillingCity($orderid) {
			$ret = "";
			$sql = "SELECT city FROM sales_flat_order_address WHERE address_type='billing' AND parent_id=$orderid LIMIT 1";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['city'];
			}
			return $ret;
		}
		
		public function getCityAreaId($cityname) {
			$ret = 0;
			$sql = "SELECT pmarea FROM tcs_pmarea_city pc INNER JOIN tcs_service_area_cities sac ON sac.servicearea_id=pc.cityid WHERE LCASE(sac.cityname)=LCASE('$cityname')";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['pmarea'];
			}
			return $ret;
		}
		
		public function flaggedForLong($ordernumber) {
			$sql = "SELECT transactionid FROM transactions where flagged=1 AND ordernum='$ordernumber' AND DATE(date_add(dateconfirmed,INTERVAL 1 HOUR)) >= DATE(NOW())";
			$result = $this->runquery($sql);
			if( mysql_num_rows($result) > 0 ) { return 0; }
			return 1;
		}
		
		public function getPaymentManagers() {
			$sql = "SELECT * FROM tcs_paymentareas";
			$result = $this->runquery($sql);
			$ret = array();
			$row = array();
			
			if( $result != null ) { 
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getOrdersForDiscrepancyReport() {
			// $sql = "SELECT DISTINCT(t.transactionid) FROM transactions t  INNER JOIN sales_flat_order sfo ON t.ordernum=sfo.increment_id INNER JOIN sales_flat_order_address sfoa ON sfo.entity_id=sfoa.parent_id INNER JOIN tcs_service_area_cities tsac ON sfoa.city=tsac.cityname INNER JOIN tcs_pmarea_city pc ON pc.cityid=tsac.servicearea_id INNER JOIN tcs_paymentareas pa ON pa.paymentareaid=pc.pmarea where DATE(t.paymentts) < DATE(NOW()) AND t.flagged=0 AND sfo.status <> 'canceled' AND sfo.status <> 'payment_review' AND sfo.status <> 'pending' AND sfo.status <> 'processing' AND sfo.additionalpaymentinfo IS NOT NULL";
			$sql = "SELECT DISTINCT(t.transactionid) FROM transactions t INNER JOIN sales_flat_order sfo ON t.ordernum=sfo.increment_id INNER JOIN sales_flat_order_address sfoa ON sfo.entity_id=sfoa.parent_id where DATE(t.paymentts) <= DATE(NOW()) AND t.flagged=0 AND sfo.status <> 'canceled' AND sfo.status <> 'payment_review' AND sfo.status <> 'pending' AND sfo.status <> 'processing' AND sfo.additionalpaymentinfo IS NOT NULL";
			
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result !=null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH)) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getTransactionDetail($transactionid) {
			$sql = "SELECT t.pm_area, pa.areacode,pc.pmarea, sfoa.city, t.transactionid, t.ordernum,t.orderdate,t.paymentts, t.orderamount,sfop.method, sfo.additionalpaymentinfo,sfop.amount_ordered, sfo.order_currency_code,sfo.base_grand_total FROM transactions t  INNER JOIN sales_flat_order sfo ON t.ordernum=sfo.increment_id INNER JOIN sales_flat_order_address sfoa ON sfo.entity_id=sfoa.parent_id INNER JOIN tcs_service_area_cities tsac ON sfoa.city=tsac.cityname INNER JOIN tcs_pmarea_city pc ON pc.cityid=tsac.servicearea_id INNER JOIN tcs_paymentareas pa ON pa.paymentareaid=pc.pmarea INNER JOIN sales_flat_order_payment sfop ON sfo.entity_id=sfop.parent_id where DATE(t.paymentts) < DATE(NOW()) AND t.transactionid='$transactionid' LIMIT 1;";
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;
		}
		
		public function getInvoiceCreateDate($order_id) {
			$sql = "SELECT created_at FROM sales_flat_invoice where order_id='$order_id' LIMIT 1";
			$ret = "";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['created_at'];
			}
			return $ret;
		}
		// Move function to ordersforward helper function
		public function checkCustomerCreditWasUsed($order_id) {
			$sql = "SELECT * FROM customercredit_credit_log where order_id=$order_id";
			$row = array();
			$result = $this->runquery($sql);
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}
		
		public function getVendorByOrderId($order_id) {
			$ret = "";
			try {
				$orderObj=Mage::getModel('sales/order')->loadByIncrementId($order_id);
				foreach ($orderObj->getAllItems() as $item) {
					$productvendorid = $item->getData('udropship_vendor');
					$vendor = $this->getVendor($productvendorid);
					$ret .= $vendor['vendor_name'] . " (" . $vendor['city'] . ")<BR>";
				}
			} catch(Exception $e) { $ret = $e->getMessage(); }
			return $ret;
		}
		
		public function getShippingrates($quote_id) {
			$row = array();
			$sql = "SELECT grand_total,subtotal, base_subtotal_with_discount, base_grand_total, base_subtotal,quote_currency_code,shipping FROM sales_flat_quote where entity_id='$quote_id' LIMIT 1";
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}
		
		public function getCompleteOrdersForPLR($startdate, $enddate) {
			//$sql = " select o.entity_id, o.increment_id, o.status, o.grand_total,o.shipping_amount,o.additionalpaymentinfo,i.created_at FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE o.status IN ('paid', 'complete', 'received') AND DATE(i.created_at) >= '$startdate' AND DATE(i.created_at) <= '$enddate'";
			//$sql = "Select distinct(o.entity_id) FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE o.status IN ('paid', 'complete', 'received') AND DATE(i.created_at) >= '$startdate' AND DATE(i.created_at) <= '$enddate' ORDER BY o.entity_id";
			
			// $sql ="Select distinct(o.entity_id) FROM sales_flat_order o WHERE o.status IN ('paid', 'complete', 'received') AND DATE(o.created_at) >= '$startdate' AND DATE(o.created_at) <= '$enddate' ORDER BY o.entity_id";
			
			// $sql ="Select o.entity_id, o.increment_id, o.status, o.grand_total,o.shipping_amount,o.additionalpaymentinfo,o.created_at, o.order_currency_code, o.customer_firstname, o.customer_lastname, o.total_paid FROM sales_flat_order o WHERE o.status IN ('paid', 'complete', 'received') AND DATE(o.created_at) >= '$startdate' AND DATE(o.created_at) <= '$enddate' ORDER BY o.entity_id";
			
			$sql = "Select o.entity_id, o.increment_id, o.status, o.grand_total,o.shipping_amount,o.additionalpaymentinfo,i.created_at, o.order_currency_code, o.customer_firstname, o.customer_lastname, o.total_paid, o.base_shipping_amount FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE o.status != 'canceled' AND DATE(i.created_at) >= '$startdate' AND DATE(i.created_at) <= '$enddate' ORDER BY o.entity_id";
			
			$sql = "Select DISTINCT(o.entity_id) FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE o.status != 'canceled' AND DATE(i.created_at) >= '$startdate' AND DATE(i.created_at) <= '$enddate' ORDER BY o.entity_id";
			
			$ret = array();
			$result = $this->runquery($sql);
			if( $result !=null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH)) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getPLROrderDetailsByEntityId($entity_id) {
			$sql = "Select o.entity_id, o.increment_id, o.status, o.grand_total,o.shipping_amount,o.additionalpaymentinfo,i.created_at, o.order_currency_code, o.customer_firstname, o.customer_lastname, o.total_paid, o.base_shipping_amount FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE o.entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;
		}
		
		public function getOrderDetailsForPLR($orderid) {
			$sql = " select o.entity_id, o.increment_id, o.status, o.grand_total,o.shipping_amount,o.additionalpaymentinfo,i.created_at FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE i.order_id=$orderid LIMIT 1";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;
			
		}
		
		public function getInvoiceTotal($orderid) {
			$sql = "SELECT grand_total FROM sales_flat_invoice WHERE order_id=$orderid LIMIT 1";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $ret['grand_total'];
			}
			return $ret;
		}
		
		public function getRealPaymentMethodName($method) {
			$ret = "";
			if( $method == 'cash' ) { $ret = "Cash"; }
			else if( $method == 'checkatbank' ) { $ret = "Cheque"; }
			else if( $method == 'internetbanking' ) { $ret = "Net Banking"; }
			else if( $method == 'paypal_standard' ) { $ret = "Paypal"; }
			else if( $method == 'twocheckout_shared' ) { $ret = "2Checkout"; }
			else if( $method == 'creditcardoffline' ) { $ret = "Credit Card Offline"; }
			else if( $method == 'easypaisa' ) { $ret = "Easy Paisa"; }
			else if( $method == 'customercredit' ) { $ret = "Customer Credit"; }
			else if( $method == 'cod' ) { $ret = "Cash On Delivery"; }
			return $ret;
		}
		
		public function getCityId( $cityname ) {
			$sql = "SELECT cityname FROM tcs_service_area_cities WHERE cityname='$cityname' LIMIT 1";
			$ret = "";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['cityname'];
			}
			return $ret;
		}
		
		public function getPMAreaByCityId($cityid) {
			$sql = "select distinct(p.areacode) FROM tcs_paymentareas p INNER JOIN tcs_pmarea_city pc ON pc.pmarea = p.paymentareaid WHERE pc.cityid=$cityid LIMIT 1";
			$ret = "";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['areacode'];
			}
			return $ret;
		}
		
		public function getInsuranceRate($price) {
			$sql = "select rate FROM insurance_rates where from_price <= $price AND to_price >= $price LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return 0;
			$row = mysql_fetch_array($result, MYSQL_BOTH);	
			return $row['rate'];
		}
		
		public function getProductInsuranceAmount($productsku) {
			$_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productsku);	
			
			$insuranceamt = $_product->getResource()->getAttribute('insurance_chartis')->getFrontend()->getValue($_product);
			
			return $insuranceamt;
			
		}
		
		public function getProductDuties($sku) {
			$ret = 0.00;
			
			try {
								
				$_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);	
				
				if( $_product ) {
					$duty_insurance_amount     = str_replace(",", "", $_product->getResource()->getAttribute('duty_insurance_amount')->getFrontend()->getValue($_product));
					
					$duty_incom_tax 	  	   = str_replace(",", "", $_product->getResource()->getAttribute('duty_incom_tax')->getFrontend()->getValue($_product));
					
					$duty_inbond_shipment_cost = str_replace(",", "", $_product->getResource()->getAttribute('duty_inbond_shipment_cost')->getFrontend()->getValue($_product));
					$duty_custom_duty		   = str_replace(",", "", $_product->getResource()->getAttribute('duty_custom_duty')->getFrontend()->getValue($_product));
					$duty_clearing		   	   = str_replace(",", "", $_product->getResource()->getAttribute('duty_clearing')->getFrontend()->getValue($_product));
					$duty_caa_charges		   = str_replace(",", "", $_product->getResource()->getAttribute('duty_caa_charges')->getFrontend()->getValue($_product));
					$duty_additional_sale_tax  = str_replace(",", "", $_product->getResource()->getAttribute('duty_additional_sale_tax')->getFrontend()->getValue($_product));
					$duty_sales_tax			   = str_replace(",", "", $_product->getResource()->getAttribute('duty_sales_tax')->getFrontend()->getValue($_product));
					
					$ret = $duty_insurance_amount + $duty_incom_tax + $duty_inbond_shipment_cost + $duty_custom_duty + $duty_clearing + duty_caa_charges + $duty_additional_sale_tax + $duty_sales_tax;
				}
			} catch(Exception $e) { ; }
			
			return $ret;
		}
		
		public function getCouponcodeByEntityId($entity_id) {
			$sql = "SELECT coupon_code FROM sales_flat_order where entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			
			
			if( $result == null ) return 0;
			$row = mysql_fetch_array($result, MYSQL_BOTH);	
			return $row['coupon_code'];
		}
		
		public function getCodeDiscountByProductIdOrderId($productid, $orderid) {
			$sql = "SELECT base_discount_amount FROM sales_flat_order_item WHERE product_id=$productid AND order_id='$orderid' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return 0;
			
			$row = mysql_fetch_array($result, MYSQL_BOTH);
			return $row['base_discount_amount'];
		}
		
		public function getSpecialPriceDiscount($productid, $productsku, $orderid) {
			$sql = "SELECT base_cost, base_price FROM sales_flat_order_item WHERE product_id='$productid' AND sku='$productsku' AND order_id='$orderid' AND product_type='simple' AND parent_item_id IS NULL LIMIT 1";
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result == null ) return $ret;
			
			return mysql_fetch_array($result, MYSQL_BOTH);	;
			
		}
		
		public function getCoupodecodediscount($entity_id) {
			$sql = "SELECT base_discount_amount FROM sales_flat_order where entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			
			
			if( $result == null ) return 0;
			$row = mysql_fetch_array($result, MYSQL_BOTH);	
			return $row['base_discount_amount'];
		}
		
		public function getInsurance($increment_id) {
			$ret = 0.00;
			try {
				$orderObj=Mage::getModel('sales/order')->loadByIncrementId($increment_id);
				
				foreach ($orderObj->getAllItems() as $item) {
					$productid 	 = $item->getId();
					$productname = $item->getName();
					$price 		 = $item->getPrice();
					$_product	 = Mage::getSingleton('catalog/product')->load($item->getProductId());
					
					$insurance_offered = $_product->getResource()->getAttribute('insurance_offered')->getFrontend()->getValue($_product);
					
					if( $_product->getResource()->getAttribute('insurance_offered')->getFrontend()->getValue($_product) == "User Paid") {
						$ret += $this->getInsuranceRate($price);
					}
					else if( $_product->getResource()->getAttribute('insurance_offered')->getFrontend()->getValue($_product) == "No" ) {
						if( count($orderObj->getAllItems()) == 1 ) {
							return "N/A";
						}
					}
				}
				
			} catch (Exception $e) {  Mage::getSingleton('adminhtml/session')->addError($e->getMessage()); }
			return $ret;
		}
		
		public function getBankCharges($paidtype, $orderamount) {
			$ret = 0.00;
			
			if($paidtype == "paypal_standard") {
				$rate = $this->getConfigConstant('ppcharges');
				$rate = $rate['config_value'];
				$ret  = $orderamount * $rate/100;
			}
			else if( $paidtype == "twocheckout_shared" ) { 
				$rate = $this->getConfigConstant('2cocharges');
				$rate = $rate['config_value'];
				$ret  = $orderamount * $rate/100;
			}
			else if( $paidtype == "cash Bank Deposit- (United Bank Limited)" || $paidtype == "cash Bank Deposit- (Allied Bank Limited)" || 
				     $paidtype == "cash Bank Deposit- (Habib Bank Limited)") {
				$rate = $this->getConfigConstant('bankcharges');
				$rate = $rate['config_value'];
				$ret  = $rate;
			}
			else if( $paidtype == "creditcardoffline CC Offline - Express Center" || $paidtype == "creditcardoffline" ||
					$paidtype == "creditcardoffline CC Offline - Doorstep" ) {
				$rate = $this->getConfigConstant('offlinecc');
				$rate = $rate['config_value'];
				$ret  = $prderamount * $rate/100;		
			}
			else if( $paidtype == "checkatbank Bank Deposit - (United Bank Limited)" || $paidtype == "checkatbank Bank Deposit - (Allied Bank Limited)" ||
					 $paidtype == "checkatbank Bank Deposit - (Habib Bank Limited)") {
				$rate = $this->getConfigConstant('bankcharges');
				$rate = $rate['config_value'];
				$ret  = $rate;	
			}
			else if( $paidtype == "internetbanking" || $paidtype == "internetbanking UBL Net-Banking" || $paidtype == "internetbanking HBL Net-Banking" ||
				$paidtype == "internetbanking ABL Net-Banking" ) {
				$rate = $this->getConfigConstant('bankcharges');
				$rate = $rate['config_value'];
				$ret  = $rate;	
			}
			else if( $paidtype == "easypaisa" ) {
				$rate = $this->getConfigConstant('easypaisacharges');
				$rate = $rate['config_value'];
				$ret  = $rate;	
			}
			else if( $paidtype == "cash Express Center" ) {
				$rate = $this->getConfigConstant('cashateccharges');
				$rate = $rate['config_value'];
				$ret  = $rate;	
			}
			else if( $paidtype == "cash Door-step" ) {
				$rate = $this->getConfigConstant('cashatdoorstepcharges');
				$rate = $rate['config_value'];
				$ret  = $rate;	
			}
			
			else if( $paidtype == "ublomni" ) {
				$rate = $this->getConfigConstant('cashatdoorstepcharges');
				$rate = $rate['config_value'];
				$ret  = $rate;	
			}
				
			return $ret;
		}
		
		public function getOrdersForPickUpEmail() {
			$sql = "SELECT entity_id, increment_id FROM sales_flat_order WHERE status IN ('paid', 'cod')";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { 
					$ret[] = $row ;	
				}
			}
			return $ret;
		}
		
		public function updateOrderPickupFlag($incrementid) {
			$sql = "UPDATE sales_flat_order SET pickup_email=1 WHERE increment_id='$incrementid' LIMIT 1";
			$this->runquery($sql);
		}
		
		public function updateOrderGridVendors($entity_id, $vendors) {
			$sql = "UPDATE sales_flat_order_grid SET vendors='$vendors' WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		
		public function updateSalesFlatOrderGrid($incrementid) {
			$ret = "";
			try {
				$orderObj=Mage::getModel('sales/order')->loadByIncrementId($incrementid);
				foreach ($orderObj->getAllItems() as $item) {
					$productvendorid = $item->getData('udropship_vendor');
					$vendor = $this->getVendor($productvendorid);
					//if vendor not found of product from order grid item then get vendor id by product_id and then update odrer item list
					$product_id = $item->getData('product_id');
					if(empty($vendor)){
						$current_item_id = $item->getData('item_id');
						$updateVendorOrderItemByProduct	=	$this->updateVendorOrderItemByProduct($product_id, $current_item_id);
						$vendor = $this->getVendor($updateVendorOrderItemByProduct);
					}
					
					$ret .= $vendor['vendor_name'] . " (" . $vendor['city'] . "), ";
				}
				$entityid = $orderObj->getId();
				
				$sql = "UPDATE sales_flat_order_grid SET vendors='$ret' WHERE entity_id=$entityid LIMIT 1";
				$result = $this->runquery($sql);

				$sql = 'UPDATE sales_flat_order SET udropship_shipping_details=\'{"version":"1.10.6.0"}\' WHERE entity_id=$entityid LIMIT 1';
				$result = $this->runquery($sql);
				
			} catch(Exception $e) { $ret = $e->getMessage(); }
			return $ret;
		}
		
		public function getAllOrders() {
			$sql = "SELECT entity_id, increment_id FROM sales_flat_order ORDER BY entity_id";
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { 
					$ret[] = $row ;	
				}
			}
			return $ret;
		}
		
		public function getOrderStatus($increment_id) {
			$sql = "SELECT status FROM sales_flat_order WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['status'];
			}
			return $ret;
		}
		
		public function getTotalDue($increment_id) {
			$sql = "SELECT total_due FROM sales_flat_order WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['total_due'];
			}
			return $ret;
		}
		
		public function updateorderstatus($entity_id, $status) {
			$sql = "INSERT INTO sales_flat_order_status_history (parent_id, is_customer_notified, is_visible_on_front,status, created_at) VALUES($entity_id, 0,0,'$status', NOW())";
			$result = $this->runquery($sql);
		}
		
		public function getOrderCurrencyCodeByIncrementId($increment_id) {
			$sql = "SELECT order_currency_code FROM sales_flat_order WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['order_currency_code'];
			}
			return $ret;
		}
		
		public function getOrderCurrencyCode($entity_id) {
			$sql = "SELECT order_currency_code FROM sales_flat_order WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['order_currency_code'];
			}
			return $ret;
		}
		
		public function getAllVAS() {
			$sql = "SELECT * FROM tcs_vas_services order by type ASC";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { 
					$ret[] = $row ;	
				}
			}
			return $ret;
		}
		
		public function getVASByType($type) {
			$sql = "SELECT * FROM tcs_vas_services WHERE active=1 AND type='$type'";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { 
					$ret[] = $row ;	
				}
			}
			return $ret;
		}
		
		public function getVasCost($vasid) {
			$sql = "SELECT vas_cost FROM tcs_vas_services WHERE vas_id=$vasid LIMIT 1";
			$result = $this->runquery($sql);
			$ret = 0;
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['vas_cost'];
			}
			return $ret;
		}
		
		public function getVASTotal($vasids) {
			$sql = "SELECT SUM(vas_cost) as totalvas FROM tcs_vas_services WHERE vas_id IN ($vasids)";
			$result = $this->runquery($sql);
			$ret = 0;
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['totalvas'];
			}
			return $ret;
		}
		
		public function getquotetotals($quoteid) {
			$sql = "SELECT base_grand_total, grand_total FROM sales_flat_quote WHERE entity_id='$quoteid' LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}	
		
		public function storeVasIfo($quoteid, $vascharges, $vas_ids, $greeting_card) {
			$sql = "UPDATE sales_flat_quote SET vas_charges='$vascharges', selected_vas='$vas_ids', greeting_card='$greeting_card' Where entity_id='$quoteid' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		
		
		public function getQuoteVas($quoteid) {
			$sql = "SELECT vas_charges FROM sales_flat_quote WHERE entity_id=$quoteid LIMIT 1";
			$result = $this->runquery($sql);
			$row = "";
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$row = $row['vas_charges'];
			}
			return $row;
		}
		
		public function updateBGTGTForSalesOrderGrid($entity_id, $bgt, $gt) {
			$sql = "UPDATE sales_flat_order_grid SET base_grand_total=$bgt, grand_total=$gt WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function updateOrderTotalWithVAS($grandtotal, $base_grand_total, $vastotal, $selected_vas, $incrementid, $greeting_card) {
			$sql = "UPDATE sales_flat_order SET base_grand_total=$base_grand_total, grand_total=$grandtotal, selected_vas='$selected_vas', vas_charges='$vastotal', greeting_card='$greeting_card' WHERE increment_id='$incrementid' LIMIT 1";
			$result = $this->runquery($sql);
			
			$sql = "UPDATE sales_flat_order_grid SET base_grand_total=$grandtotal, grand_total=$grandtotal WHERE increment_id='$incrementid' LIMIT 1";	$result = $this->runquery($sql);
			
			$data = array( 'incrementid' => $incrementid );
			try { 
				Mage::dispatchEvent('evt_vas_updated', $data);
			} catch(Exception $e) { ; }
		}
		
		public function getOrderQuoteId($increment_id) {
			$sql = "SELECT quote_id FROM sales_flat_order WHERE increment_id=$increment_id LIMIT 1";
			$result = $this->runquery($sql);
			$row = "";
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$row = $row['quote_id'];
			}
			return $row;
		}
		
		// Move function to ordersforward helper function
		public function getOrderVasInfoByEntityId($entity_id) {
			$sql = "SELECT vas_charges, selected_vas,greeting_card FROM sales_flat_order WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}
		
		public function getOrderVasInfo($quoteid) {
			$sql = "SELECT * FROM sales_flat_quote WHERE entity_id=$quoteid LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}
		
		public function isCityHasVas($cityname) {
			$list = array( "karachi","hyderabad","quetta","sukkur","lahore","multan","faisalabad","gujranwala","peshawar","rawalpindi" );
			if( in_array(strtolower($cityname), $list) ) { return 1; }
			return 0;
		}
		
		public function updateVAS($vasid, $active) {
			$sql = "UPDATE tcs_vas_services SET active=$active WHERE vas_id=$vasid LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function addNewVas($vas_name, $vas_cost, $active, $type, $imgurl, $shortdesc) {
			$sql = "INSERT INTO tcs_vas_services (vas_name,vas_cost,active,type,imgurl, shortdesc) VALUES('$vas_name', '$vas_cost', $active, '$type', '$imgurl','$shortdesc')";
			$result = $this->runquery($sql);
		}
		
		public function saveGreetingCard($entity_id, $greeting_card) {
			$sql = "UPDATE sales_flat_order SET greeting_card='$greeting_card' WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getGreetingCardMessage($entity_id) {
			$sql = "SELECT greeting_card FROM sales_flat_order WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			$ret = "";
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['greeting_card'];
			}
			return $ret;
		}
		
		public function getVasById($vasid) {
			$sql = "SELECT * FROM tcs_vas_services where vas_id=$vasid LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			if( $result != null ) 
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			return $row;
		}
		
		public function getOrderVasByQuoteId($quoteid) {
			$sql = "SELECT vas_charges FROM sales_flat_quote where entity_id=$quoteid LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			$ret = "";
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['vas_charges'];
			}
			return $ret;
		}
		
		public function getOrderVAS($entity_id) {
			$sql = "SELECT vas_charges FROM sales_flat_order where entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			$ret = "";
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['vas_charges'];
			}
			return $ret;
		}
		
		public function getOrderVasItems($entity_id) {
			$sql = "SELECT selected_vas FROM sales_flat_order WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			$ret = "";
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['selected_vas'];
			}
			return $ret;
		}
		
		public function getAddressAndTelephone($increment_id) {
			$sql = "SELECT o.increment_id, concat(concat(concat(a.street,' ', a.city),' ', a.region), ' ', a.postcode) as addy, telephone FROM sales_flat_order o INNER JOIN sales_flat_order_address a ON o.entity_id=a.parent_id WHERE o.increment_id='$increment_id' AND address_type='billing' LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			$ret = "";
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$addy = str_replace(",", " ", $row['addy']);
				$ret =  $addy;
			} 
			return $ret;
		}
		
		public function getAddressAndTelephoneShipping($increment_id) {
			$sql = "SELECT o.increment_id, concat(concat(concat(a.street,' ', a.city),' ', a.region), ' ', a.postcode) as addy, telephone FROM sales_flat_order o INNER JOIN sales_flat_order_address a ON o.entity_id=a.parent_id WHERE o.increment_id='$increment_id' AND address_type='shipping' LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			$ret = "";
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$addy = str_replace(",", " ", $row['addy']);
				$addy = nl2br($addy);
				$addy = str_replace("\r\n", "", $addy);
				$addy = str_replace("<br />", "", $addy);
				$addy = str_replace("\n", "", $addy);
				$addy = str_replace("\\n", "", $addy);
				
				$ret =  $addy;
			} 
			return $ret;
		}
		
		
		
		public function addcountry($countryname, $countryabbr, $zone) {
			$sql = "INSERT INTO tcs_countrylist (countryname, countryabbr, countryzone) VALUES('$countryname', '$countryabbr', $zone)";
			$result = $this->runquery($sql);
		}
		
		public function addintlshipping($weight, $zone1, $zone2, $zone3, $zone4, $zone5, $zone6, $zone7, $zone8, $zone9, $zone10 ) {
			$sql = "INSERT INTO tcs_intl_shipping (parcelwt, zone1, zone2, zone3, zone4, zone5, zone6, zone7, zone8, zone9, zone10) VALUES ('$weight', '$zone1', '$zone2', '$zone3', '$zone4', '$zone5', '$zone6', '$zone7', '$zone8', '$zone9', '$zone10')";
			$result = $this->runquery($sql);
		}
		
		public function getshippingcountries() {
			$ret = array();
			$sql = "SELECT * FROM tcs_countrylist";
			$result = $this->runquery($sql);
			$row = array();
			while( $row = mysql_fetch_array($result, MYSQL_BOTH)) { 
				$ret[] = $row ;	
			}
			return $ret;
		}
		
		public function getShippingCountryZone($countryabbr) {
			$sql = "SELECT countryzone FROM tcs_countrylist WHERE countryabbr='$countryabbr' LIMIT 1";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret['countryzone'];
		}
		
		public function getInternationalShippingRate($weight) {
			if( $weight < 0.5 ) $weight = 0.5;
			if( $weight > 500 ) $weight = 500;
			$ret = array();
			$sql = "select * FROM tcs_intl_shipping where parcelwt <= $weight ORDER BY parcelwt DESC LIMIT 1;";
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;
		}
		
		public function updateQuoteShippingCountry($quoteid, $countrycode) {
			$sql = "UPDATE sales_flat_quote set shipping_country='$countrycode' WHERE entity_id='$quoteid' LIMIT 1";			
			$result = $this->runquery($sql);
		}
		
		
		public function getQuoteShippingCountry($quoteid) {
			$sql = "SELECT shipping_country FROM sales_flat_quote WHERE entity_id=$quoteid LIMIT 1";
			$ret = "";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $ret['shipping_country'];
			}
			return $ret;
		}
		
		public function updateQuoteShippingCity($quoteid, $city) {
			$sql = "UPDATE sales_flat_quote_address SET city='$city' WHERE quote_id=$quoteid AND address_type='shipping' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getItemPurchasePrice($orderid, $productid) {
			$sql = "SELECT price FROM sales_flat_order_item WHERE order_id=$orderid AND product_id=$productid LIMIT 1";
			
			$result = $this->runquery($sql);
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $ret['price'];
			}
			return $ret;
		}
		
		public function getProductItemNamePriceCost($sku, $startdate, $enddate) {
			$sql = "SELECT name, base_price, base_cost FROM sales_flat_order_item WHERE sku='$sku' AND created_at >= '$fromdate' AND created_at <= '$enddate' LIMIT 1";
			
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;
		}
		
		public function getDistinctSkusForPaidOrders($fromdate, $enddate) {
			$sql = "select distinct(sku) from sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id AND o.status IN ('complete', 'paid', 'received', 'holded') AND i.created_at >= '$fromdate' AND i.created_at <= '$enddate';";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row['sku'];
				}
			}
			return $ret;
		}
		
		public function getDistinctVendorsForPaidOrders($fromdate, $enddate) {
			$sql = "select distinct(udropship_vendor) from sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id AND o.status IN ('complete', 'paid', 'received') AND i.created_at >= '$fromdate' AND i.created_at <= '$enddate'";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getNumVendorProductsByVendorId($vendor_id, $fromdate, $enddate) {
			$sql = "SELECT count(item_id) as numprods FROM sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id AND o.status IN ('complete', 'paid', 'received') AND i.udropship_vendor=$vendor_id AND i.created_at >= '$fromdate' AND i.created_at <= '$enddate'";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $ret['numprods'];
			}
			
			return $ret;
		}
		
		public function getNumSkuForPaidOrderBySku($sku, $fromdate, $enddate) {
			$sql = "SELECT sum(qty_ordered) as numprods FROM sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id AND o.status IN ('complete', 'paid', 'received', 'holded') AND i.sku='$sku' AND i.created_at >= '$fromdate' AND i.created_at <= '$enddate'";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $ret['numprods'];
			}
			
			return $ret;
		}
		
		public function getTotalSalesByVendorId($vendor_id, $fromdate, $enddate) {
			$sql = "SELECT sum(base_price) as totalsales FROM sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id AND o.status IN ('complete', 'paid', 'received') AND udropship_vendor=$vendor_id  AND i.created_at >= '$fromdate' AND i.created_at <= '$enddate'";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $ret['totalsales'];
			}
			
			return $ret;
		}
		
		public function updateOrderItemVendor($item_id, $vendor_id) {
			$sql = "UPDATE sales_flat_order_item SET udropship_vendor=$vendor_id WHERE item_id=$item_id LIMIT 1";
			$result = $this->runquery($sql);
		}	
		
		public function getOrderItemSkus($entity_id) {
			$sql = "SELECT item_id, sku FROM sales_flat_order_item WHERE order_id=$entity_id";
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}	
			}
			
			return $ret;
		}
		
		public function getProductCostByVendor($vendor_id, $fromdate, $enddate) {
			$sql = "SELECT sum(base_cost) as cogs FROM sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id AND o.status IN ('complete', 'paid', 'received') AND udropship_vendor=$vendor_id AND i.created_at >= '$fromdate' AND i.created_at <= '$enddate'";
			$result = $this->runquery($sql);
			
			$ret = array();
			$total = 0.00;
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$total = $row['cogs'];
				}
			}
			return $total;
		}
		
		public function saveShippingCharges($quoteid, $shipping) {
			$sql = "UPDATE sales_flat_quote SET shipping='$shipping' WHERE entity_id=$quoteid LIMIT 1";
			$result = $this->runquery($sql);	
		}
		
		public function getShippingChargesByQuoteId( $quoteid) {
			$sql = "SELECT shipping FROM sales_flat_quote WHERE entity_id=$quoteid LIMIT 1";
			$result = $this->runquery($sql);
			$ret = 0;			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['shipping'];
			}
			return $ret;
		}
		
		public function getQuoteShipping($quoteid) {
			$sql = "SELECT * FROM sales_flat_quote_address WHERE quote_id=$quoteid LIMIT 1";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row;
			}
			return $ret;
		}
		
		public function updateQuoteShipping($quoteid, $shippingmethod, $shippingamount, $baseshippingamount) {
			$sql = "UPDATE sales_flat_quote_address set shipping_method='$shippingmethod', shipping_amount='$shippingamount', base_shipping_amount='$baseshippingamount' WHERE quote_id='$quoteid' LIMIT 2";			
			$result = $this->runquery($sql);
		}
		
		public function updateGrandAndBaseGrandTotalWithShippingAndCurrency($quoteid, $grandtotal, $basegrandtotal) {
			$sql = "UPDATE sales_flat_quote SET grand_total=$grandtotal, base_grand_total=$basegrandtotal WHERE entity_id=$quoteid LIMIT 1";
			$result = $this->runquery($sql);
		}		
		
		public function updateGTBGTForOrder($entity_id, $base_grand_total, $grand_total) {
			$sql = "UPDATE sales_flat_order SET grand_total='$grand_total', base_grand_total='$base_grand_total' WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function updateGrandAndBaseGrandTotalWithShipping($quoteid, $grandtotal) {
			$sql = "UPDATE sales_flat_quote SET grand_total=$grandtotal, base_grand_total=$grandtotal WHERE entity_id=$quoteid LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getCostPrice($order_id, $product_id) {
			$sql = "SELECT base_cost FROM sales_flat_order_item WHERE order_id=$order_id AND product_id=$product_id LIMIT 1";
			$result = $this->runquery($sql);
			$ret = 0;
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['base_cost'];
			}
			return $ret;
		}
		
		public function get_BaseBuyerPrice($order_id, $product_id) {
			$sql = "SELECT base_price FROM sales_flat_order_item WHERE order_id=$order_id AND product_id=$product_id LIMIT 1";
			$result = $this->runquery($sql);
			$ret = 0;
			
			
			if($entityid == "10657" ) {
				@file_put_contents("/tmp/pnl.log", "$sql\n");
			}
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['base_price'];
			}
			return $ret;
		}
		
		public function getBuyerPrice($order_id, $product_id) {
			$sql = "SELECT price FROM sales_flat_order_item WHERE order_id=$order_id AND product_id=$product_id LIMIT 1";
			$result = $this->runquery($sql);
			$ret = 0;
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['price'];
			}
			return $ret;
		}
		
		public function getUBDBDB($method, $api) {
			$method  = trim($method);
			$api	 = trim($api);
			
			if ( $method == "internetbanking" )
				return "DBD";
			
			if( $method == "creditcardoffline" ) 
				return "UBD";
				
			if( 
				$method == "paypal_standard" || $method == "twocheckout_shared" ||
				$method == "easypaisa" || $method == "customercredit"
			)
				return "DBD";
				
			if( $method == "cod" )
				return "UBD";
				
			
			if( $method == "cash" &&  stripos( strtolower($api), "bank") !== FALSE )
					return "DBD";
			else 
					return "UBD";
					
			if( $method == "checkatbank" && stripos( strtolower($api), "bank") !== FALSE )
					return "DBD";
			else 
					return "UBD";
			
				
		}
		
		public function getTotals($entity_id) {
			$sql = "SELECT base_grand_total FROM sales_flat_order WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$ret = 0;
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['base_grand_total'];
			}
			return $ret;	
		}
		
		public function getPMAreaCode($areaid) {
			$sql = "SELECT areacode FROM tcs_payment_areas WHERE areaid=$areaid LIMIT 1";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['areacode'];
			}
			return $ret;	
		}
		
		public function getShippingMethodForAddress($quoteid) {
			$sql = "select shipping_method from sales_flat_quote_address where quote_id=$quoteid AND LENGTH(shipping_method) > 0";
			
			
			
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['shipping_method'];
			}
			return $ret;	
		}
		
		public function update_shipping($entity_id, $base_shipping, $shipping) {
			$sql = "update sales_flat_order SET base_shipping_amount=$base_shipping, shipping_amount=$shipping WHERE entity_id=$entity_id LIMIT 1";
			
			$result = $this->runquery($sql);
		}
		
		public function getIntlShippingAmount($quoteid) {
			$sql = "SELECT shipping FROM sales_flat_quote WHERE entity_id=$quoteid LIMIT 1";
			$result = $this->runquery($sql);
			$ret = "";
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['shipping'];
			}
			return $ret;
		}
		
		public function getProductVendorBySku($sku) {
			$vendor_id = "";
			try {
				$_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
				$vendor_id = $_product->getData('udropship_vendor');
				
			} catch(Exception $e) { ; }
			return $vendor_id;
		}
		
		public function getGetOrderItemsByOrderId($increment_id) {
			$sql = "select i.item_id, i.name, i.sku, i.udropship_vendor FROM sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id  WHERE o.increment_id='$increment_id'";
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getAllVendors() {
			$sql = "SELECT vendor_id, vendor_name FROM udropship_vendor ORDER BY vendor_name";
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function updateOrderItemVendorwithItemid($itemid, $vendorid) {
			$sql = "UPDATE sales_flat_order_item SET udropship_vendor=$vendorid WHERE item_id=$itemid LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function fixpaypalorderstatus($entity_id) {
			$sql = "UPDATE sales_flat_order SET state='processing', status='processing' WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function updateCustomerforOrder($entity_id, $customerid, $firstname, $lastname, $email, $increment_id) {
			$sql = "UPDATE sales_flat_order SET customer_id=$customerid, customer_firstname='$firstname', customer_lastname='$lastname', customer_email='$email' WHERE entity_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);

			$fullname = $firstname . " " . $lastname;

			$sql = "UPDATE sales_flat_order_grid SET customer_id=$customerid, billing_name='" . $fullname . "' WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function updateOrderAddress($entity_id, $firstname, $lastname, $addressstreet, $city, $country, $type, $telephone) {
			$sql = "UPDATE sales_flat_order_address SET firstname='$firstname', lastname='$lastname', street='$addressstreet', city='$city', country_id='$country', telephone='$telephone' WHERE parent_id=$entity_id AND address_type='$type' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getOrderPaymentDate($entity_id) {
			$sql = "SELECT created_at FROM sales_flat_invoice WHERE order_id=$entity_id";
			$result = $this->runquery($sql);
			$ret = "";
			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['created_at']; 
			}
			return $ret;
		}
		
		public function getCompleteOrdersForDateRange($fromdate, $todate) {
			//$sql = "SELECT entity_id, increment_id, created_at, updated_at FROM sales_flat_order WHERE created_at >= '$fromdate' AND created_at <= '$todate' AND status='complete' ORDER BY entity_id";
			$sql   = <<<SQL
							SELECT 
								   sfo.entity_id,
								   sfo.increment_id,
								   sfo.created_at,
								   sfo.updated_at,
								   sfo.delivery_date,
								   DATE(sofh.created_at) AS sfohCreatedDate
							FROM 
							 sales_flat_order_status_history AS sofh
							LEFT JOIN 
							 sales_flat_order AS sfo 
							ON 
							 sfo.entity_id = sofh.parent_id
							WHERE
							  sofh.status='complete'
							GROUP BY
							 sofh.parent_id
							 HAVING 
							  sfohCreatedDate >= '$fromdate'
							   AND
							  sfohCreatedDate <= '$todate'
							ORDER BY
							 sfo.entity_id
SQL;

			$result = $this->runquery($sql);
			$ret = array();
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getAllItemsForPaidCompletedOrders($fromdate, $enddate) {
			$sql = "select order_id, sku, name, base_grand_total from sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id AND o.status IN ('complete', 'paid', 'received') AND i.created_at >= '$fromdate' AND i.created_at <= '$enddate' ORDER BY order_id";	
			
			$result = $this->runquery($sql);
			$ret = array();
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;			
		}
		
		public function getPMAreaCodeByAreaId($paymentareaid) {
			$sql = "SELECT * FROM tcs_paymentareas WHERE paymentareaid=$paymentareaid";
			$result = $this->runquery($sql);
			$row = array();
			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH); 
			}
			return $row;
		}
		
		public function getPaymentAreaName($areacode) {
			$sql = "SELECT paymentarea FROM tcs_paymentareas WHERE areacode='$areacode' LIMIT 1";
			$result = $this->runquery($sql);
			$row = "";
			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH); 
				$row = $row['paymentarea'];
			}
			return $row;
		}
		
		public function getAllCouponOrders($fromdate, $enddate) {
			$sql = "Select entity_id, increment_id, coupon_code, base_discount_amount, base_grand_total  from sales_flat_order where date(created_at) >= '$fromdate' AND date(created_at) <= '$enddate' AND coupon_code IS NOT NULL";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getRedirectURL($orderid) {
			   // $ret = array();
                        // $result = $this->runquery($sql);
                        // if( $result  != null ) {
                                // $ret = mysql_fetch_array($result, MYSQL_BOTH) ;
                        // }
                        // return $ret;
                        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
                        $sql = "SELECT * FROM tcs_ublcclog WHERE orderid='$orderid' AND calltype='R' ORDER BY cclog_id DESC LIMIT 1";
                        $ret = $read->fetchAll($sql);
                        return $ret;

		}
		
		public function getUBLCCLog($orderid, $type) {
			$sql = "SELECT * FROM tcs_ublcclog WHERE orderid='$orderid' AND calltype='$type' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH) ;
			}
			return $ret;
		}
		
		public function updateCCLogStatus($orderid, $responsecode, $responsedesc, $transactionid, $orderamount, $approvalcode) {
			$sql = "INSERT INTO tcs_ublcclog (orderid, orderdate, responsecode, responsedesc, transactionid, orderamount, redirectpage, approvalcode, calltype) VALUES ('$orderid', NOW(), '$responsecode', '$responsedesc', '$transactionid', '$orderamount','Magento', '$approvalcode', 'F')";
			$result = $this->runquery($sql);
		}
		
		public function updatePaymentmethodForGrid($entity_id, $paymentmethod) {
			$sql = "UPDATE sales_flat_order_payment set method='$paymentmethod' WHERE entity_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getOrderPayDate($ordernumber) {
			$sql = "select i.created_at FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON o.entity_id=i.order_id WHERE o.increment_id='$ordernumber'";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH) ;
			}
			$ret = $ret['created_at'];
			
			return $ret;
		}
		
		public function update_config_constant( $config_constant, $config_value) {
			$sql = "UPDATE config_constants SET config_value='$config_value' WHERE config_name='$config_constant' LIMIT 1"; 
			$result = $this->runquery($sql);
		}
		
		public function getUninvoicedOrders($fromdate, $todate) {
			$sql = "select o.entity_id, o.status, p.method, o.updated_at from sales_flat_order o INNER JOIN sales_flat_order_payment p ON o.entity_id=p.parent_id AND p.entity_id NOT IN (select order_id FROM sales_flat_invoice i) WHERE o.status='complete' AND o.updated_at >= '$fromdate' AND o.updated_at <= '$todate' and p.method='cod'";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}

		public function setInvoiceDate($orderid, $invoicedate) {
			$sql = "UPDATE sales_flat_invoice SET updated_at='$invoicedate', created_at='$invoicedate' WHERE order_id=$order_id LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getOmsInfoByOrderIdAndSku($incrementid, $sku) {
			$sql = "select * FROM oms_transactions o INNER JOIN oms_transaction_products p ON o.ordercnid=p.ordercnid  WHERE o.orderid='$incrementid' AND sku='$sku';";
			$result = $this->runquery($sql);
			$ret = array();
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}
			return $ret;
		}
		
		public function getDiffOfPriceNSpecialPrice($increment_id) {
			$sql = "SELECT SUM(i.base_price) as paid, SUM(i.base_original_price) as original FROM sales_flat_order_item i INNER JOIN sales_flat_order o ON i.order_id=o.entity_id WHERE o.increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			if( $result	!= null ) {
				$row = mysql_fetch_array( $result, MYSQL_BOTH );
			}
			return $row;
		}
		
		public function getQuoteItems($quote_id) {
			$sql = "SELECT product_id, sku FROM sales_flat_quote_item WHERE quote_id='$quote_id'";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function update_orders_audit($increment_id, $entity_id, $logged_user, $logged_userid, $user_ip) {
			$sql = "INSERT INTO tcs_orders_audit (increment_id,  logged_user, logged_userid, entity_id, date_moved, user_ip) VALUES('$increment_id', '$logged_user', '$logged_userid', $entity_id, NOW(), '$user_ip')";
			$result = $this->runquery($sql);	
		}
		
		public function get_invoicedorders($fromdate, $todate) {
			$sql = "SELECT distinct(order_id) from sales_flat_invoice WHERE date(created_at)>= '$fromdate' and date(created_at) <= '$todate'";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function get_orderdetails($entity_id) {
			$sql = "SELECT o.entity_id, o.increment_id, o.customer_firstname, o.customer_lastname, o.additionalpaymentinfo, o.base_grand_total, o.base_subtotal, o.base_shipping_amount, o.vas_charges, o.status FROM sales_flat_order o WHERE entity_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}
		
		public function get_ordersforcompreport($fromdate, $todate) {
			$sql = "SELECT o.entity_id, o.increment_id, o.customer_firstname, o.customer_lastname, o.additionalpaymentinfo, i.created_at, o.base_grand_total, o.base_subtotal, o.base_shipping_amount, o.vas_charges FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON o.entity_id=i.order_id WHERE i.created_at >= '$fromdate' AND i.created_at <= '$todate' AND status != 'canceled' ORDER BY i.created_at";
			
			$result = $this->runquery($sql);
			$ret = array();
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;			
		}
		
		public function updateOrderStatusForIncrementId($increment_id, $status, $state) {
			$sql = "UPDATE sales_flat_order set status='$status', state='$state' WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function deleteOrderPaidStatus($entity_id) {
			$sql = "DELETE FROM sales_flat_order_status_history where parent_id=$entity_id AND status='paid' AND comment IS NULL LIMIT 1";
			$result = $this->runquery($sql);
		}

		public function deleteOrderStatusHistory($entity_id,$status) {
			$sql = "DELETE FROM sales_flat_order_status_history where parent_id=$entity_id AND status='".$status."' LIMIT 1";
			$result = $this->runquery($sql);
		}
		// shifted to ordersforward helper
		public function updateOrderStatusHistory($entity_id, $status, $comment) {
			$sql = "INSERT into sales_flat_order_status_history (parent_id, is_customer_notified, is_visible_on_front, comment, status, created_at) VALUES($entity_id, 0,0,'$comment', '$status', NOW())";
			$result = $this->runquery($sql);			
		}
		
		public function findOrderDetailsForFixOrders($increment_id) {
			$sql = "select entity_id, base_shipping_amount, base_grand_total, base_subtotal, base_discount_amount, vas_charges, shipping_amount, grand_total, subtotal,discount_amount, order_currency_code FROM sales_flat_order WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			$row = array();
			
			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row;
		}
		
		
		
		public function fixOrderDetails($entity_id, $base_shipping_amount, $base_grand_total, $base_subtotal, $base_discount_amount, $vas_charges, $shipping_amount, $grand_total, $subtotal, $discount_amount, $ordernumber) {
			$sql = "UPDATE sales_flat_order set base_shipping_amount='$base_shipping_amount', base_grand_total='$base_grand_total', base_subtotal='$base_subtotal', base_discount_amount='$base_discount_amount', vas_charges='$vas_charges', shipping_amount='$shipping_amount', grand_total='$grand_total',subtotal='$subtotal', discount_amount='$discount_amount' WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);

			$base_grand_total = number_format($base_grand_total,2, '.' , '');

			$sql = "UPDATE transactions SET orderamount=". $base_grand_total ." WHERE ordernum='". $ordernumber ."'";
			$result = $this->runquery($sql);

		}
		
		public function getNumberOfProductsOrdered($increment_id, $sku) {
			$sql = "Select i.qty_ordered FROM sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id WHERE o.increment_id='$increment_id' AND i.sku='$sku' LIMIT 1";
			$result = $this->runquery($sql);

			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row['qty_ordered'];	
		}	
		
		public function getNumberOfitemsForGivenCNNumber($ordercnid) {
			$sql = "SELECT SUM(quantity) as qty FROM oms_transaction_products WHERE ordercnid='$ordercnid'";
			$result = $this->runquery($sql);

			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row['qty'];	
		}
		
		public function getOrderItemDetails($order_id) {
			$sql = "SELECT item_id, name, sku, qty_ordered, row_total, base_row_total,price,base_price FROM sales_flat_order_item WHERE order_id='$order_id'";
			$result = $this->runquery($sql);
			$ret = array();

			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function delete_OrderItem($itemid) {
			$sql = "DELETE FROM sales_flat_order_item WHERE item_id=$itemid LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function update_OrderItemInform($itemid, $qty_ordered, $row_total, $base_row_total) {
			$sql = "UPDATE sales_flat_order_item SET qty_ordered='$qty_ordered', row_total='$row_total', base_row_total='$base_row_total' WHERE item_id=$itemid LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function get_TransactionCity($increment_id) {
			$sql = "SELECT * FROM transactions WHERE ordernum='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row['pm_area'];	
		}
		
		public function getOrderShippingCity($orderid) {
			$ret = "";
			$sql = "SELECT city FROM sales_flat_order_address WHERE address_type='shipping' AND parent_id=$orderid LIMIT 1";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['city'];
			}
			return $ret;
		}	
		
		public function getAllAreaAccountants() {
			$ret = array();
			$sql = "SELECT * FROM tcs_paymentareas";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;	
		}
		
		public function getOrdersforAPM($areacode=null) {	
			$ret = array();
			$cond = "";
			
			if($areacode != null ) {
				$cond = " AND t.pm_area='$areacode' ";
			}
			$sql = "SELECT o.increment_id, o.entity_id, o.created_at, o.status, o.base_grand_total,o.additionalpaymentinfo, DATEDIFF(NOW(), t.paymentts) as 'lag',t.paymentts, 	t.approved_by, t.pm_area, t.flagged, dateconfirmed FROM sales_flat_order o INNER JOIN sales_flat_order_payment p ON o.entity_id=p.parent_id INNER JOIN transactions t ON o.increment_id=t.ordernum WHERE p.method IN ('creditcardoffline', 'cash','checkatbank', 'cod') AND o.additionalpaymentinfo IN ('Door-step', 'CC Offline - Doorstep','Express Center', 'CC Offline - Express Center','','NULL', 'cod') AND o.status IN ('paid', 'complete', 'cod', 'received', 'Awaiting', 'order_refunded', 'holded') $cond ORDER BY DATEDIFF(NOW(), t.paymentts) DESC ";				
			$result = $this->runquery($sql);
			
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;	
		}
		
		public function store_order_shipping($quote_id, $shipping_amount) {
			$sql = "SELECT * FROM tcs_order_shipping WHERE quote_id='$quote_id'";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				if( @$row['quote_id'] <> "" )				
					$sql = "UPDATE tcs_order_shipping SET shipping_amount='$shipping_amount' WHERE quote_id='$quote_id' LIMIT 1";			
				
			}
			$sql = "INSERT INTO tcs_order_shipping (quote_id, shipping_amount) VALUES('$quote_id', '$shipping_amount')";
			
			$result = $this->runquery($sql);
		}
			
		public function get_order_shipping_for_free_shipping($quote_id) {
			$sql = "SELECT shipping_amount FROM tcs_order_shipping WHERE quote_id='$quote_id' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['shipping_amount'];	
			}	
			return $ret;
		}
		
		
		
		public function getAllPaymentAreas() {
			$ret = array();
			$sql = "SELECT distinct(areacode) FROM tcs_paymentareas";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) )
					$ret[] = $row;
			}
			return $ret;
		}
		
		public function getPaymentManagerCitynameByAreaCode($areacode) {
			$ret    = "";
			$sql    = "SELECT paymentarea FROM tcs_paymentareas WHERE areacode='$areacode' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $ret['paymentarea'];
			}
			return $ret;
		}		
		
		public function savePaymentManagerInfo($areacode, $managerlogin, $paymentarea, $is_admin) {

			$sql = "SELECT paymentareaid FROM tcs_paymentareas WHERE managerlogin='$managerlogin'";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				$num_rows = mysql_num_rows($result);
				if ($num_rows > 0) {
					$ret = mysql_fetch_array($result, MYSQL_BOTH);
					$paymentareaid = $ret['paymentareaid'];

					$sql = "UPDATE tcs_paymentareas SET areacode='$areacode', managerlogin='$managerlogin', paymentarea='$paymentarea',is_admin=$is_admin WHERE paymentareaid='$paymentareaid' LIMIT 1";
					$this->runquery($sql);
				}
				else {
					$sql = "INSERT INTO tcs_paymentareas (paymentarea, managerlogin, areacode, is_admin) VALUES ('$paymentarea', '$managerlogin', '$areacode', $is_admin)";
					$result = $this->runquery($sql);
				}
			}
		}
		
		public function checkIfAdminUserExcists($username) {
			$sql = "SELECT user_id FROM admin_user WHERE username LIKE '$username' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result	!= null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
				if( $ret['user_id'] <> "" || $ret['user_id'] > 0 ) return true;				
			}
			return false;
		}
		
		public function updateOrderStatusByEntityId($entity_id, $newstatus) {
			$sql = "UPDATE sales_flat_order SET status='$newstatus' WHERE entity_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);
			
			$sql = "UPDATE sales_flat_order_grid SET status='$newstatus' WHERE entity_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function getOrderInfo($entity_id) {
			$sql = "SELECT * FROM sales_flat_order WHERE entity_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result	!= null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;
		}
		
		public function getTransactionDataforIds($orderids) {			
			$sql = "SELECT transactionid, ordernum, flagged, dateconfirmed, flagged_by, pm_area FROM transactions where ordernum IN ($orderids)";
			
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			foreach ($connection->fetchAll($sql) as $arr_row) {
				print $arr_row['default_name'];
			}
			
			
			return $ret;
		}
		
		
		public function getTcsEmails($emailtype) {
			$sql = "SELECT * FROM tcs_email_list WHERE emailtype=$emailtype ORDER BY emailaddress";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			return $connection->fetchAll($sql);
		}
		
		public function getDiscrepancyCCList($areacode) {
			$sql = "SELECT * FROM tcs_email_list WHERE emailtype=1 AND customfield='$areacode'";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			return $connection->fetchAll($sql);
		}
		
		public function getMaxmindInfo($entity_id) {
			$sql = "SELECT maxminddesc, riskscore, maxmindid FROM sales_flat_order WHERE entity_id='$entity_id' LIMIT 1";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			return $connection->fetchAll($sql);
		}
		
		public function getApprovalCode($orderid) {
			$sql = "SELECT approvalcode,responsecode FROM tcs_ublcclog WHERE orderid='$orderid' AND calltype='F' ORDER BY cclog_id DESC LIMIT 1";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$result =  $connection->fetchAll($sql);
			
			return $result[0];
		}
		
		public function update_OrderStatusToPaymentReview($orderid) {
			$sql = "UPDATE sales_flat_order SET status='payment_review' WHERE entity_id='$orderid' LIMIT 1";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			$connection->query($sql);
			
			$sql = "UPDATE sales_flat_order_grid SET status='payment_review' WHERE entity_id='$orderid' LIMIT 1";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			$connection->query($sql);
		}
		
		public function removeExtraProgressingStatus($orderid) {
			$sql = "DELETE FROM sales_flat_order_status_history WHERE parent_id='$orderid' AND status='processing' order by entity_id DESC LIMIT 1";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			$connection->query($sql);
		}
		
		public function markOrderPaidGenInvoice($orderId) {
			try {
				
				//$orderId	=	$order;
				
				//MOVING AMOUNT DUE TO PAID
					
					$order=Mage::getModel('sales/order')->load($orderId);
                    $convertOrderObj=Mage::getSingleton('sales/convert_order');
                    $invoiceObj=$convertOrderObj->toInvoice($order);

                    foreach ($order->getAllItems() as $item) {
                        $invoiceItem = $convertOrderObj->itemToInvoiceItem($item);
                        $row = array();
                        
                        $productname     = $item->getName();
                        $productvendorid = $item->getData('udropship_vendor');
                        $productvendor   = $this->getVendor($productvendorid);
                        
                        $row["productname"] = $item->getName();
                        $row["vendor"]      = $productvendor;
                        $row["qty"]         = $item->getData('qty_ordered');
                        $row["weight"]      = $item->getWeight();
                        $row["ordernum"]    = $order->getIncrementId();
                        
                        $products[] = $row;

                        if ($item->getParentItem()) {
                            $invoiceItem->setParentItem($invoiceObj->getItemById($item->getParentItem()->getId()));
                        }
                        $invoiceItem->setQty($item->getQtyToInvoice());
                        $invoiceObj->addItem($invoiceItem);
                    }

                    $invoiceObj->collectTotals();
                    $invoiceObj->register();

                    $orderPaymentObj=$order->getPayment();
                    
                   // $method = $orderPaymentObj->getMethod();
                    //$api = $order->getAdditionalPaymentInfo();
                    
                    $orderPaymentObj->pay($invoiceObj);
                    $invoiceObj->getOrder()->setIsInProcess(true);
                    $transactionObj = Mage::getModel('core/resource_transaction');
                    $transactionObj->addObject($invoiceObj);
                    $transactionObj->addObject($invoiceObj->getOrder());
                    $transactionObj->save();

                    $invoiceObj->save();
                    $invoiceId=$invoiceObj->getId();
				
				
				 $order->setStatus('paid',true)->save();  
	
				
				//$this->applypaymentwithvas($orderId); 
				
				$this->updateorderstatus($orderId, "Status shifted to Paid Automatically due to low risk score");
				
			} catch (Exception $e) {  
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage()); 
			}
		}
		
		public function get_quote_item($item_id) {
			$sql = "SELECT * FROM sales_flat_quote_item WHERE item_id=$item_id LIMIT 1";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$result =  $connection->fetchAll($sql);

			return $result;
		}
		
		public function update_quote_item_price($item_id, $total) {
			$sql = "UPDATE sales_flat_quote_item SET base_price='$total' WHERE item_id=$item_id LIMIT 1";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			$connection->query($sql);
		}
		
		public function update_quote_insurance($item_id, $insurance_amount, $quote_id) {
			$totalinsurance = $this->getTotalInsuranceForQuote($quote_id);
			
			$totalinsurance += $insurance_amount;
		
			$sql = "UPDATE sales_flat_quote_item SET base_insurance='$insurance_amount' WHERE item_id=$item_id LIMIT 1";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			$connection->query($sql);
		}
				
		public function update_sales_insurance($item_id, $insurance_amount) {
			$sql = "UPDATE sales_flat_order_item SET base_insurance='$insurance_amount' WHERE item_id=$item_id LIMIT 1";
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			$connection->query($sql);
		}
		
		public function get_quote_base_insurance($quote_id) {
			$sql = "SELECT SUM(base_insurance) as base_insurance FROM sales_flat_quote_item WHERE quote_id=$quote_id";
			$result = $this->runquery($sql);
			$ret = 0;
			
			if( $result != null ) { 
				if( ($row=mysql_fetch_array($result, MYSQL_BOTH)) !== FALSE ) {
					$ret = $row['base_insurance'];
				}
			}
			return $ret;			
		}
		
		public function apply_insurance_to_sales($order_id, $quote_id, $base_insurance) {
			$read  = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			
			//get order currency code
			$sql 		   = "SELECT order_currency_code FROM sales_flat_order WHERE entity_id=$order_id LIMIT 1";
			$result 	   = $read->fetchAll($sql);
			$currency_code = $result[0]['order_currency_code'];
			
			$conv_insurance = $base_insurance;
			
			if( $currency_code <> "PKR" ) {
				$rate = Mage::helper('directory')->currencyConvert(1, "PKR", "USD");
				$conv_insurance = $base_insurance/$rate;
			}
			
			$sql = "SELECT base_grand_total, base_subtotal, subtotal, grand_total FROM sales_flat_order WHERE entity_id=$order_id LIMIT 1";
			$order_obj = $read->fetchAll($sql);
			
			$base_grand_total = $order_obj[0]['base_grand_total'] + $base_insurance;
			$grand_total 	  = $order_obj[0]['grand_total'] + $conv_insurance;
			$base_subtotal    = $order_obj[0]['base_subtotal'] + $base_insurance;
			$subtotal 		  = $order_obj[0]['subtotal'] + $conv_insurance;
			
			$sql = "UPDATE sales_flat_order SET base_grand_total='$base_grand_total', base_subtotal='$base_subtotal', subtotal='$subtotal', grand_total='$grand_total' WHERE entity_id=$order_id LIMIT 1";
			$write->query($sql);
			
			$sql = "UPDATE sales_flat_order_grid SET base_grand_total='$base_grand_total', grand_total='$grand_total' WHERE entity_id=$order_id LIMIT 1";
			$write->query($sql);
			
			$sql = "SELECT product_id, sku, base_insurance, price, base_price,row_total,base_row_total, qty FROM sales_flat_quote_item WHERE quote_id=$quote_id AND base_insurance > 0";
			$result = $read->fetchAll($sql);
			
			for($i=0; $i < count($result); $i++) {
				$pid 			  = $result[$i]['product_id'];
				$sku 			  = $result[$i]['sku'];
				$qty			  = $result[$i]['qty'];
				$price  		  = $result[$i]['price'] + $conv_insurance/$qty;
				$base_price  	  = $result[$i]['base_price'] + $base_insurance/$qty;
				$row_total   	  = $result[$i]['row_total'] + $conv_insurance;
				$base_row_total   = $result[$i]['base_row_total'] + $base_insurance;
				
				$sql = "UPDATE sales_flat_order_item SET base_insurance='$base_insurance', price='$price', base_price='$base_price', row_total='$row_total', base_row_total='$base_row_total' WHERE order_id=$order_id AND sku='$sku' AND product_id='$pid' LIMIT 1";
				$write->query($sql);	
			}
		}
		
		public function apply_insurance_to_quote($item_id, $base_insurance, $quote_id) {
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			
			//get the quote information
			$sql        = "SELECT * FROM sales_flat_quote where entity_id=$quote_id LIMIT 1";
			$quote_obj  = $connection->fetchAll($sql);
			$quote_obj  = $quote_obj[0];
			
			//Get the Item Information
			$sql      = "SELECT * FROM sales_flat_quote_item WHERE item_id=$item_id LIMIT 1";
			$item_obj = $connection->fetchAll($sql);
			$item_obj = $item_obj[0];
			
			$currency_code    = $quote_obj['quote_currency_code'];
			$grand_total      = $quote_obj['grand_total'];
			$base_grand_total = $quote_obj['base_grand_total'];
			$subtotal 		  = $quote_obj['subtotal'];
			$base_subtotal 	  = $quote_obj['base_subtotal'];
			$converted_insura = $base_insurance;
			
			if( $currency_code <> "PKR" ) {
				$rate  			  = Mage::helper('directory')->currencyConvert(1, "PKR", "USD");
				$converted_insura = $base_insurance/$rate;
			}
			
			/*********** New Sales_flat_quote Values *****************/
			$grand_total 	  = $converted_insura + $grand_total;
			$base_grand_total = $base_grand_total + $base_insurance;
			$subtotal 		  = $subtotal + $converted_insura;
			$base_subtotal    = $base_subtotal + $base_insurance;
			
			$sql = "UPDATE sales_flat_quote SET grand_total='$grand_total', base_grand_total='$base_grand_total', subtotal='$subtotal', base_subtotal='$base_subtotal' WHERE entity_id=$quote_id LIMIT 1";			
			$write->query($sql);
			/*********** New Sales_flat_quote Values *****************/
			
			/************* New Sales_flat_quote_item values *************/
			$price 			= $item_obj['price'] + $converted_insura;
			$base_price 	= $item_obj['base_price'] + $base_insurance;
			$row_total  	= $item_obj['row_total'] + $converted_insura;
			$base_row_total = $item_obj['base_row_total'] + $base_insurance;
			
			$sql = "UPDATE sales_flat_quote_item SET price='$price', base_price='$base_price', row_total='$row_total', base_row_total='$base_row_total' WHERE item_id=$item_id LIMIT 1";
			$write->query($sql);
			/************* New Sales_flat_quote_item values *************/			
		}
		
		//New Shipping tariff <!--Zaid Code-->		

		public function get_region_id($city_name){
			
			$sql = "SELECT region_id FROM nextgeni_shipping_region WHERE city_name='$city_name' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			else
				//return mysql_fetch_array($result, MYSQL_BOTH);
				
				$row	=	mysql_fetch_array($result);
				
				return 	$ret 	= 	$row['region_id'];
			
			}
			
		public function get_shipping_rate($column,$region_to,$region_from){
			
			$sql = "SELECT $column FROM nextgeni_shipping_tariff WHERE region_id_to='$region_to' AND region_id_from='$region_from' LIMIT 1";

			$result = $this->runquery($sql);
			if( $result == null ) return array();
			else
				 		$row	=	mysql_fetch_array($result);
				
				return 	$ret 	= 	$row[$column];
			
			}
	
		//New Shipping tariff <!--Zaid Code-->
		
		public function store_quote_shipping_type($quote_id, $shipping_type) {
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$sql = "UPDATE sales_flat_quote SET shipping_type='$shipping_type' WHERE entity_id=$quote_id LIMIT 1";
			$write->query($sql);
		}
		
		public function checkItisEidOrder($entity_id) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT e.* from sales_flat_order o INNER JOIN tcs_eidorders e ON e.quoteid=o.quote_id WHERE o.entity_id='$entity_id'";
			$ret = $read->fetchAll($sql);
			
			if( count($ret) > 0 ) 
				return 1;
			else
				return 0;
		}
		
		public function update_billingcity($entity_id, $quote_id) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$sql  = "SELECT bill_city FROM sales_flat_quote WHERE entity_id=$quote_id";
			$city = $read->fetchAll($sql);
			if( count($city) > 0 ) {
				$city = $city[0]['bill_city'];
				if( strlen($city) > 0 ) {
					$sql = "UPDATE sales_flat_order_address SET city='$city' WHERE parent_id='$entity_id' AND address_type='billing' LIMIT 1";
					$write->query($sql);	
				}
			}
		}
		// Move function to ordersforward helper function
		public function applypaymentwithvas($entity_id) {
			$read  = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			
			$sql    = "SELECT grand_total, base_grand_total FROM sales_flat_order WHERE entity_id=$entity_id";
			$totals = $read->fetchAll($sql);
			$grandtotal     = $totals[0]['grand_total'];
			$basegrandtotal = $totals[0]['base_grand_total'];
			
			$sql = "UPDATE sales_flat_order SET total_due=0, base_total_due=0, total_paid='$grandtotal', base_total_paid='$basegrandtotal' WHERE entity_id=$entity_id LIMIT 1";
			$write->query($sql);
		}
		
		public function getDirectOrders() {
			$read  = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT o.entity_id, o.increment_id FROM sales_flat_order o INNER JOIN tcs_direct d ON d.order_number=o.increment_id WHERE o.status IN ('paid','cod')";
			return $read->fetchAll($sql);
		}
		
		public function getBaseInsuranceByItemId($item_id) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql  = "SELECT base_insurance FROM sales_flat_order_item WHERE item_id=$item_id LIMIT 1";
			$ret  = $read->fetchAll($sql);
			if( count($ret) < 1 ) return 0;
			return $ret[0]['base_insurance'];
		}
		
		public function getOrderInsurance($entity_id) {
			$read  = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = " select SUM(base_insurance) as totalbaseinsurance FROM sales_flat_order_item WHERE order_id=$entity_id";
			$ret = $read->fetchAll($sql);
			$retval = 0;
			
			if( count($ret) > 0 ) 
				$retval = $ret[0]['totalbaseinsurance'];
				
			return $retval;
		}
		
		public function getRawAPMData() {
			$sql  = "SELECT o.entity_id,o.increment_id, o.created_at,o.base_subtotal, o.status,p.method,o.base_grand_total, o.additionalpaymentinfo FROM sales_flat_order o LEFT JOIN sales_flat_order_payment p ON o.entity_id=p.parent_id INNER JOIN transactions t ON t.ordernum=o.increment_id WHERE p.method IN ('creditcardoffline', 'cash', 'checkatbank', 'cod') AND o.status IN ('paid', 'complete', 'cod', 'received', 'Awaiting', 'order_refunded', 'holded') AND o.additionalpaymentinfo IN ('Door-step', 'CC Offline - Doorstep', 'Express Center', 'CC Offline - Express Center', '', 'NULL', 'cod') AND t.flagged=0 AND o.increment_id='100020370'"; 
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function getAPMData() {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql  = "SELECT 
						o.entity_id,o.increment_id, o.created_at,o.base_subtotal, o.status,p.method,o.base_grand_total, o.additionalpaymentinfo 
					FROM 
							sales_flat_order o 
					LEFT JOIN 
								sales_flat_order_payment p 
					ON 
								o.entity_id=p.parent_id 
					INNER JOIN 
								transactions t 
					ON 
								t.ordernum=o.increment_id 
					WHERE 
							p.method IN ('creditcardoffline', 'cash', 'checkatbank', 'cod') 
					AND 
							o.status 
					IN 
							('paid', 'complete', 'cod', 'received', 'Awaiting', 'order_refunded', 'holded') 
					AND 
							t.flagged=0";
							
			return $read->fetchAll($sql);
		}
		
		public function getRecentlyFlaggedOrders() {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql  = "SELECT o.entity_id,o.increment_id, o.created_at,o.base_subtotal, o.status,p.method,o.base_grand_total, o.additionalpaymentinfo FROM sales_flat_order o LEFT JOIN sales_flat_order_payment p ON o.entity_id=p.parent_id INNER JOIN transactions t ON t.ordernum=o.increment_id WHERE p.method IN ('creditcardoffline', 'cash', 'checkatbank', 'cod') AND o.status IN ('paid', 'complete', 'cod', 'received', 'Awaiting', 'order_refunded', 'holded') AND o.additionalpaymentinfo IN ('Door-step', 'CC Offline - Doorstep', 'Express Center', 'CC Offline - Express Center', '', 'NULL', 'cod') AND t.flagged=1 AND date_add(t.dateconfirmed,INTERVAL 1 HOUR) >= NOW()"; 
			return $read->fetchAll($sql);
		}
		
		public function updateAdditionalPaymentInfo($order_id, $pinfo) {
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$sql   = "UPDATE sales_flat_order SET additionalpaymentinfo='$pinfo' WHERE entity_id='$order_id' LIMIT 1";
			$write->query($sql);
		}
		
		public function getAdditionalpaymentinfobyentityid($entity_id) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql  = "SELECT additionalpaymentinfo FROM sales_flat_order where entity_id='$entity_id' LIMIT 1";
			$ret  = $read->fetchAll($sql);
			return $ret[0]['additionalpaymentinfo'];
		}

		public function getBaseCost($product_id, $order_id) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql  = "SELECT base_cost FROM sales_flat_order_item where product_id = $product_id AND order_id = $order_id LIMIT 1";
			$ret  = $read->fetchAll($sql);
			return $ret[0]['base_cost'];
		}
		
		public function getTotalInsuranceForQuote($quote_id) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT SUM(base_insurance) as totalinsurance FROM sales_flat_quote_item WHERE quote_id='$quote_id'";
			
			$ret = $read->fetchAll($sql);
			return $ret[0]['totalinsurance'];
		}

		public function updateDeliveryDate($order_id, $max_shipping_time) {
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');

			$today = date("Y-m-d");
			$date = new DateTime($today);
			$date->add(new DateInterval('P'.$max_shipping_time.'D'));
			$shipping_time = date_format($date, 'Y-m-d H:i:s');
			
			$sql = "UPDATE sales_flat_order SET delivery_date='$shipping_time' WHERE increment_id='$order_id' LIMIT 1";
			$write->query($sql);

			$sql = "UPDATE sales_flat_order_grid SET delivery_date='$shipping_time' WHERE increment_id='$order_id' LIMIT 1";
			$write->query($sql);

		}
		
		//UPDATE VENDOR PRODUCT ID
		function updateVendorOrderItemByProduct($productId,$item_id){
			

			$vendorId	=	Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'udropship_vendor');
			if(!empty($vendorId)&&$vendorId!=""){
					
				$this->updateOrderItemVendor($item_id, $vendorId);
				return $vendorId;
			}
			
			return 0;

		}
		
		//GET HAZIR SHIPPING AMOUNT
		function hazirShippingAmount($quoteId,$incrementId){
			
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT hazir_shipping FROM sales_flat_quote WHERE entity_id = '$quoteId'";
			
			$ret = $read->fetchAll($sql);
			if(isset($ret[0]['hazir_shipping'])){
				if(!empty($ret[0]['hazir_shipping']) && $ret[0]['hazir_shipping']!=null){
					if($ret[0]['hazir_shipping']>0){
						$hazir_shipping	=	$ret[0]['hazir_shipping'];
						return $ret[0]['hazir_shipping'];
					}
				}
			}
			
			//if not quote sana safinaz shipping found
			return $this->updateQuoteDataForSanaSafinazAmount($incrementId,$quoteId);
			
		}
		
		//IF NOT DATA FOUND IN QUOTE
		function updateQuoteDataForSanaSafinazAmount($incrementId,$quoteId){
			
			$totalAmount	=	0;
			
			$sql	=	<<<SQL
								
								SELECT 
									so.increment_id 	AS increment_id,
									so.base_grand_total AS base_grand_total,
									so.base_subtotal 	AS base_subtotal ,
									sfoi.order_id 		AS order_id,
									sfoi.qty_ordered 	AS qty_ordered,
									sfoi.product_type 	AS product_type 
								FROM 
									sales_flat_order_item  AS sfoi
								LEFT JOIN
								sales_flat_order AS so
								ON
									so.entity_id = sfoi.order_id
								WHERE 
									sfoi.udropship_vendor = 153 
								AND
									so.increment_id	= '$incrementId'	
								AND 
									sfoi.product_type = 'simple';
SQL;

			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$ret = $read->fetchAll($sql);
			
			if(!empty($ret)){
				
				$qty_ordered	=	0;
				foreach($ret as $retRow){
					
					$qty_ordered	+=	$retRow['qty_ordered'];
				}
				//TOTAL AMOUNT OF SHIPPING BASE ON QUANTITY
				$totalAmount	=	$qty_ordered * 200;
				
				
				$resource 			= Mage::getSingleton('core/resource');
				$writeConnection 	= $resource->getConnection('core_write');
				$sql				= "UPDATE sales_flat_quote SET hazir_shipping = '$totalAmount' WHERE reserved_order_id = '$incrementId'";
				
				// echo $sql;
				$writeConnection->query($sql);
				
			}
			
			return $totalAmount;
		}

	}
	
	
	
?>