<?php
	include "db_creds.php";
	
	class cron_class {
		public function get_cronlist() {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT * FROM tcs_cronlist";
			return $read->fetchAll($sql);
		}
		
		public function toggle_cron($cron_id, $new_status) {
			$sql   = "UPDATE tcs_cronlist SET active='$new_status' WHERE cronid='$cron_id' LIMIT 1";
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$write->query($sql);
		}
		
		public function get_cron($cron_id) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT * FROM tcs_cronlist WHERE cronid=$cron_id LIMIT 1";
			return $read->fetchAll($sql);
		}
		
		function if_cron_is_active($cronid) {			
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql  = "SELECT active FROM tcs_cronlist WHERE cronid='$cronid' LIMIT 1";
			$active = $read->fetchAll($sql);
			return $active[0]['active'];
		}
		
		function update_cron_log($cronid, $numaffected, $errordesc ) {
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$sql   = "INSERT INTO tcs_cronlog (cronid,numberaffected,executedon,errordesc) VALUES ('$cronid','$numaffected',NOW(),'$errordesc')";
			$write->query($sql);
			
			if( $numaffected == 0 && $cronid != 1) {
				$email  = $this->get_emails("crondashboard_email");
				$cron   = $this->get_cron($cronid);
				$cclist = $this->get_emails("crondashboard_cc");				
				$cronname = $cron[0]['cronname'];
				
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				// Additional headers
				$headers .= 'From: TCS Connect<cs@tcsconnect.com>' . "\r\n";				
				$to      = $email . "," . str_replace(";" , ",", $cclist);

				if ( stripslashes($errordesc) == "Success" || stripslashes($errordesc) == "Success!" ) {

					$subject = "TCS Production: Cron Status: Success: $cronname";
					$body    = "<br/><br/>Cron Run Success: $cronname<br/><br/>" .
						  		"Time Report: " . date("m-d-Y H:i:s") . "<br/><br/>";

				}
				else {

					$subject = "TCS Production: Issue with $cronname Detected: " . stripslashes($errordesc);
					$body    = "<br/><br/>Issue with Cron: $cronname has been detected<br/><br/>" .
							   "Issue: $errordesc <br/>Please notify the concerned<br/><br/>" .
							   "Time Report: " . date("m-d-Y H:i:s") . "<br/><br/>";

					@mail($to, $subject, $body, $headers);

				}

				
				
			}
		}
		
		function get_emails($config_name) { 
			$read   = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql    = "SELECT * FROM config_constants WHERE config_name='$config_name' LIMIT 1";
			$emails = $read->fetchAll($sql);
			return $emails[0]['config_value'];
		}
		
		function get_cron_log($cronid) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql  = "SELECT * FROM tcs_cronlog WHERE cronid='$cronid' ORDER BY executedon  DESC LIMIT 1";
			return $read->fetchAll($sql);
		}
		
		function get_last_run_date($cronid) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql  = "SELECT executedon FROM tcs_cronlog WHERE cronid='$cronid' ORDER BY executedon  DESC LIMIT 1";
			$ret = $read->fetchAll($sql);
			return $ret[0]['executedon'];
		}
	
	}
?>