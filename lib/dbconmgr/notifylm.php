<?php
	include "/var/www/tcsconnect_newui/app/Mage.php";	
	
	include "db_creds.php";
	include "dbconmgr.php";
	include "cron_class.php";
	
	Mage::app('default');
	include "/var/www/tcsconnect_newui/lib/dbconmgr/phpmailer/class.phpmailer.php";
	
	$db = new dbconmgr();
	$db->connect();
	
	$cc = new cron_class();
	
	if( $cc->if_cron_is_active("1") <> 1 ) {
		$cc->update_cron_log("1", 0, "Cron is not active" );
		exit;
	}
	
	$orders = $db->getUnpaidDoorstepOrders();
	$orders_details = array();
	
	$raw_template = file_get_contents("/var/www/tcsconnect_newui/lib/dbconmgr/new.html");	
	
	if( count($orders) < 1 ) {
		$cc->update_cron_log("1", 0, "Didn\'t find any orders" );
	}

	$filename = "";
	
	$lm_name = $db->getConfigConstant("lm_name");
	$lm_name = $lm_name['config_value'];
	
	$lm_email = $db->getConfigConstant("lm_email");
	$lm_email = $lm_email['config_value'];
	
	$numberoforders = count($orders);
	
	for($i=0; $i < count($orders); $i++) {
		$ordernum = $orders[$i]['increment_id'];
		$entity_id = $orders[$i]['entity_id'];
		
		$order = Mage::getModel('sales/order');
		$order->loadByIncrementId($ordernum);
		$method = $db->getPaymentMethod($entity_id);
		$method = $method['method'];
		
		if( $method == "cash" ) { $method = "Cash"; }
		else {
			$method = "Credit Card Offline";
		}
		
		$filename = "/tmp/" . $ordernum . ".html";		
			
		if( $order->getId() ) {
			$orderdate		    	 = date("m-d-Y H:i:s", strtotime($order->getCreatedAt()));
			$details['orderid'] 	 = $order->getId();
			$details['ordernumber']  = $order->getIncrementId();
			$details['customername'] = $order->getCustomerName();				
			$details['totaldue'] 	 = $order->getGrandTotal();	
			$details['orderdate']	 = $orderdate;
			
			echo "Processing " . $order->getIncrementId() . "\n";
			
			
			$cccsflag = $db->getCashCCFlag($order->getIncrementId());
			if( $cccsflag == "" ) { $flag = "N/A"; }
			else {
				if( $cccsflag['ccvscash'] == "cc" )  { 
					$flag="Credit Card"; 
					$ccextra = $details['totaldue'] + 2.5 * $details['totaldue']/100;
				}
				else { 
					$flag = "Cash"; 
					$ccextra = "";
				}
			}
			
			$shipping 			= $order->getShippingAddress();
			$shippingaddress 	= $shipping->getData('street');
			$shippingaddress   .= ", " . $shipping->getData('city');
			$shippingaddress   .= ", " . $shipping->getData('region');
			$shippingaddress   .= ", " . $shipping->getData('postcode');
			$shippingaddress   .= ", " . $shipping->getCountryModel()->getName();
			$details['shipping'] = $shippingaddress;
			
			$billing = $order->getBillingAddress();
			$billingaddress = $billing->getData('street');
			$billingaddress .= ", " . $billing->getData('city');
			$billingaddress .= ", " . $billing->getData('region');
			$billingaddress .= ", " . $billing->getData('postcode');
			$billingaddress .= ", " . $billing->getCountryModel()->getName();
			$details['billing'] = $billingaddress;
			$details['telephone'] = $billing->getData('telephone');
			$details['status'] = $order->getStatusLabel();
			$details['flag'] 	= $flag;
			
			$details['rsoname'] = Mage::getSingleton('admin/session')->getData('rsoname');			
			$details['rsousername'] = Mage::getSingleton('admin/session')->getData('rsousername');	

			$product_block = "";
			
			foreach ($order->getAllItems() as $item) {
			
				$row = array();
				$row['name'] = $item->getName();
				$row['price'] = $item->getPrice();
				$row["weight"]		= $item->getWeight();
				$row['qty']	  = intval($item->getData('qty_ordered'));
				$qty = intval($item->getData('qty_ordered'));
				
				$priceperrow = $item->getPrice() * $item->getData('qty_ordered');
				
				$vendorid = $item->getData('udropship_vendor');					
				//$row['vendor'] = $db->getVendor($vendorid);
				$vendor = $db->getVendor($vendorid);
				$items[] = $row;		

				$product_block .= '<tr>'.
									'<td align="left" valign="top">' . $row['name'] .'</td>' .
									'<td align="left" valign="top"><strong>' . $vendor['vendor_name'] . '</strong><br />' .	
									$vendor['street'] . " " . $vendor['city'] . '<br />' .
									$vendor['telephone'] . '</td>' .
									'<td align="center">' . $row['qty'] .'</td>' .
									'<td align="center"><p>' . number_format($row['price'],2) .'</p></td>' .
									'<td align="center"><p>' . number_format($priceperrow,2) . '</p></td>' .
								   '</tr>';
									
			}
			
			$details['items'] = $items;
			$details['shippingrate'] = $order->getShippingAmount();
			
			$orders_details[] = $details;
			
			$email_template = str_replace("||Customer_name||", $details['customername'], $raw_template);
			$email_template = str_replace("||Order_number||", $details['ordernumber'], $email_template);
			$email_template = str_replace("||Order_Total||", number_format($details['totaldue'],2), $email_template);
			$email_template = str_replace("||Billing_Address||", $billingaddress, $email_template);
			$email_template = str_replace("||Shipping_Address||", $shippingaddress, $email_template);
			$email_template = str_replace("||Customer_phone||", $details['telephone'], $email_template);
			$email_template = str_replace("||Method||", $method, $email_template);
			$email_template = str_replace("||CC_TOTAL||", $ccextra, $email_template);
			
			$email_template = str_replace("||Product_Block||", $product_block, $email_template);
			@file_put_contents($filename, $email_template, FILE_USE_INCLUDE_PATH);
			
			$mail = new PHPMailer();
			$mail->SetFrom('cs@tcs.com.pk', 'TCS');
			$mail->Subject = "Payment Collection for Order #$ordernum";
			$mail->MsgHTML("Please find the attached files");
			$mail->AddAttachment($filename);   
			
			// $mail->AddAddress("farhan.munir@nexdegree.com", "Farhan Munir");
			$mail->AddAddress("faisal.ijaz@tcs.com.pk", "Faisal Ijaz");
            $mail->AddCC("Mursaleen.rafiq@tcs.com.pk");
            $mail->AddCC("shakeel@tcs.com.pk");
					
			
			if(!$mail->Send()) {				
				$cc->update_cron_log("1", $numberoforders, "Error sending Email: " .  $mail->ErrorInfo );
			} else {
				$db->updateLMNotified($details['ordernumber']);
			}			
		}
	}
	
	if( $numberoforders > 0 )
		$cc->update_cron_log("1", $numberoforders, "Success!" );
?>
