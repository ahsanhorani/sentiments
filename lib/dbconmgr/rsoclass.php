<?php
	include "db_creds.php";
	
	class rsoclass {
	
		var $DB_CON=null;
		
		//Returns database resource/null otherwise
		public function connect() {
			global $DBSERVER, $DBUSER, $DBPWD, $DBNAME;
			
			$link = mysql_connect($DBSERVER, $DBUSER, $DBPWD);
			if (!$link) {
				echo 'Could not connect: ' . mysql_error();
				return;
			}
			//Select the database
			mysql_select_db( $DBNAME, $link) or die("Database doesn't exist: " . mysql_error() );
			$this->DB_CON=$link;
		}
		
		//public function which executes the query
		public function runquery($Sql) {
			if( $this->DB_CON != null ) {
				$result = mysql_query($Sql, $this->DB_CON);
				
				if( !$result) { 
					//echo 'Query is : ' . mysql_error();
					return null; 
				}
				return $result;
			}
		}
		
		public function validaterso($rso_mobile, $rso_pin) {
			$sql ="SELECT * FROM tcs_ec_user WHERE rso_mobile='$rso_mobile' AND rso_pin=md5('$rso_pin') LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public function getStationCode($ecid) {
			$sql = "SELECT * FROM expresscenter where eccode='$ecid' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public function getAllRSO($limit,$index) {
			$sql = "SELECT * FROM tcs_ec_user ORDER BY rso_mobile LIMIT $index,$limit";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) 
				$ret[] = $row;
			return $ret;
		}
		
		public function getRSO($rsoid) {
			$sql = "SELECT * FROM tcs_ec_user WHERE rsoid=$rsoid LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public function updateRSO($rsoid, $rsoname,$rsousername,$password) {
			$sql = "UPATE tcs_ec_user SET rso_name='$rsoname',rso_mobile='$rsousername', rso_pin=md5('$password') WHERE rsoid=$rsoid LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function toggleRSO($rsoid, $newstatus) {
			$sql = "UPDATE tcs_ec_user SET active=$newstatus WHERE rsoid=$rsoid LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function addRSO($rso_name,$rso_username,$rso_pin,$role) {
			$sql = "INSERT INTO tcs_ec_user (rso_name,rso_mobile,rso_pin,active,role) VALUES('$rso_name','$rso_username',md5('$rso_pin'), 1,'$role')";
			$result = $this->runquery($sql);
		}	
		
		public function updateRSOPin($rsoid, $newpin) {
			$sql = "UPDATE tcs_ec_user SET rso_pin=md5('$newpin') WHERE rsoid=$rsoid LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function isRSOPinValidById($rsoid, $rsopin) {
			$sql = "SELECT COUNT(rsoid) as valid FROM tcs_ec_user WHERE rsoid='$rsoid' AND rso_pin=md5('$rsopin') LIMIT 1";
			
			$result = $this->runquery($sql);
			
			if( $result == null ) { return 0; }
			$row = mysql_fetch_array($result,MYSQL_BOTH);
			return $row['valid'];
			
		}
	}	
?>