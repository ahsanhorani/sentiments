<?php
	include "db_creds.php";
	include "dbconmgr.php";

	include "../../app/Mage.php";
	
	Mage::app('admin');
	
	$db = new dbconmgr();
	$db->connect();	
	
	$orders = $db->getAllOrders();
	
	for($i=0; $i < count($orders); $i++) {
		$entityid = $orders[$i]['entity_id'];
		$incrementid = $orders[$i]['increment_id'];
		
		echo "$entityid - $incrementid\n";
		$db->updateSalesFlatOrderGrid($incrementid);
	}
?>