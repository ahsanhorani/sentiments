<?php
class dc_express_info {
  public $address; // string
  public $area; // string
  public $contact; // string
  public $exp_code; // string
  public $exp_name; // string
  public $exp_type; // string
  public $nearest_mark; // string
  public $other_contact; // string
  public $region; // string
  public $station; // string
}

class CsFaults {
  public $Operation; // string
  public $ProblemType; // string
}

class dc_tracking_info {
  public $bkg_date; // dateTime
  public $cnsg_no; // string
  public $consignee_name; // string
  public $cus_no; // string
  public $dstn; // string
  public $orgn; // string
  public $shipper_name; // string
}

class dc_tracking_delivery {
  public $area; // string
  public $cnsg_no; // string
  public $dlvry_date; // string
  public $dlvry_time; // string
  public $recvd_by; // string
  public $status; // string
}

class dc_Area_Name {
  public $Area_Name; // string
}

class dc_pickups {
  public $area; // string
  public $call_id; // string
  public $consignee_address; // string
  public $consignee_name; // string
  public $need_packaging_material; // string
  public $parcel_type; // string
  public $shipper_address; // string
  public $shipper_cell_ph; // string
  public $shipper_email; // string
  public $shipper_name; // string
  public $weight; // double
}

class dc_complaint_stype {
  public $cmplntype; // string
}

class dc_complaint_category {
  public $category_code; // string
  public $category_desc; // string
}

class dc_complaint_IssueType {
  public $ComplainType; // string
  public $Routing; // string
  public $category_code; // string
  public $issue_code; // string
  public $issue_desc; // string
}

class GetDataBy_Name {
  public $exp_name; // string
}

class GetDataBy_NameResponse {
  public $GetDataBy_NameResult; // ArrayOfdc_express_info
}

class GetDataBy_Code {
  public $exp_code; // string
}

class GetDataBy_CodeResponse {
  public $GetDataBy_CodeResult; // ArrayOfdc_express_info
}

class GetDataBy_Station {
  public $station; // string
}

class GetDataBy_StationResponse {
  public $GetDataBy_StationResult; // ArrayOfdc_express_info
}

class GetDataBy_cnsgno {
  public $cnsg_no; // string
}

class GetDataBy_cnsgnoResponse {
  public $GetDataBy_cnsgnoResult; // ArrayOfdc_tracking_info
}

class GetDataBy_CusNo {
  public $cus_no; // string
  public $area; // string
  public $from_date; // string
  public $to_date; // string
}

class GetDataBy_CusNoResponse {
  public $GetDataBy_CusNoResult; // ArrayOfdc_tracking_info
}

class GetDataBy_Delivery {
  public $cnsg_no; // string
}

class GetDataBy_DeliveryResponse {
  public $GetDataBy_DeliveryResult; // ArrayOfdc_tracking_delivery
}

class GetAreasBy_CusNo {
  public $cnsg_no; // string
}

class GetAreasBy_CusNoResponse {
  public $GetAreasBy_CusNoResult; // ArrayOfdc_Area_Name
}

class Add_pickup {
  public $_object; // dc_pickups
}

class Add_pickupResponse {
  public $Add_pickupResult; // string
}

class Get_PickNo {
  public $pickupid; // string
}

class Get_PickNoResponse {
  public $Get_PickNoResult; // ArrayOfdc_pickups
}

class Add_Complaint {
  public $vcnsgno; // string
  public $vshiperName; // string
  public $vshipercellno; // string
  public $vshipdetail; // string
  public $vcnsgname; // string
  public $vorigin; // string
  public $vdestn; // string
  public $vcategory; // string
  public $vissue_type; // string
  public $vPriority; // string
  public $vcomplaintype; // string
  public $vbkgdate; // dateTime
  public $first_name; // string
  public $last_name; // string
}

class Add_ComplaintResponse {
  public $Add_ComplaintResult; // string
}

class Get_CompliantType {
}

class Get_CompliantTypeResponse {
  public $Get_CompliantTypeResult; // ArrayOfdc_complaint_stype
}

class Get_CompliantCategory {
}

class Get_CompliantCategoryResponse {
  public $Get_CompliantCategoryResult; // ArrayOfdc_complaint_category
}

class Get_CompliantIssueType {
  public $category_code; // string
  public $ComplainType; // string
}

class Get_CompliantIssueTypeResponse {
  public $Get_CompliantIssueTypeResult; // ArrayOfdc_complaint_IssueType
}

class char {
}

class duration {
}

class guid {
}


/**
 * CSWebservice class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class CSWebservice extends SoapClient {

  private static $classmap = array(
                                    'dc_express_info' => 'dc_express_info',
                                    'CsFaults' => 'CsFaults',
                                    'dc_tracking_info' => 'dc_tracking_info',
                                    'dc_tracking_delivery' => 'dc_tracking_delivery',
                                    'dc_Area_Name' => 'dc_Area_Name',
                                    'dc_pickups' => 'dc_pickups',
                                    'dc_complaint_stype' => 'dc_complaint_stype',
                                    'dc_complaint_category' => 'dc_complaint_category',
                                    'dc_complaint_IssueType' => 'dc_complaint_IssueType',
                                    'GetDataBy_Name' => 'GetDataBy_Name',
                                    'GetDataBy_NameResponse' => 'GetDataBy_NameResponse',
                                    'GetDataBy_Code' => 'GetDataBy_Code',
                                    'GetDataBy_CodeResponse' => 'GetDataBy_CodeResponse',
                                    'GetDataBy_Station' => 'GetDataBy_Station',
                                    'GetDataBy_StationResponse' => 'GetDataBy_StationResponse',
                                    'GetDataBy_cnsgno' => 'GetDataBy_cnsgno',
                                    'GetDataBy_cnsgnoResponse' => 'GetDataBy_cnsgnoResponse',
                                    'GetDataBy_CusNo' => 'GetDataBy_CusNo',
                                    'GetDataBy_CusNoResponse' => 'GetDataBy_CusNoResponse',
                                    'GetDataBy_Delivery' => 'GetDataBy_Delivery',
                                    'GetDataBy_DeliveryResponse' => 'GetDataBy_DeliveryResponse',
                                    'GetAreasBy_CusNo' => 'GetAreasBy_CusNo',
                                    'GetAreasBy_CusNoResponse' => 'GetAreasBy_CusNoResponse',
                                    'Add_pickup' => 'Add_pickup',
                                    'Add_pickupResponse' => 'Add_pickupResponse',
                                    'Get_PickNo' => 'Get_PickNo',
                                    'Get_PickNoResponse' => 'Get_PickNoResponse',
                                    'Add_Complaint' => 'Add_Complaint',
                                    'Add_ComplaintResponse' => 'Add_ComplaintResponse',
                                    'Get_CompliantType' => 'Get_CompliantType',
                                    'Get_CompliantTypeResponse' => 'Get_CompliantTypeResponse',
                                    'Get_CompliantCategory' => 'Get_CompliantCategory',
                                    'Get_CompliantCategoryResponse' => 'Get_CompliantCategoryResponse',
                                    'Get_CompliantIssueType' => 'Get_CompliantIssueType',
                                    'Get_CompliantIssueTypeResponse' => 'Get_CompliantIssueTypeResponse',
                                    'char' => 'char',
                                    'duration' => 'duration',
                                    'guid' => 'guid',
                                   );

  public function CSWebservice($wsdl = "http://tcspk.com/csweb/cswcfsvc.svc?wsdl", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param GetDataBy_Name $parameters
   * @return GetDataBy_NameResponse
   */
  public function GetDataBy_Name(GetDataBy_Name $parameters) {
    return $this->__soapCall('GetDataBy_Name', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetDataBy_Code $parameters
   * @return GetDataBy_CodeResponse
   */
  public function GetDataBy_Code(GetDataBy_Code $parameters) {
    return $this->__soapCall('GetDataBy_Code', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetDataBy_Station $parameters
   * @return GetDataBy_StationResponse
   */
  public function GetDataBy_Station(GetDataBy_Station $parameters) {
    return $this->__soapCall('GetDataBy_Station', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetDataBy_cnsgno $parameters
   * @return GetDataBy_cnsgnoResponse
   */
  public function GetDataBy_cnsgno(GetDataBy_cnsgno $parameters) {
    return $this->__soapCall('GetDataBy_cnsgno', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetDataBy_CusNo $parameters
   * @return GetDataBy_CusNoResponse
   */
  public function GetDataBy_CusNo(GetDataBy_CusNo $parameters) {
    return $this->__soapCall('GetDataBy_CusNo', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetDataBy_Delivery $parameters
   * @return GetDataBy_DeliveryResponse
   */
  public function GetDataBy_Delivery(GetDataBy_Delivery $parameters) {
    return $this->__soapCall('GetDataBy_Delivery', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetAreasBy_CusNo $parameters
   * @return GetAreasBy_CusNoResponse
   */
  public function GetAreasBy_CusNo(GetAreasBy_CusNo $parameters) {
    return $this->__soapCall('GetAreasBy_CusNo', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param Add_pickup $parameters
   * @return Add_pickupResponse
   */
  public function Add_pickup(Add_pickup $parameters) {
    return $this->__soapCall('Add_pickup', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param Get_PickNo $parameters
   * @return Get_PickNoResponse
   */
  public function Get_PickNo(Get_PickNo $parameters) {
    return $this->__soapCall('Get_PickNo', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param Add_Complaint $parameters
   * @return Add_ComplaintResponse
   */
  public function Add_Complaint(Add_Complaint $parameters) {
    return $this->__soapCall('Add_Complaint', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param Get_CompliantType $parameters
   * @return Get_CompliantTypeResponse
   */
  public function Get_CompliantType(Get_CompliantType $parameters) {
    return $this->__soapCall('Get_CompliantType', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param Get_CompliantCategory $parameters
   * @return Get_CompliantCategoryResponse
   */
  public function Get_CompliantCategory(Get_CompliantCategory $parameters) {
    return $this->__soapCall('Get_CompliantCategory', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param Get_CompliantIssueType $parameters
   * @return Get_CompliantIssueTypeResponse
   */
  public function Get_CompliantIssueType(Get_CompliantIssueType $parameters) {
    return $this->__soapCall('Get_CompliantIssueType', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

}

?>
