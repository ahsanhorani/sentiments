<?php
	set_time_limit(0);

	include "db_creds.php";
	include "dbconmgr.php";
	include "/var/www/tcsconnect/app/Mage.php";
	
	Mage::app('admin');
	
	$db = new dbconmgr();
	$db->connect();
	
	$fromdate = "2012-06-01";
	$todate   = "2012-06-30";
	
	$orders   = $db->getCompleteOrdersForPLR($fromdate, $todate);
	
	$filename = "/tmp/PL_Report_" . date("d_m_Y") . ".csv";
	
	$fp = @fopen($filename, "a");
	
	$header = "TCS(PVT)LTD - E-Business Revenue For the " . date("d/M/Y", strtotime($fromdate)) . " - " . date("d/M/Y", strtotime($todate)) . "\n\n";
	
	$header .= ",,,,,,,,,,,,Revenue Amount (Cash Inflow),,,,,Expense Amount (Cash Outflow)\n";
	
	$header .= "S.No, Order #, Customer name, Product sold, Product Category, Area/Location, Method,Received at, Order 'Paid' on, Buyer Price, Vendor name, Payable to vendor (Cost Price), Commsion Rate %, Commission Amount, Courier Charges CUSTOMER being charged, Insurance Cost CUSTOMER being charged, Total, Bank Charges, Insurance Cost TCS Connect being charged, Insurance in transit, Packing Cost TCS Connect being charged, Courier Charges TCS Connect being charged, Total, Net Revenue (Proft/ Loss)\n";

	fwrite($fp, $header);
	$totalbuyerprice  =0; $totalcostprice    =0; $totalcommission  =0; $totalshipping         =0; $totalcustinsurance =0;
	$totalinflowtotal =0; $totalbankcharges  =0; $totaltcsinsurance=0; $totalinsurancetransit =0; $totalpackingcost   =0; 
	$totaltcsshipping =0; $totaloutflowtotal =0; $totalrevenue     =0;
	
	$bankcharges   = $db->getConfigConstant("bankcharges");
	$bankcharges   = $bankcharges['config_value'];
	
	$sno = 1;
	
	try {
		
		for($i=0; $i < count($orders); $i++) {
			$entityid 		= $orders[$i]['entity_id'];
			
			//$order_ind = $db->getOrderDetailsForPLR($entityid);
			
			$incrementid 	= $orders[$i]['increment_id'];
			$status 	 	= $orders[$i]['status'];
			$grandtotal 	= $orders[$i]['grand_total'];
			$shippingamount = $orders[$i]['shipping_amount'];
			$api   			= $orders[$i]['additionalpaymentinfo'];
			$method 		= $db->getPaymentmethod($entityid);
			$method 		= $method['method'];
			$createdat		= date("d/M/Y H:i", strtotime($orders[$i]['created_at']));
			$paymethod 		= $db->getRealPaymentMethodName($method);
			$productcategory = "";
			
			// echo $orders[$i]['entity_id'] . "\n";
			
			$orderObj=Mage::getModel('sales/order')->load($entityid);
			
			
			$customername = $orderObj->getCustomerName();
			$products     = $orderObj->getAllItems();
			
			$productname = $products[0]->getName();
			$productname = str_replace(",", " ", $productname);
			$productid   = $products[0]->getId();
			
			$shipping 			= $orderObj->getShippingAddress();
			$shippingcity		= $shipping->getCity();
			$shippingcityid 	= $db->getCityId( $shippingcity );
			$pmarea				= $db->getPMAreaByCityId($shippingcityid);
			
			$_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $products[0]->getData('sku'));	
			
			if( $_product == "" || $_product == null ) continue;
			$categoryIds = $_product->getCategoryIds();
			
			// foreach($categoryIds as $categoryId) {
				// $category = Mage::getModel('catalog/category')->load($categoryId);
				// $productcategory .=  $category->getName() . "/";
			// }

			$buyerprice = str_replace("," , "", number_format($_product->getPrice(),2));
			$costprice  = str_replace(",", "", number_format($_product->getCost(),2));
			$commissionrate = ceil(($buyerprice-$costprice)*100/$buyerprice);
			$commission = $buyerprice - $costprice;
			
			$vendorid   = $_product->getData('udropship_vendor');
			
			$vendor 	= $db->getVendor($vendorid);
			$vendorname = str_replace(",", " ", $vendor['vendor_name']);
			$insurancecost = $db->getProductInsuranceAmount($products[0]->getData('sku'));
			if( $insurancecost == "") {
				$insurancecost = 0;
			}
			$totalinflow   = $buyerprice + $shippingamount + $insurancecost;
			
			
			$paidtype = $method . " " . $api;
			$paidtype = trim($paidtype);
			
			$insurancetransit = $buyerprice * 0.2/100;
			
			$packingcost = $db->getConfigConstant("packing_rate");
			$packingcost = $packingcost['config_value'];
			
			$couriercharges = $shippingamount;
			
			$transaction = $db->getTransactionByOrderID($incrementid);
			
			if( $method == "easypaisa" || $method == "internetbanking" || $method == "twocheckout_shared" || $method == "paypal_standard" ) {
				$arealocation = "DBD";
			}
			else {
				if( $transaction['eccode'] <> 0 ) {
					$ecinfo = $db->getECInformationByEcID($transaction['eccode']);
					$arealocation = $ecinfo[0]['ecstation'];
				}	
				
				if( strlen($transaction['pm_area']) > 0 ) {
					$arealocation = $transaction['pm_area'];
				}
			
				// if( $transaction['eccode'] == 0 && strlen($transaction['pm_area']) > 0 ) 
					// $arealocation = $transaction['pm_area'];
				// else if( $transaction['eccode'] > 0 ) {
					// $ecinfo = $db->getECInformation($transaction['eccode']);
					// $arealocation = $ecinfo[0]['ecstation'];
				// }
				// else { $arealocation = ""; }
				
				//$arealocation = $transaction['eccode'] . " - " . $transaction['pm_area'];
			}
			
			$totaloutflow = $bankcharges + $insurancecost + $insurancetransit + $packingcost + $couriercharges;
			$revenue = $commission + $shippingamount + $insurancecost - $totaloutflow;
			
			
			$totalbuyerprice += $buyerprice; $totalcostprice  += $costprice; $totalcommission += $commission; 
			$totalshipping   += $shippingamount; $totalcustinsurance += $insurancecost; $totalinflowtotal +=$totalinflow;
			$totalbankcharges += $bankcharges; $totaltcsinsurance += $insurancecost; $totalinsurancetransit += $insurancetransit;
			$totalpackingcost += $packingcost; $totaltcsshipping += $couriercharges; $totaloutflowtotal += $totaloutflow;
			$totalrevenue += $revenue;			
			
			fwrite($fp, "$sno, $incrementid, $customername,$productname,$productcategory, $arealocation, $paymethod, $api, $createdat, $buyerprice,$vendorname, $costprice, $commissionrate, $commission, $shippingamount, $insurancecost, $totalinflow, $bankcharges,$insurancecost, $insurancetransit, $packingcost, $couriercharges, $totaloutflow, $revenue\n");
			
		
			if( count($products) > 1 ) {
				for($j=1; $j < count($products); $j++) {
					$sno++;
					$productname = $products[$j]->getName();
					$productcategory = "";
					$_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $products[$j]->getData('sku'));			
					
					$categoryIds = $_product->getCategoryIds();
					
					// foreach($categoryIds as $categoryId) {
						// $category = Mage::getModel('catalog/category')->load($categoryId);
						// $productcategory .=  $category->getName() . "/";
					// }
					
					$buyerprice = str_replace("," , "", number_format($_product->getPrice(),2));
					$costprice  = str_replace(",", "", number_format($_product->getCost(),2));
					
					$vendorid   = $_product->getData('udropship_vendor');			
					$vendor 	= $db->getVendor($vendorid);
					$vendorname = str_replace(",", " ", $vendor['vendor_name']);
					
					$commissionrate = ceil(($buyerprice-$costprice)*100/$buyerprice);
					$commission = $buyerprice - $costprice;
					
					$shippingamount = 0;
					$insurancecost = $db->getProductInsuranceAmount($products[$j]->getData('sku'));
					if( $insurancecost == "") {
						$insurancecost = 0;
					}
					$totalinflow   = $buyerprice + $shippingamount + $insurancecost;
					$bankcharges = 0;
					
					$insurancetransit = $buyerprice * 0.2/100;
					$packingcost = $db->getConfigConstant("packing_rate");
					$packingcost = $packingcost['config_value'];
					
					$totaloutflow = $bankcharges + $insurancecost + $insurancetransit + $packingcost + $couriercharges;
					$revenue = $commission + $shippingamount + $insurancecost - $totaloutflow;
					
					fwrite($fp, "$sno,$incrementid,$customername, $productname, $productcategory,$arealocation,$paymethod,$api,$createdat, $buyerprice,$vendorname,$costprice,$commissionrate,$commission,$shippingamount,$insurancecost,$totalinflow,$bankcharges,$insurancecost,$insurancetransit,$packingcost,$shippingamount,$totaloutflow, $revenue\n");
					
					$totalbuyerprice += $buyerprice; $totalcostprice  += $costprice; $totalcommission += $commission; 
					$totalshipping   += $shippingamount; $totalcustinsurance += $insurancecost; $totalinflowtotal +=$totalinflow;
					$totalbankcharges += $bankcharges; $totaltcsinsurance += $insurancecost; $totalinsurancetransit += $insurancetransit;
					$totalpackingcost += $packingcost; $totaltcsshipping += $shippingamount; $totaloutflowtotal += $totaloutflow;
					$totalrevenue += $revenue;
					
				}
			} 
			$sno++;
		}
		
		
		fwrite($fp, "\n,,,,,,,,Total, $totalbuyerprice,, $totalcostprice, , $totalcommission, $totalshipping, $totalcustinsurance,$totalinflowtotal, $totalbankcharges, $totaltcsinsurance, $totalinsurancetransit, $totalpackingcost, $totaltcsshipping, $totaloutflowtotal, $totalrevenue\n");
		
	} catch (Exception $e) {  ; }
	
	@fclose($fp);	
?>