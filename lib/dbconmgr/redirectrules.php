<?php
	include "db_creds.php";
	
	class redirectrules {
		var $DB_CON=null;
		
		//Returns database resource/null otherwise
		public function connect() {
			global $DBSERVER, $DBUSER, $DBPWD, $DBNAME;
			
			$link = mysql_connect($DBSERVER, $DBUSER, $DBPWD);
			if (!$link) {
				echo 'Could not connect: ' . mysql_error();
				return;
			}
			//Select the database
			mysql_select_db( $DBNAME, $link) or die("Database doesn't exist: " . mysql_error() );
			$this->DB_CON=$link;
		}
		
		//public function which executes the query
		public function runquery($Sql) {
			if( $this->DB_CON != null ) {
				$result = mysql_query($Sql, $this->DB_CON);				
				if( !$result) { 
					//echo 'Query is : ' . mysql_error();
					return null; 
				}
				return $result;
			}
		}
		
		public function getVendors() {
			$sql = "SELECT vendor_id, url_key FROM udropship_vendor";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			
			while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
				$ret[] = $row;
			}
			return $ret;
		}
		
		public function writeAccessfile() {
			$vendors = $this->getVendors();
			$rules = "";
			for($i=0; $i < count($vendors); $i++) {
				$line = "RewriteCond %{REQUEST_URI} ^/vendors/" . $vendors[$i]['url_key'] . "\n" .
				        "RewriteRule (.*)$ /vendors/index/show/id/" . $vendors[$i]['vendor_id'] . "\?udropship_vendor\=" . $vendors[$i]['vendor_id'] . " [R=301,L]\n\n";
						
				$rules .= $line;
			}
			
			$filedata = @file_get_contents("htaccess");
			$filedata = str_replace("||VENDOR_BLOCK||", $rules, $filedata);
			
			@file_put_contents("/var/www/tcsconnect/.htaccess", $filedata);
		}
	}	
	
	$rr = new redirectrules();
	$rr->connect();
	$rr->writeAccessfile();
	
?>