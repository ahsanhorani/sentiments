<?php
	include "CSWebservice.php";
	
	class trackingapi {
		function getDeliveryInfo($cnnumber) {
			$client = new SoapClient("http://tcspk.com/csweb/cswcfsvc.svc?wsdl");
			$parameters['cnsg_no'] = $cnnumber;
			$consignment_data = $client->GetDataBy_Delivery($parameters);
			$status =  $consignment_data->GetDataBy_DeliveryResult;

			$data = array();
			
			if( count($consignment_data) > 0 ) {
				$data['error'] = 0;
				$data['dlvry_date'] = $status->dc_tracking_delivery->dlvry_date;
				$data['dlvry_time'] = $status->dc_tracking_delivery->dlvry_time;
				$data['recvd_by'] 	= $status->dc_tracking_delivery->recvd_by;
				$data['status'] 	= $status->dc_tracking_delivery->status;
			}
			else {
				$data['error'] = 1;
				$data['dlvry_date'] = "";
				$data['dlvry_time'] = "";
				$data['recvd_by'] 	= "";
				$data['status'] 	= "";
			}

			echo "<HR>";

			return $data;
		}
	
		function getTrackingInfo($cnnumber) {
			$data = array();
			
			if( strlen($cnnumber) < 5 || $cnnumber == null ) {
				$data['valid'] = 0;
				$data['error1'] = 1;
				$data['bkg_date'] = null;
				$data['cndata'] = array();
				$data = array();
			}
			else {
				$client = new CSWebservice();
				$consignment = new GetDataBy_cnsgno();
				$consignment->cnsg_no = $cnnumber;
				
				//$consignment_data = $client->GetDataBy_cnsgno($consignment);
				$consignment_data = $client->GetDataBy_Delivery($cnnumber);
				
				if(isset($consignment_data->GetDataBy_cnsgnoResult->dc_tracking_info))
				{
					$consignment_data = $consignment_data->GetDataBy_cnsgnoResult->dc_tracking_info;
					
					$data_size = sizeof($consignment_data);
					$data = array();
					
					if($data_size == 1)
					{
						$data['valid'] = 1;
						$data['error1'] = 0;
						$cnsgn = (array)$consignment_data;
						$bkg_date = $cnsgn['bkg_date'];
						$bkg_date = strtotime($bkg_date);
						$bkg_date = date('D M d H:i:s ',$bkg_date).'GMT'.date('O Y',$bkg_date);
						$data['bkg_date'] = $bkg_date;
						$data['cndata'] = $cnsgn;		
					}
					elseif($data_size > 1)
					{
						$data['valid'] = 1;
						$data['error1'] = 0;
						$cnsgn = (array)$consignment_data[$data_size-1];		
						$bkg_date = $cnsgn['bkg_date'];
						$bkg_date = strtotime($bkg_date);
						$bkg_date = date('D M d H:i:s ',$bkg_date).'GMT'.date('O Y',$bkg_date);
						$data['bkg_date'] = $bkg_date;
						$data['cndata'] = $cnsgn;
					}
					elseif($data_size == 0)
					{
						$data['valid'] = 0;
						$data['error1'] = 1;
						$data['bkg_date'] = null;
						$data['cndata'] = array();
					}
					
					if($data['valid'] == 1)
					{
						$delivery = new GetDataBy_Delivery();
						$delivery->cnsg_no = $_POST['cnnum'];
						
						$delivery_data = $client->GetDataBy_Delivery($delivery);
						
						if(isset($delivery_data->GetDataBy_DeliveryResult->dc_tracking_delivery))
						{
							$delivery_data = $delivery_data->GetDataBy_DeliveryResult->dc_tracking_delivery;
							$data_size1 = sizeof($delivery_data);
							
							if($data_size1 == 1)
							{
								$data['valid'] = 1;
								$data['error2'] = 0;
								$data['delivery'] = (array)$delivery_data;
							}
							elseif($data_size1 > 1)
							{
								$data['valid'] = 1;
								$data['error2'] = 0;
								$data['delivery'] = (array)$delivery_data[$data_size1-1];
							}
							elseif($data_size1 == 0)
							{
								$data['valid'] = 0;
								$data['error2'] = 1;
								$data['delivery'] = array();
							}
						}
						else
						{
							$data['valid'] = 0;
							$data['error2'] = 1;
							$data['delivery'] = array();
						}
					}
			}
			
		}
		return $data;
		}
		
		function getManifestbycnnumber($cn_number) {
			$res = NULL;
			
			if( strlen($cn_number) < 5 || $cn_number == null ) {
				$res =	FALSE;
			}
			else{
				$client = new SoapClient("http://tcspk.com/csweb/cswcfsvc.svc?wsdl");
				//$parameters['cnsg_no'] = "30035564424";
				$parameters['cnsg_no'] = $cn_number;
				$consignment_data = $client->GetManifestStatus($parameters);
				$res	=	$consignment_data->GetManifestStatusResult->Manifested;
				
				$tmp = print_r($consignment_data,true);
				@file_put_contents("/tmp/manifest.log", $tmp);
				
			}
			
			return $res;
		}
		
		function getStatusbycnnumber($cnnumber){
			
			$data['error'] = 1;
			$data['dlvry_date'] = NULL;
			$data['dlvry_time'] = NULL;
			$data['recvd_by'] 	= NULL;
			$data['status'] 	= NULL;				
				
			$client 				=	new SoapClient("http://tcspk.com/csweb/cswcfsvc.svc?wsdl");
			$parameters['cnsg_no'] 	= 	$cnnumber;
			$consignment_data 		= 	$client->GetDataBy_Delivery($parameters);//Mage::debug($consignment_data);			
			$cn_data				=	$consignment_data->GetDataBy_DeliveryResult->dc_tracking_delivery;
			$count					=	count($cn_data);
			if(count($cn_data)>0){
				if($count	==	1){
					if($cn_data->status == "DELIVERED"){				
						$data['error'] = 0;
						$data['dlvry_date'] = $cn_data->dlvry_date;
						$data['dlvry_time'] = $cn_data->dlvry_time;
						$data['recvd_by'] 	= $cn_data->recvd_by;
						$data['status'] 	= $cn_data->status;
					}
				}
				else{
				
					for($i=0; $i < $count; $i++) {
						// $index	=	$count-1;
						if($cn_data[$i]->status == "DELIVERED"){
							$data['error'] = 0;
							$data['dlvry_date'] = $cn_data[$i]->dlvry_date;
							$data['dlvry_time'] = $cn_data[$i]->dlvry_time;
							$data['recvd_by'] 	= $cn_data[$i]->recvd_by;
							$data['status'] 	= $cn_data[$i]->status;
							break;
						}
					}
				}
			}

			return $data;
		}
		
	function getOnlyStatusbycnnumber($cnnumber){
			
			$data['error'] = 1;
			$data['dlvry_date'] = NULL;
			$data['dlvry_time'] = NULL;
			$data['recvd_by'] 	= NULL;
			$data['status'] 	= NULL;				
				
			$client 				=	new SoapClient("http://tcspk.com/csweb/cswcfsvc.svc?wsdl");
			$parameters['cnsg_no'] 	= 	$cnnumber;
			$consignment_data 		= 	$client->GetDataBy_Delivery($parameters);//Mage::debug($consignment_data);			
			$cn_data				=	$consignment_data->GetDataBy_DeliveryResult->dc_tracking_delivery;
			$count					=	count($cn_data);
			if(count($cn_data)>0){
				if($count	==	1){
					//if($cn_data->status == "DELIVERED"){				
						$data['error'] = 0;
						$data['dlvry_date'] = $cn_data->dlvry_date;
						$data['dlvry_time'] = $cn_data->dlvry_time;
						$data['recvd_by'] 	= $cn_data->recvd_by;
						$data['status'] 	= $cn_data->status;
					//}
				}
				else{
					$index	=	$count-1;
					//if($cn_data[$index]->status == "DELIVERED"){
						$data['error'] = 0;
						$data['dlvry_date'] = $cn_data[$index]->dlvry_date;
						$data['dlvry_time'] = $cn_data[$index]->dlvry_time;
						$data['recvd_by'] 	= $cn_data[$index]->recvd_by;
						$data['status'] 	= $cn_data[$index]->status;
					//}
					
				}
			}

			return $data;
		}
	}
?>