<?php
	include "./dbconmgr.php";
	
	$db = new dbconmgr();
	$db->connect();
	
	//Read the CSV file
	$fh = fopen("tcs_cityzonemap.csv", "r");
	$count = 0;
	
	while( ($line = fgetcsv($fh,1000,",", '"')) !== FALSE ) {	
		$city 	  = mysql_escape_string(trim($line[0]));
		$zone     = mysql_escape_string(trim($line[1]));
		$area	  = mysql_escape_string(trim($line[2]));
		$areacode = mysql_escape_string(trim($line[3]));
		
		$cityid =  $db->importcities($city, $zone);
		$areaid =  $db->getPaymentAreaId($areacode);
		
		echo "$city - $cityid ::: $areacode - $areaid\n";
		
		$db->addCityAreaMap($cityid, $areaid);
	}
	
	echo "\n\nDONE\n\n";
?>