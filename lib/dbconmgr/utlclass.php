<?php
	include "db_creds.php";
	
	class utlclass {
	
		var $DB_CON=null;
		
		//Returns database resource/null otherwise
		public function connect() {
			global $DBSERVER, $DBUSER, $DBPWD, $DBNAME;
			
			$link = mysql_connect($DBSERVER, $DBUSER, $DBPWD);
			if (!$link) {
				echo 'Could not connect: ' . mysql_error();
				return;
			}
			//Select the database
			mysql_select_db( $DBNAME, $link) or die("Database doesn't exist: " . mysql_error() );
			$this->DB_CON=$link;
		}
		
		//public function which executes the query
		public function runquery($Sql) {
			if( $this->DB_CON != null ) {
				$result = mysql_query($Sql, $this->DB_CON);
				
				if( !$result) { 
					//echo 'Query is : ' . mysql_error();
					return null; 
				}
				return $result;
			}
		}
		
		public function getpaymenttype($orderid) {
			$sql ="SELECT method FROM sales_flat_order_payment WHERE parent_id='$orderid' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public function updatecheckno($orderid, $checknum) {
			$sql = "UPDATE sales_flat_order SET checknum='$checknum' WHERE entity_id='$orderid' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function update_order_ec_pm_area($eccode, $ordernum) {
			$sql = "SELECT ecarea FROM expresscenter WHERE eccode='$eccode' LIMIT 1";
			$result = $this->runquery($sql);			
			$row = mysql_fetch_array($result, MYSQL_BOTH);
			
			if( count($row) < 2 ) {
				return;
			}
			
			$pm_area = $row['ecarea'];
			
			$sql = "UPDATE transactions set pm_area='$pm_area' WHERE ordernum='$ordernum' LIMIT 1";
			$result = $this->runquery($sql);			
		}
		
		public function saveTransaction($ordernum, $orderdate, $customername,$billingaddress, $shippingaddress, $paymentmethod, $eccode,$paymentlocation,  $orderamount, $orgtxnid, $approved_by) {
			$sql = "SELECT * FROM transactions where ordernum='$ordernum'";		
			$result = $this->runquery($sql);
			$row = mysql_fetch_array($result, MYSQL_BOTH);
			
			if( count($row) > 1 ) {
				return;
			}
			
			$sql = "INSERT INTO transactions( ordernum,orderdate,customername,billingaddress,shippingaddress,paymentmethod,eccode,paymentlocation, paymentts,orderamount, org_txnid,approved_by) VALUES( '$ordernum', '$orderdate','$customername', '$billingaddress', '$shippingaddress','$paymentmethod', '$eccode', '$paymentlocation', NOW(),$orderamount, '$orgtxnid', '$approved_by')";
			
			$this->runquery($sql);
			
			if( $eccode <> 0 && $eccode <> "")
				$this->update_order_ec_pm_area($eccode, $ordernum);
				
			if( $paymentmethod == "Cash On Delivery" ) {
				//using shipping city address get the payment area
				$sql = "SELECT a.city FROM sales_flat_order_address a INNER JOIN sales_flat_order o ON a.parent_id=o.entity_id WHERE o.increment_id='$ordernum' AND a.address_type='billing' LIMIT 1";
				$result = $this->runquery($sql);
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$shippingcity = $row['city'];
				if( $shippingcity <> "" ) {
					$sql = "select * FROM tcs_service_area_cities where cityname='$shippingcity'";
					$result = $this->runquery($sql);
					$row = mysql_fetch_array($result, MYSQL_BOTH);
					$pm_area = $row['area'];
					$sql = "UPDATE transactions set pm_area='$pm_area' WHERE ordernum='$ordernum' LIMIT 1";
					$this->runquery($sql);
				}
			}
		}
		
		public function getTcsPaymentAreas() {
			$ret = array();
			$sql = "SELECT * FROM tcs_payment_areas ORDER BY areacode ASC";
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result,MYSQL_BOTH) ) { $ret[] = $row; }
			return $ret;
		}
		
		public function updateTransactionPMArea($ordernum, $pmarea) {
			$sql = "UPDATE transactions SET pm_area='$pmarea' WHERE ordernum='$ordernum' LIMIT 1";
			$this->runquery($sql);
		}
		
		public function customercreditreport($fromdate, $todate=null) {
			if( $todate == null ) {		
				$sql = "select * FROM customercredit_credit c INNER JOIN customercredit_credit_log l ON c.credit_id=l.credit_id WHERE date(l.action_date)='$fromdate' ORDER BY action_date DESC";
			}
			else {
				$sql = "select * FROM customercredit_credit c INNER JOIN customercredit_credit_log l ON c.credit_id=l.credit_id WHERE date(l.action_date) >='$fromdate' AND date(l.action_date) <= '$todate' ORDER BY action_date DESC";
			}
			
			$result = $this->runquery($sql);
			while( $row = mysql_fetch_array($result,MYSQL_BOTH) ) { $ret[] = $row; }
			return $ret;
		}
	}	
?>