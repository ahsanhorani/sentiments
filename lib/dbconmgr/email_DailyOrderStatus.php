<?php
	include "/var/www/tcsconnect_newui/app/Mage.php";	
	include "/var/www/tcsconnect_newui/lib/dbconmgr/phpmailer/class.phpmailer.php";	
	Mage::app('admin');
	
	include "cron_class.php";
	
	$croncls = new cron_class();
	
	if( $croncls->if_cron_is_active("12") <> 1 ) {
		$croncls->update_cron_log("12", 0, "Cron is not active" );
		exit;
	}
	
	global $status;
	
	$status = array( 
			"processing" 		=> "Processing",
			"canceled"   		=> "Canceled",
			"cod"		 		=> "Cash on Delivery",
			"payment_review"	=> "Payment Review",
			"received"			=> "Received From Vendor",
			"complete"			=> "Complete",
			"paid"				=> "Paid",
			"pending"			=> "Pending",
			"order_refunded"	=> "Refunded",
			"holded"			=> "On Hold",
			"closed"			=> "Closed",
			"refund"			=> "Pending Refunds"
		);
		
	function get_OldestOrder($status) {
		$sql = "SELECT increment_id FROM sales_flat_order WHERE status='$status' and created_at >= '2012-03-23' ORDER BY created_at ASC LIMIT 1";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result = $connection->fetchAll($sql);		
		return $result[0]['increment_id'];
	}
	
		
	function get_Config($config_name) {
		$sql = "SELECT config_value FROM config_constants WHERE config_name='$config_name' LIMIT 1";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result = $connection->fetchAll($sql);
		
		return $result[0]['config_value'];
	}
	
	function get_OrdersList() {
		$sql = "select status, count(entity_id) as numorders, sum(base_grand_total) as revenue FROM sales_flat_order_grid WHERE status IN ('processing', 'canceled', 'cod', 'payment_review', 'received', 'complete','paid','pending', 'order_refunded', 'holded', 'closed', 'refund') GROUP BY status ORDER BY numorders DESC";
		
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result = $connection->fetchAll($sql);
		
		return $result;
	}
	
	function create_email($results) {
		global $status;
	
		$mail = new PHPMailer();
		$mail->SetFrom('cs@yayvo.com', 'Yayvo.com');
		$mail->Subject = "TCS Connect Daily Email Order Status (" . date("d-m-Y") . ")";
		
		$header = "<table width='100%' style='font-family:Arial; font-size:12px;' cellpadding='0' cellspacing='0' border='1'>".
			"<tr>
			  <td width='10px' align='center'><b>Sr.No.</b></td>
			  <td width='200px' align='center'><b>Status</b></td>
			  <td width='150px' align='center'><b>Total Orders</b></td>
			  <td width='180px' align='center'><b>Revenue</b></td>
			  <td width='150px' align='center'><b>Oldest Order</b></td>
			</tr>";
			
		$body = "";
			
		if( count($results) < 1 ) {
			add_logentry("Daily Order Status Cron","Daily 10:00 AM", "0 Orders found" );		
		}
		
		$sno	=	1;
		for($i=0; $i < count($results); $i++) {
			$body .= "<tr>" .
						"<td>$sno</td>" .
						"<td align='left'>" . $status[$results[$i]['status']] . "</td>" .
						"<td align='right'>" . intval($results[$i]['numorders']) . "</td>" .
						"<td align='right'>" . number_format(round($results[$i]['revenue']),2) . "</td>" .
						"<td align='right'>" . get_OldestOrder($results[$i]['status']) . "</td>" .
					"</tr>";
					
			$sno++;
		}
		
		$body .= "</table>";
		
		
		$mail->MsgHTML( $header . $body);
		
		$email   = get_Config("dailyorderstatus_cron");
		$cc_list = get_Config("dailyorderstatus_cron_cc");
		
		//  $email	=	"arshad@nextgeni.com";
		$mail->AddAddress($email);
		
		$tmp = explode(";", $cc_list);
		
		for($i=0; $i < count($tmp); $i++) {
			$mail->AddCC($tmp[$i]);
		}
		
		if(!$mail->Send()) { 
			echo "Mailer Error: " . $mail->ErrorInfo . "\n\n"; 
			add_logentry("Daily Order Status Cron","Daily 10:00 AM", "Mailer Error: " . $mail->ErrorInfo );		
		} 
		else { echo "Message sent!\n\n"; }
	}	
	
	$orders = get_OrdersList();
	$numberoforders = count($orders);
	
	if( $numberoforders < 1 ) {
		$croncls->update_cron_log("12", 0, "No orders found" );
	}
	
	create_email( $orders );
	$croncls->update_cron_log("12", $numberoforders, "Success!" );