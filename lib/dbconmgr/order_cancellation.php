<?php 

	require_once 'E:\wamp\www\tcs\app\Mage.php';
	umask(0);
	Mage::app();

class Order_cancellation{
		
	public function indexAction(){
		/**
		 * Select all orders with status processing and created date is older than 15days
		 */
		$orders = Mage::getModel('sales/order')->getCollection()
											    ->addFieldToFilter('status', 'processing')
											    ->addFieldToFilter('created_at', array(
											    	'lt' =>  new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL 15 day)")                   
										        ))
											    ->addAttributeToSelect('customer_email')
											    ->addAttributeToSelect('created_at')
											    ->addAttributeToSelect('increment_id')
											    ->addAttributeToSelect('status')
											    ->addAttributeToSelect('entity_id')
											    ->addAttributeToSort('entity_id', 'DESC')							    
	    ;	    
	   $orders->getSelect()->limit('1');
	   
	 echo  $total_records	=	count($orders);

	   /**
	    * If total records are greater than 0 than do further processing
	    */
	   if($total_records>0){
			foreach($orders as $order){
				$increment_id	=	$order->getIncrement_id();
				$order_id		=	$order->getEntity_id();
			    $customer_email = 	$order->getCustomerEmail();
			    $created_date	= 	$order->getCreated_at();
			    $status			= 	$order->getStatus();
			    
			    if($status	==	"processing"){
			    	$order = Mage::getModel('sales/order')->load($order_id);
			    	
			    	/**
			    	 * Updating order state to cancel
			    	 */
			    	$order->cancel()->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, 'Order is canceled via daily cron.', true)->save();
			    	
			    	/**
			    	 * Sending order updates email to customer
			    	 */ 
			    	$order->sendOrderUpdateEmail(true);
			    	 
			    }
		    
		    
		    
		    echo $main	=	<<<HTML
		   	 	Main Order:$order_id<br>
		   	 	Order: $increment_id<br>
		   	 	status: $status<br>
		   	 	customer_email: $customer_email<br>
		   	 	created_date: $created_date<br><br><br>
HTML;
			}
	   }
	}
	
	
}

	$oc	=	new Order_cancellation();
	$oc->indexAction();

?>