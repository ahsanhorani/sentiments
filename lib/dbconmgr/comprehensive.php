<?php
	set_time_limit(0);

	include "dbconmgr.php";

	$db = new dbconmgr();
	$db->connect();

	$orders = $db->get_invoicedorders("2013-01-01", "2013-02-28");
	
	for($i=0; $i < count($orders); $i++) {
		$entity_id    = $orders[$i]['order_id'];
		$created_at = $db->getInvoiceCreateDate($entity_id);
		
		$details = $db->get_orderdetails($entity_id);
		if( $details['status'] == "canceled" ) continue;
		
		$increment_id = $details['increment_id'];
		$customer 	  = $details['customer_firstname'] . " " . $details['customer_lastname'] ;
		$method   	  = $db->getPaymentmethod($entity_id);
		$method   	  = $method['method'];
		$api		  = $details['additionalpaymentinfo'];
		$datepaid	  = date("m-d-Y", strtotime($created_at));
		$grandtotal   = $details['base_subtotal']  +   $details['base_shipping_amount'] + $details['vas_charges'] ;
		$transaction  = $db->getTransactionByOrderID($increment_id);		
		$arealocation = $db->getUBDBDB($method, $api);
		$sno = $i+1;
		
		if( $transaction['eccode'] <> 0 ) {
			$ecinfo = $db->getECInformationByEcID(@$transaction['eccode']);
			$arealocation = @$ecinfo[0]['ecstation'];
		}	
		
		if( strlen($transaction['pm_area']) > 0 ) {
			$arealocation = @$transaction['pm_area'];
		}
		
		if( $arealocation == "UBD" ) {
			$cityname = $db->getBillingCity($entity_id);
			$arealocation = $db->getCityAreaId($cityname);
			$arealocation = $db->getPMAreaByCityId($arealocation);
		}
		
		$line = "$sno, $increment_id, $customer, $arealocation, $method, $api, $datepaid, $grandtotal\n";
		
		@file_put_contents("/tmp/comp1_2.csv", $line, FILE_APPEND);		
	}
	
	
	/*
	$orders = $db->get_ordersforcompreport("2012-03-23", "2012-12-31");	
	
	for($i=0; $i < count($orders); $i++) {
		$entity_id    = $orders[$i]['entity_id'];
		$increment_id = $orders[$i]['increment_id'];
		$customer 	  = $orders[$i]['customer_firstname'] . " " . $orders[$i]['customer_lastname'] ;
		$method   	  = $db->getPaymentmethod($entity_id);
		$method   	  = $method['method'];
		$api		  = $orders[$i]['additionalpaymentinfo'];
		$datepaid	  = date("m-d-Y", strtotime($orders[$i]['created_at']));
		$grandtotal   = $orders[$i]['base_subtotal']  +   $orders[$i]['base_shipping_amount'] + $orders[$i]['vas_charges'] ;
		$transaction  = $db->getTransactionByOrderID($increment_id);		
		$arealocation = $db->getUBDBDB($method, $api);
		$sno = $i+1;
		
		
		if( $transaction['eccode'] <> 0 ) {
			$ecinfo = $db->getECInformationByEcID(@$transaction['eccode']);
			$arealocation = @$ecinfo[0]['ecstation'];
		}	
		
		if( strlen($transaction['pm_area']) > 0 ) {
			$arealocation = @$transaction['pm_area'];
		}
		
		$line = "$sno, $increment_id, $customer, $arealocation, $method, $api, $datepaid, $grandtotal\n";
		
		@file_put_contents("/tmp/comp1.csv", $line, FILE_APPEND);		
	}
	*/
	
?>