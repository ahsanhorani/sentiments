<?php
	include "db_creds.php";
	include "dbconmgr.php";

	include "../../app/Mage.php";
	
	Mage::app('admin');	
	
	$db = new dbconmgr();
	$db->connect();
	
	$orders   = $db->getCompleteOrdersForPLR("2012-03-23", "2012-09-18");
	
	$filename = "/tmp/allordersreport" . date("d_m_Y_H_i_s") . ".csv";
	
	
	
	$fp = @fopen($filename, "w");
	
	for($i=0; $i < count($orders); $i++) {
		$order_id		= $orders[$i]['entity_id'];
		$incrementid 	= $orders[$i]['increment_id'];
		$api   			= $orders[$i]['additionalpaymentinfo'];
		$method 		= $db->getPaymentmethod($order_id);
		$method 		= $method['method'];
		$createdat		= date("d/M/Y H:i", strtotime($orders[$i]['created_at']));
		$paymethod 		= $db->getRealPaymentMethodName($method);
		$customername   = $orders[$i]['customer_firstname'] . " " . $orders[$i]['customer_lastname'];
		$totalpaid      = $orders[$i]['grand_total'];
		$arealocation   = "";
		
		if( $orders[$i]['order_currency_code'] <> "PKR" ) {
			$rate  = Mage::helper('directory')->currencyConvert(1, "PKR", "USD");
			$newtotalpaid = $totalpaid/$rate;
			$totalpaid = round($newtotalpaid);
		}
		
		$transaction = $db->getTransactionByOrderID($incrementid);
		
		if( $method == "easypaisa" || $method == "internetbanking" || $method == "twocheckout_shared" || $method == "paypal_standard" ) {
			$arealocation = "DBD";
		}
		else {
			if( @$transaction['eccode'] <> 0 ) {
				$ecinfo = $db->getECInformationByEcID($transaction['eccode']);
				$arealocation = $ecinfo[0]['ecstation'];
			}	
			
			if( strlen(@$transaction['pm_area']) > 0 ) {
				$arealocation = $transaction['pm_area'];
			}
		}
		
		$paydate        = date("m-d-Y H:i:s", strtotime($db->getInvoiceCreateDate($order_id)));
		
		fwrite($fp, "$incrementid,$customername,$arealocation,$paymethod,$api,$paydate,$totalpaid\n");
	}
	@fclose($fp);	
	
	
?>