<?php
	include "../../app/Mage.php";	
	include "/var/www/tcsconnect_dev/lib/dbconmgr/phpmailer/class.phpmailer.php";	
	Mage::app('admin');
	
	function get_config_constant($constantname) {
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql  = "SELECT config_value FROM config_constants WHERe config_name='$constantname' LIMIT 1";
		$result = $read->fetchAll($sql);
		return $result[0]['config_value'];
	}
	
	
	
	function get_list_of_orders_with_insurance($from_date, $to_date) {
		$sql = "SELECT i.entity_id, itm.name,i.created_at,itm.base_original_price,itm.base_insurance FROM sales_flat_invoice i INNER JOIN sales_flat_order_item itm ON itm.order_id=i.order_id WHERE date(i.created_at) >= '$from_date' AND date(i.created_at) <= '$to_date' AND itm.base_insurance > 0 ORDER BY i.entity_id";
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result = $read->fetchAll($sql);
		return $result;
	}
	
	function get_order_increment_id($entity_id) {
		$sql = "SELECT increment_id FROM sales_flat_order WHERE entity_id=$entity_id LIMIT 1";
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result = $read->fetchAll($sql);
		return $result[0]['increment_id'];
	}
	
	$from_date = date("Y-m-01", strtotime("-1 month") ) ;
	$to_date   = date("Y-m-01");	
	
	$from_date = "2013-07-01";
	$to_date   = "2013-08-01";
	
	$subject = "TCS Connect Monthly Insurance Report (" . date("M-Y") . ")";
	$body    = "Dear Pak-Qatar Takaful,<br/>" .
			   "Please find below TCS Connect consolidated insurance details booked in the " . date("M-Y") . "<br/><br/>";
			   
	$orders = get_list_of_orders_with_insurance($from_date, $to_date);
	$totalprice = 0;
	$totalinsurance = 0;
	
	$body .= "<table width='100%' cellpadding='0' cellspacing='0'>" .
			"<tr>" .
				"<th>Sr #</th>" .
				"<th>Order #</th>" .
				"<th>Product Title</th>" .
				"<th>Order Paid</th>" .
				"<th>Retail Price</th>" .
				"<th>Insurance Amount</th>" .
			"</tr>";
	
	for($i=0; $i < count($orders); $i++) {
		$entity_id   = $orders[$i]['entity_id'];
		$name		 = $orders[$i]['name'];
		$paid_on	 = date("d-m-Y", strtotime($orders[$i]['created_at']));
		$price       = number_format($orders[$i]['base_original_price'],2);
		$insurance   = number_format($orders[$i]['base_insurance'],2);
		$incrementid = get_order_increment_id($entity_id);
		
		$totalprice 	+= $orders[$i]['base_original_price'];
		$totalinsurance += $orders[$i]['base_insurance'];
		$sno = $i+1;
		
		$body .= "<tr>" .
					"<td align='center'>$sno</td>" .
					"<td align='center'>$incrementid</td>" .
					"<td align='center'>$name</td>" .
					"<td align='center'>$paid_on</td>" .
					"<td align='center'>$price</td>" .
					"<td align='center'>$insurance</td>" .
				"</tr>";
	}
	
	$body .= "<tr>" .	
				"<td></td>" .
				"<td></td>" .
				"<td></td>" .
				"<td>Total</td>" .
				"<td align='center'>$totalprice</td>" .
				"<td align='center'>$totalinsurance</td>" .
			"</tr>";
			
	$body .= "<br/>Regs,<br/><strong>TCS Connect</strong><br/>021-111-210-210";
	
	$email   = get_config_constant("insurance_report_email");
	$cc_list = get_config_constant("insurance_report_email_cc");
	
	$mail = new PHPMailer();
	$mail->SetFrom('cs@tcsconnect.com', 'TCSConnect');
	$mail->Subject = $subject;
		
	$mail->AddAddress($email);
	$tmp = explode(";", $cc_list);
		
	for($i=0; $i < count($tmp); $i++) {
		$mail->AddCC($tmp[$i]);
	}
	
	$mail->MsgHTML( $body);
		
	if(!$mail->Send()) { echo "Mailer Error: " . $mail->ErrorInfo . "\n\n"; } 
	else { echo "Message sent!\n\n"; }
?>