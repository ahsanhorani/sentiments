<?php
	include "db_creds.php";
	include "/var/www/tcsconnect_newui/lib/dbconmgr/excellib/Classes/PHPExcel.php";
	include "/var/www/tcsconnect_newui/lib/dbconmgr/phpmailer/class.phpmailer.php";
	include "/var/www/tcsconnect_newui/app/Mage.php";
	include "cron_class.php";
	
	Mage::app('admin');
	
	class ofscript {
	
		var $DB_CON=null;
		
		//Returns database resource/null otherwise
		public function connect() {
			global $DBSERVER, $DBUSER, $DBPWD, $DBNAME;
			
			$link = mysql_connect($DBSERVER, $DBUSER, $DBPWD);
			if (!$link) {
				echo 'Could not connect: ' . mysql_error();
				return;
			}
			//Select the database
			mysql_select_db( $DBNAME, $link) or die("Database doesn't exist: " . mysql_error() );
			$this->DB_CON=$link;
		}
		
		//public function which executes the query
		public function runquery($Sql) {
			if( $this->DB_CON != null ) {
				$result = mysql_query($Sql, $this->DB_CON);
				
				if( !$result) { 
					//echo 'Query is : ' . mysql_error();
					return null; 
				}
				return $result;
			}
		}
		
		public function getConfigConstant($config_name) {
			$sql = "SELECT config_value FROM config_constants WHERE config_name='$config_name' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return "";
			$row = mysql_fetch_array($result, MYSQL_BOTH);
			return $row['config_value'];
		}
		
		public function getNumOfProductsOrdered($increment_id) {
			$sql = "SELECT total_qty_ordered FROM sales_flat_order where increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null) return 0;
			$row = mysql_fetch_array($result,MYSQL_BOTH);
			return $row['total_qty_ordered'];
		}
		
		public function getECInformation($increment_id) {
			$sql = "select * FROM expresscenter e INNER JOIN transactions t ON t.eccode=e.eccode AND t.ordernum='$increment_id'";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			return mysql_fetch_array($result, MYSQL_BOTH);
		}
		
		public function getAreaCoordinatorInfo($increment_id) {
			$sql = "select teu.* FROM transactions t INNER JOIN tcs_ec_user teu ON t.approved_by=teu.rso_mobile WHERE t.ordernum='$increment_id' AND teu.role='a' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			return mysql_fetch_array($result, MYSQL_BOTH);	
		}
		
		public function getPMArea($increment_id) {
			$sql = "SELECT pm_area FROM transactions WHERE ordernum='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return array();
			return mysql_fetch_array($result, MYSQL_BOTH);	
		}
		
		public function markPostToOF($increment_id) {
			$sql = "UPDATE sales_flat_order SET posted_to_OF=1 WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function markDPDUBDStatus($increment_id, $payment_type) {
			$sql = "UPDATE sales_flat_order SET payment_type='$payment_type' WHERE increment_id='$increment_id' LIMIT 1";
			$result = $this->runquery($sql);
		}
		
		public function findSpecialHandling($increment_id) {
			$ret = 0.00;
			try {
				$orderObj=Mage::getModel('sales/order')->loadByIncrementId($increment_id);
				
				foreach ($orderObj->getAllItems() as $item) {
					$productid = $item->getId();
					$productname = $item->getName();
					$_product= Mage::getSingleton('catalog/product')->load($item->getProductId());
					$ret += $_product->getResource()->getAttribute('special_packing')->getFrontend()->getValue($_product);
				}
				
			} catch (Exception $e) {  Mage::getSingleton('adminhtml/session')->addError($e->getMessage()); }
			return $ret;
		}
		
		public function getInsuranceRate($price) {
			$sql = "select rate FROM insurance_rates where from_price <= $price AND to_price >= $price LIMIT 1";
			$result = $this->runquery($sql);
			if( $result == null ) return 0;
			$row = mysql_fetch_array($result, MYSQL_BOTH);	
			return $row['rate'];
		}
		
		public function getInsurance($increment_id) {
			$ret = 0.00;
			try {
				$orderObj=Mage::getModel('sales/order')->loadByIncrementId($increment_id);
				
				foreach ($orderObj->getAllItems() as $item) {
					$productid 	 = $item->getId();
					$productname = $item->getName();
					$price 		 = $item->getPrice();
					$_product	 = Mage::getSingleton('catalog/product')->load($item->getProductId());
					
					$insurance_offered = $_product->getResource()->getAttribute('insurance_offered')->getFrontend()->getValue($_product);
					
					echo $productname . " - " . $insurance_offered . "\n";
					
					if( $_product->getResource()->getAttribute('insurance_offered')->getFrontend()->getValue($_product) == "User Paid") {
						$ret += $this->getInsuranceRate($price);
					}
					else if( $_product->getResource()->getAttribute('insurance_offered')->getFrontend()->getValue($_product) == "No" ) {
						if( count($orderObj->getAllItems()) == 1 ) {
							return "N/A";
						}
					}
				}
				
			} catch (Exception $e) {  Mage::getSingleton('adminhtml/session')->addError($e->getMessage()); }
			return $ret;
		}
		
		private function add_logentry($cronname,$scheduledtime, $errordesc) {
			$sql = "INSERT INTO tcs_cronlog (cronname,scheduledtime,executedon, errordesc) VALUES ('$cronname', '$scheduledtime', NOW(), '$errordesc')";
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$write->query($sql);
		}
		
		//Function that retrieves the data for OF Transactions
		public function getTodaysPayments($reportdate=null) {
		
			if( $reportdate == null ) {
				$sql = "select sfi.entity_id, sfi.order_id, sfi.created_at, sfo.status, sfo.increment_id,sfo.subtotal,sfo.total_due,sfo.additionalpaymentinfo, sfop.method, sfo.shipping_amount FROM sales_flat_invoice sfi INNER JOIN sales_flat_order sfo ON sfo.entity_id=sfi.order_id INNER JOIN sales_flat_order_payment  sfop ON sfop.parent_id=sfo.entity_id WHERE DATE(sfi.created_at) = DATE(NOW()) AND (sfo.status='paid' or sfo.status='complete') AND sfo.posted_to_OF <> 1";
			}
			else {
				$sql = "select sfi.entity_id, sfi.order_id, sfi.created_at, sfo.status, sfo.increment_id,sfo.subtotal,sfo.total_due,sfo.additionalpaymentinfo, sfop.method, sfo.shipping_amount FROM sales_flat_invoice sfi INNER JOIN sales_flat_order sfo ON sfo.entity_id=sfi.order_id INNER JOIN sales_flat_order_payment  sfop ON sfop.parent_id=sfo.entity_id WHERE DATE(sfi.created_at) = '$reportdate' AND (sfo.status='paid' or sfo.status='complete') AND sfo.posted_to_OF <> 1";
			}
			
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;	
		}
		
		public function getIncentiveCharges($paidtype) {
			$ret = 0.00;
			
			if( $paidtype == "cash Express Center" || $paidtype == "cash Door-step" || $paidtype == "creditcardoffline CC Offline - Express Center" || $paidtype == "creditcardoffline CC Offline - Doorstep" ) {
				$rate = $this->getConfigConstant('incentivecharges');
				$ret  = $rate;	
			}
			return $ret;
		}
		
		public function getBankCharges($paidtype, $orderamount) {
			$ret = 0.00;
			
			if($paidtype == "paypal_standard") {
				$rate = $this->getConfigConstant('ppcharges');
				$ret  = $orderamount * $rate/100;
			}
			else if( $paidtype == "twocheckout_shared" ) { 
				$rate = $this->getConfigConstant('2cocharges');
				echo "rate for 2cocharages; $rate\n\n";
				$ret  = $orderamount * $rate/100;
			}
			else if( $paidtype == "cash Bank Deposit- (United Bank Limited)" || $paidtype == "cash Bank Deposit- (Allied Bank Limited)" || 
				     $paidtype == "cash Bank Deposit- (Habib Bank Limited)") {
				$rate = $this->getConfigConstant('bankcharges');
				$ret  = $rate;
			}
			else if( $paidtype == "creditcardoffline CC Offline - Express Center" || $paidtype == "creditcardoffline" ||
					$paidtype == "creditcardoffline CC Offline - Doorstep" ) {
				$rate = $this->getConfigConstant('offlinecc');
				$ret  = $prderamount * $rate/100;		
			}
			else if( $paidtype == "checkatbank Bank Deposit - (United Bank Limited)" || $paidtype == "checkatbank Bank Deposit - (Allied Bank Limited)" ||
					 $paidtype == "checkatbank Bank Deposit - (Habib Bank Limited)") {
				$rate = $this->getConfigConstant('bankcharges');
				$ret  = $rate;	
			}
			else if( $paidtype == "internetbanking" || $paidtype == "internetbanking UBL Net-Banking" || $paidtype == "internetbanking HBL Net-Banking" ||
				$paidtype == "internetbanking ABL Net-Banking" ) {
				$rate = $this->getConfigConstant('bankcharges');
				$ret  = $rate;	
			}
			else if( $paidtype == "easypaisa" ) {
				$rate = $this->getConfigConstant('easypaisacharges');
				$ret  = $rate;	
			}
			else if( $paidtype == "cash Express Center" ) {
				$rate = $this->getConfigConstant('cashateccharges');
				$ret  = $rate;	
			}
			else if( $paidtype == "cash Door-step" ) {
				$rate = $this->getConfigConstant('cashatdoorstepcharges');
				$ret  = $rate;	
			}
			
			else if( $paidtype == "ublomni" ) {
				$rate = $this->getConfigConstant('cashatdoorstepcharges');
				$ret  = $rate;	
			}
				
			return $ret;
		}
		
		public function getPaymenttype($paidtype) {
			$map = array( "paypal_standard" 							=> "DPD",
				  "twocheckout_shared" 									=> "DPD",
				  "cash Bank Deposit- (United Bank Limited)" 			=> "DPD",
				  "cash Bank Deposit- (Allied Bank Limited)"			=> "DPD",
				  "internetbanking"										=> "DPD",
				  "internetbanking UBL Net-Banking"						=> "DPD",
				  "internetbanking HBL Net-Banking"						=> "DPD",
				  "internetbanking CITI Net-Banking"					=> "DPD",
				  "internetbanking ABL Net-Banking"						=> "DPD",
				  "cash Bank Deposit- (Habib Bank Limited)" 			=> "DPD",
				  "easypaisa"											=> "DPD",
				  "ublomni"												=> "DPD",
				  "cash Express Center" 								=> "UBD",
				  "cash Door-step" 										=> "UBD",
				  "checkatbank" 										=> "UBD",
				  "creditcardoffline CC Offline - Express Center"		=> "UBD",
				  "creditcardoffline" 									=> "UBD",
				  "creditcardoffline CC Offline - Doorstep"				=> "UBD",
				  "checkatbank Bank Deposit - (United Bank Limited)" 	=> "UBD",
				  "checkatbank Bank Deposit - (Allied Bank Limited)"	=> "UBD",
				  "checkatbank Bank Deposit - (Habib Bank Limited)"		=> "UBD",
				);
				
			return $map[trim($paidtype)];
		}
		
		public function createExcel($paymentinfo, $email=null, $reportdate) {
			$td = explode("-", $reportdate);
			$reportdate = $td[2].$td[1].$td[0];
			$accperiod  = $td[2] . "/" . $td[1] . "/" . $td[0];
			$headerdate = $td[2].$td[1].($td[0]-2000);
			$filename = "/tmp/EB" . $reportdate. ".xlsx";
			
			$objPHPExcel = new PHPExcel();

			// Set properties
			$objPHPExcel->getProperties()->setCreator("TCS Connect");
			$objPHPExcel->getProperties()->setLastModifiedBy("TCS Connect");
			$objPHPExcel->getProperties()->setTitle("");
			$objPHPExcel->getProperties()->setSubject("");
			$objPHPExcel->getProperties()->setDescription("");

			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'EB' . $headerdate . ".HOF");
			
			$objPHPExcel->getActiveSheet()->SetCellValue('B3', 'ACC_NO');
			$objPHPExcel->getActiveSheet()->SetCellValue('C3', 'ORIGIN');
			$objPHPExcel->getActiveSheet()->SetCellValue('D3', 'RATED_AMT');
			$objPHPExcel->getActiveSheet()->SetCellValue('E3', 'H_CHARGES');
			$objPHPExcel->getActiveSheet()->SetCellValue('F3', 'DISC_SHIP');
			$objPHPExcel->getActiveSheet()->SetCellValue('G3', 'DISC_BOTT');
			$objPHPExcel->getActiveSheet()->SetCellValue('H3', 'INVOICE_NO');
			$objPHPExcel->getActiveSheet()->SetCellValue('I3', 'SHIPMENTS');
			$objPHPExcel->getActiveSheet()->SetCellValue('J3', 'PE_DUTY');
			$objPHPExcel->getActiveSheet()->SetCellValue('K3', 'GST');
			$objPHPExcel->getActiveSheet()->SetCellValue('L3', 'PREMIUM');
			$objPHPExcel->getActiveSheet()->SetCellValue('M3', 'PRODUCT');
			$objPHPExcel->getActiveSheet()->SetCellValue('N3', 'ACC_PERIOD');
			$objPHPExcel->getActiveSheet()->SetCellValue('O3', 'PARTNER_AMT');
			$objPHPExcel->getActiveSheet()->SetCellValue('P3', 'PACKING');
			$objPHPExcel->getActiveSheet()->SetCellValue('Q3', 'BANK_CHARGES');
			$objPHPExcel->getActiveSheet()->SetCellValue('R3', 'INCENTIVE');
			$objPHPExcel->getActiveSheet()->SetCellValue('S3', 'SPECIAL_PACKING');
			$objPHPExcel->getActiveSheet()->SetCellValue('T3', 'INSURANCE');

			$linenumber 		 = 4;
			$totalrated 		 = 0;
			$totalgst			 = 0;
			$totalinsurance 	 = 0;
			$totalproduct		 = 0;
			$totalubd			 = 0;
			$totalbpd			 = 0;
			$totalpacking		 = 0;
			$totalbankcharges 	 = 0;
			$totalincentives  	 = 0;
			$totalspecialpacking = 0;
			
			
			for($i=0; $i < count($paymentinfo); $i++) {
				
				$accountno 				= "EB.23.TC.";				
				$incrementid 			= $paymentinfo[$i]['increment_id'];
				$totalamt	 			= $paymentinfo[$i]['subtotal'];
				$shippingamount			= $paymentinfo[$i]['shipping_amount'];
				$additionalpaymentinfo 	= trim($paymentinfo[$i]['additionalpaymentinfo']);
				$method					= trim($paymentinfo[$i]['method']);
				
				$deposittype			= $this->getPaymenttype($method . " " . $additionalpaymentinfo);
				$paidtype				= trim($method . " " . $additionalpaymentinfo);
				
				//update payment type for this incrementid
				$this->markDPDUBDStatus($incrementid, $deposittype);
				//$this->markPostToOF($incrementid);
				
				/************* New Fields Integration for 1.0.5 Starts *************/
				
				//Packing Cost P3
				$packing_cost = $this->getConfigConstant('packing_rate');
				$numproducts  = $this->getNumOfProductsOrdered($incrementid);
				$packing = $packing_cost * $numproducts;
				
				//Bank Charges Q3
				$bank_charges = $this->getBankCharges($paidtype, $totalamt);
				
				//Incentive Charges R3
				$incentive_charges = $this->getIncentiveCharges($paidtype);
				
				//Special Packing S3
				$special_packing = $this->findSpecialHandling($incrementid);
				
				//Insurance Rates T3
				$insurance = $this->getInsurance($incrementid);
				
				$totalinsurance   	 += $insurance;
				$totalpacking	  	 += $packing;
				$totalbankcharges 	 += $bank_charges;
				$totalincentives  	 += $incentive_charges;
				$totalspecialpacking += $special_packing;
				
				/************* New Fields Integration for 1.0.5 Ends  *************/
				
				if($deposittype == "DPD") { 
					$origin 	= "HOF";
					$invoiceno  = "$origin.HOF.$incrementid";
					
					$totaldpd += $totalamt + $shippingamount + $insurance + $packing + $bank_charges + $incentive_charges + $special_packing;
				}
				else {
					$ecinfo = $this->getECInformation($incrementid);
					if( count($ecinfo) < 10 ) {	
						//Order has been moved forward by Area Accountant - get his info
						// $aainfo = $this->getAreaCoordinatorInfo($incrementid);
						
						$pmarea = $this->getPMArea($incrementid);
						
						if( count($pmarea) > 1 ) {
							$origin = $pmarea['pm_area'];
							$stcode = $pmarea['pm_area'];
						}
						else {					
							$origin = "HOF";
							$stcode = "HOF";
						}
					}
					else {
						$origin = $ecinfo['ecarea'];
						$stcode = $ecinfo['ecstation'];
					}
					
					// $origin = "RWP";
					// $stcode = "CKL";
					
					$invoiceno = "$origin.$stcode.$incrementid";
					
					$totalubd += $totalamt + $shippingamount + $insurance + $packing + $bank_charges + $incentive_charges + $special_packing;
				}
				$accountno .= $origin . "." . $deposittype;
				$ratedamt   = $shippingamount;
				$totalrated += $ratedamt;
				
				$gst	 	= $ratedamt * 0.16;
				$totalgst	+= $gst;
								
				$premium    = $ratedamt * 0.01;
				
				
				
				
				//$accperiod  = date("m/d/Y");
				//$partneramt = $totalamt * 0.95;
				$partneramt = $totalamt - $gst - $premium;
				$totalproduct += $partneramt;
				
				$fieldname = "B$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($accountno);
				
				$fieldname = "C$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($origin);
				
				$fieldname = "D$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($ratedamt);
				
				$fieldname = "E$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue('0');
				
				$fieldname = "F$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue('0');
				
				$fieldname = "G$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue('0');
				
				$fieldname = "H$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($invoiceno);
				
				$fieldname = "I$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue('1');
				
				$fieldname = "J$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue('0');
				
				$fieldname = "K$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($gst);
				
				$fieldname = "L$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($premium);
				
				$fieldname = "M$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue('TC');
				
				$fieldname = "N$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($accperiod);
				
				$fieldname = "O$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($partneramt);
				
				$fieldname = "P$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($packing);
				
				$fieldname = "Q$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($bank_charges);
				
				$fieldname = "R$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($incentive_charges);
				
				$fieldname = "S$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($special_packing);
				
				$fieldname = "T$linenumber";
				$objPHPExcel->getActiveSheet()->getCell($fieldname)->setValue($insurance);
				$linenumber++;
			}
			
			$linenumber += 3;
			
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $linenumber, 'DIVISIONS');
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $linenumber, 'TCS SERVICE CHGS');
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $linenumber, 'GST');
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $linenumber, 'INSURANCE');
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $linenumber, 'PRODUCT AMOUNT');
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $linenumber, 'PACKING');
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $linenumber, 'BANKCHARGES');
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $linenumber, 'INCENTIVE');
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $linenumber, 'SPECIALPACKING');
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $linenumber, 'TOTAL');
			
			$linenumber++;
			$totalamount = $totalrated + $totalgst + $totalinsurance + $totalproduct + $totalpacking + $totalbankcharges + $totalincentives + $totalspecialpacking;
			
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $linenumber, 'TCS CONNECT  - TC');
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $linenumber, $totalrated);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $linenumber, $totalgst);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $linenumber, $totalinsurance);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $linenumber, $totalproduct);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $linenumber, $totalpacking);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $linenumber, $totalbankcharges);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $linenumber, $totalincentives);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $linenumber, $totalspecialpacking);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $linenumber, $totalamount);

			$linenumber++;
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $linenumber, 'MY TCS - MT');
			
			$linenumber++;
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $linenumber, 'Shop & ship - SS');
			
			$linenumber++;
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $linenumber, 'Boutique - BQ');
			
			$linenumber += 2;
			
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $linenumber, 'TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $linenumber, $totalrated);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $linenumber, $totalgst);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $linenumber, $totalinsurance);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $linenumber, $totalproduct);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $linenumber, $totalamount);
			
			$linenumber += 2;
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $linenumber, 'UBD TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $linenumber, $totalubd);
			
			$linenumber++;
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $linenumber, 'DPD TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $linenumber, $totaldpd);
			
			$linenumber++;
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $linenumber, 'GRAND TOTAL');
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $linenumber, $totaldpd + $totalubd);
			
			
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save($filename);

			if( $email != null ) { $toemail = $email; }
			else { $toemail = "farhan.munir@nexdegree.com"; }
		}
		
		public function createCSV($paymentinfo, $email=null, $reportdate) {
			$td = explode("-", $reportdate);
			$reportdate = $td[2].$td[1].$td[0];
			$csvdate	= $td[2] . "/" . $td[1] . "/" . $td[0];
			
			$filename = "/tmp/EB" . $reportdate . ".txt";
			$fh = fopen($filename, 'w');
			$line = "";
			
			for($i=0; $i < count($paymentinfo); $i++) {
				$accountno 				= "EB.23.TC.";
				
				$incrementid 			= $paymentinfo[$i]['increment_id'];
				$totalamt	 			= $paymentinfo[$i]['subtotal'];
				$shippingamount			= $paymentinfo[$i]['shipping_amount'];
				$additionalpaymentinfo 	= trim($paymentinfo[$i]['additionalpaymentinfo']);
				$method					= trim($paymentinfo[$i]['method']);
				$deposittype			= $this->getPaymenttype($method . " " . $additionalpaymentinfo);
				$paidtype				= trim($method . " " . $additionalpaymentinfo);
				
				if($deposittype == "DPD") { 
					$origin 	= "HOF";
					$invoiceno  = "$origin.HOF.$incrementid";
				}
				else {
					$ecinfo = $this->getECInformation($incrementid);
					if( count($ecinfo) < 10 ) {
						//Order has been moved forward by Area Accountant - get his info
						//$aainfo = $this->getAreaCoordinatorInfo($incrementid);
						$pmarea = $this->getPMArea($incrementid);
						
						if( count($pmarea) > 1 ) {
							$origin = $pmarea['pm_area'];
							$stcode = $pmarea['pm_area'];
						}
						else {					
							$origin = "HOF";
							$stcode = "HOF";
						}
					}
					else {
						$origin = $ecinfo['ecarea'];
						$stcode = $ecinfo['ecstation'];
					}
					// $origin = "RWP";
					// $stcode = "CKL";
					$invoiceno = "$origin.$stcode.$incrementid";
				}
				$accountno .= $origin . "." . $deposittype;
				$ratedamt   = $shippingamount;
				$gst	 	= $ratedamt * 0.16;
				$premium    = $ratedamt * 0.01;
				$accperiod  = date("m/d/Y");
				//$partneramt = $totalamt * 0.95;
				$partneramt = $totalamt - $gst - $premium;
				
				/************* New Fields Integration for 1.0.5 Starts *************/
				
				//Packing Cost P3
				$packing_cost 		= $this->getConfigConstant('packing_rate');
				$numproducts  		= $this->getNumOfProductsOrdered($incrementid);
				$total_packing 		= $packing_cost * $numproducts;
				$bank_charges 		= $this->getBankCharges($paidtype, $totalamt);
				$incentive_charges 	= $this->getIncentiveCharges($paidtype);
				$special_handling 	= $this->findSpecialHandling($incrementid);
				$insurance			= $this->getInsurance($incrementid);
				echo "$paidtype - $totalamt - $bank_charges - $incentive_charges - $special_handling - $insurance\n\n";
				
				/************* New Fields Integration for 1.0.5 Endss  *************/	
				
				$line .= "$accountno|$origin|$ratedamt|0|0|0|$invoiceno|1|0|$gst|$premium|TC|$csvdate|$partneramt|$total_packing|$bank_charges|$incentive_charges|$special_handling|$insurance\n";
			}
			fwrite($fh,$line);
			fclose($fh);
			
			if( $email != null ) { $toemail = $email; }
			else { $toemail = "tcs@nextgeni.com"; }
		}
	}	
	
	$os = new ofscript();
	$os->connect();
	
	$cc = new cron_class();
	
	if( $cc->if_cron_is_active("3") <> 1 ) {
		$cc->update_cron_log("3", 0, "Cron is not active" );
		exit;
	}
	
	$args = $_SERVER['argv'];
	
	if( count($args) > 1 ) { 	
		$email = trim($args[1]);
		$date  = trim($args[2]);
		
		//Assumed date format PK dd-mm-yyyy
		$td = explode( "-", $date);
		$reportdate = $td[2] . "-" . $td[1] . "-" . $td[0];
		
		if( $td[2] < 2012 || $td[2] > 2020 ) {
			echo "\n\nInvalid Date enter please keep year between (2012 and 2020)\n\n";
			exit;
		}
		else if( $td[1] < 1 || $td[1] > 12 ) {
			echo "\n\nInvalid Date enter please keep month between (1 and 12)\n\n";
			exit;
		}
		else if( $td[0] < 1 || $td[0] > 31) {
			echo "\n\nInvalid Date enter please keep day between (1 and 31)\n\n";
			exit;
		}
		
		$paymentinfo = $os->getTodaysPayments($reportdate);
		$os->createCSV($paymentinfo,$email, $reportdate);
		$os->createExcel($paymentinfo, $email, $reportdate);
	}
	else {
		$reportdate = date('Y-m-d');
		$paymentinfo = $os->getTodaysPayments();
		$os->createCSV($paymentinfo, null, $reportdate);
		$os->createExcel($paymentinfo, null, $reportdate);
	}
	
	$numberoforders = 0;
	
	if( count($paymentinfo) < 3 ) {
		$cc->update_cron_log("3", 0, "Didn\'t find any orders" );
	}
	
	$numberoforders = count($paymentinfo);
	
	$td = explode("-", $reportdate);
	$filedate = $td[2] . $td[1] . $td[0];
	$subjectdate = $td[2] . "-" . $td[1] . "-" . $td[0];
	
	$excelfilename = "/tmp/EB" . $filedate . ".xlsx";
	$txtfilename   = "/tmp/EB" . $filedate . ".txt";
	
	$template = "<table width='100%' cellpadding='0' cellspacing='0' style='font:12px Arial, Helvetica, sans-serif;'><tr><td>OF Team,<BR><BR>".
				"Attached is the data from TCS Connect for date $subjectdate<BR><BR>" .
				"Total Transactions: " . count($paymentinfo) . "<BR><BR>" .
				"THIS IS A SYSTEM GENERATED E-MAIL, PLEASE DO NOT RESPOND TO THE E-MAIL ADDRESS SPECIFIED ABOVE.</td></tr></table>";
	
	$mail = new PHPMailer();
	$mail->SetFrom('cs@tcs.com.pk', 'TCS');
	$mail->Subject    = "TCSConnect OF Files - $reportdate";
	$mail->MsgHTML($template);
	$mail->AddAttachment($excelfilename);   
	$mail->AddAttachment($txtfilename);   
	
	
	if( strlen($email) > 3 ) { $address = $email; }
	else { $address = "ashrafq@tcs.com.pk"; }
	
		
	$mail->AddAddress($address, "TCS");
	
	if( $numberoforders > 0 ) {
		$mail->AddCC("ali.z@tcs.com.pk");
		$mail->AddCC("mghazanfar@tcs.com.pk");
		$mail->AddCC("tausif@tcs.com.pk");
		$mail->AddCC("faisal.ijaz@tcs.com.pk");
	}	
	
	if(!$mail->Send()) {
		$cc->update_cron_log("3", $numberoforders, "Mail Error: " . $mail->ErrorInfo );
	} else {
		if( $numberoforders > 0 )
			$cc->update_cron_log("3", $numberoforders, "Success!" );
		echo "Message sent!\n\n";
	}
	
?>