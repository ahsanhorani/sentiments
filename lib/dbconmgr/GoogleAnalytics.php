<?php
include_once "gapi.class.php";

class GoogleAnalytics 
{
	private  $username = 'dev@tcsconnect.com';
	private  $password = 'dev@tcs.com.pk';
	private  $profileId = '52655601';
	private  $ga;
	public function __construct()
	{
		$this->ga = new gapi($this->username, $this->password);
	}
	public function getVisits($start,$end)
	{
		$this->ga->requestReportData($this->profileId,array('visitCount'),array('pageviews','visits'), null , null , str_replace(" 00:00:00", "", $start), str_replace(" 23:59:59", "", $end));
		return $this->ga->getVisits();
	}
}