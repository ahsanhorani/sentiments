
<?php
	include "db_creds.php";
	include "dbconmgr.php";
	include "/var/www/tcsconnect_newui/lib/dbconmgr/phpmailer/class.phpmailer.php";
	include "/var/www/tcsconnect_newui/app/Mage.php";
	include "cron_class.php";
	
	Mage::app('default');
	
	function dispatchOrderForwardingStatus($bookingdate, $reportdate) {
		$db = new dbconmgr();
		$db->connect();
		
		$croncls = new cron_class();
	
		if( $croncls->if_cron_is_active("5") <> 1 ) {
			$croncls->update_cron_log("5", 0, "Cron is not active" );
			exit;
		}

		$orders = $db->getTodayCNOrders($reportdate);	
		
		$numberoforders = count($orders);
		
		if( count($orders) < 1 ) {
			$croncls->update_cron_log("5", 0, "No orders found!" );
		}
		
		$subject = "TCS Connect � List of orders reported at TCS Operations $bookingdate";
		$header = "<table width='100%' cellpadding='0' cellspacing='0' style='font:14px Arial, Helvetica, sans-serif;'><tr><td>Team TCS Connect,<BR><BR></td></tr>";
		$footer = "<tr><td height='10px'><BR><BR><I>THIS IS A SYSTEM GENERATED E-MAIL, PLEASE DO NOT RESPOND TO THE E-MAIL ADDRESS SPECIFIED ABOVE.</I></td></tr></table>";
		$body = "<table width='100%' cellpadding='0' cellspacing='0' style='font:14px Arial, Helvetica, sans-serif;'>";
		
		if( count($orders) > 0 ) {	
			for($i=0; $i < count($orders); $i++) {
				$entityid = $db->getRealOrderId($orders[$i]['orderid']);
				$entityid = $entityid['entity_id'];
				
				$body .= "<tr><td width='150px'><a href='http://tcsconnect.com/index.php/admin/sales_order/view/order_id/$entityid/'>" . $orders[$i]['orderid'] . "</a></td><td width='150px'>" . $orders[$i]['cnnumber'] . "</td><td>" . "Manifest status OK (yes/no)</td></tr>";
			}
		}
		else {
			$body = "<tr><td colspan='3'>No data was entered today</td></tr>";
		}
		
		$body .= "</table>";
		
		$mail = new PHPMailer();
		$mail->SetFrom('cs@tcs.com.pk', 'TCS Connect');
		$mail->Subject    = $subject;
		$mail->MsgHTML($header . $body . $footer);
		
		$address = $db->getConfigConstant("orderfowarding_address");
		$address = $address['config_value'];
		
		$cc	= $db->getConfigConstant("orderforwarding_cc");
		$cc = $cc['config_value'];
		
		$mail->AddAddress($address);
		$tmp_cc = explode(";", $cc);
		
		for($i=0; $i < count($tmp_cc); $i++) {
			$mail->AddCC($tmp_cc[$i]);
		}
			
		if(!$mail->Send()) {
			add_logentry("Order","Every 10 Minutes", "Error sending Email: " .  $mail->ErrorInfo );
			$croncls->update_cron_log("5", $numberoforders, "Mailer Error: " . $mail->ErrorInfo );
		} else {
			$croncls->update_cron_log("5", $numberoforders, "Success");
		}
	}
	
	$args = $_SERVER['argv'];
	
	if( count($args) < 2 ) { $bookingdate = date("d-m-Y"); }
	else { $bookingdate = trim($args[1]); }
	//Assumed date format PK dd-mm-yyyy
	$td = explode( "-", $bookingdate);
	$reportdate = $td[2] . "-" . $td[1] . "-" . $td[0];
	
	if( $td[2] < 2012 || $td[2] > 2020 ) {
		echo "\n\nInvalid Date enter please keep year between (2012 and 2020)\n\n";
		exit;
	}
	else if( $td[1] < 1 || $td[1] > 12 ) {
		echo "\n\nInvalid Date enter please keep month between (1 and 12)\n\n";
		exit;
	}
	else if( $td[0] < 1 || $td[0] > 31) {
		echo "\n\nInvalid Date enter please keep day between (1 and 31)\n\n";
		exit;
	}
	
	dispatchOrderForwardingStatus($bookingdate, $reportdate);
?>
