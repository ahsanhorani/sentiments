	<?php

	include "db_creds.php";

	class misClass {
	
		var $DB_CON=null;
		
		//Returns database resource/null otherwise
		public function connect() {
			global $DBSERVER, $DBUSER, $DBPWD, $DBNAME;
			
			$link = mysql_connect($DBSERVER, $DBUSER, $DBPWD);
			if (!$link) {
				echo 'Could not connect: ' . mysql_error();
				return;
			}
			//Select the database
			mysql_select_db( $DBNAME, $link) or die("Database doesn't exist: " . mysql_error() );
			$this->DB_CON=$link;
		}
		
		//public function which executes the query
		public function runquery($Sql) {
			if( $this->DB_CON != null ) {
				$result = mysql_query($Sql, $this->DB_CON);
				
				if( !$result) { 
					@file_put_contents("C:\\mysqlerror.log", mysql_errno($this->DB_CON) . " : " . mysql_error($this->DB_CON) . "\n", FILE_APPEND);
					return null; 
				}
				return $result;
			}
		}
		
		public function getNumberofOrdersPlaced($orderdate=null, $querytype=null, $ordertodate=null) {
			$ret = 0;
			if( $querytype != null ) { $cond = " AND 1=1 ";}
			
			if( $orderdate == null )
				$orderdate = "date(now())";
			else if ($querytype != "custom_date") {
				$orderdate = "date('$orderdate')";
			}
			else {
				$ordertodate = "'".$ordertodate."'";
			}
			
			
			$sql = "SELECT count(entity_id) as totalorders FROM sales_flat_order WHERE date(created_at)=$orderdate";
			
			if( $querytype <> null ) {
				if( $querytype == "thisweek" ) {
					$fromdate = date('Y-m-d', strtotime('this week', time()));
					$todate   = "now()";
				}
				else if( $querytype == "lastweek" ) {
					$fromdate = date('Y-m-d', strtotime('last week', time()));
					$todate   = strtotime('this week', time())-86400;
					$todate   = "'" . date('Y-m-d', $todate) . "'";
					
				}
				else if( $querytype == "thismonth" ) {
					$fromdate = date("Y-m-01");
					$todate   = "now()";
				}
				else if( $querytype == "YTD" ) {
					$fromdate = date("Y-01-01");
					$todate   = "now()";
				}				
				else if( $querytype == "inception" ) {
					$fromdate = date("2012-03-23");
					$todate   = "now()";
				}
				else if( $querytype == "custom_date" ) {
					$fromdate = $orderdate;
					$todate   = $ordertodate;
				}
				
				$sql = "SELECT count(entity_id) as totalorders FROM sales_flat_order WHERE date(created_at) >= date('$fromdate') AND date(created_at) <= date($todate)";
			}
			
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['totalorders'];
			}
			return $ret;	
		}
		
		public function getNumberOfOrderPaid($orderdate=null, $querytype=null, $ordertodate=null) {
			$ret = 0;
			if( $querytype != null ) { $cond = " AND 1=1 ";}
			
			if( $orderdate == null )
				$orderdate = "date(now())";
			else if ($querytype != "custom_date") {
				$orderdate = "date('$orderdate')";
			}
			else {
				$ordertodate = "'".$ordertodate."'";
			}
			
				$sql = "SELECT count(o.entity_id) as totalorders FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at)=$orderdate AND o.status NOT IN ('payment_review', 'order_refunded', 'canceled', 'processing', 'pending') GROUP BY o.entity_id";				
			
			if( $querytype <> null ) {
				if( $querytype == "thisweek" ) {
					$fromdate = date('Y-m-d', strtotime('this week', time()));
					$todate   = "now()";
				}
				else if( $querytype == "lastweek" ) {
					$fromdate = date('Y-m-d', strtotime('last week', time()));
					$todate   = strtotime('this week', time())-86400;
					$todate   = "'" . date('Y-m-d', $todate) . "'";
					
				}
				else if( $querytype == "thismonth" ) {
					$fromdate = date("Y-m-01");
					$todate   = "now()";
				}
				else if( $querytype == "YTD" ) {
					$fromdate = date("Y-01-01");
					$todate   = "now()";
				}				
				else if( $querytype == "inception" ) {
					$fromdate = date("2012-03-23");
					$todate   = "now()";
				}
				else if( $querytype == "custom_date" ) {
					$fromdate = $orderdate;
					$todate   = $ordertodate;
				}
				
				$sql = "SELECT distinct(o.entity_id) as totalorders FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at) >= '$fromdate' AND date(i.created_at) <= date($todate) AND o.status NOT IN ('payment_review', 'order_refunded', 'canceled', 'processing', 'pending') GROUP BY o.entity_id";
			}
			$result = $this->runquery($sql);
			
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret++;
				}
			}
			return $ret;	
		}
		
		public function getConfirmedCod($orderdate=null, $querytype=null) {
			$ret = 0;
			if( $querytype != null ) { $cond = " AND 1=1 ";}
			
			if( $orderdate == null )
				$orderdate = "date(now())";
			else
				$orderdate = "date('$orderdate')";
			
			$sql = "SELECT count(o.entity_id) as totalorders FROM sales_flat_order o WHERE date(o.updated_at)=$orderdate AND o.status='cod'";
			
			if( $querytype <> null ) {
				if( $querytype == "thisweek" ) {
					$fromdate = date('Y-m-d', strtotime('this week', time()));
					$todate   = "now()";
				}
				else if( $querytype == "lastweek" ) {
					$fromdate = date('Y-m-d', strtotime('last week', time()));
					$todate   = strtotime('this week', time())-86400;
					$todate   = "'" . date('Y-m-d', $todate) . "'";
					
				}
				else if( $querytype == "thismonth" ) {
					$fromdate = date("Y-m-01");
					$todate   = "now()";
				}
				else if( $querytype == "YTD" ) {
					$fromdate = date("Y-01-01");
					$todate   = "now()";
				}				
				else if( $querytype == "inception" ) {
					$fromdate = date("2012-03-23");
					$todate   = "now()";
				}
				
				$sql = "SELECT count(o.entity_id) as totalorders FROM sales_flat_order o WHERE date(o.updated_at) >= '$fromdate' AND date(o.updated_at) <= date($todate) AND o.status='cod'";
			}
				
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['totalorders'];
			}
			return $ret;	
		}
		
		public function getNumberofConfirmedOrders($orderdate=null, $querytype=null, $ordertodate=null) {
			$ret = 0;
			
			return $this->getNumberOfOrderPaid($orderdate, $querytype, $ordertodate);// + $this->getConfirmedCod($orderdate, $querytype);
		}
		
		public function getNumberOfOrdersInProcessing($orderdate=null, $querytype=null, $ordertodate=null) {
			$ret = 0;
			if( $querytype != null ) { $cond = " AND 1=1 ";}
			
			if( $orderdate == null )
				$orderdate = "date(now())";
			else if ($querytype != "custom_date") {
				$orderdate = "date('$orderdate')";
			}
			else {
				$ordertodate = "'".$ordertodate."'";
			}
				
				$sql = "SELECT count(entity_id) as totalorders FROM sales_flat_order WHERE date(created_at)=$orderdate AND status IN ('pending', 'processing')";
				
			if( $querytype <> null ) {
				if( $querytype == "thisweek" ) {
					$fromdate = date('Y-m-d', strtotime('this week', time()));
					$todate   = "now()";
				}
				else if( $querytype == "lastweek" ) {
					$fromdate = date('Y-m-d', strtotime('last week', time()));
					$todate   = strtotime('this week', time())-86400;
					$todate   = "'" . date('Y-m-d', $todate) . "'";
					
				}
				else if( $querytype == "thismonth" ) {
					$fromdate = date("Y-m-01");
					$todate   = "now()";
				}
				else if( $querytype == "YTD" ) {
					$fromdate = date("Y-01-01");
					$todate   = "now()";
				}				
				else if( $querytype == "inception" ) {
					$fromdate = date("2012-03-23");
					$todate   = "now()";
				}
				else if( $querytype == "custom_date" ) {
					$fromdate = $orderdate;
					$todate   = $ordertodate;
				}
				
				$sql = "SELECT count(entity_id) as totalorders FROM sales_flat_order WHERE date(created_at) >= '$fromdate' AND date(created_at) <= date($todate) AND status IN ('pending', 'processing')";
			}

			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['totalorders'];
			}
			return $ret;	
		}
		
		public function getNumberofdaysInWeek() {
			 $datediff = time() - strtotime('this week', time());
			return floor($datediff/(60*60*24))+1;
		}
		
		public function getNumberofdayssinceInception() {
			$datediff = time() - strtotime('23-03-2012');
			return floor($datediff/(60*60*24))+1;
		}
		
		public function getOrderRevenue($orderdate=null, $querytype=null, $ordertodate) {
			$ret = 0;
			if( $querytype != null ) { $cond = " AND 1=1 ";}
			
			if( $orderdate == null )
				$orderdate = "date(now())";
			else if ($querytype != "custom_date") {
				$orderdate = "date('$orderdate')";
			}
			else {
				$ordertodate = "'".$ordertodate."'";
			}
			
				//$sql = "SELECT SUM(o.base_grand_total) as totalrevenue FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at)=$orderdate";
				
				$sql = "SELECT o.entity_id, o.increment_id,  o.base_grand_total, SUM(o.base_grand_total) as totalrevenue FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at) = $orderdate AND o.status NOT IN ('canceled') GROUP BY o.entity_id";
			
			if( $querytype <> null ) {
				if( $querytype == "thisweek" ) {
					$fromdate = date('Y-m-d', strtotime('this week', time()));
					$todate   = "now()";
				}
				else if( $querytype == "lastweek" ) {
					$fromdate = date('Y-m-d', strtotime('last week', time()));
					$todate   = strtotime('this week', time())-86400;
					$todate   = "'" . date('Y-m-d', $todate) . "'";
					
				}
				else if( $querytype == "thismonth" ) {
					$fromdate = date("Y-m-01");
					$todate   = "now()";
				}
				else if( $querytype == "YTD" ) {
					$fromdate = date("Y-01-01");
					$todate   = "now()";
				}				
				else if( $querytype == "inception" ) {
					$fromdate = date("2012-03-23");
					$todate   = "now()";
				}
				else if( $querytype == "custom_date" ) {
					$fromdate = $orderdate;
					$todate   = $ordertodate;
				}
				
				//$sql = "SELECT SUM(o.base_grand_total) as totalrevenue FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at) >= '$fromdate' AND date(i.created_at) <= date($todate)";
				
				$sql = "SELECT o.entity_id, o.increment_id,  o.base_grand_total, SUM(o.base_grand_total) as totalrevenue FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at) >= '$fromdate' AND date(i.created_at) <= date($todate) AND o.status NOT IN ('canceled') GROUP BY o.entity_id;";
				
				//$sql = "SELECT SUM(IFNULL(base_subtotal, 0) - IFNULL(base_subtotal_refunded, 0) - IFNULL(base_subtotal_canceled, 0) - ABS(IFNULL(base_discount_amount, 0)) + IFNULL(base_discount_refunded, 0)) AS totalrevenue FROM sales_flat_order WHERE (status NOT IN('canceled', 'pending', 'processing', 'pending', 'pending_payment', 'processing', 'fraud', 'payment_review', 'pending_paypal', 'refund', 'exchange', 'cheque_clearing', 'cheque_cancel')) AND (state NOT IN('new', 'pending_payment')) AND date(updated_at) >= '$fromdate' AND date(updated_at) <= date($todate)";
			}
			
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) )
				$ret += $row['base_grand_total'];
			}
			
			//$ret += $this->getCodOrdersRevenue($orderdate, $querytype);
			
			return $ret;	
		}
		
		private function getCodOrdersRevenue($orderdate=null, $querytype=null) {
			$ret = 0;
			if( $querytype != null ) { $cond = " AND 1=1 ";}
			
			if( $orderdate == null )
				$orderdate = "date(now())";
			else if ($querytype != "custom_date") {
				$orderdate = "date('$orderdate')";
			}
			else {
				$ordertodate = "'".$ordertodate."'";
			}
			
				$sql = "SELECT SUM(o.base_grand_total) as totalrevenue FROM sales_flat_order o WHERE date(o.updated_at)=$orderdate AND status='cod'";
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['totalrevenue'];
			}
			return $ret;	
		}
		
		private function getOrderAggregates($orderdate=null, $querytype=null, $ordertodate=null) {
			$ret = array();
			if( $querytype != null ) { $cond = " AND 1=1 ";}
			
			if( $orderdate == null )
				$orderdate = "date(now())";
			else if ($querytype != "custom_date") {
				$orderdate = "date('$orderdate')";
			}
			else {
				$ordertodate = "'".$ordertodate."'";
			}
			
			$sql = "SELECT o.increment_id, o.base_grand_total, o.base_shipping_amount, vas_charges, SUM(o.base_grand_total) as totalrevenue,SUM(o.base_shipping_amount) as totalshipping, sum(vas_charges) as totalvas FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at)=$orderdate AND o.status NOT IN ('payment_review','order_refunded', 'canceled') GROUP BY i.order_id ORDER BY o.entity_id;";			
			
			if( $querytype <> null ) {
				if( $querytype == "thisweek" ) {
					$fromdate = date('Y-m-d', strtotime('this week', time()));
					$todate   = "now()";
				}
				else if( $querytype == "lastweek" ) {
					$fromdate = date('Y-m-d', strtotime('last week', time()));
					$todate   = strtotime('this week', time())-86400;
					$todate   = "'" . date('Y-m-d', $todate) . "'";
					
				}
				else if( $querytype == "thismonth" ) {
					$fromdate = date("Y-m-01");
					$todate   = "now()";
				}
				else if( $querytype == "YTD" ) {
					$fromdate = date("Y-01-01");
					$todate   = "now()";
				}				
				else if( $querytype == "inception" ) {
					$fromdate = date("2012-03-23");
					$todate   = "now()";
				}
				else if( $querytype == "custom_date" ) {
					$fromdate = $orderdate;
					$todate   = $ordertodate;
				}
				
				$sql = "SELECT o.increment_id, o.base_grand_total, o.base_shipping_amount, vas_charges, SUM(o.base_grand_total) as totalrevenue,SUM(o.base_shipping_amount) as totalshipping, sum(vas_charges) as totalvas FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at) >= '$fromdate' AND date(i.created_at) <= date($todate) AND o.status NOT IN ('payment_review','order_refunded', 'canceled') GROUP BY i.order_id ORDER BY o.entity_id";
				// echo $sql;
			}	
			
			$result = $this->runquery($sql);			
			$ret = array( "totalrevenue" => 0, "totalshipping" => 0, "totalvas" => 0 );
			
			$tr=0; $ts=0; $tv=0;
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					// $ret['totalrevenue']  = $ret['totalrevenue']  + $row['base_grand_total'];
					// $ret['totalshipping'] = $ret['totalshipping'] + $row['base_shipping_amount'];
					// $ret['totalvas']      = $ret['totalvas']      + $row['vas_charges'];
					$tr += $row['base_grand_total'];
					$ts += $row['base_shipping_amount'];
					$tv += $row['vas_charges'];
				}
			}
			
			
			$ret['totalrevenue']  = $tr;
			$ret['totalshipping'] = $ts;
			$ret['totalvas']      = $tv;
			return $ret;
		}
		
		public function getPaidOrderIds($orderdate=null, $querytype=null, $ordertodate=null) {
			$ret = array();
			$sql = "";
			
			if( $orderdate == null )
				$orderdate = "date(now())";
			else if ($querytype != "custom_date") {
				$orderdate = "date('$orderdate')";
			}
			else {
				$ordertodate = "'".$ordertodate."'";
			}
			
			$sql = "SELECT distinct(o.entity_id) FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at)=$orderdate AND o.status NOT IN ('payment_review', 'order_refunded', 'canceled')";
			
			if( $querytype <> null ) {
				if( $querytype == "thisweek" ) {
					$fromdate = date('Y-m-d', strtotime('this week', time()));
					$todate   = "now()";
				}
				else if( $querytype == "lastweek" ) {
					$fromdate = date('Y-m-d', strtotime('last week', time()));
					$todate   = strtotime('this week', time())-86400;
					$todate   = "'" . date('Y-m-d', $todate) . "'";
					
				}
				else if( $querytype == "thismonth" ) {
					$fromdate = date("Y-m-01");
					$todate   = "now()";
				}
				else if( $querytype == "YTD" ) {
					$fromdate = date("Y-01-01");
					$todate   = "now()";
				}				
				else if( $querytype == "inception" ) {
					$fromdate = date("2012-03-23");
					$todate   = "now()";
				}
				else if( $querytype == "custom_date" ) {
					$fromdate = $orderdate;
					$todate   = $ordertodate;
				}
				
				$sql = "SELECT distinct(o.entity_id) FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON i.order_id=o.entity_id WHERE date(i.created_at) >= '$fromdate' AND date(i.created_at) <= date($todate) AND o.status NOT IN ('payment_review','order_refunded', 'canceled')";
			}	
			
			$result = $this->runquery($sql);
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) { $ret[] = $row['entity_id']; }
			}
			
			if( $orderdate == null ) 
				$orderdate = "date(now())";
			else
				$orderdate = "date('$orderdate')";
			
			return $ret;
		}
		
		public function getCogs($orderids) {
			$sql = "SELECT SUM(base_cost) as cogs FROM sales_flat_order_item WHERE order_id IN ($orderids)";			
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $row['cogs'];
		}
		
		//Grand total - VAS - Shipping - COGS
		public function getNetProfit($orderdate=null, $querytype=null, $ordertodate=null) {
			$ret = 0;
			
			$orderaggregates = $this->getOrderAggregates($orderdate, $querytype, $ordertodate);
			
			//get the paid order ids 
			$orderids = array();
			$orderids = $this->getPaidOrderIds($orderdate, $querytype, $ordertodate);
			$orderids = implode(",", $orderids);
			$cogs = $this->getCogs($orderids);
			
			$grandtotal = $orderaggregates['totalrevenue']  ; 
			$shiptotal  = $orderaggregates['totalshipping'] ;
			$vastotal   = $orderaggregates['totalvas']      ;
			
			if( $querytype== "thismonth")
				@file_put_contents("/tmp/mis/mis.log", "$orderids\n\nGT: $grandtotal\nCogs: $cogs\nship: $shiptotal\nvas:$vastotal\n-------------\n");
			
			
			$ret = $grandtotal - ($shiptotal + $vastotal + $cogs);
			
			return $ret;
		}
		
		public function getTotalMarketingDiscount($orderdate=null, $querytype=null, $ordertodate=null) {
			$orderids = $this->getPaidOrderIds($orderdate, $querytype, $ordertodate);
			$orderids = implode(",", $orderids);
			
			$sql = "SELECT SUM(base_discount_amount) as discountamount FROM sales_flat_order WHERE entity_id IN ($orderids)";
			
			$discountamount = 0;
			$ret			= 0;
			
			$result = $this->runquery($sql);
			if( $result != null ) {
				$discountamount = mysql_fetch_array($result, MYSQL_BOTH);
				$discountamount = abs($discountamount['discountamount']);
			}
			
			$ret = $discountamount + $this->getOrderSpecialPrice( $orderids );
			return $ret;
		}
		
		public function getOrderSpecialPrice( $orderids) {
			$ret = 0;
			$orderids = implode(",", $orderids);
			
			$sql = "SELECT order_id, base_price, base_cost FROM sales_flat_order_item WHERE order_id IN ($orderids) AND base_cost > base_price";
			
			$result = $this->runquery($sql);
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret += ($row['base_cost'] - $row['base_price']);
				}
			}
			
			return $ret;
		}
		
		public function getTotalProductRevenue($productskus) {
			$sql = "SELECT sku, SUM(base_price) as totalprice,SUM(base_cost) as totalcost FROM sales_flat_order_item WHERE sku IN ($productskus) GROUP BY sku";
			
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[$row['sku']] = $row;
				}
			}
			return $ret;
		}
		
		public function getTotalVendorsByCity() {
			$sql = "select city, count(vendor_id) as numvendors from udropship_vendor GROUP  BY city";
			$result = $this->runquery($sql);
			$ret = array();
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[$row['city']] = $row;
				}
			}
			return $ret;
		}
		
		public function getVendorIdsByCity($city) {
			$sql = "SELECT vendor_id FROM udropship_vendor WHERE city='$city'";
			$result = $this->runquery($sql);
			$ret = array();
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row['vendor_id'];
				}
			}
			return $ret;
		}
		
		public function getCustomerStats($querytype ) {
			$cond = " 1 = 1 ";
			if( $querytype == "today" ) 
				$cond = " date(login_at)= date(now()) ";
			
			if( $querytype == "thisweek" ) {
				$fromdate = date('Y-m-d', strtotime('this week', time()));
				$todate   = "now()";
				$cond = " date(login_at) >= '$fromdate' AND date(login_at) <= date(now()) ";
			}
			if( $querytype == "lastmonth" ) {
				$fromdate = date("Y-m-1", strtotime("-1 month") ) ;
				$todate   = date("Y-m-t", strtotime("-1 month") ) ;
				$cond = " date(login_at) >= '$fromdate' AND date(login_at) <= '$todate' ";
			}
			if( $querytype == "todate" ) {
				$fromdate = "2012-03-23";
				$todate   = date("Y-m-d") ;
				$cond = " date(login_at) >= '$fromdate' AND date(login_at) <= '$todate' ";
			}
			$sql = "SELECT count(visitor_id) AS totalvisits FROM log_customer WHERE $cond ";
			$result = $this->runquery($sql);
			$ret = 0;
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['totalvisits'];
			}
			return $ret;
		}
		
		
		public function getVisitorStats($querytype ) {
			$cond = " 1 = 1 ";
			if( $querytype == "today" ) 
				$cond = " date(first_visit_at)= date(now()) ";
			
			if( $querytype == "thisweek" ) {
				$fromdate = date('Y-m-d', strtotime('this week', time()));
				$todate   = "now()";
				$cond = " date(first_visit_at) >= '$fromdate' AND date(first_visit_at) <= date(now()) ";
			}
			if( $querytype == "lastmonth" ) {
				$fromdate = date("Y-m-1", strtotime("-1 month") ) ;
				$todate   = date("Y-m-t", strtotime("-1 month") ) ;
				$cond = " date(first_visit_at) >= '$fromdate' AND date(first_visit_at) <= '$todate' ";
			}
			if( $querytype == "todate" ) {
				$fromdate = "2012-03-23";
				$todate   = date("Y-m-d") ;
				$cond = " date(first_visit_at) >= '$fromdate' AND date(first_visit_at) <= '$todate' ";
			}
			$sql = "SELECT count(visitor_id) AS totalvisits FROM log_visitor WHERE $cond ";
			$result = $this->runquery($sql);
			$ret = 0;
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['totalvisits'];
			}
			return $ret;
		}
		
		public function getNewsletterStats($querytype ) {
			$cond = " 1 = 1 ";
			if( $querytype == "today" ) 
				$cond = " date(created)= date(now()) ";
			
			if( $querytype == "thisweek" ) {
				$fromdate = date('Y-m-d', strtotime('this week', time()));
				$todate   = "now()";
				$cond = " date(created) >= '$fromdate' AND date(created) <= date(now()) ";
			}
			if( $querytype == "lastmonth" ) {
				$fromdate = date("Y-m-1", strtotime("-1 month") ) ;
				$todate   = date("Y-m-t", strtotime("-1 month") ) ;
				$cond = " date(created) >= '$fromdate' AND date(created) <= '$todate' ";
			}
			if( $querytype == "todate" ) {
				$fromdate = "2012-03-23";
				$todate   = date("Y-m-d") ;
				$cond = " date(created) >= '$fromdate' AND date(created) <= '$todate' ";
			}
			$sql = "SELECT count(subsriber_id) AS totalvisits FROM tcs_newsletter_subsribers WHERE $cond ";			
			$result = $this->runquery($sql);
			$ret = 0;
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['totalvisits'];
			}
			return $ret;
		}
		
		
		public function getFacebookstats() {
            $trends_url = "http://graph.facebook.com/162589387146711";
            $response = json_decode(file_get_contents($trends_url), true);
			
			$ret = array( "likes" => $response['likes'], "talking" => $response['talking_about_count'] );
			
			return $ret;
		}	
		
		public function getPringFollowers() {
           
            return count(explode("\n",file_get_contents("http://www.pringit.com/api/?username=tcsconnect&secret=ec048633d63c109582fcfba72e8d363d&v=1&action=LIST")));
        }		
		
		public function getVendorsPaymentDue($querytype=null) {
			$cond = " AND 1 = 1 ";
		
			if( $querytype == "thismonth" ) {
				$from = date("Y-m-01");
				$to   = date("Y-m-d");
				$cond = " AND date(payment_due_date) >= '$from' AND date(payment_due_date) <= '$to' ";
			}
			else if( $querytype == "lastmonth" ) {
				$from = date("Y-m-1", strtotime("-1 month") ) ;
				$to   = date("Y-m-t", strtotime("-1 month") ) ;
				$cond = " AND date(payment_due_date) >= '$from' AND date(payment_due_date) <= '$to' ";
			}
			
			$sql = "select SUM(i.base_cost) as vp FROM sales_flat_order_item i INNER JOIN tcs_vendor_payments v ON v.order_id=i.order_id WHERE payment_status=0 $cond ";
			
			
			$result = $this->runquery($sql);
			$ret = 0;
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['vp'];
			}
			return $ret;
		}
		
		public function getVendorsPaymentMade($querytype=null) {
			$cond = " AND 1 = 1 ";
		
			if( $querytype == "thismonth" ) {
				$from = date("Y-m-01");
				$to   = date("Y-m-d");
				$cond = " AND date(payment_date) >= '$from' AND date(payment_date) <= '$to' ";
			}
			else if( $querytype == "lastmonth" ) {
				$from = date("Y-m-01", strtotime("-1 month") ) ;
				$to   = date("Y-m-t", strtotime("-1 month") ) ;
				$cond = " AND date(payment_date) >= '$from' AND date(payment_date) <= '$to' ";
			}
			
			$sql = "select SUM(i.base_cost) as vp FROM sales_flat_order_item i INNER JOIN tcs_vendor_payments v ON v.order_id=i.order_id WHERE payment_status=1 $cond ";
			$result = $this->runquery($sql);
			$ret = 0;
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['vp'];
			}
			return $ret;
		}
		
		public function getOrderTat($querytype, $interval) {
			if( $querytype == "thismonth" ) {
				$from = date("Y-m-01");
				$to   = date("Y-m-d");
			}
			else if( $querytype == "lastmonth" ) {
				$from = date("Y-m-01", strtotime("-1 month") ) ;
				$to   = date("Y-m-t", strtotime("-1 month") ) ;
			}	
			else {
				$from = "2012-03-23";
				$to   = date("Y-m-d");
			}
			
			if( $interval == "within2" )
				$intcond = " AND DATEDIFF(updated_at, created_at) <= 2 ";
				
			if( $interval == "2to5" )
				$intcond = " AND DATEDIFF(updated_at, created_at) > 2 AND DATEDIFF(updated_at, created_at) <= 5 ";
			
			if( $interval == "over5" )
				$intcond = " AND DATEDIFF(updated_at, created_at) > 5 ";
			
			$sql = "SELECT count(entity_id) as tat FROM sales_flat_order WHERE status='complete' AND (date(created_at) >= '$from' AND date(updated_at) <= '$to') $intcond";
			
			$ret = 0;			
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['tat'];
			}
			return $ret;
		}		
		
		public function getNumberOfOrdersForGivenPids($pids, $querytype) {
			if( $querytype == "thismonth" ) {
				$from = date("Y-m-01");
				$to   = date("Y-m-d");
				$cond = " AND date(i.created_at) >= '$from' AND date(i.created_at) <= '$to' ";
			}			
			else if( $querytype == "lastmonth" ) {
				$from = date("Y-m-01", strtotime("-1 month") ) ;
				$to   = date("Y-m-t", strtotime("-1 month") ) ;
				$cond = " AND date(i.created_at) >= '$from' AND date(i.created_at) <= '$to' ";
			}	
			else if( $querytype == "ytd" ) {
				$from = date("Y-m-01", strtotime("-1 month") ) ;
				$to   = date("Y-m-t", strtotime("-1 month") ) ;
				$cond = " AND date(i.created_at) >= '$from' AND date(i.created_at) <= '$to' ";
			}	
			else if( $querytype == "inception" ) {
				$from = "2012-03-23";
				$to   = date("Y-m-d");
				$cond = " AND date(i.created_at) >= '$from' AND date(i.created_at) <= '$to' ";
			}	
			else { $cond = " AND 1 = 1 "; }
			
		
			$sql = "select distinct(count(i.order_id)) as total FROM sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id INNER JOIN sales_flat_invoice n ON n.order_id=o.entity_id WHERE product_id IN ($pids) AND o.status != 'canceled' $cond";
			$result = $this->runquery($sql);
						
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['total'];
			}
			return $ret;
		}
		
		public function getNumberofProductOrders($productname) {
			$sql = "select count(distinct(i.order_id)) as total FROM sales_flat_order_item i INNER JOIN sales_flat_order o ON o.entity_id=i.order_id INNER JOIN sales_flat_invoice n ON n.order_id=o.entity_id WHERE i.name='$productname' AND o.status != 'canceled'";
			
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				$row = mysql_fetch_array($result, MYSQL_BOTH);
				$ret = $row['total'];
			}
			return $ret;
		}
		
		//GET REFUNDED ORDER
		public function getOrderRefunded($orderdate=null, $querytype=null, $ordertodate) {
			$ret = 0;
			if( $querytype != null ) { $cond = " AND 1=1 ";}
			
			if( $orderdate == null )
				$orderdate = "date(now())";
			else if ($querytype != "custom_date") {
				$orderdate = "date('$orderdate')";
			}
			else {
				$ordertodate = "'".$ordertodate."'";
			}
			
				
				$sql = "SELECT o.entity_id,
							   o.increment_id,
							   SUM(cm.base_grand_total) AS amount_refunded
						FROM   sales_flat_order o
							   LEFT JOIN sales_flat_order_payment r
									  ON r.entity_id = o.entity_id
							   LEFT JOIN sales_flat_creditmemo AS cm
									  ON cm.order_id = o.entity_id
						WHERE  Date(cm.updated_at) = $orderdate
							   AND o.status NOT IN ( 'canceled' )
						GROUP  BY o.entity_id";
			
		
			if( $querytype <> null ) {
				if( $querytype == "thisweek" ) {
					$fromdate = date('Y-m-d', strtotime('this week', time()));
					$todate   = "now()";
				}
				else if( $querytype == "lastweek" ) {
					$fromdate = date('Y-m-d', strtotime('last week', time()));
					$todate   = strtotime('this week', time())-86400;
					$todate   = "'" . date('Y-m-d', $todate) . "'";
					
				}
				else if( $querytype == "thismonth" ) {
					$fromdate = date("Y-m-01");
					$todate   = "now()";
				}
				else if( $querytype == "YTD" ) {
					$fromdate = date("Y-01-01");
					$todate   = "now()";
				}				
				else if( $querytype == "inception" ) {
					$fromdate = date("2012-03-23");
					$todate   = "now()";
				}
				else if( $querytype == "custom_date" ) {
					$fromdate = $orderdate;
					$todate   = $ordertodate;
				}
				
				$sql = " SELECT o.entity_id,
							   o.increment_id,
							  SUM(cm.base_grand_total) AS amount_refunded
						FROM   sales_flat_order o
							   LEFT JOIN sales_flat_order_payment r
									  ON r.entity_id = o.entity_id
							   LEFT JOIN sales_flat_creditmemo AS cm
									  ON cm.order_id = o.entity_id
						WHERE  Date(cm.updated_at)  >= '$fromdate'
							AND Date(cm.updated_at)   <= Date($todate)
							   AND o.status NOT IN ( 'canceled' )
						GROUP  BY o.entity_id";
						
						//$dbugQUery	=	$querytype."->".$sql;
				
			}
			// echo "<pre>";print_r($sql);echo"</pre><hr/>";
			$result = $this->runquery($sql);
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) )
				$ret += $row['amount_refunded'];
			}
			
			
			return $ret;	
		}
	}	
?>