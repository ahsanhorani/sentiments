<?php
	include "db_creds.php";
	
	class vendors_class {
					
		public function get_all_vendors() {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT * FROM udropship_vendor";
			return $read->fetchAll($sql);
		}
		
		public function get_vendor_by_id($vendor_id) {
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT * FROM udropship_vendor WHERE vendor_id='$vendor_id' LIMIT 1";
			$ret = $read->fetchAll($sql);
			return $ret;
		}
		
		public function toggle_vendor($vendor_id, $action ) {
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$sql   = "UPDATE udropship_vendor SET status='$action' WHERE vendor_id=$vendor_id";
			$write->query($sql);
		}
		
		public function update_vendor($vendor_id, $vendor_name, $vendor_email, $street, $zip, $city,  $country, $telephone) {
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$read  = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql   = "SELECT count(vendor_id) as cnt FROM udropship_vendor WHERE vendor_id='$vendor_id'";
			$ret = $read->fetchAll($sql);
			
			if( $ret[0]['cnt'] > 0 ) { 
				$sql = "UPDATE udropship_vendor SET vendor_name='$vendor_name', email='$vendor_email', street='$street', zip='$zip', city='$city', country_id='$country',telephone='$telephone' WHERE vendor_id='$vendor_id' LIMIT 1";
			}
			else {
				$sql = "INSERT INTO udropship_vendor (vendor_name,email,street,zip,city,country_id,telephone, status) VALUES('$vendor_name','$vendor_email', '$street','$zip','$city','$country','$telephone','A')";
			}
			
			$write->query($sql);
		}
	}   
?>