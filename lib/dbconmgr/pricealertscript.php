<?php
	require_once '/var/www/tcsconnect_dev/app/Mage.php';
	umask(0);
	Mage::app();
	
	class pricealertscript
	{
				//--> intializing class variables and get set function for email body
				private $body;
				private function getBody(){
					return $this->body;
				}
				
				private function setBody($body){
					if(!empty($body)){
						$this->body=$body;
					}
				}
				
				
				
				public function sendPriceAlerts(){
				
					//--> Magento default objects to read and write in to db	
					$coreResource						=	Mage::getSingleton('core/resource');
					$coreRead							=	$coreResource->getConnection('core_read');
					$coreWrite							=	$coreResource->getConnection('core_write');
					
					//  get table name from module config
					$tcs_pricealert_subscription		=	$coreResource->getTableName('pricealert/pricealert'); 
					$tcs_pricealert_pricechange			=	$coreResource->getTableName('pricealert/pricechange'); 
					
					//--> email address and mobile number of product whose price is change
					$emailMobileQuery					=	"SELECT 
															tps.customer_email,tps.mobile_no,tps.product_id,
															tpp.new_product_price,tpp.old_product_price 
															FROM 
															tcs_pricealert_pricechange tpp, tcs_pricealert_subscription tps
															WHERE tpp.sento_customer = 0 
															AND 
															tps.product_id = tpp.product_id";
															 
					$queryResult						=	$coreRead->fetchAll($emailMobileQuery);	 // get result set from db
					
					foreach($queryResult as $result){
							
							$customerEmail	=	$result['customer_email']; // get customer email
							$customerMobile	=	$result['mobile_no'];	// get customer mobile no
							$productId		=	$result['product_id']; // get product id
							$oldPrice		=	$result['old_product_price']; // get old price
							$newPrice		=	$result['new_product_price']; // get new price
							
							// create product model load by product id get from db whose price is changed
							$productModel	=	Mage::getModel('catalog/product')->load($productId); 
							$productName	=	$productModel->getName();// get product name
							$productUrl		=	$productModel->getProductUrl(); // get product url
			
							//--> if email address or mobile number one of them is exists send email or sms
							//--> if both exists send email + sms 
							if(!empty($customerEmail) || !empty($customerMobile)){
									$flag	=	FALSE; // flag for update sent to customer column check if email or sms sent to customer
									//--> if email exists send email
									if(!empty($customerEmail)){
										
											$_email_subject = 'TCS Connect Price Change Alert for '.$productName; // set email subject
											// sending email
											$this->sendEmail($customerEmail,'cs@tcsconnect.com','TCS Connect','cs@tcsconnect.com',$_email_subject,$productName,$newPrice,$productUrl); 
											//--> if email send to customer set flag to true
											$flag	=	TRUE;
											
									}
									//--> if mobile number exists send sms
									if(!empty($customerMobile)){
										// sending sms
										$this->sendSMS($customerMobile,$productName,$productUrl,$newPrice,$oldPrice);
										//--> if sms send to customer set flag to true
										$flag	=	TRUE;
									}
									//--> if mobile number not found
									else{
											//--> get db name from magento config
											$dbname 	= (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');
											$tableName	=	$dbname.'.tcs_pring_log'; //--> pring logging table name 
											$typeId		=	'5';	// pring message type , 5 for price change sms
											$error		=	"Mobile Number not found";
											//--> Magento core object to insert record in db
											$coreWrite				=	Mage::getSingleton('core/resource')->getConnection('core_write');
											$pringInsertResponse	=	"INSERT INTO $tableName(type,url,response) VALUES 	('$typeId','$apiUrl','$error')";
											$coreWrite->query($pringInsertResponse);
									}
									
									if($flag){
										// if flag is true update sent to customer  from 0 to 1
										$this->sentToFlag();
									}
	
							}
					
							
					}
			}
			
			
			
			public function sendEmail($customerEmail,$fromEmail,$fromName,$replyTo,$subject,$productName,$newPrice,$productUrl){

					// set email message
					$_email_message = "Hello ,";
					$_email_message .= "\r\n\r\n The price for ".$productName." has been updated. The new Price is ".number_format($newPrice)."PKR.";
					$_email_message .= "\r\n\r\n Please visit ".$productUrl." for details and/or purchasing this product.";
					$_email_message .= "\r\n\r\n Thank you.";
					$_email_message .= "\r\n\r\n Regards,";
					$_email_message .= "\r\n\r\n CS@TCSCONNECT";
					
					$this->setBody($_email_message); // Set email message in body by class function setBody()
					$emailMessage = $this->getBody(); // get email body from class getBody function
					
					// email fields: to, from, subject, and so on
					$to			=	$customerEmail;
					$from		=	$fromEmail;
					$message	=	$emailMessage;
					$headers	.= "From: ".$fromName." <".$fromEmail.">";
					
					// sending email
					$ok	=	@mail($to,$subject,$message);
					if($ok){
						echo "<p>mail sent to $to!</p>"; 
					}
					else{
						echo "<p>mail could not be sent to $to!</p>"; 
					}
	

		}
		
		
				public function sendSMS($customerMobile,$productName,$productUrl,$newPrice,$oldPrice){
					
					$accountApi		=	Mage::getStoreConfig('pringconfig/pring_group/pring_username');// get account api from config
					$accountSecret	=	Mage::getStoreConfig('pringconfig/pring_group/pring_secretkey'); // get secert key from config
					$pringMessage	=	urlencode("Hello the price of " . $productName." has been changed from PKR. ".number_format($oldPrice)." to PKR. ".number_format($newPrice).". Visit ".$productUrl." for details and/or purchasing this Product. Call (021) 111-192-986 to speak with customer services."); // pring message 
					
					//--> Default values needed for pring api to send message
					$action			=	"PM";
					$version		=	"1";
					$keyword		=	"TCSConnect";
					$apiUrl			=		"http://www.pringit.com/api/?username=$accountApi&secret=$accountSecret&v=$version&action=$action&message=$pringMessage&mobile=$customerMobile&keyword=$keyword"; 
							
					//--> Send Message to customer through curl
					$search	=	array('\\', "\0", "\n", "\r", "'", '"', "\x1a");
					$ch	=	curl_init();
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_URL, $apiUrl);
					$contents = str_replace($search, '', curl_exec($ch));
					curl_close($ch);
							
					//--> get db name from magento config
					$dbname 	= (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');
					$tableName	=	$dbname.'.tcs_pring_log'; //--> pring logging table name 
					$typeId		=	'5';	// pring message type , 5 for price change sms
					
					//--> Magento core object to insert record in db
					$coreWrite				=	Mage::getSingleton('core/resource')->getConnection('core_write');
					$pringInsertResponse	=	"INSERT INTO $tableName(type,url,response) VALUES 	('$typeId','$apiUrl','$contents')";
					$coreWrite->query($pringInsertResponse);	
	
			
		}
		
		
		 public function sentToFlag(){
				
				//--> Magento default objects to read and write in to db	
				$coreResource				=	Mage::getSingleton('core/resource');
				$coreWrite					=	$coreResource->getConnection('core_write');
				//--> get table name from module config
				$tcs_pricealert_pricechange	=	$coreResource->getTableName('pricealert/pricechange'); 
				
				$sentToFlagQuery			=	"UPDATE $tcs_pricealert_pricechange set sento_customer = 1 , sento_customer_time = NOW() where sento_customer = 0";	
				$coreWrite->query($sentToFlagQuery);
				
				
		}
	}
	
	$pricealertscript	=	new pricealertscript();
	$pricealertscript->sendPriceAlerts();