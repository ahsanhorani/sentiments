<?php
	include "/var/www/tcsconnect/app/Mage.php";
	
	Mage::app('admin');
	
	$products = Mage::getModel('catalog/product')->getCollection();
	$outputfile = "margin_report.csv";
	
	foreach($products as $prod) {
		$_product = Mage::getModel('catalog/product')->load($prod->getId());
		
		$productname = $_product->getName();
		$productsku  = $_product->getSku();
		$cp		     = $_product->getCost();
		$pp			 = $_product->getPrice();
		$margin		 = ($pp-$cp)*100/$pp;
		
		if( $margin > 10 ) {
			$line = "$productname, $productsku, $cp, $pp, $margin\n";
			file_put_contents($outputfile, $line, FILE_APPEND);
		}
	}
	
	echo "DONE";
?>