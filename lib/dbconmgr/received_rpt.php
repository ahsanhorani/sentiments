<?php
	include "../../app/Mage.php";
	
	Mage::app('admin');
	
	function getPaidDate($entity_id) {
		$sql = "SELECT created_at FROM sales_flat_invoice where order_id='$entity_id' LIMIT 1";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result =  $connection->fetchAll($sql);
		return $result[0]['created_at'];
	}
	
	function getCNNos($increment_id) {
		$sql = "SELECT cnnumber FROM oms_transactions WHERE orderid='$increment_id'";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result =  $connection->fetchAll($sql);
		$ret = "";
		
		for($i=0; $i < count($result); $i++) {
			$ret .= $result[$i]['cnnumber'] . ", ";
		}
		
		return $ret;
	}
	
	$sql = "select entity_id, increment_id, created_at FROM sales_flat_order WHERE status='received' and created_at >= '2012-03-23'";
	$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
	$result =  $connection->fetchAll($sql);
	
	@file_put_contents("/tmp/received.csv", "Order #, Order Date, Paid Date, CNo\n", FILE_APPEND);
	
	for($i=0; $i < count($result); $i++) {
		$entity_id    = $result[$i]['entity_id'];
		$increment_id = $result[$i]['increment_id'];
		$orderdate    = $result[$i]['created_at'];
		$paiddate     = getPaidDate($entity_id);
		$cnno		  = getCNNos($increment_id);
		@file_put_contents("/tmp/received.csv", "$increment_id, $orderdate, $paiddate, $cnno\n", FILE_APPEND);
	}
?>