<?php
	include "reportClass.php";
	include "/var/www/tcsconnect/lib/dbconmgr/excellib/Classes/PHPExcel.php";
	include "/var/www/tcsconnect/lib/dbconmgr/phpmailer/class.phpmailer.php";
	include "/var/www/tcsconnect/app/Mage.php";
	
	Mage::app('admin');
	
	$rc = new reportClass();
	$rc->connect();
	
	$currentmonth = intval(date("m"));
	
	if( $currentmonth == 1 )
		$prev_month = 12;
	else
		$prev_month = $currentmonth - 1;
		
		
	if( $prev_month < 10 )
		$prev_month = "0$prev_month";
		
	if( $currentmonth < 10 )
		$currentmonth = "0$currentmonth";
		
	$fromdate = date("Y") . "-$prev_month-01";
	$todate   = date("Y") . "-$currentmonth-01";
	
	$status_to_include = "'paid', 'complete', 'cod', 'received', 'Awaiting', 'order_refunded', 'holded'";
	
	$orders = $rc->get_orders_bydaterange($fromdate, $todate, $status_to_include);
	
	$header = "Products Sold Report\n";
	$header .= "Period, Order Number, Product, Customer Name, Retail Price, Qty Ordered, Date Order Paid\n";
	
	$filename = "monthly_products_report" . date("m-d-Y") . ".csv";
	@unlink("/tmp/$filename");
	@file_put_contents("/tmp/$filename", $header, FILE_APPEND);
	
	for($i=0; $i < count($orders); $i++) {
		$entity_id    = $orders[$i]['entity_id'];
		$orderinfo    = $rc->get_order_info($entity_id);
		$orderprod    = $rc->get_item_ordered($entity_id);
		
		$orderdate    = date("d-m-Y", strtotime($orders[$i]['created_at']));
		$customername = $orderinfo['customer_firstname'] . " " . $orderinfo['customer_lastname'];
		$incrementid  = $orderinfo['increment_id'];
		
		for($j=0; $j < count($orderprod); $j++) {
			if( $i==0 && $j == 0 )
				$period = "01-$prev_month-" . date("Y") . " - 01-$currentmonth-" . date("Y");
			else
				$period = "";
			
			$product = $orderprod[$j]['name'];
			$price   = $orderprod[$j]['base_price'];
			$qty	 = $orderprod[$j]['qty_ordered'];
				
			$line = "$period, $incrementid, $product,$customername, $price, $qty, $orderdate\n";
			@file_put_contents("/tmp/$filename", $line, FILE_APPEND);
		}
	}
	
	$body = "Hi, \n".
	        "Please find the attached files for Month Products Ordered Report \n";
	
	$mail = new PHPMailer();
	$mail->AddAddress("ali.z@tcs.com.pk");
	$mail->AddCC("faisal.ijaz@tcs.com.pk");
	$mail->SetFrom('cs@tcsconnect.com', 'TCSConnect');
	$mail->Subject = "TCS Connect � Monthly Product Ordered Report 01-$prev_month-" . date("Y") . " - 01-$currentmonth-" . date("Y");
	$mail->MsgHTML($body);
	$mail->AddAttachment("/tmp/$filename");
	
	if(!$mail->Send()) {
		echo "Mailer Error: " . $mail->ErrorInfo . "\n\n";
	} else {
		echo "Message sent!\n\n";
	}
	
?>