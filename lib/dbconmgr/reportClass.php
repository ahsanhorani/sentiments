<?php
	include "db_creds.php";
	
	class reportClass {
	
		var $DB_CON=null;
		
		//Returns database resource/null otherwise
		public function connect() {
			global $DBSERVER, $DBUSER, $DBPWD, $DBNAME;
			
			$link = mysql_connect($DBSERVER, $DBUSER, $DBPWD);
			if (!$link) {
				echo 'Could not connect: ' . mysql_error();
				return;
			}
			//Select the database
			mysql_select_db( $DBNAME, $link) or die("Database doesn't exist: " . mysql_error() );
			$this->DB_CON=$link;
		}
		
		//public function which executes the query
		public function runquery($Sql) {
			if( $this->DB_CON != null ) {
				$result = mysql_query($Sql, $this->DB_CON);
				
				if( !$result) { 
					@file_put_contents("C:\\mysqlerror.log", mysql_errno($this->DB_CON) . " : " . mysql_error($this->DB_CON) . "\n", FILE_APPEND);
					return null; 
				}
				return $result;
			}
		}

		public function get_orders_bydaterange($fromdate, $todate, $status_to_include) {
			$sql = "SELECT o.entity_id, i.created_at FROM sales_flat_order o INNER JOIN sales_flat_invoice i ON o.entity_id=i.order_id WHERE date(i.created_at) >= '$fromdate' AND date(i.created_at) <= '$todate' AND o.status IN ($status_to_include)";
			
			echo $sql . "\n\n";
			
			$result = $this->runquery($sql);
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function get_item_ordered($order_id) {
			$ret = array();
			$sql = "SELECT name,sku, base_price, qty_ordered, created_at FROM sales_flat_order_item WHERE order_id=$order_id";
			$result = $this->runquery($sql);
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
		
		public function get_order_info($entity_id) {
			$ret = array();
			$sql = "SELECT * FROM sales_flat_order WHERE entity_id='$entity_id' LIMIT 1";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret;
		}
		
		public function get_order_paid_date($entity_id) {
			$ret = "";
			$sql = "SELECT created_at FROM sales_flat_invoice WHERE order_id=$entity_id LIMIT 1";
			$result = $this->runquery($sql);
			if( $result != null ) {
				$ret = mysql_fetch_array($result, MYSQL_BOTH);
			}
			return $ret['created_at'];
		}	
		
		//public function which retrieves information about the user based on provided user id
		public function getECInformation($eccode) {
			$ret = array();
			$Sql = "SELECT * FROM expresscenter WHERE ecadmin='$eccode' LIMIT 1";
			$result = $this->runquery($Sql);
			
			if( $result != null ) {
				while( $row = mysql_fetch_array($result, MYSQL_BOTH) ) {
					$ret[] = $row;
				}
			}
			return $ret;
		}
	}
?>